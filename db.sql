-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: video_klub
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `video_klub`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `video_klub` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `video_klub`;

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naslov` varchar(255) NOT NULL,
  `godina` int(11) NOT NULL,
  `slika` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `film`
--

LOCK TABLES `film` WRITE;
/*!40000 ALTER TABLE `film` DISABLE KEYS */;
INSERT INTO `film` VALUES (13,'The Shawshank Redemption',1994,'Resources\\covers\\the_shawshank_redemption.jpg'),(14,'The Godfather',1972,'Resources\\covers\\the_godfather.jpg'),(15,'The Godfather Part II',1974,'Resources\\covers\\the_godfather_part_ii.jpg'),(16,'The Dark Knight',2008,'Resources\\covers\\the_dark_knight.jpg'),(17,'12 Angry Men',1957,'Resources\\covers\\12_angry_men.jpg'),(18,'Schindler\'s List',1993,'Resources\\covers\\schindlers_list.jpg'),(19,'The Lord of the Rings: The Return of the King',2003,'Resources\\covers\\the_lord_of_the_rings_the_return_of_the_king.jpg'),(20,'Pulp Fiction',1994,'Resources\\covers\\pulp_fiction.jpg'),(21,'The Good, the Bad and the Ugly',1966,'Resources\\covers\\the_good_the_bad_and_the_ugly.jpg'),(22,'Fight Club',1999,'Resources\\covers\\fight_club.jpg'),(23,'The Lord of the Rings: The Fellowship of the Ring',2001,'Resources\\covers\\the_lord_of_the_rings_the_fellowship_of_the_ring.jpg'),(24,'Titanic',1997,'Resources\\covers\\titanic.jpg'),(25,'Forrest Gump',1994,'Resources\\covers\\forrest_gump.jpg'),(26,'Inception',2010,'Resources\\covers\\inception.jpg'),(27,'Star Wars: Episode V - The Empire Strikes Back',1980,'Resources\\covers\\star_wars_episode_v_the_empire_strikes_back.jpg'),(28,'The Lord of the Rings: The Two Towers',2002,'Resources\\covers\\the_lord_of_the_rings_the_two_towers.jpg'),(29,'One Flew Over the Cuckoo\'s Nest',1975,'Resources\\covers\\one_flew_over_the_cuckoos_nest.jpg'),(30,'Goodfellas',1990,'Resources\\covers\\goodfellas.jpg'),(31,'The Matrix',1999,'Resources\\covers\\the_matrix.jpg'),(32,'Seven Samurai',1954,'Resources\\covers\\seven_samurai.jpg'),(33,'Se7en',1995,'Resources\\covers\\se7en.jpg'),(34,'City of God',2002,'Resources\\covers\\city_of_god.jpg'),(35,'The Silence of the Lambs',1991,'Resources\\covers\\the_silence_of_the_lambs.jpg'),(36,'Star Wars: Episode IV - A New Hope',1977,'Resources\\covers\\star_wars_episode_iv_a_new_hope.jpg'),(37,'Life Is Beautiful',1997,'Resources\\covers\\life_is_beautiful.jpg'),(38,'It\'s a Wonderful Life',1946,'Resources\\covers\\its_a_wonderful_life.jpg'),(39,'Saving Private Ryan',1998,'Resources\\covers\\savin_private_ryan.jpg'),(40,'Spirited Away',2001,'Resources\\covers\\spirited_away.jpg'),(41,'Avengers: Endgame',2019,'Resources\\covers\\avengers_endgame.jpg'),(42,'The Usual Suspects',1995,'Resources\\covers\\the_usual_suspects.jpg'),(43,'Leon: The Professional',1994,'Resources\\covers\\leon_the_professional.jpg'),(44,'The Green Mile',1999,'Resources\\covers\\the_green_mile.jpg'),(45,'Interstellar',2014,'Resources\\covers\\interstellar.jpg'),(46,'American History X',1998,'Resources\\covers\\american_history_x.jpg'),(47,'Psycho',1960,'Resources\\covers\\psycho.jpg'),(48,'The Lion King',1994,'Resources\\covers\\the_lion_king.jpg'),(49,'City Lights',1931,'Resources\\covers\\city_lights.jpg'),(50,'The Pianist',2002,'Resources\\covers\\the_pianist.jpg'),(51,'Modern Times',1936,'Resources\\covers\\modern_times.jpg'),(52,'Back to the Future',1985,'Resources\\covers\\back_to_the_future.jpg'),(53,'The Intouchables',2011,'Resources\\covers\\the_intouchables.jpg'),(54,'Terminator 2: Judgment Day',1991,'Resources\\covers\\terminator_2_judgement_day.jpg'),(55,'Once Upon a Time in the West',1968,'Resources\\covers\\once_upon_a_time_in_the_west.jpg'),(56,'The Departed',2006,'Resources\\covers\\the_departed.jpg'),(57,'Gladiator',2000,'Resources\\covers\\gladiator.jpg'),(58,'Whiplash',2014,'Resources\\covers\\whiplash.jpg'),(59,'Casablanca',1942,'Resources\\covers\\casablanca.jpg'),(60,'The Prestige',2006,'Resources\\covers\\the_prestige.jpg'),(61,'Rear Window',1954,'Resources\\covers\\rear_window.jpg'),(62,'Raiders of the Lost Ark',1981,'Resources\\covers\\raiders_of_the_lost_ark.jpg'),(63,'Grave of the Fireflies',1988,'Resources\\covers\\grave_of_the_fireflies.jpg'),(64,'Alien',1979,'Resources\\covers\\alien.jpg'),(65,'Memento',2000,'Resources\\covers\\memento.jpg'),(66,'Apocalypse Now',1979,'Resources\\covers\\apocalypse_now.jpg'),(67,'Kod amidže Idriza',2004,'Resources\\covers\\kod_amidze_idriza.jpg');
/*!40000 ALTER TABLE `film` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `film_glumac`
--

DROP TABLE IF EXISTS `film_glumac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `film_glumac` (
  `film_id` int(11) NOT NULL,
  `glumac_id` int(11) NOT NULL,
  PRIMARY KEY (`film_id`,`glumac_id`),
  KEY `fk_film_glumac_film1_idx` (`film_id`),
  KEY `fk_film_glumac_glumac1` (`glumac_id`),
  CONSTRAINT `fk_film_glumac_film1` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`),
  CONSTRAINT `fk_film_glumac_glumac1` FOREIGN KEY (`glumac_id`) REFERENCES `glumac` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `film_glumac`
--

LOCK TABLES `film_glumac` WRITE;
/*!40000 ALTER TABLE `film_glumac` DISABLE KEYS */;
INSERT INTO `film_glumac` VALUES (13,14),(13,15),(13,16),(14,17),(14,18),(14,19),(15,18),(15,20),(15,21),(16,22),(16,23),(16,24),(17,25),(17,26),(18,27),(18,28),(18,29),(19,30),(19,31),(19,32),(20,33),(20,34),(20,35),(21,36),(21,37),(21,38),(22,39),(22,40),(22,41),(23,30),(23,32),(23,42),(24,1),(24,13),(24,43),(25,44),(25,45),(25,46),(26,1),(26,47),(26,48),(27,49),(27,50),(27,51),(28,30),(28,31),(28,32),(29,52),(29,53),(29,54),(30,21),(30,55),(30,56),(31,57),(31,58),(31,59),(32,60),(32,61),(32,62),(33,15),(33,39),(33,63),(34,64),(34,65),(34,66),(35,67),(35,68),(35,69),(36,50),(36,51),(37,70),(37,71),(37,72),(38,73),(38,74),(38,75),(39,44),(39,76),(39,77),(40,78),(40,79),(40,80),(41,81),(41,82),(41,83),(42,63),(42,84),(42,85),(43,86),(43,87),(43,88),(44,44),(44,89),(44,90),(45,91),(45,92),(45,93),(46,40),(46,94),(46,95),(47,96),(47,97),(47,98),(48,99),(48,100),(48,101),(49,102),(49,103),(49,104),(50,105),(50,106),(50,107),(51,102),(51,108),(51,109),(52,110),(52,111),(52,112),(53,113),(53,114),(53,115),(54,94),(55,25),(55,118),(55,119),(56,52),(57,120),(57,121),(57,122),(58,123),(58,124),(58,125),(59,126),(59,127),(59,128),(60,22),(60,129),(60,130),(61,73),(61,131),(61,132),(62,50),(62,133),(62,134),(63,135),(63,136),(63,137),(64,138),(64,139),(64,140),(65,59),(65,141),(65,142),(66,17),(66,20),(66,143),(67,8),(67,144);
/*!40000 ALTER TABLE `film_glumac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `film_u_magacinu`
--

DROP TABLE IF EXISTS `film_u_magacinu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `film_u_magacinu` (
  `na_stanju` int(11) DEFAULT NULL,
  `ocjena` decimal(3,2) DEFAULT NULL,
  `cijena` decimal(5,2) DEFAULT NULL,
  `aktivan` tinyint(4) NOT NULL,
  `film_id` int(11) NOT NULL,
  PRIMARY KEY (`film_id`),
  CONSTRAINT `fk_film_u_magacinu_film1` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `film_u_magacinu`
--

LOCK TABLES `film_u_magacinu` WRITE;
/*!40000 ALTER TABLE `film_u_magacinu` DISABLE KEYS */;
INSERT INTO `film_u_magacinu` VALUES (2,0.00,5.00,1,13),(10,0.00,5.00,1,14),(10,0.00,5.00,1,15),(5,0.00,4.00,1,16),(2,0.00,5.00,1,17),(5,0.00,4.50,1,18),(4,0.00,6.00,1,19),(10,0.00,6.00,1,20),(10,0.00,6.00,1,21),(7,0.00,7.00,1,22),(5,0.00,8.00,1,23),(9,0.00,10.00,1,24),(3,0.00,5.00,1,25),(5,0.00,5.00,1,26),(6,0.00,5.00,1,27),(4,0.00,7.00,1,28),(7,0.00,4.00,1,29),(6,0.00,5.00,1,30),(5,0.00,7.00,1,31),(4,0.00,4.00,1,32),(5,0.00,5.00,1,33),(5,0.00,6.00,1,34),(4,0.00,7.00,1,35),(5,0.00,5.00,1,36),(3,0.00,5.00,1,37),(5,0.00,5.00,1,38),(4,0.00,6.00,1,39),(6,0.00,4.00,1,40),(0,0.00,7.00,1,41),(5,0.00,5.00,1,42),(7,0.00,4.00,1,43),(7,0.00,5.00,1,44),(8,0.00,7.00,1,45),(5,0.00,7.00,1,46),(5,0.00,5.00,1,47),(2,0.00,5.00,1,48),(3,0.00,5.00,1,49),(6,0.00,5.00,1,50),(4,0.00,5.00,1,51),(4,0.00,6.00,1,52),(2,0.00,5.00,1,53),(6,0.00,7.00,1,54),(4,0.00,5.00,1,55),(7,0.00,6.00,1,56),(5,0.00,5.00,1,57),(4,0.00,5.00,1,58),(5,0.00,4.00,1,59),(4,0.00,5.00,1,60),(6,0.00,6.00,1,61),(5,0.00,5.00,1,62),(5,0.00,3.00,1,63),(8,0.00,6.00,1,64),(5,0.00,5.00,1,65),(3,0.00,5.00,1,66),(5,0.00,3.00,1,67);
/*!40000 ALTER TABLE `film_u_magacinu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `film_zanr`
--

DROP TABLE IF EXISTS `film_zanr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `film_zanr` (
  `film_id` int(11) NOT NULL,
  `zanr_id` int(11) NOT NULL,
  PRIMARY KEY (`film_id`,`zanr_id`),
  KEY `fk_film_zanr_film1_idx` (`film_id`),
  KEY `fk_film_zanr_zanr1` (`zanr_id`),
  CONSTRAINT `fk_film_zanr_film1` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`),
  CONSTRAINT `fk_film_zanr_zanr1` FOREIGN KEY (`zanr_id`) REFERENCES `zanr` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `film_zanr`
--

LOCK TABLES `film_zanr` WRITE;
/*!40000 ALTER TABLE `film_zanr` DISABLE KEYS */;
INSERT INTO `film_zanr` VALUES (13,11),(14,4),(14,11),(15,4),(15,11),(16,4),(16,9),(16,11),(17,11),(18,6),(18,11),(18,13),(19,10),(19,11),(19,23),(20,4),(20,11),(21,24),(22,11),(23,10),(23,11),(23,23),(24,11),(24,17),(25,11),(25,17),(26,7),(26,9),(26,23),(27,9),(27,10),(27,23),(28,10),(28,11),(28,23),(29,11),(30,4),(30,11),(30,13),(31,7),(31,9),(32,11),(32,23),(33,4),(33,11),(33,25),(34,4),(34,11),(35,4),(35,11),(35,18),(36,9),(36,10),(36,23),(37,1),(37,11),(37,17),(38,10),(38,11),(38,16),(39,3),(39,11),(40,16),(40,19),(40,23),(41,7),(41,9),(41,23),(42,4),(42,18),(42,25),(43,4),(43,9),(43,11),(44,4),(44,10),(44,11),(45,7),(45,11),(45,23),(46,11),(47,2),(47,18),(47,25),(48,11),(48,19),(48,23),(49,1),(49,11),(49,17),(50,11),(50,13),(50,26),(51,1),(51,11),(51,16),(52,1),(52,7),(52,23),(53,1),(53,11),(53,13),(54,7),(54,9),(55,24),(56,4),(56,11),(56,18),(57,9),(57,11),(57,23),(58,11),(58,26),(59,3),(59,11),(59,17),(60,7),(60,11),(60,25),(61,18),(61,25),(62,9),(62,23),(63,3),(63,11),(63,19),(64,2),(64,7),(65,18),(65,25),(66,3),(66,11),(67,11);
/*!40000 ALTER TABLE `film_zanr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glumac`
--

DROP TABLE IF EXISTS `glumac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `glumac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(255) NOT NULL,
  `prezime` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glumac`
--

LOCK TABLES `glumac` WRITE;
/*!40000 ALTER TABLE `glumac` DISABLE KEYS */;
INSERT INTO `glumac` VALUES (1,'Leonardo','DiCaprio'),(4,'Antonio','Banderas'),(6,'Dragan','Bjelogrlić'),(7,'Nikola','Kojo'),(8,'Mustafa','Nadarević'),(9,'Emir','Hadžihafizbegović'),(10,'Srđan','Todorović'),(11,'Zoran','Radmilović'),(12,'Vuk','Kostić'),(13,'Kate','Winslet'),(14,'Tim','Robbins'),(15,'Morgan','Freeman'),(16,'Bob','Gunton'),(17,'Marlon','Brando'),(18,'Al','Pacino'),(19,'James','Caan'),(20,'Robert','Duvall'),(21,'Robert','De Niro'),(22,'Christian','Bale'),(23,'Heath','Ledger'),(24,'Aaron','Eckhart'),(25,'Henry','Fonda'),(26,'Martin','Balsam'),(27,'Liam','Neeson'),(28,'Ralph','Fiennes'),(29,'Ben','Kingsley'),(30,'Elijah','Wood'),(31,'Viggo','Mortensen'),(32,'Ian','McKellen'),(33,'John','Travolta'),(34,'Uma','Thurman'),(35,'Samuel','L. Jackson'),(36,'Clint','Eastwood'),(37,'Eli','Wallach'),(38,'Lee','Van Cleef'),(39,'Brad','Pitt'),(40,'Edward','Norton'),(41,'Meat','Loaf'),(42,'Orlando','Bloom'),(43,'Billy','Zane'),(44,'Tom','Hanks'),(45,'Robin','Wright'),(46,'Gary','Sinise'),(47,'Ellen','Page'),(48,'Joseph','Gordon-Levitt'),(49,'Mark','Hamill'),(50,'Harrison','Ford'),(51,'Carrie','Fisher'),(52,'Jack','Nicholson'),(53,'Louise','Fletcher'),(54,'Will','Sampson'),(55,'Ray','Liotta'),(56,'Joe','Pesci'),(57,'Keanu','Reeves'),(58,'Laurence','Fishburne'),(59,'Carrie-Anne','Moss'),(60,'Toshiro','Mifune'),(61,'Takashi','Shimura'),(62,'Keiko','Tsushima'),(63,'Kevin','Spacey'),(64,'Alexandre','Rodrigues'),(65,'Leandro','Firmino'),(66,'Matheus','Nachtergaele'),(67,'Jodie','Foster'),(68,'Anthony','Hopkins'),(69,'Lawrence A.','Bonney'),(70,'Roberto','Benigni'),(71,'Nicoletta','Braschi'),(72,'Giorgio','Cantarini'),(73,'James','Stewart'),(74,'Donna','Reed'),(75,'Lionel','Barrymore'),(76,'Matt','Damon'),(77,'Tom','Sizemore'),(78,'Daveigh','Chase'),(79,'Suzanne','Pleshette'),(80,'Miyu','Irino'),(81,'Robert','Downey Jr.'),(82,'Chris','Evans'),(83,'Mark','Ruffalo'),(84,'Gabriel','Byrne'),(85,'Chazz','Palminteri'),(86,'Jean','Reno'),(87,'Gary','Oldman'),(88,'Natalie','Portman'),(89,'Michael','Clarke Duncan'),(90,'David','Morse'),(91,'Matthew','McConaughey'),(92,'Anne','Hathaway'),(93,'Jessica','Chastain'),(94,'Edward','Furlong'),(95,'Beverly','D\'Angelo'),(96,'Anthony','Perkins'),(97,'Janet','Leigh'),(98,'Vera','Miles'),(99,'Matthew','Broderick'),(100,'Jeremy','Irons'),(101,'James','Earl Jones'),(102,'Charles','Chaplin'),(103,'Virginia','Cherrill'),(104,'Florence','Lee'),(105,'Adrien','Brody'),(106,'Thomas','Kretschmann'),(107,'Frank','Finlay'),(108,'Henry','Bergman'),(109,'Paulette','Goddard'),(110,'Michael J.','Fox'),(111,'Christopher','Lloyd'),(112,'Lea','Thompson'),(113,'Francois','Cluzet'),(114,'Omar','Sy'),(115,'Anne','Le Ny'),(116,'Arnold','Schwarzenegger'),(117,'Linda','Hamilton'),(118,'Charles','Bronson'),(119,'Claudia','Cardinale'),(120,'Russell','Crowe'),(121,'Joaquin','Phoenix'),(122,'Connie','Nielsen'),(123,'Miles','Teller'),(124,'J. K.','Simmons'),(125,'Melissa','Benoist'),(126,'Humphrey','Bogart'),(127,'Ingrid','Bergman'),(128,'Paul','Henreid'),(129,'Hugh','Jackman'),(130,'Scarlett','Johansson'),(131,'Grace','Kelly'),(132,'Wendell','Corey'),(133,'Karen','Allen'),(134,'Paul','Freeman'),(135,'Tsutomu','Tatsumi'),(136,'Ayano','Shiraishi'),(137,'Akemi','Yamaguchi'),(138,'Sigourney','Weaver'),(139,'Tom','Skerritt'),(140,'John','Hurt'),(141,'Guy','Pearce'),(142,'Joe','Pantoliano'),(143,'Martin','Sheen'),(144,'Senad','Bašić'),(145,'Sylvester','Stallone'),(146,'Steven','Seagal');
/*!40000 ALTER TABLE `glumac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorija_zanra`
--

DROP TABLE IF EXISTS `kategorija_zanra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `kategorija_zanra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opis` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorija_zanra`
--

LOCK TABLES `kategorija_zanra` WRITE;
/*!40000 ALTER TABLE `kategorija_zanra` DISABLE KEYS */;
INSERT INTO `kategorija_zanra` VALUES (1,'Ostalo'),(2,'Radnja'),(3,'Raspoloženje'),(4,'Format filma'),(5,'Ciljna grupa');
/*!40000 ALTER TABLE `kategorija_zanra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `korisnik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(255) NOT NULL,
  `prezime` varchar(255) NOT NULL,
  `jmbg` char(13) NOT NULL,
  `broj_kartice` char(10) NOT NULL,
  `kartica_aktivna` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `broj_UNIQUE` (`broj_kartice`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES (1,'Mladen','Popovic','0807988123456','QPT1UC1A6D',1),(2,'Milan','Popovic','2903964123432','HR40E2RATR',1),(3,'Milena','Popovic','2303965123212','NFAQYDWBCW',0),(4,'NIkolaj','Popovic','0401019123658','WECFJ9SDGQ',1),(5,'Nikola','Mirotic','1231231231231','23O46W1MBM',1),(6,'Milan','Aleksic','2098120871209','4KWBEIASQ2',1),(7,'Jeremija','Protic','2908301974109','EPXBD58D9M',1),(8,'Nemanja','Miskovic','2908103719082','1RWU2NW200',1),(9,'Duška','Petkovic','9981029831092','248O8EBQP0',1),(10,'Marko','Maric','2913803018309','HFAVPLE4DV',1),(11,'Djordje','Djalic','2948201380122','8DD64K8Z7Y',1),(12,'Marko','Atanaskovic','2193801510232','M29GU6JZ3C',1),(13,'Marko','Gavrilovic','9281239018230','LWEUIOBI19',1),(14,'Branka','Stankovic','1292908412093','ZM2KPCT2XC',1),(15,'Marija','Popovic','9219208541029','AGW0LCLAH5',1),(16,'Vanja','Popovic','2190481209318','6H9W5CY0GT',1),(17,'Drazen','Sofric','2132130410231','INIBUFVJA5',1),(18,'Ramo','Tatarevic','2910491039120','JEETSSLKWY',1),(19,'Dejan','Zoranovic','2103149123912','EHRSODPW0U',1),(20,'Stanko','Gavric','2921039412393','CXRXKQU7XJ',1),(21,'Simo','Pozderovic','2012391241231','RZRGX6RFIM',1),(22,'Zoran','Petkovic','2132413021391','5LHUNT0IR0',1),(23,'Miladin','Bacic','2039120319201','VGOVHA91GF',1),(24,'Sladjan','Kitic','2190831029123','M2SB11Y2LL',1),(25,'Slavisa','Lipovcic','6758734356359','CNVJGGXDDJ',1),(26,'Da','Da','2098120871209','YS8DT077VZ',0),(27,'Mladen','Knezevic','2139019310293','J1LMSCUA91',1),(28,'Aleksije','Popovic','2190830192831','2T0ABBPWA9',1),(29,'Rajko','Zoranovic','2098109820971','95MOBOFWQI',1),(30,'Petar','Matic','1093180210938','7T1Y2WMX2M',1),(31,'Nemanja','Markovic','1230192013921','L6JQ6O81L6',1),(32,'Petar','Aranitović','2904991123122','NBBN1799AT',1),(33,'Milan','Popović','2903964123456','OH1VGMYTQV',1),(34,'Marko','Marinković','1902381092310','Z7W6UB1RVY',1);
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zaduzenje_filma`
--

DROP TABLE IF EXISTS `zaduzenje_filma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `zaduzenje_filma` (
  `datum_zaduzenja` datetime NOT NULL,
  `datum_razduzenja` datetime DEFAULT NULL,
  `ocjena_filma` int(11) DEFAULT NULL,
  `zaposleni_id` int(11) NOT NULL,
  `korisnik_id` int(11) NOT NULL,
  `film_u_magacinu_film_id` int(11) NOT NULL,
  PRIMARY KEY (`datum_zaduzenja`,`zaposleni_id`,`korisnik_id`,`film_u_magacinu_film_id`),
  KEY `fk_zaduzenje_filma_zaposleni_idx` (`zaposleni_id`),
  KEY `fk_zaduzenje_filma_korisnicka_kartica1_idx` (`korisnik_id`),
  KEY `fk_zaduzenje_filma_film_u_magacinu1_idx` (`film_u_magacinu_film_id`),
  CONSTRAINT `fk_zaduzenje_filma_film_u_magacinu1` FOREIGN KEY (`film_u_magacinu_film_id`) REFERENCES `film_u_magacinu` (`film_id`),
  CONSTRAINT `fk_zaduzenje_filma_korisnicka_kartica1` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`),
  CONSTRAINT `fk_zaduzenje_filma_zaposleni` FOREIGN KEY (`zaposleni_id`) REFERENCES `zaposleni` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zaduzenje_filma`
--

LOCK TABLES `zaduzenje_filma` WRITE;
/*!40000 ALTER TABLE `zaduzenje_filma` DISABLE KEYS */;
INSERT INTO `zaduzenje_filma` VALUES ('2019-08-12 16:18:18',NULL,NULL,2,5,48),('2019-08-12 16:18:26',NULL,NULL,2,30,17),('2019-08-12 16:18:33',NULL,NULL,2,9,46),('2019-08-12 16:18:38',NULL,NULL,2,28,41),('2019-08-12 16:18:46',NULL,NULL,2,6,33),('2019-08-12 16:18:55',NULL,NULL,2,6,59),('2019-08-12 16:19:04',NULL,NULL,2,5,51),('2019-08-12 16:19:11',NULL,NULL,2,23,48),('2019-08-12 16:19:19',NULL,NULL,2,12,41),('2019-08-12 16:19:28',NULL,NULL,2,4,24),('2019-08-12 16:19:34',NULL,NULL,2,28,66),('2019-08-12 16:20:00',NULL,NULL,2,28,45),('2019-08-12 16:20:06',NULL,NULL,2,5,49),('2019-08-12 16:20:13',NULL,NULL,2,1,46),('2019-08-12 16:20:21',NULL,NULL,2,21,28),('2019-08-12 16:20:36',NULL,NULL,2,24,66),('2019-08-12 16:20:44',NULL,NULL,2,21,40),('2019-08-12 16:20:52',NULL,NULL,2,23,55),('2019-08-12 16:20:58','2019-08-12 16:22:56',NULL,2,4,41),('2019-08-12 16:21:05',NULL,NULL,2,23,49),('2019-08-12 16:21:12',NULL,NULL,2,9,25),('2019-08-12 16:21:27',NULL,NULL,2,12,60),('2019-08-12 16:23:09',NULL,NULL,2,8,41),('2019-08-12 16:23:16',NULL,NULL,2,30,41),('2019-08-12 16:23:32',NULL,NULL,2,8,17),('2019-08-12 16:23:40',NULL,NULL,2,22,41),('2019-08-12 16:23:51',NULL,NULL,2,4,41),('2019-08-13 00:14:33',NULL,NULL,2,6,64);
/*!40000 ALTER TABLE `zaduzenje_filma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zanr`
--

DROP TABLE IF EXISTS `zanr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `zanr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  `kategorija_zanra_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_zanr_kategorija_zanra` (`kategorija_zanra_id`),
  CONSTRAINT `fk_zanr_kategorija_zanra` FOREIGN KEY (`kategorija_zanra_id`) REFERENCES `kategorija_zanra` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zanr`
--

LOCK TABLES `zanr` WRITE;
/*!40000 ALTER TABLE `zanr` DISABLE KEYS */;
INSERT INTO `zanr` VALUES (1,'Komedija',3),(2,'Horor',3),(3,'Ratni',2),(4,'Krimi',2),(6,'Istorijski',2),(7,'Naučnofantastični',2),(8,'Sportski',2),(9,'Akcija',3),(10,'Fantazija',3),(11,'Drama',2),(13,'Biografski',4),(14,'Dokumentarni',4),(16,'Porodični',5),(17,'Romantika',3),(18,'Triler',3),(19,'Animacija',4),(20,'Dječiji',5),(21,'Crni',2),(23,'Avantura',3),(24,'Kaubojski',3),(25,'Misterija',2),(26,'Mjuzikl',4);
/*!40000 ALTER TABLE `zanr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zaposleni`
--

DROP TABLE IF EXISTS `zaposleni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `zaposleni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(255) NOT NULL,
  `prezime` varchar(255) NOT NULL,
  `korisnicko_ime` varchar(255) NOT NULL,
  `lozinka` varchar(255) NOT NULL,
  `aktivan` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zaposleni`
--

LOCK TABLES `zaposleni` WRITE;
/*!40000 ALTER TABLE `zaposleni` DISABLE KEYS */;
INSERT INTO `zaposleni` VALUES (1,'Admin','Admin','admin','21232f297a57a5a743894a0e4a801fc3',1),(2,'Duško','Popović','dusko','1c13fdd9e6d3b1d89a91724c15857191',1);
/*!40000 ALTER TABLE `zaposleni` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-13  0:25:00
