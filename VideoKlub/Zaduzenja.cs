﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class Zaduzenja: Form
    {
        #region Promjenljive
        private Form roditeljskaForma = null;
        private string brojKartice;
        private bool celijaFokusiranaNakonPretrage = false; // potrebno za fokus odgovaracujeg tekstualnog polja za pretragu 
        private int offset = 0; // potrebno za ucitavanje korisnika prilikom scroll-ovanja
        private ZaduzenjaUtil zaduzenjaUtil = new ZaduzenjaUtil(); // potrebno za manipulaciju podacima u bazi
        #endregion
        public Zaduzenja(Form roditeljskaForma, string brojKartice)
        {
            InitializeComponent();

            this.brojKartice = brojKartice;
            lblBrojKartice.Text = "Broj kartice: " + brojKartice;

            this.roditeljskaForma = roditeljskaForma;

            string[] ocjene = { "5", "4", "3", "2", "1" };
            clnOcjena.DataSource = ocjene;

            ActiveControl = tbNaslovFilma;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);

            pen.Color = Color.Gray;
        } // za iscrtavanje granica forme

        private void Pretraga()
        {
            string naslov;
            bool nerazduzeno;

            naslov = tbNaslovFilma.Text.EndsWith("...") ? "" : tbNaslovFilma.Text;

            nerazduzeno = chbNerazduzeno.Checked;

            List<zaduzenje_filma> zaduzenja = zaduzenjaUtil.Pretrazi(brojKartice, naslov, nerazduzeno).Take(20).ToList();

            bsrZaduzenja.Clear();

            foreach (zaduzenje_filma zaduzenje in zaduzenja)
            {
                bsrZaduzenja.Add(zaduzenje);
            }

            offset = 0;
        }
        private void UcitajZaduzenja()
        {
            List<zaduzenje_filma> zaduzenja = zaduzenjaUtil.GetPrvihZaKorisnika(20, brojKartice);

            bsrZaduzenja.Clear();

            foreach (var zaduzenje in zaduzenja)
            {
                bsrZaduzenja.Add(zaduzenje);
            }

            if (dgvZaduzenja.SelectedRows.Count != 0)
            {
                dgvZaduzenja.SelectedRows[0].Selected = true;
            }
        }
        private void Zaduzenja_FormClosing(object sender, FormClosingEventArgs e)
        {
            roditeljskaForma.Show();
        }
        private void Zaduzenja_Load(object sender, EventArgs e)
        {
            UcitajZaduzenja();

            if (dgvZaduzenja.SelectedRows.Count != 0)
            {
                dgvZaduzenja.SelectedRows[0].Selected = false;
            }
        }

        #region Button-i
        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        private void BtnNazad_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region DataGridView - zaduzenja
        private void DgvZaduzenja_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgvZaduzenja.Columns[e.ColumnIndex].Name == "clnNaslovFilma")
            {
                int filmUMagacinuId = (int)dgvZaduzenja.Rows[e.RowIndex].Cells["clnFilmUMagacinuId"].Value;
                string naslovFilma = zaduzenjaUtil.GetNaslovFilma(filmUMagacinuId);

                e.Value = naslovFilma;
            }
            else if (dgvZaduzenja.Columns[e.ColumnIndex].Name == "clnDatumZaduzenja")
            {
                string datum = e.Value.ToString();

                e.Value = datum.Split(' ')[0];
            }
            else if (dgvZaduzenja.Columns[e.ColumnIndex].Name == "clnDatumRazduzenja")
            {
                if (e.Value != null)
                {
                    string datum = e.Value.ToString();

                    e.Value = datum.Split(' ')[0];

                    dgvZaduzenja.Rows[e.RowIndex].ReadOnly = false;
                }
                else
                {
                    dgvZaduzenja.Rows[e.RowIndex].ReadOnly = true;
                }
            }
            else if (dgvZaduzenja.Columns[e.ColumnIndex].Name == "clnOcjena")
            {
                if (e.Value == null)
                {
                    e.Value = dgvZaduzenja.Rows[e.RowIndex].Cells["clnOcjenaFilma"].Value;
                }
            }    
        }
        private void DgvZaduzenja_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            // ukoliko je neka celija selektovana fokusira se textBox koji odgovara koloni u kojoj se nalazi ta celija
            // zbog mogucnosti pretrage vrijednosti koje se nalaze u toj koloni
            if (celijaFokusiranaNakonPretrage)
            {
                celijaFokusiranaNakonPretrage = false;
                return;
            }
            switch (e.ColumnIndex)
            {
                case 0: tbNaslovFilma.Focus(); break;
                case 1: chbNerazduzeno.Focus(); break;
                case 2: chbNerazduzeno.Focus(); break;
            }
        }
        private void DgvZaduzenja_Scroll(object sender, ScrollEventArgs e)
        {
            // dinamicko ucitavanje zaduzenja prilikom scroll-ovanja
            if ((e.NewValue + ((DataGridView)sender).DisplayedRowCount(false)) % 20 == 0)
            {
                offset += 20;

                string naslov = tbNaslovFilma.Text.EndsWith("...") ? "" : tbNaslovFilma.Text;
                bool nerazduzeno = chbNerazduzeno.Checked;

                List<zaduzenje_filma> zaduzenja = zaduzenjaUtil.GetSljedecihUzPretragu(brojKartice, naslov, nerazduzeno, 20, offset);

                foreach (var zaduzenje in zaduzenja)
                {
                    bsrZaduzenja.Add(zaduzenje);
                }
            }
        }
        private void DgvZaduzenja_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvZaduzenja.CurrentCell.ColumnIndex == 3 && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                comboBox.SelectedIndexChanged -= LastColumnComboSelectionChanged;
                comboBox.SelectedIndexChanged += LastColumnComboSelectionChanged;
            }
        }
        private void LastColumnComboSelectionChanged(object sender, EventArgs e)
        {
            var currentcell = dgvZaduzenja.CurrentCellAddress;
            var sendingCB = sender as DataGridViewComboBoxEditingControl;

            int indexReda = dgvZaduzenja.Rows[currentcell.Y].Index;
            zaduzenje_filma zaduzenje = (zaduzenje_filma)bsrZaduzenja[indexReda];

            if (sendingCB.EditingControlFormattedValue != null && sendingCB.EditingControlFormattedValue.ToString() != "" &&
                int.Parse(sendingCB.EditingControlFormattedValue.ToString()) != zaduzenje.ocjena_filma)
            {
                zaduzenjaUtil.OcijeniFilm(zaduzenje.film_u_magacinu_film_id, zaduzenje.datum_zaduzenja,
                    int.Parse(sendingCB.EditingControlFormattedValue.ToString()), brojKartice);
            }
        }
        #endregion

        #region GroupBox - pretraga zaduzenja
        private void GrbPretragaZaduzenja_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Black, Color.Black);
        }
        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Brisanje teksta i ivica
                g.Clear(this.BackColor);

                // Crtanje teksta
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Crtanje ivica
                //Lijeva
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Desna
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Donja
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Gornja1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Gornja2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }
        #endregion

        #region TextBox-ovi
        private void Tb_EnterFocus(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // onemogucavanje selekcije placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.SelectionStart = 0;
            }
            else
            {
                textBox.SelectAll();
            }
        }
        private void TbNaslovFilma_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbNaslovFilma_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }
            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-Z0-9šđčćžŠĐČĆŽ '.:!?,&-]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                textBox.Text = "Naslov filma...";
                textBox.ForeColor = Color.LightGray;
            }

            // kako bi textBox koji je bio fokusiran prilikom pretrage ostao fokusiran
            celijaFokusiranaNakonPretrage = true;

            Pretraga();

            textBox.TextChanged += TbNaslovFilma_TextChanged;
        }
        private void Tb_MouseUp(object sender, MouseEventArgs e)
        {
            // selekcija ili kompletnog sadrzaja textBox-a ili nista
            TextBox textBox = sender as TextBox;
            if (!textBox.Text.EndsWith("...") && textBox.SelectionLength == textBox.TextLength)
            {
                textBox.SelectAll();
            }
            else if (textBox.SelectionLength > 0)
            {
                textBox.SelectionLength = 0;
            }
        }
        private void Tb_KeyDown(object sender, KeyEventArgs e) // manipulacija redoslijedom fokusiranja komponenti i selekcije zaduzenja u dataGridView-u
        {
            // ako je neki textBox fokusiran i pritisnut je taster Up vrsi se selekcija zaduzenja koje se nalazi iznad vec odabranog
            // a ukoliko nije odabrano ni jedno onda se selektuje prvo zaduzenje u tabeli
            if (e.KeyCode == Keys.Up)
            {
                e.SuppressKeyPress = true;

                if (dgvZaduzenja.Rows.Count == 0)
                {
                    return;
                }
                if (dgvZaduzenja.SelectedRows.Count > 0)
                {
                    if (dgvZaduzenja.SelectedRows[0].Index > 0)
                    {
                        dgvZaduzenja.Rows[dgvZaduzenja.SelectedRows[0].Index - 1].Selected = true;
                        if (dgvZaduzenja.SelectedRows[0].Displayed == false)
                        {
                            if (dgvZaduzenja.SelectedRows[0].Index > dgvZaduzenja.FirstDisplayedScrollingRowIndex + dgvZaduzenja.DisplayedRowCount(false) - 1)
                            {
                                dgvZaduzenja.FirstDisplayedScrollingRowIndex = dgvZaduzenja.SelectedRows[0].Index - dgvZaduzenja.DisplayedRowCount(false) + 1;
                            }
                            else
                            {
                                dgvZaduzenja.FirstDisplayedScrollingRowIndex = dgvZaduzenja.FirstDisplayedScrollingRowIndex - 1;
                            }
                        }
                    }
                }
                else
                {
                    dgvZaduzenja.Rows[0].Selected = true;
                }
            }
            // ako je neki textBox fokusiran i pritisnut je taster Down vrsi se selekcija zaduzenja koje se nalazi ispod vec odabranog
            // a ukoliko nije odabrano ni jedno onda se selektuje prvo zaduzenje u tabeli
            else if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;

                if (dgvZaduzenja.Rows.Count == 0)
                {
                    return;
                }
                if (dgvZaduzenja.SelectedRows.Count > 0)
                {
                    if (dgvZaduzenja.SelectedRows[0].Index < dgvZaduzenja.Rows.Count - 1)
                    {
                        dgvZaduzenja.Rows[dgvZaduzenja.SelectedRows[0].Index + 1].Selected = true;
                        if (dgvZaduzenja.SelectedRows[0].Displayed == false)
                        {
                            if (dgvZaduzenja.SelectedRows[0].Index < dgvZaduzenja.FirstDisplayedScrollingRowIndex)
                            {
                                dgvZaduzenja.FirstDisplayedScrollingRowIndex = dgvZaduzenja.SelectedRows[0].Index;
                            }
                            else
                            {
                                dgvZaduzenja.FirstDisplayedScrollingRowIndex = dgvZaduzenja.FirstDisplayedScrollingRowIndex + 1;
                            }
                        }
                    }
                }
                else
                {
                    dgvZaduzenja.Rows[0].Selected = true;
                }
            }
            // ako je pritisnut taster Right fokusiramo komponentu koja se nalazi desno od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Right)
            {
                e.SuppressKeyPress = true;

                if (sender is TextBox)
                {
                    chbNerazduzeno.Focus();
                }
                else if (sender is CheckBox)
                {
                    tbNaslovFilma.Focus();
                }
            }
            // ako je pritisnut taster Right fokusiramo komponentu koja se nalazi lijevo od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Left)
            {
                e.SuppressKeyPress = true;

                if (sender is TextBox)
                {
                    chbNerazduzeno.Focus();
                }
                else if (sender is CheckBox)
                {
                    tbNaslovFilma.Focus();
                }
            }
            // ako je fokusiran neki textBox kombinacijom tastera Control i odredjenih tastera simulira se klik na odgovarajuci button
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
            else if (e.Modifiers == Keys.Control)
            {
                if (dgvZaduzenja.SelectedRows.Count > 0)
                {
                    if (e.KeyCode == Keys.D1 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.NumPad2 ||
                        e.KeyCode == Keys.D3 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.D4 || e.KeyCode == Keys.NumPad4 ||
                        e.KeyCode == Keys.D5 || e.KeyCode == Keys.NumPad5)
                    {
                        dgvZaduzenja.SelectedRows[0].Cells["clnOcjena"].Value = e.KeyCode.ToString().Last().ToString();

                        zaduzenje_filma zaduzenje = (zaduzenje_filma)bsrZaduzenja[dgvZaduzenja.SelectedRows[0].Index];

                        zaduzenjaUtil.OcijeniFilm(zaduzenje.film_u_magacinu_film_id, zaduzenje.datum_zaduzenja,
                             int.Parse(e.KeyCode.ToString().Last().ToString()), brojKartice);
                    }
                }
            }
        }
        #endregion

        #region CheckBox - prikaz samo nerazduzenih zaduzenja
        private void ChbNerazduzeno_CheckedChanged(object sender, EventArgs e)
        {
            // kako bi checkBox ostao fokusiran i nakon pretrage
            celijaFokusiranaNakonPretrage = true;

            Pretraga();
        }
        #endregion
    }
}
