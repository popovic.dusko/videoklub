﻿namespace VideoKlub
{
    partial class DodajGlumca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DodajGlumca));
            this.lblNoviGlumac = new System.Windows.Forms.Label();
            this.lblIme = new System.Windows.Forms.Label();
            this.lblPrezime = new System.Windows.Forms.Label();
            this.tbPrezime = new System.Windows.Forms.TextBox();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnNazad = new System.Windows.Forms.Button();
            this.tbIme = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblNoviGlumac
            // 
            this.lblNoviGlumac.AutoSize = true;
            this.lblNoviGlumac.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoviGlumac.Location = new System.Drawing.Point(13, 13);
            this.lblNoviGlumac.Name = "lblNoviGlumac";
            this.lblNoviGlumac.Size = new System.Drawing.Size(143, 24);
            this.lblNoviGlumac.TabIndex = 1;
            this.lblNoviGlumac.Text = "Novi glumac";
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIme.Location = new System.Drawing.Point(30, 77);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(44, 22);
            this.lblIme.TabIndex = 2;
            this.lblIme.Text = "Ime";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrezime.Location = new System.Drawing.Point(30, 125);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(79, 22);
            this.lblPrezime.TabIndex = 4;
            this.lblPrezime.Text = "Prezime";
            // 
            // tbPrezime
            // 
            this.tbPrezime.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPrezime.Location = new System.Drawing.Point(164, 122);
            this.tbPrezime.MaxLength = 255;
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.Size = new System.Drawing.Size(211, 31);
            this.tbPrezime.TabIndex = 5;
            this.tbPrezime.TextChanged += new System.EventHandler(this.Tb_TextChanged);
            this.tbPrezime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Enabled = false;
            this.btnDodaj.FlatAppearance.BorderSize = 2;
            this.btnDodaj.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodaj.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDodaj.Location = new System.Drawing.Point(164, 185);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(103, 34);
            this.btnDodaj.TabIndex = 6;
            this.btnDodaj.Text = "&Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.BtnDodaj_Click);
            // 
            // btnNazad
            // 
            this.btnNazad.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNazad.FlatAppearance.BorderSize = 2;
            this.btnNazad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNazad.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNazad.Location = new System.Drawing.Point(273, 185);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(102, 34);
            this.btnNazad.TabIndex = 7;
            this.btnNazad.Text = "&Nazad";
            this.btnNazad.UseVisualStyleBackColor = true;
            // 
            // tbIme
            // 
            this.tbIme.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIme.Location = new System.Drawing.Point(164, 74);
            this.tbIme.MaxLength = 255;
            this.tbIme.Name = "tbIme";
            this.tbIme.Size = new System.Drawing.Size(211, 31);
            this.tbIme.TabIndex = 4;
            this.tbIme.TextChanged += new System.EventHandler(this.Tb_TextChanged);
            this.tbIme.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            // 
            // DodajGlumca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.btnNazad;
            this.ClientSize = new System.Drawing.Size(395, 231);
            this.Controls.Add(this.tbIme);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.tbPrezime);
            this.Controls.Add(this.lblPrezime);
            this.Controls.Add(this.lblIme);
            this.Controls.Add(this.lblNoviGlumac);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DodajGlumca";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DodajZanr";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNoviGlumac;
        private System.Windows.Forms.Label lblIme;
        private System.Windows.Forms.Label lblPrezime;
        private System.Windows.Forms.TextBox tbPrezime;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.TextBox tbIme;
    }
}