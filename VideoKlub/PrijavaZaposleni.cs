﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoKlub
{
    public partial class PrijavaZaposleni : Form
    {

        private bool logged = false;
        private string ime = "";
        private string prezime = "";
        private int idZaposlenog;
        public PrijavaZaposleni()
        {
            InitializeComponent();
            ActiveControl = tbKorisnickoIme;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // iscrtavanje ivica
        private string LozinkaHashMD5(string lozinka)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(lozinka);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Konverzija niza bajtova u hash string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        private void Prijava(string korisnickoIme, string lozinka)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                string lozinkaHash = LozinkaHashMD5(lozinka);
                List<zaposleni> zaposleni = ctx.zaposleni.Where((z) => z.korisnicko_ime == korisnickoIme && z.lozinka == lozinkaHash).ToList();
                if (zaposleni.Count == 1)
                {
                    if (zaposleni[0].aktivan != 0)
                    {
                        logged = true;
                        prezime = zaposleni[0].prezime;
                        ime = zaposleni[0].ime;
                        idZaposlenog = zaposleni[0].id;
                        return;
                    }
                }
            }
            logged = false;
        }
        private void EnableComponents(bool enable)
        {
            tbKorisnickoIme.Enabled = enable;
            tbLozinka.Enabled = enable;
            btnPrijaviSe.Enabled = enable;
            btnPrijaviSe.FlatAppearance.BorderColor = enable ? Color.Black : SystemColors.Control;
            btnClose.Enabled = enable;
        } // o(ne)mogucavanje svih komponenti na formi prilikom prijave
        private void PrijavaZaposleni_FormClosing(object sender, FormClosingEventArgs e)
        {
            new GlavnaForma().Show();
        }

        #region Button-i
        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private async void BtnPrijaviSe_Click(object sender, EventArgs e)
        { 
            string korisnickoIme = tbKorisnickoIme.Text, lozinka = tbLozinka.Text;

            lblUnesiteKorisnickoIme.Visible = lblUnesiteLozinku.Visible = lblPrijavaNeuspjesna.Visible = false;

            if(string.IsNullOrEmpty(korisnickoIme))
            {
                lblUnesiteKorisnickoIme.Visible = true;
                tbKorisnickoIme.TextChanged += TbKorisnickoIme_TextChanged;
                tbKorisnickoIme.Focus();
                if(string.IsNullOrEmpty(lozinka))
                {
                    lblUnesiteLozinku.Visible = true;
                    tbLozinka.TextChanged += TbLozinka_TextChanged;
                }
                return;
            }
            if(string.IsNullOrEmpty(lozinka))
            {
                lblUnesiteLozinku.Visible = true;
                tbLozinka.TextChanged += TbLozinka_TextChanged;
                tbLozinka.Focus();
                return;
            }

            // prikaz loader-a kako bi korisnik dobio povratnu informaciju da je prijava u toku
            pbLoader.Visible = true;
            // onemogucavanje komponenti kako korisnik ne bi mogao da pokrene ponovo proces prijave dok je postojeci u toku
            EnableComponents(false);

            // asinhroni poziv kako bi forma ostala responsivna i kako se loader ne bi zaledio
            // ovo je prva konekcija na bazu u tok rada zaposlenog i moze duze da potraje pa je  zbog toga potrebno 
            // da korisnik ima povratnu informaciju da je prijava u toku
            await Task.Run(() => { Prijava(korisnickoIme, lozinka); });

            if(logged)
            {
                new GlavnaFormaZaposleni(ime, prezime, idZaposlenog).Show();
                FormClosing -= PrijavaZaposleni_FormClosing;
                Close();
            }
            else
            {
                lblPrijavaNeuspjesna.Visible = true;
                pbLoader.Visible = false;
                EnableComponents(true);
                tbKorisnickoIme.Focus();
            }
        }
        #endregion

        #region TextBox-ovi
        private void TbKorisnickoIme_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnPrijaviSe.PerformClick();
            }
        }
        private void TbLozinka_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnPrijaviSe.PerformClick();
            }
        }
        private void TbKorisnickoIme_Enter(object sender, EventArgs e)
        {
            tbKorisnickoIme.SelectAll();
        }
        private void TbKorisnickoIme_Leave(object sender, EventArgs e)
        {
            tbKorisnickoIme.DeselectAll();
        }
        private void TbLozinka_Enter(object sender, EventArgs e)
        {
            tbLozinka.SelectAll();
        }
        private void TbLozinka_Leave(object sender, EventArgs e)
        {
            tbLozinka.DeselectAll();
        }
        private void TbKorisnickoIme_TextChanged(object sender, EventArgs e)
        {
            if (lblUnesiteKorisnickoIme.Visible == true)
            {
                lblUnesiteKorisnickoIme.Visible = false;
            }

            // ovaj handler se koristi samo za sakrivanje labele koja govori da nije uneseno korisnicko ime
            // pa ce se pozvati samo kada korisnik unese prvi karakter
            tbKorisnickoIme.TextChanged -= TbKorisnickoIme_TextChanged;
        }
        private void TbLozinka_TextChanged(object sender, EventArgs e)
        {
            if (lblUnesiteLozinku.Visible == true)
            {
                lblUnesiteLozinku.Visible = false;
            }

            // ovaj handler se koristi samo za sakrivanje labele koja govori da nije unesena lozinka
            // pa ce se pozvati samo kada korisnik unese prvi karakter
            tbLozinka.TextChanged -= TbLozinka_TextChanged;
        }
        #endregion
    }
}
