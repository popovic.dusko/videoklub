﻿namespace VideoKlub
{
    partial class IzmijeniFilm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IzmijeniFilm));
            this.lblFilm = new System.Windows.Forms.Label();
            this.grbOsnovneInformacije = new System.Windows.Forms.GroupBox();
            this.btnUkloniSliku = new System.Windows.Forms.Button();
            this.pbSlika = new System.Windows.Forms.PictureBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.tbSlika = new System.Windows.Forms.TextBox();
            this.lblSlika = new System.Windows.Forms.Label();
            this.nudGodina = new System.Windows.Forms.NumericUpDown();
            this.lblGodina = new System.Windows.Forms.Label();
            this.tbNaslov = new System.Windows.Forms.TextBox();
            this.lblNaslov = new System.Windows.Forms.Label();
            this.grbZanr = new System.Windows.Forms.GroupBox();
            this.btnUkloniZanr = new System.Windows.Forms.Button();
            this.btnDodajZanr = new System.Windows.Forms.Button();
            this.lvZanrovi = new System.Windows.Forms.ListView();
            this.clnNaziv = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbZanrPretraga = new System.Windows.Forms.TextBox();
            this.grbGlumci = new System.Windows.Forms.GroupBox();
            this.tbIme = new System.Windows.Forms.TextBox();
            this.btnUkloniGlumca = new System.Windows.Forms.Button();
            this.btnDodajGlumca = new System.Windows.Forms.Button();
            this.lvGlumci = new System.Windows.Forms.ListView();
            this.clnPrezime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnIme = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbPrezime = new System.Windows.Forms.TextBox();
            this.lblCijena = new System.Windows.Forms.Label();
            this.nudCijena = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.nudNaStanju = new System.Windows.Forms.NumericUpDown();
            this.btnNazad = new System.Windows.Forms.Button();
            this.btnIzmijeni = new System.Windows.Forms.Button();
            this.grbOsnovneInformacije.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSlika)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodina)).BeginInit();
            this.grbZanr.SuspendLayout();
            this.grbGlumci.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCijena)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNaStanju)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFilm
            // 
            this.lblFilm.AutoSize = true;
            this.lblFilm.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilm.Location = new System.Drawing.Point(13, 13);
            this.lblFilm.Name = "lblFilm";
            this.lblFilm.Size = new System.Drawing.Size(49, 24);
            this.lblFilm.TabIndex = 1;
            this.lblFilm.Text = "Film";
            // 
            // grbOsnovneInformacije
            // 
            this.grbOsnovneInformacije.Controls.Add(this.btnUkloniSliku);
            this.grbOsnovneInformacije.Controls.Add(this.pbSlika);
            this.grbOsnovneInformacije.Controls.Add(this.btnBrowse);
            this.grbOsnovneInformacije.Controls.Add(this.tbSlika);
            this.grbOsnovneInformacije.Controls.Add(this.lblSlika);
            this.grbOsnovneInformacije.Controls.Add(this.nudGodina);
            this.grbOsnovneInformacije.Controls.Add(this.lblGodina);
            this.grbOsnovneInformacije.Controls.Add(this.tbNaslov);
            this.grbOsnovneInformacije.Controls.Add(this.lblNaslov);
            this.grbOsnovneInformacije.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbOsnovneInformacije.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbOsnovneInformacije.Location = new System.Drawing.Point(17, 50);
            this.grbOsnovneInformacije.Name = "grbOsnovneInformacije";
            this.grbOsnovneInformacije.Size = new System.Drawing.Size(656, 149);
            this.grbOsnovneInformacije.TabIndex = 21;
            this.grbOsnovneInformacije.TabStop = false;
            this.grbOsnovneInformacije.Text = "Osnovne informacije";
            this.grbOsnovneInformacije.Paint += new System.Windows.Forms.PaintEventHandler(this.Grb_Paint);
            // 
            // btnUkloniSliku
            // 
            this.btnUkloniSliku.BackgroundImage = global::VideoKlub.Properties.Resources.redX;
            this.btnUkloniSliku.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUkloniSliku.FlatAppearance.BorderSize = 0;
            this.btnUkloniSliku.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUkloniSliku.Location = new System.Drawing.Point(628, 30);
            this.btnUkloniSliku.Name = "btnUkloniSliku";
            this.btnUkloniSliku.Size = new System.Drawing.Size(15, 15);
            this.btnUkloniSliku.TabIndex = 4;
            this.btnUkloniSliku.UseVisualStyleBackColor = true;
            this.btnUkloniSliku.Visible = false;
            this.btnUkloniSliku.Click += new System.EventHandler(this.BtnUkloniSliku_Click);
            // 
            // pbSlika
            // 
            this.pbSlika.BackgroundImage = global::VideoKlub.Properties.Resources.logo;
            this.pbSlika.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbSlika.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbSlika.InitialImage = null;
            this.pbSlika.Location = new System.Drawing.Point(539, 30);
            this.pbSlika.Name = "pbSlika";
            this.pbSlika.Size = new System.Drawing.Size(104, 104);
            this.pbSlika.TabIndex = 7;
            this.pbSlika.TabStop = false;
            this.pbSlika.Tag = "Logo";
            this.pbSlika.Click += new System.EventHandler(this.PbSlika_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowse.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Location = new System.Drawing.Point(480, 106);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(40, 27);
            this.btnBrowse.TabIndex = 3;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.BtnBrowse_Click);
            // 
            // tbSlika
            // 
            this.tbSlika.Enabled = false;
            this.tbSlika.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSlika.Location = new System.Drawing.Point(80, 106);
            this.tbSlika.Name = "tbSlika";
            this.tbSlika.Size = new System.Drawing.Size(394, 27);
            this.tbSlika.TabIndex = 5;
            // 
            // lblSlika
            // 
            this.lblSlika.AutoSize = true;
            this.lblSlika.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlika.Location = new System.Drawing.Point(6, 109);
            this.lblSlika.Name = "lblSlika";
            this.lblSlika.Size = new System.Drawing.Size(43, 21);
            this.lblSlika.TabIndex = 4;
            this.lblSlika.Text = "Slika";
            // 
            // nudGodina
            // 
            this.nudGodina.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudGodina.Location = new System.Drawing.Point(80, 69);
            this.nudGodina.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudGodina.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.nudGodina.Name = "nudGodina";
            this.nudGodina.Size = new System.Drawing.Size(111, 27);
            this.nudGodina.TabIndex = 2;
            this.nudGodina.Value = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.nudGodina.Click += new System.EventHandler(this.Nud_Enter);
            this.nudGodina.Enter += new System.EventHandler(this.Nud_Enter);
            this.nudGodina.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Nud_KeyDown);
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGodina.Location = new System.Drawing.Point(6, 71);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(69, 21);
            this.lblGodina.TabIndex = 2;
            this.lblGodina.Text = "Godina";
            // 
            // tbNaslov
            // 
            this.tbNaslov.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNaslov.Location = new System.Drawing.Point(80, 30);
            this.tbNaslov.MaxLength = 255;
            this.tbNaslov.Name = "tbNaslov";
            this.tbNaslov.Size = new System.Drawing.Size(440, 27);
            this.tbNaslov.TabIndex = 1;
            // 
            // lblNaslov
            // 
            this.lblNaslov.AutoSize = true;
            this.lblNaslov.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaslov.Location = new System.Drawing.Point(6, 33);
            this.lblNaslov.Name = "lblNaslov";
            this.lblNaslov.Size = new System.Drawing.Size(62, 21);
            this.lblNaslov.TabIndex = 0;
            this.lblNaslov.Text = "Naslov";
            // 
            // grbZanr
            // 
            this.grbZanr.Controls.Add(this.btnUkloniZanr);
            this.grbZanr.Controls.Add(this.btnDodajZanr);
            this.grbZanr.Controls.Add(this.lvZanrovi);
            this.grbZanr.Controls.Add(this.tbZanrPretraga);
            this.grbZanr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbZanr.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbZanr.Location = new System.Drawing.Point(17, 205);
            this.grbZanr.Name = "grbZanr";
            this.grbZanr.Size = new System.Drawing.Size(320, 337);
            this.grbZanr.TabIndex = 22;
            this.grbZanr.TabStop = false;
            this.grbZanr.Text = "Žanr";
            this.grbZanr.Paint += new System.Windows.Forms.PaintEventHandler(this.Grb_Paint);
            // 
            // btnUkloniZanr
            // 
            this.btnUkloniZanr.Enabled = false;
            this.btnUkloniZanr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUkloniZanr.Location = new System.Drawing.Point(164, 301);
            this.btnUkloniZanr.Name = "btnUkloniZanr";
            this.btnUkloniZanr.Size = new System.Drawing.Size(150, 27);
            this.btnUkloniZanr.TabIndex = 8;
            this.btnUkloniZanr.TabStop = false;
            this.btnUkloniZanr.Text = "Ukloni";
            this.btnUkloniZanr.UseVisualStyleBackColor = true;
            this.btnUkloniZanr.Click += new System.EventHandler(this.BtnUkloniZanr_Click);
            // 
            // btnDodajZanr
            // 
            this.btnDodajZanr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodajZanr.Location = new System.Drawing.Point(7, 301);
            this.btnDodajZanr.Name = "btnDodajZanr";
            this.btnDodajZanr.Size = new System.Drawing.Size(150, 27);
            this.btnDodajZanr.TabIndex = 7;
            this.btnDodajZanr.TabStop = false;
            this.btnDodajZanr.Text = "Dodaj";
            this.btnDodajZanr.UseVisualStyleBackColor = true;
            this.btnDodajZanr.Click += new System.EventHandler(this.BtnDodajZanr_Click);
            // 
            // lvZanrovi
            // 
            this.lvZanrovi.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvZanrovi.CheckBoxes = true;
            this.lvZanrovi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clnNaziv});
            this.lvZanrovi.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvZanrovi.FullRowSelect = true;
            this.lvZanrovi.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvZanrovi.HideSelection = false;
            this.lvZanrovi.Location = new System.Drawing.Point(7, 61);
            this.lvZanrovi.MultiSelect = false;
            this.lvZanrovi.Name = "lvZanrovi";
            this.lvZanrovi.ShowItemToolTips = true;
            this.lvZanrovi.Size = new System.Drawing.Size(307, 234);
            this.lvZanrovi.TabIndex = 6;
            this.lvZanrovi.TabStop = false;
            this.lvZanrovi.UseCompatibleStateImageBehavior = false;
            this.lvZanrovi.View = System.Windows.Forms.View.Details;
            this.lvZanrovi.SelectedIndexChanged += new System.EventHandler(this.LvZanrovi_SelectedIndexChanged);
            this.lvZanrovi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LvZanrovi_KeyDown);
            // 
            // clnNaziv
            // 
            this.clnNaziv.Text = "Naziv žanra";
            this.clnNaziv.Width = 204;
            // 
            // tbZanrPretraga
            // 
            this.tbZanrPretraga.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbZanrPretraga.ForeColor = System.Drawing.Color.Silver;
            this.tbZanrPretraga.Location = new System.Drawing.Point(7, 27);
            this.tbZanrPretraga.MaxLength = 255;
            this.tbZanrPretraga.Name = "tbZanrPretraga";
            this.tbZanrPretraga.Size = new System.Drawing.Size(307, 27);
            this.tbZanrPretraga.TabIndex = 5;
            this.tbZanrPretraga.Text = "Žanr...";
            this.tbZanrPretraga.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbZanrPretraga.TextChanged += new System.EventHandler(this.TbZanrPretraga_TextChanged);
            this.tbZanrPretraga.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbZanrPretraga.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbZanrPretraga_KeyDown);
            this.tbZanrPretraga.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // grbGlumci
            // 
            this.grbGlumci.Controls.Add(this.tbIme);
            this.grbGlumci.Controls.Add(this.btnUkloniGlumca);
            this.grbGlumci.Controls.Add(this.btnDodajGlumca);
            this.grbGlumci.Controls.Add(this.lvGlumci);
            this.grbGlumci.Controls.Add(this.tbPrezime);
            this.grbGlumci.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbGlumci.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbGlumci.Location = new System.Drawing.Point(353, 205);
            this.grbGlumci.Name = "grbGlumci";
            this.grbGlumci.Size = new System.Drawing.Size(320, 337);
            this.grbGlumci.TabIndex = 23;
            this.grbGlumci.TabStop = false;
            this.grbGlumci.Text = "Glumci";
            this.grbGlumci.Paint += new System.Windows.Forms.PaintEventHandler(this.Grb_Paint);
            // 
            // tbIme
            // 
            this.tbIme.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIme.ForeColor = System.Drawing.Color.Silver;
            this.tbIme.Location = new System.Drawing.Point(164, 27);
            this.tbIme.MaxLength = 255;
            this.tbIme.Name = "tbIme";
            this.tbIme.Size = new System.Drawing.Size(150, 27);
            this.tbIme.TabIndex = 10;
            this.tbIme.TabStop = false;
            this.tbIme.Tag = "Ime";
            this.tbIme.Text = "Ime...";
            this.tbIme.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbIme.TextChanged += new System.EventHandler(this.TbPrezime_TextChanged);
            this.tbIme.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbIme.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbPrezime_KeyDown);
            this.tbIme.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // btnUkloniGlumca
            // 
            this.btnUkloniGlumca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUkloniGlumca.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUkloniGlumca.Location = new System.Drawing.Point(164, 301);
            this.btnUkloniGlumca.Name = "btnUkloniGlumca";
            this.btnUkloniGlumca.Size = new System.Drawing.Size(150, 27);
            this.btnUkloniGlumca.TabIndex = 13;
            this.btnUkloniGlumca.TabStop = false;
            this.btnUkloniGlumca.Text = "Ukloni";
            this.btnUkloniGlumca.UseVisualStyleBackColor = true;
            this.btnUkloniGlumca.Click += new System.EventHandler(this.BtnUkloniGlumca_Click);
            // 
            // btnDodajGlumca
            // 
            this.btnDodajGlumca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodajGlumca.Location = new System.Drawing.Point(7, 301);
            this.btnDodajGlumca.Name = "btnDodajGlumca";
            this.btnDodajGlumca.Size = new System.Drawing.Size(150, 27);
            this.btnDodajGlumca.TabIndex = 12;
            this.btnDodajGlumca.TabStop = false;
            this.btnDodajGlumca.Text = "Dodaj";
            this.btnDodajGlumca.UseVisualStyleBackColor = true;
            this.btnDodajGlumca.Click += new System.EventHandler(this.BtnDodajGlumca_Click);
            // 
            // lvGlumci
            // 
            this.lvGlumci.CheckBoxes = true;
            this.lvGlumci.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clnPrezime,
            this.clnIme});
            this.lvGlumci.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvGlumci.FullRowSelect = true;
            this.lvGlumci.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvGlumci.HideSelection = false;
            this.lvGlumci.Location = new System.Drawing.Point(7, 61);
            this.lvGlumci.MultiSelect = false;
            this.lvGlumci.Name = "lvGlumci";
            this.lvGlumci.ShowItemToolTips = true;
            this.lvGlumci.Size = new System.Drawing.Size(307, 234);
            this.lvGlumci.TabIndex = 11;
            this.lvGlumci.TabStop = false;
            this.lvGlumci.UseCompatibleStateImageBehavior = false;
            this.lvGlumci.View = System.Windows.Forms.View.Details;
            this.lvGlumci.SelectedIndexChanged += new System.EventHandler(this.LvGlumci_SelectedIndexChanged);
            this.lvGlumci.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LvGlumci_KeyDown);
            // 
            // clnPrezime
            // 
            this.clnPrezime.Text = "Prezime";
            this.clnPrezime.Width = 156;
            // 
            // clnIme
            // 
            this.clnIme.Text = "Ime";
            this.clnIme.Width = 129;
            // 
            // tbPrezime
            // 
            this.tbPrezime.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPrezime.ForeColor = System.Drawing.Color.Silver;
            this.tbPrezime.Location = new System.Drawing.Point(7, 27);
            this.tbPrezime.MaxLength = 255;
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.Size = new System.Drawing.Size(150, 27);
            this.tbPrezime.TabIndex = 9;
            this.tbPrezime.Tag = "Prezime";
            this.tbPrezime.Text = "Prezime...";
            this.tbPrezime.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbPrezime.TextChanged += new System.EventHandler(this.TbPrezime_TextChanged);
            this.tbPrezime.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbPrezime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbPrezime_KeyDown);
            this.tbPrezime.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // lblCijena
            // 
            this.lblCijena.AutoSize = true;
            this.lblCijena.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCijena.Location = new System.Drawing.Point(13, 549);
            this.lblCijena.Name = "lblCijena";
            this.lblCijena.Size = new System.Drawing.Size(100, 21);
            this.lblCijena.TabIndex = 24;
            this.lblCijena.Text = "Cijena (KM)";
            // 
            // nudCijena
            // 
            this.nudCijena.DecimalPlaces = 2;
            this.nudCijena.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCijena.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudCijena.Location = new System.Drawing.Point(181, 547);
            this.nudCijena.Name = "nudCijena";
            this.nudCijena.Size = new System.Drawing.Size(156, 27);
            this.nudCijena.TabIndex = 14;
            this.nudCijena.Click += new System.EventHandler(this.Nud_Enter);
            this.nudCijena.Enter += new System.EventHandler(this.Nud_Enter);
            this.nudCijena.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Nud_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 580);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 21);
            this.label1.TabIndex = 26;
            this.label1.Text = "Na stanju (kom)";
            // 
            // nudNaStanju
            // 
            this.nudNaStanju.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudNaStanju.Location = new System.Drawing.Point(181, 578);
            this.nudNaStanju.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudNaStanju.Name = "nudNaStanju";
            this.nudNaStanju.Size = new System.Drawing.Size(156, 27);
            this.nudNaStanju.TabIndex = 15;
            this.nudNaStanju.Click += new System.EventHandler(this.Nud_Enter);
            this.nudNaStanju.Enter += new System.EventHandler(this.Nud_Enter);
            this.nudNaStanju.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Nud_KeyDown);
            // 
            // btnNazad
            // 
            this.btnNazad.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNazad.FlatAppearance.BorderSize = 2;
            this.btnNazad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNazad.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNazad.Location = new System.Drawing.Point(571, 620);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(102, 34);
            this.btnNazad.TabIndex = 17;
            this.btnNazad.Text = "&Nazad";
            this.btnNazad.UseVisualStyleBackColor = true;
            // 
            // btnIzmijeni
            // 
            this.btnIzmijeni.Enabled = false;
            this.btnIzmijeni.FlatAppearance.BorderSize = 2;
            this.btnIzmijeni.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIzmijeni.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzmijeni.Location = new System.Drawing.Point(462, 620);
            this.btnIzmijeni.Name = "btnIzmijeni";
            this.btnIzmijeni.Size = new System.Drawing.Size(103, 34);
            this.btnIzmijeni.TabIndex = 16;
            this.btnIzmijeni.Text = "&Izmijeni";
            this.btnIzmijeni.UseVisualStyleBackColor = true;
            this.btnIzmijeni.Click += new System.EventHandler(this.BtnIzmijeni_Click);
            // 
            // IzmijeniFilm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.btnNazad;
            this.ClientSize = new System.Drawing.Size(690, 666);
            this.Controls.Add(this.btnIzmijeni);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.nudNaStanju);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudCijena);
            this.Controls.Add(this.lblCijena);
            this.Controls.Add(this.grbGlumci);
            this.Controls.Add(this.grbZanr);
            this.Controls.Add(this.grbOsnovneInformacije);
            this.Controls.Add(this.lblFilm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "IzmijeniFilm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IzmijeniFilm";
            this.Load += new System.EventHandler(this.IzmijeniFilm_Load);
            this.grbOsnovneInformacije.ResumeLayout(false);
            this.grbOsnovneInformacije.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSlika)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodina)).EndInit();
            this.grbZanr.ResumeLayout(false);
            this.grbZanr.PerformLayout();
            this.grbGlumci.ResumeLayout(false);
            this.grbGlumci.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCijena)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNaStanju)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFilm;
        private System.Windows.Forms.GroupBox grbOsnovneInformacije;
        private System.Windows.Forms.GroupBox grbZanr;
        private System.Windows.Forms.Label lblNaslov;
        private System.Windows.Forms.TextBox tbNaslov;
        private System.Windows.Forms.NumericUpDown nudGodina;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.PictureBox pbSlika;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox tbSlika;
        private System.Windows.Forms.Label lblSlika;
        private System.Windows.Forms.GroupBox grbGlumci;
        private System.Windows.Forms.TextBox tbZanrPretraga;
        private System.Windows.Forms.Button btnUkloniZanr;
        private System.Windows.Forms.Button btnDodajZanr;
        private System.Windows.Forms.ListView lvZanrovi;
        private System.Windows.Forms.Button btnUkloniGlumca;
        private System.Windows.Forms.Button btnDodajGlumca;
        private System.Windows.Forms.ListView lvGlumci;
        private System.Windows.Forms.TextBox tbPrezime;
        private System.Windows.Forms.Label lblCijena;
        private System.Windows.Forms.NumericUpDown nudCijena;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudNaStanju;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.Button btnIzmijeni;
        private System.Windows.Forms.ColumnHeader clnNaziv;
        private System.Windows.Forms.ColumnHeader clnPrezime;
        private System.Windows.Forms.ColumnHeader clnIme;
        private System.Windows.Forms.TextBox tbIme;
        private System.Windows.Forms.Button btnUkloniSliku;
    }
}