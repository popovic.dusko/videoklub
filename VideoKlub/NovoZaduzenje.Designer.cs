﻿namespace VideoKlub
{
    partial class NovoZaduzenje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NovoZaduzenje));
            this.lblNovoZaduzenje = new System.Windows.Forms.Label();
            this.lblBrojKartice = new System.Windows.Forms.Label();
            this.cmbBrojKartice = new System.Windows.Forms.ComboBox();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnNazad = new System.Windows.Forms.Button();
            this.lblKarticaNeaktivna = new System.Windows.Forms.Label();
            this.lblNaslovFilma = new System.Windows.Forms.Label();
            this.cmbNaslovFilma = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblNovoZaduzenje
            // 
            this.lblNovoZaduzenje.AutoSize = true;
            this.lblNovoZaduzenje.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNovoZaduzenje.Location = new System.Drawing.Point(13, 13);
            this.lblNovoZaduzenje.Name = "lblNovoZaduzenje";
            this.lblNovoZaduzenje.Size = new System.Drawing.Size(176, 24);
            this.lblNovoZaduzenje.TabIndex = 1;
            this.lblNovoZaduzenje.Text = "Novo zaduženje";
            // 
            // lblBrojKartice
            // 
            this.lblBrojKartice.AutoSize = true;
            this.lblBrojKartice.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrojKartice.Location = new System.Drawing.Point(30, 77);
            this.lblBrojKartice.Name = "lblBrojKartice";
            this.lblBrojKartice.Size = new System.Drawing.Size(110, 22);
            this.lblBrojKartice.TabIndex = 2;
            this.lblBrojKartice.Text = "Broj kartice";
            // 
            // cmbBrojKartice
            // 
            this.cmbBrojKartice.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbBrojKartice.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbBrojKartice.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBrojKartice.FormattingEnabled = true;
            this.cmbBrojKartice.IntegralHeight = false;
            this.cmbBrojKartice.Location = new System.Drawing.Point(164, 74);
            this.cmbBrojKartice.MaxDropDownItems = 12;
            this.cmbBrojKartice.Name = "cmbBrojKartice";
            this.cmbBrojKartice.Size = new System.Drawing.Size(297, 30);
            this.cmbBrojKartice.TabIndex = 1;
            this.cmbBrojKartice.SelectedIndexChanged += new System.EventHandler(this.CmbBrojKartice_SelectedIndexChanged);
            this.cmbBrojKartice.TextUpdate += new System.EventHandler(this.CmbBrojKartice_TextUpdate);
            this.cmbBrojKartice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cmb_KeyDown);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Enabled = false;
            this.btnDodaj.FlatAppearance.BorderSize = 2;
            this.btnDodaj.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodaj.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDodaj.Location = new System.Drawing.Point(250, 189);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(103, 34);
            this.btnDodaj.TabIndex = 3;
            this.btnDodaj.Text = "&Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.BtnDodaj_Click);
            // 
            // btnNazad
            // 
            this.btnNazad.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNazad.FlatAppearance.BorderSize = 2;
            this.btnNazad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNazad.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNazad.Location = new System.Drawing.Point(359, 189);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(102, 34);
            this.btnNazad.TabIndex = 4;
            this.btnNazad.Text = "&Nazad";
            this.btnNazad.UseVisualStyleBackColor = true;
            // 
            // lblKarticaNeaktivna
            // 
            this.lblKarticaNeaktivna.AutoSize = true;
            this.lblKarticaNeaktivna.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKarticaNeaktivna.ForeColor = System.Drawing.Color.Red;
            this.lblKarticaNeaktivna.Location = new System.Drawing.Point(160, 104);
            this.lblKarticaNeaktivna.Name = "lblKarticaNeaktivna";
            this.lblKarticaNeaktivna.Size = new System.Drawing.Size(160, 21);
            this.lblKarticaNeaktivna.TabIndex = 19;
            this.lblKarticaNeaktivna.Text = "Kartica nije aktivna";
            this.lblKarticaNeaktivna.Visible = false;
            // 
            // lblNaslovFilma
            // 
            this.lblNaslovFilma.AutoSize = true;
            this.lblNaslovFilma.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaslovFilma.Location = new System.Drawing.Point(30, 133);
            this.lblNaslovFilma.Name = "lblNaslovFilma";
            this.lblNaslovFilma.Size = new System.Drawing.Size(119, 22);
            this.lblNaslovFilma.TabIndex = 4;
            this.lblNaslovFilma.Text = "Naslov filma";
            // 
            // cmbNaslovFilma
            // 
            this.cmbNaslovFilma.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbNaslovFilma.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNaslovFilma.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNaslovFilma.FormattingEnabled = true;
            this.cmbNaslovFilma.IntegralHeight = false;
            this.cmbNaslovFilma.Location = new System.Drawing.Point(164, 130);
            this.cmbNaslovFilma.MaxDropDownItems = 12;
            this.cmbNaslovFilma.Name = "cmbNaslovFilma";
            this.cmbNaslovFilma.Size = new System.Drawing.Size(297, 30);
            this.cmbNaslovFilma.TabIndex = 2;
            this.cmbNaslovFilma.SelectedIndexChanged += new System.EventHandler(this.CmbNaslovFilma_SelectedIndexChanged);
            this.cmbNaslovFilma.TextUpdate += new System.EventHandler(this.CmbNaslovFilma_TextUpdate);
            this.cmbNaslovFilma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cmb_KeyDown);
            // 
            // NovoZaduzenje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.btnNazad;
            this.ClientSize = new System.Drawing.Size(483, 235);
            this.Controls.Add(this.cmbNaslovFilma);
            this.Controls.Add(this.lblKarticaNeaktivna);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.lblNaslovFilma);
            this.Controls.Add(this.cmbBrojKartice);
            this.Controls.Add(this.lblBrojKartice);
            this.Controls.Add(this.lblNovoZaduzenje);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NovoZaduzenje";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DodajZanr";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNovoZaduzenje;
        private System.Windows.Forms.Label lblBrojKartice;
        private System.Windows.Forms.ComboBox cmbBrojKartice;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.Label lblKarticaNeaktivna;
        private System.Windows.Forms.Label lblNaslovFilma;
        private System.Windows.Forms.ComboBox cmbNaslovFilma;
    }
}