﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class DodajGlumca : Form
    {
        private int idGlumca;
        private FilmoviUtil filmoviUtil = new FilmoviUtil();

        public DodajGlumca()
        {
            InitializeComponent();

            OmoguciDugmeDodaj();

            ActiveControl = tbIme;
        }
        public DialogResult ShowDialog(out int idGlumca)
        {
            DialogResult dialogResult = ShowDialog();
            if (DialogResult.Yes == dialogResult)
            {
                idGlumca = this.idGlumca;
                return DialogResult.Yes;
            }

            idGlumca = -1;
            return DialogResult.Cancel;
        } // prilagodjena metoda ShowDialog kako bi se znalo koji je ID dodanog glumca
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // za iscrtavanje granica forme
        private bool OmoguciDugmeDodaj()
        {
            if (string.IsNullOrEmpty(tbIme.Text) || string.IsNullOrEmpty(tbPrezime.Text))
            {
                btnDodaj.FlatAppearance.BorderColor = SystemColors.Control;
                return btnDodaj.Enabled = false;
            }
            btnDodaj.FlatAppearance.BorderColor = Color.Black;
            return btnDodaj.Enabled = true;
        } // ukoliko potrebni podaci nisu uneseni dugme ce biti onemogucen

        private void BtnDodaj_Click(object sender, EventArgs e)
        {
            idGlumca =  filmoviUtil.DodajGlumca(tbIme.Text, tbPrezime.Text);

            DialogResult = DialogResult.Yes;
        }

        private void Tb_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            var pozicijaKursora = textBox.SelectionStart;
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ. '-]", "");
            textBox.SelectionStart = pozicijaKursora;

            OmoguciDugmeDodaj();
        }
        private void Tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodaj.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
            else if (e.KeyCode == Keys.Return)
            {
                btnDodaj.PerformClick();
            }
        }
    }
}
