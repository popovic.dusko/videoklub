﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace VideoKlub
{
    public partial class DijalogUpozorenje : Form
    {
        public DijalogUpozorenje()
        {
            InitializeComponent();
        }
        public DijalogUpozorenje(string naslov = "", string poruka = "")
        {
            InitializeComponent();
            lblNaslov.Text = naslov == "" ? lblNaslov.Text : naslov;
            lblPoruka.Text = poruka == "" ? lblPoruka.Text : poruka;

            int sirinaNaslova = lblNaslov.Location.X + lblNaslov.Size.Width + 13;
            int sirinaPoruke = lblPoruka.Location.X + lblPoruka.Size.Width + 13;

            if (sirinaNaslova > sirinaPoruke)
            {
                if (sirinaNaslova > Size.Width)
                {
                    int newWidth = sirinaNaslova;
                    lblPoruka.Location = new Point(lblPoruka.Location.X + (newWidth - Width), lblPoruka.Location.Y);
                    btnDa.Location = new Point(btnDa.Location.X + (newWidth - Width), btnDa.Location.Y);
                    btnNe.Location = new Point(btnNe.Location.X + (newWidth - Width), btnNe.Location.Y);
                    Width = newWidth;
                }
            }
            else
            {
                if (sirinaPoruke > Size.Width)
                {
                    int newWidth = sirinaPoruke;
                    btnDa.Location = new Point(btnDa.Location.X + (newWidth - Width), btnDa.Location.Y);
                    btnNe.Location = new Point(btnNe.Location.X + (newWidth - Width), btnNe.Location.Y);
                    Width = newWidth;
                }
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // iscrtavanje linija
        private void BtnDa_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
        }
    }
}
