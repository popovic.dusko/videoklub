﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class DodajZanr : Form
    {
        private string nazivZanra, kategorijaZanra;
        private FilmoviUtil filmoviUtil = new FilmoviUtil();

        public DodajZanr()
        {
            InitializeComponent();

            //popunjavanje comboBox-a
            List<kategorija_zanra> kategorijeZanra = filmoviUtil.GetKategorijeZanra().ToList();

            foreach (var kategorija in kategorijeZanra)
            {
                cmbKategorijeZanra.Items.Add(kategorija.opis);
            }

            OmoguciDugmeDodaj();

            ActiveControl = cmbKategorijeZanra;
        }
        public DialogResult ShowDialog(out string nazivZanra, out string kategorijaZanra)
        {
            DialogResult dialogResult = ShowDialog();
            if (DialogResult.Yes == dialogResult)
            {
                nazivZanra = this.nazivZanra;
                kategorijaZanra = this.kategorijaZanra;
                return DialogResult.Yes;
            }

            nazivZanra = null;
            kategorijaZanra = null;
            return DialogResult.Cancel;
        } // prilagodjena metoda ShowDialog kako bi se znalo koji su naziv i kategorija dodanog zanra
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // za iscrtavanje granica forme

        private bool IsKategorijaValidna(string kategorija)
        {
            foreach(var item in cmbKategorijeZanra.Items)
            {
                if(kategorija == (string)item)
                {
                    return true;
                }
            }
            return false;
        } // koristeno za omogucavanje dugmeta Dodaj
        private bool OmoguciDugmeDodaj()
        {
            if (string.IsNullOrEmpty(tbNazivZanra.Text) || !IsKategorijaValidna(cmbKategorijeZanra.Text))
            {
                btnDodaj.FlatAppearance.BorderColor = SystemColors.Control;
                return btnDodaj.Enabled = false;
            }
            btnDodaj.FlatAppearance.BorderColor = Color.Black;
            return btnDodaj.Enabled = true;
        } // ukoliko potrebni podaci nisu uneseni dugme ce biti onemogucen

        #region CmbKategorijeZanra
        private void CmbKategorijeZanra_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lblZanrPostoji.Visible)
            {
                lblZanrPostoji.Visible = false;
            }

            tbNazivZanra.Focus();

            OmoguciDugmeDodaj();
        }
        private void CmbKategorijeZanra_TextUpdate(object sender, EventArgs e)
        {
            if (lblZanrPostoji.Visible)
            {
                lblZanrPostoji.Visible = false;
            }
            OmoguciDugmeDodaj();
        }
        private void CmbKategorijeZanra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    btnDodaj.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    btnNazad.PerformClick();
                }
            }
        }
        #endregion

        #region TbNazivZanra
        private void TbNazivZanra_TextChanged(object sender, EventArgs e)
        {
            if (lblZanrPostoji.Visible)
            {
                lblZanrPostoji.Visible = false;
            }

            TextBox textBox = sender as TextBox;

            var pozicijaKursora = textBox.SelectionStart;
            if (!string.IsNullOrEmpty(textBox.Text) && textBox.Text[0] >= 'a' && textBox.Text[0] <= 'z')
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ ]", "");
            textBox.SelectionStart = pozicijaKursora;

            OmoguciDugmeDodaj();
        }
        private void TbNazivZanra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    btnDodaj.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    btnNazad.PerformClick();
                }
            }
        }
        #endregion

        private void BtnDodaj_Click(object sender, EventArgs e)
        {
            nazivZanra = tbNazivZanra.Text;
            kategorijaZanra = cmbKategorijeZanra.Text;

            if (filmoviUtil.IsZanrPostojeci(kategorijaZanra, nazivZanra))
            {
                lblZanrPostoji.Visible = true;
                return;
            }

            filmoviUtil.DodajZanr(kategorijaZanra, nazivZanra);

            DialogResult = DialogResult.Yes;
        }
    }
}
