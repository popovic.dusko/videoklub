﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class PrijavaKorisnik : Form
    {
        private static readonly string brojeviKarticaPutanja = "./brojeviKartica.txt"; // upamceni brojevi kartica
        private string brojKartice;
        private KorisniciUtil korisniciUtil = new KorisniciUtil();
        public PrijavaKorisnik()
        {
            InitializeComponent();

            if (File.Exists(brojeviKarticaPutanja))
            {
                string[] brojeviKartica = File.ReadAllLines(brojeviKarticaPutanja);
                brojeviKartica = brojeviKartica.Reverse().ToArray();
                if (brojeviKartica.Length == 0)
                {
                    btnOK.Enabled = false;
                }
                else
                {
                    cmbBrojKartice.Items.AddRange(brojeviKartica);

                    // selekcija poslednjeg prijavljenog korisnika
                    cmbBrojKartice.SelectedIndex = 0;
                    chbZapamti.Checked = true;

                    cmbBrojKartice.SelectAll();
                }
            }
            else
            {
                btnOK.Enabled = false;
            }

            ActiveControl = cmbBrojKartice;
        }
        public DialogResult ShowDialog(out string brojKartice)
        {
            DialogResult dialogResult = ShowDialog();
            if (DialogResult.Yes == dialogResult)
            {
                brojKartice = this.brojKartice;
                return DialogResult.Yes;
            }

            brojKartice = null;
            return DialogResult.Cancel;
        } // prilagodjena metoda ShowDialog kako bi se znalo koji je ID dodanog korisnika
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // iscrtavanje ivica

        private void BtnOK_EnabledChanged(object sender, EventArgs e)
        {
            // podesavanje izgleda button-a u zavisnoti da li je enabled
            Button btn = sender as Button;
            if (btn.Enabled == true)
            {
                btn.FlatAppearance.BorderColor = Color.Black;
            }
            else
            {
                btn.FlatAppearance.BorderColor = SystemColors.Control;
            }
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            if(!korisniciUtil.IsBrojKarticePostojeci(cmbBrojKartice.Text))
            {
                lblKarticaNeaktivna.Text = "Broj kartice je nepostojeći";
                lblKarticaNeaktivna.Visible = true;
                return;
            }
            if(!korisniciUtil.IsKarticaAktivna(cmbBrojKartice.Text))
            {
                lblKarticaNeaktivna.Text = "Kartica nije aktivna";
                lblKarticaNeaktivna.Visible = true;
                return;
            }

            List<string> zapamceniBrojeviKartica = null;

            try
            {
                zapamceniBrojeviKartica = File.ReadAllLines(brojeviKarticaPutanja).ToList();
            }
            catch (FileNotFoundException)
            {
                zapamceniBrojeviKartica = new List<string>();
            }

            zapamceniBrojeviKartica.Remove(cmbBrojKartice.Text);

            if(chbZapamti.Checked)
            {
                zapamceniBrojeviKartica.Add(cmbBrojKartice.Text);
            }

            File.WriteAllLines(brojeviKarticaPutanja, zapamceniBrojeviKartica.ToArray());

            brojKartice = cmbBrojKartice.Text;

            DialogResult = DialogResult.Yes;
        }

        #region CmbBrojKartice
        private void CmbBrojKartice_TextUpdate(object sender, EventArgs e)
        {
            if (lblKarticaNeaktivna.Visible)
            {
                lblKarticaNeaktivna.Visible = false;
            }

            ComboBox comboBox = sender as ComboBox;
            comboBox.TextUpdate -= CmbBrojKartice_TextUpdate;

            // provjera ispravnosti unosa
            var pozicijaKursora = comboBox.SelectionStart;
            comboBox.ForeColor = Color.Black;
            comboBox.Text = Regex.Replace(comboBox.Text, "[^0-9a-zA-Z]", "").ToUpper();
            comboBox.SelectionStart = pozicijaKursora;

            btnOK.Enabled = string.IsNullOrEmpty(comboBox.Text) ? false : true;

            btnOK.Enabled = comboBox.Text.Length == 10 ? true : false;

            comboBox.TextUpdate += CmbBrojKartice_TextUpdate;
        }
        private void CmbBrojKartice_Enter(object sender, EventArgs e)
        {
            cmbBrojKartice.SelectAll();
        }
        private void CmbBrojKartice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
                else if (e.KeyCode == Keys.O)
                {
                    e.SuppressKeyPress = true;
                    btnOK.PerformClick();
                }
            }
            else if (e.KeyCode == Keys.Return)
            {
                btnOK.PerformClick();
            }
        }
        private void CmbBrojKartice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbBrojKartice.Text))
            {
                btnOK.Enabled = true;
            }
            else
            {
                btnOK.Enabled = false;
            }
        }
        #endregion
    }
}
