﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace VideoKlub
{
    public partial class FilmPanel : UserControl
    {

        public FilmPanel(film film, List<string> zanrovi, List<string> glumci, decimal ocjena, int brojZaduzivanja, decimal cijena)
        {
            InitializeComponent();

            lblNaslov.Text = film.naslov;

            if (File.Exists(Application.StartupPath + "\\" + film.slika))
            {
                pbSlika.BackgroundImage = Image.FromFile(Application.StartupPath + "\\" + film.slika);
            }
            else
            {
                pbSlika.BackgroundImage = Properties.Resources.logo;
            }

            lblGodina.Text = film.godina.ToString();
            lblGodina.Location = new Point(lblGodina.Location.X, lblNaslov.Location.Y + lblNaslov.Height + 5);
            pbOcjena.Location = new Point(pbOcjena.Location.X, lblGodina.Location.Y);
            lblOcjena.Location = new Point(lblOcjena.Location.X, pbOcjena.Location.Y + 3);

            lblZanr.Location = new Point(lblZanr.Location.X, lblGodina.Location.Y + lblGodina.Height + 13);

            string labelaZanr = "Žanr: ";
            foreach(string zanr in zanrovi)
            {
                labelaZanr += zanr + ", ";
            }
            labelaZanr = labelaZanr.Substring(0, labelaZanr.Length - 2);
            lblZanr.Text = labelaZanr;

            lblGlavneUloge.Location = new Point(lblGlavneUloge.Location.X, lblZanr.Location.Y + lblZanr.Height + 11);

            string labelaGlavneUloge = "Glavne uloge: ";
            foreach (string glumac in glumci)
            {
                labelaGlavneUloge += glumac + ", ";
            }
            labelaGlavneUloge = labelaGlavneUloge.Substring(0, labelaGlavneUloge.Length - 2);
            lblGlavneUloge.Text = labelaGlavneUloge;

            lblOcjena.Text = decimal.Round(ocjena, 1).ToString();
            lblBrojIzdavanja.Text = "Broj izdavanja: " + brojZaduzivanja.ToString();

            lblCijena.Text = cijena.ToString() + " KM";
            lblCijena.Location = new Point(Width - 4 - lblCijena.Width, lblCijena.Location.Y);
        }
    }
}
