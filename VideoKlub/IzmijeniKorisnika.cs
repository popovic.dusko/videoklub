﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class IzmijeniKorisnika : Form
    {
        private int idKorisnika;
        private string ime, prezime;
        private KorisniciUtil korisniciUtil = new KorisniciUtil();
        public IzmijeniKorisnika(int idKorisnika)
        {
            InitializeComponent();
            this.idKorisnika = idKorisnika;

            ActiveControl = tbIme;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // iscrtavanje linija
        private bool OmoguciDugmeIzmijeni()
        {
            if (string.IsNullOrEmpty(tbIme.Text) || string.IsNullOrEmpty(tbPrezime.Text) ||
                (tbIme.Text.Equals(ime) && tbPrezime.Text.Equals(prezime)))
            {
                btnIzmijeni.FlatAppearance.BorderColor = SystemColors.Control;
                return btnIzmijeni.Enabled = false;
            }
            btnIzmijeni.FlatAppearance.BorderColor = Color.Black;
            return btnIzmijeni.Enabled = true;
        } // ukoliko nema izmjena

        private void IzmijeniKorisnika_Load(object sender, EventArgs e)
        {
            korisnik korisnik = korisniciUtil.GetNaOsnovuId(idKorisnika);

            ime = tbIme.Text = korisnik.ime;
            prezime = tbPrezime.Text = korisnik.prezime;
            tbJMBG.Text = korisnik.jmbg;
            tbBrojKartice.Text = korisnik.broj_kartice;

            tbBrojKartice.Enabled = tbJMBG.Enabled = false;

            OmoguciDugmeIzmijeni();
            tbIme.TextChanged += TbIme_TextChanged;
            tbPrezime.TextChanged += TbIme_TextChanged;
        } // popunjavanje forme podacima o korisniku

        private void BtnIzmijeni_Click(object sender, EventArgs e)
        {
            korisniciUtil.IzmijeniKorisnika(idKorisnika, tbIme.Text, tbPrezime.Text);
 
            DialogResult = DialogResult.Yes;
        }

        #region TextBox-ovi
        private void Tb_EnterFocus(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            textBox.SelectAll();
        }
        private void Tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.I)
                {
                    e.SuppressKeyPress = true;
                    btnIzmijeni.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
            else if (e.KeyCode == Keys.Enter)
            {
                btnIzmijeni.PerformClick();
            }
        }
        private void TbIme_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;
            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(textBox.Text) && textBox.Text[0] >= 'a' && textBox.Text[0] <= 'z')
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ .'-]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko su izmjene unesene dugme ce biti omoguceno
            OmoguciDugmeIzmijeni();
        }
        #endregion
    }
}
