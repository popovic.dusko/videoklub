﻿namespace VideoKlub
{
    partial class DijalogUpozorenje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DijalogUpozorenje));
            this.lblNaslov = new System.Windows.Forms.Label();
            this.pbUpozorenje = new System.Windows.Forms.PictureBox();
            this.lblPoruka = new System.Windows.Forms.Label();
            this.btnNe = new System.Windows.Forms.Button();
            this.btnDa = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbUpozorenje)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNaslov
            // 
            this.lblNaslov.AutoSize = true;
            this.lblNaslov.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaslov.Location = new System.Drawing.Point(13, 13);
            this.lblNaslov.Name = "lblNaslov";
            this.lblNaslov.Size = new System.Drawing.Size(373, 24);
            this.lblNaslov.TabIndex = 0;
            this.lblNaslov.Text = "Da li zaista želite napustiti aplikaciju?";
            // 
            // pbUpozorenje
            // 
            this.pbUpozorenje.BackgroundImage = global::VideoKlub.Properties.Resources.alert_triangle;
            this.pbUpozorenje.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbUpozorenje.Location = new System.Drawing.Point(17, 56);
            this.pbUpozorenje.Name = "pbUpozorenje";
            this.pbUpozorenje.Size = new System.Drawing.Size(80, 80);
            this.pbUpozorenje.TabIndex = 1;
            this.pbUpozorenje.TabStop = false;
            // 
            // lblPoruka
            // 
            this.lblPoruka.AutoSize = true;
            this.lblPoruka.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoruka.Location = new System.Drawing.Point(123, 89);
            this.lblPoruka.Name = "lblPoruka";
            this.lblPoruka.Size = new System.Drawing.Size(259, 21);
            this.lblPoruka.TabIndex = 2;
            this.lblPoruka.Text = "Nesačuvan rad će biti izgubljen!";
            // 
            // btnNe
            // 
            this.btnNe.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNe.FlatAppearance.BorderSize = 2;
            this.btnNe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNe.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNe.Location = new System.Drawing.Point(299, 142);
            this.btnNe.Name = "btnNe";
            this.btnNe.Size = new System.Drawing.Size(87, 34);
            this.btnNe.TabIndex = 3;
            this.btnNe.Text = "Ne";
            this.btnNe.UseVisualStyleBackColor = true;
            // 
            // btnDa
            // 
            this.btnDa.FlatAppearance.BorderSize = 2;
            this.btnDa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDa.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDa.Location = new System.Drawing.Point(202, 142);
            this.btnDa.Name = "btnDa";
            this.btnDa.Size = new System.Drawing.Size(87, 34);
            this.btnDa.TabIndex = 4;
            this.btnDa.Text = "Da";
            this.btnDa.UseVisualStyleBackColor = true;
            this.btnDa.Click += new System.EventHandler(this.BtnDa_Click);
            // 
            // DijalogUpozorenje
            // 
            this.AcceptButton = this.btnNe;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.btnNe;
            this.ClientSize = new System.Drawing.Size(400, 188);
            this.Controls.Add(this.btnDa);
            this.Controls.Add(this.btnNe);
            this.Controls.Add(this.lblPoruka);
            this.Controls.Add(this.pbUpozorenje);
            this.Controls.Add(this.lblNaslov);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DijalogUpozorenje";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CloseDialog";
            ((System.ComponentModel.ISupportInitialize)(this.pbUpozorenje)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNaslov;
        private System.Windows.Forms.PictureBox pbUpozorenje;
        private System.Windows.Forms.Label lblPoruka;
        private System.Windows.Forms.Button btnNe;
        private System.Windows.Forms.Button btnDa;
    }
}