﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class DodajKorisnika : Form
    {
        private int idDodanogKorisnika;
        private KorisniciUtil korisniciUtil = new KorisniciUtil();

        public DodajKorisnika()
        {
            InitializeComponent();
            ActiveControl = tbIme;
        }
        public DialogResult ShowDialog(out int idDodanogKorisnika)
        {
            DialogResult dialogResult = ShowDialog();
            if (DialogResult.Yes == dialogResult)
            {
                idDodanogKorisnika = this.idDodanogKorisnika;
                return DialogResult.Yes;
            }
            else if (DialogResult.Retry == dialogResult)
            {
                idDodanogKorisnika = this.idDodanogKorisnika;
                return DialogResult.Retry;
            }
            idDodanogKorisnika = -1;
            return DialogResult.Cancel;
        } // prilagodjena metoda ShowDialog kako bi se znalo koji je ID dodanog korisnika
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // iscrtavanje linija
        private string RandomString(int length)
        {
            Random rng = new Random();
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            char[] buffer = new char[length];

            for (int i = 0; i < length; i++)
            {
                buffer[i] = chars[rng.Next(chars.Length)];
            }
            return new string(buffer);
        } // za generisanje broja kartice
        private bool OmoguciDugmeDodaj()
        {
            if(string.IsNullOrEmpty(tbBrojKartice.Text) || string.IsNullOrEmpty(tbIme.Text) ||
                string.IsNullOrEmpty(tbPrezime.Text) || string.IsNullOrEmpty(tbJMBG.Text) ||
                tbJMBG.Text.Length != 13 || tbBrojKartice.Text.Length != 10)
            {
                btnDodaj.FlatAppearance.BorderColor = SystemColors.Control;
                return btnDodaj.Enabled = false;
            }
            btnDodaj.FlatAppearance.BorderColor = Color.Black;
            return btnDodaj.Enabled = true;           
        } // ukoliko potrebni podaci nisu uneseni dugme ce biti onemogucen
        private void DodajKorisnika_Load(object sender, EventArgs e)
        {
            bool brojKarticeJedinstven = false;
                
            while (!brojKarticeJedinstven)
            {
                string brojKartice = RandomString(10);
                if(korisniciUtil.IsBrojKarticeJedinstven(brojKartice))
                {
                    brojKarticeJedinstven = true;
                    tbBrojKartice.Text = brojKartice;
                }
            }
        }

        private void BtnDodaj_Click(object sender, EventArgs e)
        {
            if(!korisniciUtil.IsJmbgJedinstven(tbJMBG.Text))
            {
                BackColor = SystemColors.Control;
                if (DialogResult.Yes == new DijalogUpozorenje("Korisnik sa unesenim JMBG već postoji!", "Pregledati informacije o korisniku?").ShowDialog())
                {
                    idDodanogKorisnika = korisniciUtil.GetIdKorisnikaNaOsnovuJmbg(tbJMBG.Text);
                    DialogResult = DialogResult.Retry;
                }
                BackColor = SystemColors.Window;
                return;
            }
            if(!korisniciUtil.IsBrojKarticeJedinstven(tbBrojKartice.Text))
            {
                BackColor = SystemColors.Control;
                if (DialogResult.Yes == new DijalogUpozorenje("Korisnik sa unesenim brojem kartice već postoji!", "Pregledati informacije o korisniku?").ShowDialog())
                {
                    idDodanogKorisnika = korisniciUtil.GetIdKorisnikaNaOsnovuBrojaKartice(tbBrojKartice.Text);
                    DialogResult = DialogResult.Retry;
                }
                BackColor = SystemColors.Window;
                return;
            }

            idDodanogKorisnika = korisniciUtil.DodajKorisnika(tbIme.Text, tbPrezime.Text, tbJMBG.Text, tbBrojKartice.Text);

            DialogResult = DialogResult.Yes;
        }

        #region TextBox-ovi
        private void Tb_EnterFocus(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            textBox.SelectAll();
        }
        private void Tb_KeyDown(object sender, KeyEventArgs e)
        {
           // Control + Shift
           if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodaj.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
            else if (e.KeyCode == Keys.Enter)
            {
                btnDodaj.PerformClick();
            }
        }
        private void TbIme_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            var pozicijaKursora = textBox.SelectionStart;
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ .'-]", "");
            textBox.SelectionStart = pozicijaKursora;

            OmoguciDugmeDodaj();
        }
        private void TbJMBG_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            var pozicijaKursora = textBox.SelectionStart;
            textBox.Text = Regex.Replace(textBox.Text, "[^0-9]", "");
            textBox.SelectionStart = pozicijaKursora;

            OmoguciDugmeDodaj();
        }
        private void TbBrojKartice_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            var pozicijaKursora = textBox.SelectionStart;
            textBox.Text = Regex.Replace(textBox.Text, "[^0-9a-zA-Z]", "").ToUpper();
            textBox.SelectionStart = pozicijaKursora;

            OmoguciDugmeDodaj();
        }
        #endregion
    }
}
