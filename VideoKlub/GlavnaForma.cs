﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace VideoKlub
{
    public partial class GlavnaForma : Form
    {
        public GlavnaForma()
        {
            InitializeComponent();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // iscrtavanje ivica
        private void GlavnaForma_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        #region Button-i
        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void BtnKatalogFilmova_Click(object sender, EventArgs e)
        {
            new KatalogFilmova(this).Show();
            Hide();
        }

        private void BtnZaduzenja_Click(object sender, EventArgs e)
        {
            BackColor = SystemColors.Control;
            string brojKartice;

            if (DialogResult.Yes == new PrijavaKorisnik().ShowDialog(out brojKartice))
            {
                new Zaduzenja(this, brojKartice).Show();
                Hide();
            }
            BackColor = SystemColors.Window;
        }

        private void BtnPrijava_MouseEnter(object sender, EventArgs e)
        {
            btnPrijava.Cursor = Cursors.Hand;
        }

        private void BtnPrijava_MouseLeave(object sender, EventArgs e)
        {
            btnPrijava.Cursor = Cursors.Arrow;
        }

        private void BtnPrijava_Click(object sender, EventArgs e)
        {
            PrijavaZaposleni prijavaZaposleni = new PrijavaZaposleni();
            prijavaZaposleni.Show();
            FormClosing -= GlavnaForma_FormClosing;
            Close();
            FormClosing += GlavnaForma_FormClosing;
        }
        #endregion
    }
}
