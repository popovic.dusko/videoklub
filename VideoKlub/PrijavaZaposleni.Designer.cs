﻿namespace VideoKlub
{
    partial class PrijavaZaposleni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrijavaZaposleni));
            this.tbKorisnickoIme = new System.Windows.Forms.TextBox();
            this.tbLozinka = new System.Windows.Forms.TextBox();
            this.lblKorisnickoIme = new System.Windows.Forms.Label();
            this.lblLozinka = new System.Windows.Forms.Label();
            this.btnPrijaviSe = new System.Windows.Forms.Button();
            this.lblUnesiteKorisnickoIme = new System.Windows.Forms.Label();
            this.lblUnesiteLozinku = new System.Windows.Forms.Label();
            this.lblPrijavaNeuspjesna = new System.Windows.Forms.Label();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pbLoader = new System.Windows.Forms.PictureBox();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // tbKorisnickoIme
            // 
            this.tbKorisnickoIme.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKorisnickoIme.Location = new System.Drawing.Point(414, 131);
            this.tbKorisnickoIme.MaxLength = 255;
            this.tbKorisnickoIme.Name = "tbKorisnickoIme";
            this.tbKorisnickoIme.Size = new System.Drawing.Size(326, 37);
            this.tbKorisnickoIme.TabIndex = 0;
            this.tbKorisnickoIme.Enter += new System.EventHandler(this.TbKorisnickoIme_Enter);
            this.tbKorisnickoIme.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbKorisnickoIme_KeyDown);
            this.tbKorisnickoIme.Leave += new System.EventHandler(this.TbKorisnickoIme_Leave);
            // 
            // tbLozinka
            // 
            this.tbLozinka.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLozinka.Location = new System.Drawing.Point(414, 233);
            this.tbLozinka.MaxLength = 255;
            this.tbLozinka.Name = "tbLozinka";
            this.tbLozinka.PasswordChar = '*';
            this.tbLozinka.Size = new System.Drawing.Size(326, 37);
            this.tbLozinka.TabIndex = 1;
            this.tbLozinka.UseSystemPasswordChar = true;
            this.tbLozinka.Enter += new System.EventHandler(this.TbLozinka_Enter);
            this.tbLozinka.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbLozinka_KeyDown);
            this.tbLozinka.Leave += new System.EventHandler(this.TbLozinka_Leave);
            // 
            // lblKorisnickoIme
            // 
            this.lblKorisnickoIme.AutoSize = true;
            this.lblKorisnickoIme.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKorisnickoIme.Location = new System.Drawing.Point(410, 104);
            this.lblKorisnickoIme.Name = "lblKorisnickoIme";
            this.lblKorisnickoIme.Size = new System.Drawing.Size(152, 24);
            this.lblKorisnickoIme.TabIndex = 13;
            this.lblKorisnickoIme.Text = "Korisničko ime";
            // 
            // lblLozinka
            // 
            this.lblLozinka.AutoSize = true;
            this.lblLozinka.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLozinka.Location = new System.Drawing.Point(410, 206);
            this.lblLozinka.Name = "lblLozinka";
            this.lblLozinka.Size = new System.Drawing.Size(84, 24);
            this.lblLozinka.TabIndex = 14;
            this.lblLozinka.Text = "Lozinka";
            // 
            // btnPrijaviSe
            // 
            this.btnPrijaviSe.FlatAppearance.BorderSize = 2;
            this.btnPrijaviSe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrijaviSe.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrijaviSe.Location = new System.Drawing.Point(414, 315);
            this.btnPrijaviSe.Name = "btnPrijaviSe";
            this.btnPrijaviSe.Size = new System.Drawing.Size(326, 55);
            this.btnPrijaviSe.TabIndex = 2;
            this.btnPrijaviSe.Text = "&Prijavi se";
            this.btnPrijaviSe.UseVisualStyleBackColor = true;
            this.btnPrijaviSe.Click += new System.EventHandler(this.BtnPrijaviSe_Click);
            // 
            // lblUnesiteKorisnickoIme
            // 
            this.lblUnesiteKorisnickoIme.AutoSize = true;
            this.lblUnesiteKorisnickoIme.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnesiteKorisnickoIme.ForeColor = System.Drawing.Color.Red;
            this.lblUnesiteKorisnickoIme.Location = new System.Drawing.Point(410, 169);
            this.lblUnesiteKorisnickoIme.Name = "lblUnesiteKorisnickoIme";
            this.lblUnesiteKorisnickoIme.Size = new System.Drawing.Size(176, 21);
            this.lblUnesiteKorisnickoIme.TabIndex = 16;
            this.lblUnesiteKorisnickoIme.Text = "Unesite korisničko ime";
            this.lblUnesiteKorisnickoIme.Visible = false;
            // 
            // lblUnesiteLozinku
            // 
            this.lblUnesiteLozinku.AutoSize = true;
            this.lblUnesiteLozinku.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnesiteLozinku.ForeColor = System.Drawing.Color.Red;
            this.lblUnesiteLozinku.Location = new System.Drawing.Point(410, 271);
            this.lblUnesiteLozinku.Name = "lblUnesiteLozinku";
            this.lblUnesiteLozinku.Size = new System.Drawing.Size(122, 21);
            this.lblUnesiteLozinku.TabIndex = 17;
            this.lblUnesiteLozinku.Text = "Unesite lozinku";
            this.lblUnesiteLozinku.Visible = false;
            // 
            // lblPrijavaNeuspjesna
            // 
            this.lblPrijavaNeuspjesna.AutoSize = true;
            this.lblPrijavaNeuspjesna.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrijavaNeuspjesna.ForeColor = System.Drawing.Color.Red;
            this.lblPrijavaNeuspjesna.Location = new System.Drawing.Point(410, 371);
            this.lblPrijavaNeuspjesna.Name = "lblPrijavaNeuspjesna";
            this.lblPrijavaNeuspjesna.Size = new System.Drawing.Size(150, 21);
            this.lblPrijavaNeuspjesna.TabIndex = 18;
            this.lblPrijavaNeuspjesna.Text = "Prijava nije uspjela";
            this.lblPrijavaNeuspjesna.Visible = false;
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackgroundImage = global::VideoKlub.Properties.Resources.minimize_button;
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Location = new System.Drawing.Point(690, 3);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(50, 50);
            this.btnMinimize.TabIndex = 9;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::VideoKlub.Properties.Resources.close_button;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(746, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(50, 50);
            this.btnClose.TabIndex = 8;
            this.btnClose.TabStop = false;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // pbLoader
            // 
            this.pbLoader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbLoader.Image = global::VideoKlub.Properties.Resources.loader;
            this.pbLoader.Location = new System.Drawing.Point(747, 396);
            this.pbLoader.Name = "pbLoader";
            this.pbLoader.Size = new System.Drawing.Size(49, 50);
            this.pbLoader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLoader.TabIndex = 19;
            this.pbLoader.TabStop = false;
            this.pbLoader.Visible = false;
            // 
            // pbLogo
            // 
            this.pbLogo.BackgroundImage = global::VideoKlub.Properties.Resources.logo;
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbLogo.Location = new System.Drawing.Point(54, 70);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(300, 300);
            this.pbLogo.TabIndex = 10;
            this.pbLogo.TabStop = false;
            // 
            // PrijavaZaposleni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pbLoader);
            this.Controls.Add(this.lblPrijavaNeuspjesna);
            this.Controls.Add(this.lblUnesiteLozinku);
            this.Controls.Add(this.lblUnesiteKorisnickoIme);
            this.Controls.Add(this.btnPrijaviSe);
            this.Controls.Add(this.lblLozinka);
            this.Controls.Add(this.lblKorisnickoIme);
            this.Controls.Add(this.tbLozinka);
            this.Controls.Add(this.tbKorisnickoIme);
            this.Controls.Add(this.pbLogo);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrijavaZaposleni";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrijavaZaposleni";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PrijavaZaposleni_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbLoader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.TextBox tbKorisnickoIme;
        private System.Windows.Forms.TextBox tbLozinka;
        private System.Windows.Forms.Label lblKorisnickoIme;
        private System.Windows.Forms.Label lblLozinka;
        private System.Windows.Forms.Button btnPrijaviSe;
        private System.Windows.Forms.Label lblUnesiteKorisnickoIme;
        private System.Windows.Forms.Label lblUnesiteLozinku;
        private System.Windows.Forms.Label lblPrijavaNeuspjesna;
        private System.Windows.Forms.PictureBox pbLoader;
    }
}