﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VideoKlub.Util
{
    class ZaduzenjaUtil
    {

        public List<zaduzenje_filma> GetPrvih(int brojZaduzenja)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<zaduzenje_filma> zaduzenja = ctx.zaduzenje_filma.OrderByDescending(z => z.datum_zaduzenja).Take(brojZaduzenja).ToList();
                return zaduzenja;
            }
        }
        public List<zaduzenje_filma> GetPrvihZaKorisnika(int brojZaduzenja, string brojKartice)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                int idKorisnika = ctx.korisnik.Where(k => k.broj_kartice == brojKartice).First().id;
                List<zaduzenje_filma> zaduzenja = ctx.zaduzenje_filma.Where(zf => zf.korisnik_id == idKorisnika).
                        OrderByDescending(z => z.datum_zaduzenja).Take(brojZaduzenja).ToList();
                return zaduzenja;
            }
        }
        public bool IsKarticaAktivna(string brojKartice)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.korisnik.Where(k => k.broj_kartice == brojKartice).First().kartica_aktivna != 0;
            }
        }
        public string GetBrojKartice(int idKorisnika)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.korisnik.Where(k => k.id == idKorisnika).First().broj_kartice;
            }
        }
        public List<string> GetBrojeveKartica()
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.korisnik.Select(k => k.broj_kartice).ToList();
            }
        }
        public string GetNaslovFilma(int idFilma)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                string naslovFilma = ctx.film.Where(f => f.id == idFilma).First().naslov;

                return naslovFilma + " (" + ctx.film.Where(f => f.id == idFilma).First().godina + ")";
            }
        }
        public List<string> GetNasloveFilmova() // format - Naslov (godina) // samo filmovi kojie postoje na stanju i aktivni
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<int> idFilmovaNaStanju = ctx.film_u_magacinu.Where(fum => fum.na_stanju > 0 && fum.aktivan != 0).Select(fum => fum.film_id).ToList();
                return ctx.film.Where(f => idFilmovaNaStanju.Contains(f.id)).OrderBy(f => f.naslov).ThenBy(f => f.godina).Select(f => f.naslov + " (" + f.godina + ")").ToList();
            }
        }

        public List<string> GetZaduzeneFilmoveKorisnika(string brojKartice)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<zaduzenje_filma> zaduzenja = ctx.zaduzenje_filma.Join(ctx.korisnik, z => z.korisnik_id, k => k.id,
                    (z, k) => new { Zaduzenje = z, Korisnik = k }).Where(z_k => z_k.Korisnik.broj_kartice == brojKartice).Select(z_k => z_k.Zaduzenje).
                    Where(z => z.datum_razduzenja == null).ToList();

                if (zaduzenja.Count == 0)
                {
                    return null;
                }

                List<string> naslovi = new List<string>();

                foreach (zaduzenje_filma zaduzenje in zaduzenja)
                {
                    film film = ctx.film.Where(f => f.id == zaduzenje.film_u_magacinu_film_id).First();

                    naslovi.Add(film.naslov + " (" + film.godina + ")");
                }

                return naslovi;
            }
        }

        public zaduzenje_filma Razduzi(DateTime datumZaduzenja, int idKorisnik, int idFilmUMagacinu)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                zaduzenje_filma zaduzenje = ctx.zaduzenje_filma.Where(z => z.datum_zaduzenja.CompareTo(datumZaduzenja) == 0 &&
                    z.korisnik_id == idKorisnik && z.film_u_magacinu_film_id == idFilmUMagacinu).First();

                if (zaduzenje.datum_razduzenja == null)
                {
                    zaduzenje.datum_razduzenja = DateTime.Now;
                }

                ctx.film_u_magacinu.Where(fum => fum.film_id == zaduzenje.film_u_magacinu_film_id).First().na_stanju++;

                ctx.SaveChanges();

                return zaduzenje;
            }
        }
        public void Zaduzi(string brojKartice, string naslovFilma, int idZaposlenog)
        {
            string naslov = naslovFilma.Split('(')[0].Trim();
            int godina = int.Parse(naslovFilma.Split('(')[1].Split(')')[0].Trim());
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                zaduzenje_filma novoZaduzenje = new zaduzenje_filma
                {
                    zaposleni_id = idZaposlenog,
                    korisnik_id = ctx.korisnik.Where(k => k.broj_kartice == brojKartice).First().id,
                    film_u_magacinu_film_id = ctx.film.Where(f => f.naslov == naslov && f.godina == godina).First().id,
                    datum_zaduzenja = DateTime.Now,
                    datum_razduzenja = null,
                    ocjena_filma = null
                };

                novoZaduzenje.korisnik = ctx.korisnik.Where(k => k.broj_kartice == brojKartice).First();
                novoZaduzenje.zaposleni = ctx.zaposleni.Where(z => z.id == idZaposlenog).First();
                novoZaduzenje.film_u_magacinu = ctx.film_u_magacinu.Where(fum => fum.film_id == novoZaduzenje.film_u_magacinu_film_id).First();

                ctx.korisnik.Where(k => k.broj_kartice == brojKartice).First().zaduzenje_filma.Add(novoZaduzenje);
                ctx.zaposleni.Where(z => z.id == idZaposlenog).First().zaduzenje_filma.Add(novoZaduzenje);
                ctx.film_u_magacinu.Where(fum => fum.film_id == novoZaduzenje.film_u_magacinu_film_id).First().zaduzenje_filma.Add(novoZaduzenje);

                ctx.film_u_magacinu.Where(fum => fum.film_id == novoZaduzenje.film_u_magacinu_film_id).First().na_stanju--;

                ctx.zaduzenje_filma.Add(novoZaduzenje);
                ctx.SaveChanges();
            }
        }
        public void OcijeniFilm(int idFilma, DateTime datumZaduzenja, int ocjena, string brojKartice)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                int idKorisnika = ctx.korisnik.Where(k => k.broj_kartice == brojKartice).First().id;

                ctx.zaduzenje_filma.Where(zf => zf.film_u_magacinu_film_id == idFilma && zf.korisnik_id == idKorisnika &&
                    zf.datum_zaduzenja.CompareTo(datumZaduzenja) == 0).First().ocjena_filma = ocjena;

                ctx.SaveChanges();
            }
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                ctx.film_u_magacinu.Where(fum => fum.film_id == idFilma).First().ocjena =
                    (decimal)ctx.zaduzenje_filma.Where(zf => zf.film_u_magacinu_film_id == idFilma && zf.ocjena_filma != 0.0).Select(zf => zf.ocjena_filma).Average();

                ctx.SaveChanges();
            }
            
        }

        public List<zaduzenje_filma> Pretrazi(string brojKartice, string naslov, bool nerazduzeno)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                //samo ona zaduzenja kod kojih broj kartice korisnika koji je zaduzio film pocinje sa brojKartice
                List<zaduzenje_filma> zaduzenja = ctx.zaduzenje_filma.Join(ctx.korisnik, z => z.korisnik_id, k => k.id,
                    (z, k) => new { Zaduzenje = z, Korisnik = k }).Where(z_k => z_k.Korisnik.broj_kartice.StartsWith(brojKartice)).Select(z_k => z_k.Zaduzenje)
                    .OrderByDescending(z => z.datum_zaduzenja).ToList();

                if(zaduzenja.Count == 0)
                {
                    return zaduzenja;
                }

                if (naslov.Length >= 3)
                {
                    zaduzenja = zaduzenja.Join(ctx.film, z => z.film_u_magacinu_film_id, f => f.id,
                        (z, f) => new { Zaduzenje = z, Film = f }).Where(z_f => z_f.Film.naslov.ToLower().Contains(naslov.ToLower())).Select(z_f => z_f.Zaduzenje).ToList();
                }
                else
                {
                    zaduzenja = zaduzenja.Join(ctx.film, z => z.film_u_magacinu_film_id, f => f.id,
                        (z, f) => new { Zaduzenje = z, Film = f }).Where(z_f => z_f.Film.naslov.ToLower().StartsWith(naslov.ToLower())).Select(z_f => z_f.Zaduzenje).ToList();
                }

                if(zaduzenja.Count == 0)
                {
                    return zaduzenja;
                }
                if (nerazduzeno)
                {
                    return zaduzenja.Where(z => z.datum_razduzenja == null).ToList();
                }

                return zaduzenja.OrderByDescending(z => z.datum_zaduzenja).ToList();

            }
        }
        public List<zaduzenje_filma> GetSljedecihUzPretragu(string brojKartice, string naslov, bool nerazduzeno, int brojZaduzenja, int offset)
        {
            List<zaduzenje_filma> zaduzenja = Pretrazi(brojKartice, naslov, nerazduzeno);

            return zaduzenja.Skip(offset).Take(brojZaduzenja).ToList();
        }
    }
}
