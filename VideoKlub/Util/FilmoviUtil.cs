﻿using System.Collections.Generic;
using System.Linq;

namespace VideoKlub.Util
{
    class FilmoviUtil
    {
        #region Zanrovi
        public zanr GetZanr(int idZanra)
        {
            using(VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.zanr.Where(z => z.id == idZanra).First();
            }
        }
        public zanr GetZanr(string kategorija, string nazivZanra)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                int idKategorije = ctx.kategorija_zanra.Where(k => k.opis == kategorija).First().id;
                return ctx.zanr.Where(z => z.kategorija_zanra_id == idKategorije && z.naziv == nazivZanra).First();
            }
        }
        public List<string> GetZanrove(int idFilma)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.zanr.Where(z => z.film.Select(f => f.id).Contains(idFilma)).Select(z => z.naziv).ToList();
            }
        }
        public List<kategorija_zanra> GetKategorijeZanra()
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<kategorija_zanra> kategorijeZanra = ctx.kategorija_zanra.OrderBy(k => k.opis).ToList();

                // kategorija "Ostalo" da bude poslednja
                if (kategorijeZanra != null && kategorijeZanra.Where(k => k.opis == "Ostalo").Count() != 0)
                {
                    kategorija_zanra kategorijaOstalo = kategorijeZanra.Where(k => k.opis == "Ostalo").First();
                    kategorijeZanra.Remove(kategorijaOstalo);
                    kategorijeZanra.Add(kategorijaOstalo);
                }

                return kategorijeZanra;
            }
        }
        public List<zanr> GetZanroveIzKategorije(int idKategorije)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.zanr.Where(z => z.kategorija_zanra_id == idKategorije).OrderBy(z => z.naziv).ToList();
            }
        }
        public List<zanr> GetZanroveIzKategorijeUzPretragu(int idKategorije, string naziv)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.zanr.Where(z => z.kategorija_zanra_id == idKategorije && z.naziv.StartsWith(naziv)).OrderBy(z => z.naziv).ToList();
            }
        }
        public bool IsZanrPostojeci(string kategorija, string nazivZanra)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                var kategorijaZanr = ctx.zanr.Join(ctx.kategorija_zanra, z => z.kategorija_zanra_id, k => k.id,
                    (z, k) => new { Zanr = z, Kategorija = k }).Where(z_k => z_k.Zanr.naziv == nazivZanra && z_k.Kategorija.opis == kategorija).ToList();

                return kategorijaZanr.Count != 0;
            }
        }
        public bool IsZanrKoristen(string kategorija, string nazivZanra) // da li je koristen u filmovima
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                int idKategorije = ctx.kategorija_zanra.Where(k => k.opis == kategorija).First().id;

                int idZanra = GetZanroveIzKategorije(idKategorije).Where(z => z.naziv == nazivZanra).First().id;

                return ctx.zanr.Where(z => z.id == idZanra).First().film.Count != 0;
            }
        }
        public void DodajZanr(string kategorija, string nazivZanra)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                int idKategorije = ctx.kategorija_zanra.Where(k => k.opis == kategorija).First().id;

                zanr zanr = new zanr
                {
                    naziv = nazivZanra,
                    kategorija_zanra_id = idKategorije
                };

                ctx.zanr.Add(zanr);
                ctx.SaveChanges();
            }
        }
        public void UkloniZanr(string kategorija, string nazivZanra)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                int idKategorije = ctx.kategorija_zanra.Where(k => k.opis == kategorija).First().id;

                zanr zanr = ctx.zanr.Where(z => z.kategorija_zanra_id == idKategorije).Where(z => z.naziv == nazivZanra).First();

                ctx.zanr.Remove(zanr);
                ctx.SaveChanges();
            }
        }
        #endregion

        #region Glumci
        public glumac GetGlumac(int id)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.glumac.Where(g => g.id == id).First();
            }
        }
        public List<glumac> GetGlumci()
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.glumac.OrderBy(g => g.prezime).ThenBy(g => g.ime).ToList();
            }
        }
        public List<string> GetGlumci(int idFilma)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.glumac.Where(g => g.film.Select(f => f.id).Contains(idFilma)).Select(g => g.prezime + " " + g.ime).ToList();
            }
        }
        public List<glumac> GetGlumciUzPretragu(string prezime, string ime)
        {
            using(VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.glumac.Where(g => g.prezime.StartsWith(prezime) && g.ime.StartsWith(ime)).OrderBy(g => g.prezime).ThenBy(g => g.ime).ToList();
            }
        }
        public bool IsGlumacKoristen(int idGlumca) // da li je koristen u filmovima
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.glumac.Where(g => g.id == idGlumca).First().film.Count != 0;
            }
        }
        public int DodajGlumca(string ime, string prezime)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                glumac glumac = new glumac
                {
                    ime = ime,
                    prezime = prezime
                };

                ctx.glumac.Add(glumac);
                ctx.SaveChanges();

                int id = ctx.glumac.Where(g => g.ime == ime && g.prezime == prezime).First().id;
                return id;
            }
        }
        public void UkloniGlumca(int idGlumca)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                glumac glumac = ctx.glumac.Where(g => g.id == idGlumca).First();

                ctx.glumac.Remove(glumac);
                ctx.SaveChanges();
            }
        }
        #endregion

        #region Filmovi
        public string GetNaslovFilma(int idFilma)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.film.Where(f => f.id == idFilma).First().naslov;
            }
        }
        public int GetIdFilma(string naslov, int godina)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.film.Where(f => f.naslov == naslov && f.godina == godina).First().id;
            }
        }
        public void GetInfoOFilmu(int idFilma, out string naslov, out int godina, out string slika, out List<int> zanrovi, out List<int> glumci, out decimal cijena, out int naStanju)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                film film = ctx.film.Where(f => f.id == idFilma).First();

                naslov = film.naslov;
                godina = film.godina;
                slika = film.slika;

                film_u_magacinu filmUMagacinu = ctx.film_u_magacinu.Where(fum => fum.film_id == idFilma).First();

                cijena = (decimal)filmUMagacinu.cijena;
                naStanju = (int)filmUMagacinu.na_stanju;

                zanrovi = ctx.zanr.Where(z => z.film.Select(f => f.id).ToList().Contains(idFilma)).Select(z => z.id).ToList();
                glumci = ctx.glumac.Where(g => g.film.Select(f => f.id).ToList().Contains(idFilma)).Select(g => g.id).ToList();
            }
        }
        public List<film> GetPrvihFilmova(int brojFilmova)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<film> filmovi = ctx.film.OrderBy(f => f.naslov).ThenBy(f => f.godina).Take(brojFilmova).ToList();
                return filmovi;
            }
        }
        public List<film> GetNakonNaslovaPrvihFilmova(string naslov, int brojFilmova)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<film> filmovi = ctx.film.Where(f => f.naslov.CompareTo(naslov) >= 0).
                        OrderBy(f => f.naslov).ThenBy(f => f.godina).Take(brojFilmova).ToList();
                return filmovi;
            }
        }
        public List<film> GetZadnjihFilmova(int brojFilmova)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<film> filmovi = ctx.film.OrderByDescending(f => f.naslov).ThenByDescending(f => f.godina).Take(brojFilmova).
                        OrderBy(f => f.naslov).ThenBy(f => f.godina).ToList();
                return filmovi;
            }
        }

        public List<film> GetSljedecihUzPretragu(string naslov, string godina, bool aktivniFilmovi, int brojFilmova, int offset,
            List<zanr> zanrovi = null, string tipPretrageZanrova = null, List<glumac> glumci = null, string tipPretrageGlumaca = null)
        {
            List<film> filmovi = PretraziFilmove(naslov, godina, aktivniFilmovi, zanrovi, tipPretrageZanrova, glumci, tipPretrageGlumaca);

            return filmovi.Skip(offset).Take(brojFilmova).ToList();
        }
        public List<film> GetSljedecihUzPretraguZaKatalog(string naslov, string poredajPo, int godinaOd, int godinaDo, decimal cijenaOd, decimal cijenaDo,
            decimal ocjenaOd, decimal ocjenaDo, int brojFilmova, int offset, List<zanr> zanrovi = null, string tipPretrageZanrova = null,
            List<glumac> glumci = null, string tipPretrageGlumaca = null)
        {
            List<film> filmovi = PretraziFilmoveZaKatalog(naslov, poredajPo, godinaOd, godinaDo, cijenaOd, cijenaDo, ocjenaOd, ocjenaDo,
                zanrovi, tipPretrageZanrova, glumci, tipPretrageGlumaca);

            return filmovi.Skip(offset).Take(brojFilmova).ToList();
        }

        public sbyte GetIsAktivan(string naslov, int godina)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                int idFilma = ctx.film.Where(f => f.naslov == naslov && f.godina == godina).First().id;

                return ctx.film_u_magacinu.Where(fum => fum.film_id == idFilma).First().aktivan;
            }
        }
        public int GetNaStanju(string naslov, int godina)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                int idFilma = ctx.film.Where(f => f.naslov == naslov && f.godina == godina).First().id;

                return (int)ctx.film_u_magacinu.Where(fum => fum.film_id == idFilma).First().na_stanju;
            }
        }
        public decimal GetCijena(string naslov, int godina)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                int idFilma = ctx.film.Where(f => f.naslov == naslov && f.godina == godina).First().id;

                return (decimal)ctx.film_u_magacinu.Where(fum => fum.film_id == idFilma).First().cijena;
            }
        }
        public decimal GetOcjena(int idFilma)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return (decimal)ctx.film_u_magacinu.Where(fum => fum.film_id == idFilma).First().ocjena;
            }
        }
        public int GetBrojIzdavanja(int idFilma)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.zaduzenje_filma.Where(zf => zf.film_u_magacinu_film_id == idFilma).Count();
            }
        }

        public bool IsFilmPostojeci(string naslov, int godina)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<film> filmovi = ctx.film.Where(f => f.naslov == naslov && f.godina == godina).ToList();

                if (filmovi != null && filmovi.Count > 0)
                {
                    return true;
                }

                return false;
            }
        }
        public bool IsAktivan(int idFilma)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.film_u_magacinu.Where(fum => fum.film_id == idFilma).First().aktivan != 0;
            }
        }
        public bool IsZaduzen(int idFilma)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                if (ctx.zaduzenje_filma.Where(zf => zf.film_u_magacinu_film_id == idFilma && zf.datum_razduzenja == null).ToList().Count == 0)
                {
                    return false;
                }
                return true;
            }
        }

        public List<film> PretraziFilmove(string naslov, string godina, bool aktivniFilmovi, List<zanr> zanrovi = null, string tipPretrageZanrova = null, 
            List<glumac> glumci = null, string tipPretrageGlumaca = null)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<film> filmovi = null;
                if (aktivniFilmovi)
                {
                    if (naslov.Length >= 3)
                    {
                        List<int> aktivniFilmoviIds = ctx.film_u_magacinu.Where(fum => fum.aktivan != 0).Select(fum => fum.film_id).ToList();
                        filmovi = ctx.film.Where(f => aktivniFilmoviIds.Contains(f.id) && f.naslov.ToLower().Contains(naslov.ToLower()) && f.godina.ToString().StartsWith(godina)).
                            OrderBy(f => f.naslov).ThenBy(f => f.godina).ToList();
                    }
                    else
                    {
                        List<int> aktivniFilmoviIds = ctx.film_u_magacinu.Where(fum => fum.aktivan != 0).Select(fum => fum.film_id).ToList();
                        filmovi = ctx.film.Where(f => aktivniFilmoviIds.Contains(f.id) && f.naslov.ToLower().StartsWith(naslov.ToLower()) && f.godina.ToString().StartsWith(godina)).
                            OrderBy(f => f.naslov).ThenBy(f => f.godina).ToList();
                    }
                }
                else
                {
                    if (naslov.Length >= 3)
                    {
                        filmovi = ctx.film.Where(f => f.naslov.ToLower().Contains(naslov.ToLower()) && f.godina.ToString().StartsWith(godina)).
                            OrderBy(f => f.naslov).ThenBy(f => f.godina).ToList();
                    }
                    else
                    {
                        filmovi = ctx.film.Where(f => f.naslov.ToLower().StartsWith(naslov.ToLower()) && f.godina.ToString().StartsWith(godina)).
                            OrderBy(f => f.naslov).ThenBy(f => f.godina).ToList();
                    }
                }

                List<film> zaUklanjanje = new List<film>();

                if (zanrovi != null)
                {
                    foreach (film film in filmovi)
                    {
                        if (tipPretrageZanrova.StartsWith("Ili"))
                        {
                            if (!zanrovi.Any(z => film.zanr.Select(z1 => z1.id).ToList().Contains(z.id)))
                            {
                                if (!zaUklanjanje.Contains(film))
                                {
                                    zaUklanjanje.Add(film);
                                }
                            }
                        }
                        else if (tipPretrageZanrova.StartsWith("I"))
                        {
                            if (!zanrovi.All(z => film.zanr.Select(z1 => z1.id).ToList().Contains(z.id)))
                            {
                                if (!zaUklanjanje.Contains(film))
                                {
                                    zaUklanjanje.Add(film);
                                }
                            }
                        }
                    }
                }
                if (glumci != null)
                {
                    foreach (film film in filmovi)
                    {
                        if (tipPretrageGlumaca.StartsWith("Ili"))
                        {
                            if (!glumci.Any(g => film.glumac.Select(g1 => g1.id).ToList().Contains(g.id)))
                            {
                                if (!zaUklanjanje.Contains(film))
                                {
                                    zaUklanjanje.Add(film);
                                }
                            }
                        }
                        else if (tipPretrageGlumaca.StartsWith("I"))
                        {
                            if (!glumci.All(g => film.glumac.Select(g1 => g1.id).ToList().Contains(g.id)))
                            {
                                if (!zaUklanjanje.Contains(film))
                                {
                                    zaUklanjanje.Add(film);
                                }
                            }
                        }
                    }
                }

                foreach (film film in zaUklanjanje)
                {
                    filmovi.Remove(film);
                }
                return filmovi;
            }
        }

        public List<film> PretraziFilmoveZaKatalog(string naslov, string poredajPo, int godinaOd, int godinaDo, decimal cijenaOd, decimal cijenaDo, 
            decimal ocjenaOd, decimal ocjenaDo, List<zanr> zanrovi = null, string tipPretrageZanrova = null,
            List<glumac> glumci = null, string tipPretrageGlumaca = null)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<film> filmovi = null;

                if (naslov.Length >= 3)
                {
                    filmovi = ctx.film.Where(f => f.naslov.ToLower().Contains(naslov.ToLower()) && f.godina >= godinaOd && f.godina <= godinaDo).
                     OrderBy(f => f.naslov).ToList();
                }
                else
                {
                    filmovi = ctx.film.Where(f => f.naslov.ToLower().StartsWith(naslov.ToLower()) && f.godina >= godinaOd && f.godina <= godinaDo).
                     OrderBy(f => f.naslov).ToList();
                }

                filmovi = filmovi.Where(f => ctx.film_u_magacinu.Where(fum => fum.film_id == f.id).First().aktivan != 0 &&
                    ctx.film_u_magacinu.Where(fum => fum.film_id == f.id).First().cijena >= cijenaOd &&
                    ctx.film_u_magacinu.Where(fum => fum.film_id == f.id).First().ocjena >= ocjenaOd &&
                    ctx.film_u_magacinu.Where(fum => fum.film_id == f.id).First().cijena <= cijenaDo &&
                    ctx.film_u_magacinu.Where(fum => fum.film_id == f.id).First().ocjena <= ocjenaDo).ToList();

                List<film> zaUklanjanje = new List<film>();

                if (zanrovi != null)
                {
                    foreach (film film in filmovi)
                    {
                        if (tipPretrageZanrova.StartsWith("Ili"))
                        {
                            if (!zanrovi.Any(z => film.zanr.Select(z1 => z1.id).ToList().Contains(z.id)))
                            {
                                if (!zaUklanjanje.Contains(film))
                                {
                                    zaUklanjanje.Add(film);
                                }
                            }
                        }
                        else if (tipPretrageZanrova.StartsWith("I"))
                        {
                            if (!zanrovi.All(z => film.zanr.Select(z1 => z1.id).ToList().Contains(z.id)))
                            {
                                if (!zaUklanjanje.Contains(film))
                                {
                                    zaUklanjanje.Add(film);
                                }
                            }
                        }
                    }
                }
                if (glumci != null)
                {
                    foreach (film film in filmovi)
                    {
                        if (tipPretrageGlumaca.StartsWith("Ili"))
                        {
                            if (!glumci.Any(g => film.glumac.Select(g1 => g1.id).ToList().Contains(g.id)))
                            {
                                if (!zaUklanjanje.Contains(film))
                                {
                                    zaUklanjanje.Add(film);
                                }
                            }
                        }
                        else if (tipPretrageGlumaca.StartsWith("I"))
                        {
                            if (!glumci.All(g => film.glumac.Select(g1 => g1.id).ToList().Contains(g.id)))
                            {
                                if (!zaUklanjanje.Contains(film))
                                {
                                    zaUklanjanje.Add(film);
                                }
                            }
                        }
                    }
                }

                foreach (film film in zaUklanjanje)
                {
                    filmovi.Remove(film);
                }

                switch (poredajPo)
                {
                    case "Naslovu": filmovi = filmovi.OrderBy(f => f.naslov).ThenBy(f => f.godina).ToList(); break;
                    case "Najnoviji filmovi": filmovi = filmovi.OrderByDescending(f => f.godina).ThenBy(f => f.naslov).ToList(); break;
                    case "Najstariji filmovi": filmovi = filmovi.OrderBy(f => f.godina).ThenBy(f => f.naslov).ToList(); break;
                    case "Cijeni - najnižoj":
                        filmovi = filmovi.Join(ctx.film_u_magacinu, f => f.id, fum => fum.film_id,
                        (f, fum) => new { Film = f, FilmUMagacinu = fum }).OrderBy(f_fum => f_fum.FilmUMagacinu.cijena).ThenBy(f_fum => f_fum.Film.naslov).
                        ThenBy(f_fum => f_fum.Film.godina).Select(f_fum => f_fum.Film).ToList(); break;
                    case "Cijeni - najvišoj":
                        filmovi = filmovi.Join(ctx.film_u_magacinu, f => f.id, fum => fum.film_id,
                        (f, fum) => new { Film = f, FilmUMagacinu = fum }).OrderByDescending(f_fum => f_fum.FilmUMagacinu.cijena).ThenBy(f_fum => f_fum.Film.naslov).
                        ThenBy(f_fum => f_fum.Film.godina).Select(f_fum => f_fum.Film).ToList(); break;
                    case "Ocjeni - najnižoj":
                        filmovi = filmovi.Join(ctx.film_u_magacinu, f => f.id, fum => fum.film_id,
                        (f, fum) => new { Film = f, FilmUMagacinu = fum }).OrderBy(f_fum => f_fum.FilmUMagacinu.ocjena).ThenBy(f_fum => f_fum.Film.naslov).
                        ThenBy(f_fum => f_fum.Film.godina).Select(f_fum => f_fum.Film).ToList(); break;
                    case "Ocjeni - najvišoj":
                        filmovi = filmovi.Join(ctx.film_u_magacinu, f => f.id, fum => fum.film_id,
                        (f, fum) => new { Film = f, FilmUMagacinu = fum }).OrderByDescending(f_fum => f_fum.FilmUMagacinu.ocjena).ThenBy(f_fum => f_fum.Film.naslov).
                        ThenBy(f_fum => f_fum.Film.godina).Select(f_fum => f_fum.Film).ToList(); break;
                }
                return filmovi;
            }
        }

        public int DodajFilm(string naslov, int godina, string slika, List<zanr> zanrovi, List<glumac> glumci, decimal cijena, int naStanju)
        {
            film film = new film
            {
                naslov = naslov,
                godina = godina,
                slika = slika,
            };
            
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                ctx.film.Add(film);
                ctx.SaveChanges();
            }

            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                film dodaniFilm = ctx.film.Where(f => f.naslov == naslov && f.godina == godina).First();

                film_u_magacinu filmUMagacinu = new film_u_magacinu
                {
                    film_id = dodaniFilm.id,
                    cijena = cijena,
                    na_stanju = naStanju,
                    ocjena = 0.0m,
                    aktivan  = 1,
                    film = dodaniFilm
                };

                dodaniFilm.film_u_magacinu = filmUMagacinu;

                ctx.film_u_magacinu.Add(filmUMagacinu);

                foreach (zanr zanr in zanrovi)
                {
                    zanr zanrFilma = ctx.zanr.Where(z => z.id == zanr.id).First();

                    dodaniFilm.zanr.Add(zanrFilma);

                    zanrFilma.film.Add(dodaniFilm);
                }
                foreach (glumac glumac in glumci)
                {
                    glumac glumacUFilmu = ctx.glumac.Where(g => g.id == glumac.id).First();
                    dodaniFilm.glumac.Add(glumacUFilmu);

                    glumacUFilmu.film.Add(dodaniFilm);
                }

                ctx.SaveChanges();

                return dodaniFilm.id;
            }
        }
        public void IzmijeniFilm(int idFilma, string slika, List<zanr> zanrovi, List<glumac> glumci, decimal cijena, int naStanju)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                film film = ctx.film.Where(f => f.id == idFilma).First();

                film_u_magacinu filmUMagacinu = ctx.film_u_magacinu.Where(fum => fum.film_id == idFilma).First();

                filmUMagacinu.cijena = cijena;
                filmUMagacinu.na_stanju = naStanju;

                List<zanr> stariZanrovi = ctx.zanr.Where(z => z.film.Select(f => f.id).ToList().Contains(idFilma)).ToList();

                foreach (zanr stariZanr in stariZanrovi)
                {
                    stariZanr.film.Remove(film);
                }

                List<glumac> stariGlumci = ctx.glumac.Where(g => g.film.Select(f => f.id).ToList().Contains(idFilma)).ToList();

                foreach (glumac stariGlumac in stariGlumci)
                {
                    stariGlumac.film.Remove(film);
                }

                film.slika = slika;
                film.zanr.Clear();
                film.glumac.Clear();

                if (zanrovi != null)
                {
                    foreach (zanr zanr in zanrovi)
                    {
                        zanr zanrFilma = ctx.zanr.Where(z => z.id == zanr.id).First();

                        film.zanr.Add(zanrFilma);

                        zanrFilma.film.Add(film);
                    }
                }
                if (glumci != null)
                {
                    foreach (glumac glumac in glumci)
                    {
                        glumac glumacUFilmu = ctx.glumac.Where(g => g.id == glumac.id).First();
                        film.glumac.Add(glumacUFilmu);

                        glumacUFilmu.film.Add(film);
                    }
                }
                ctx.SaveChanges();
            }
        }
        public film AktivirajFilm(int idFilma)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                film_u_magacinu filmUMagacinu = ctx.film_u_magacinu.Where(fum => fum.film_id == idFilma).First();
                if (filmUMagacinu.aktivan == 0)
                {
                    filmUMagacinu.aktivan = 1;
                }
                else
                {
                    filmUMagacinu.aktivan = 0;
                }

                film film = ctx.film.Where(f => f.id == idFilma).First();

                ctx.SaveChanges();

                return film;
            }
        }
        #endregion
    }
}