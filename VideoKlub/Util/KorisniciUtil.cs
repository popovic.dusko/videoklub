﻿using System.Collections.Generic;
using System.Linq;

namespace VideoKlub.Util
{
    class KorisniciUtil
    {
        public korisnik GetNaOsnovuId(int idKorisnika)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.korisnik.Where(k => k.id == idKorisnika).First();
            }
        }
        public int GetIdKorisnikaNaOsnovuJmbg(string jmbg)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.korisnik.Where(k => k.jmbg == jmbg).First().id;
            }
        }
        public int GetIdKorisnikaNaOsnovuBrojaKartice(string brojKartice)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.korisnik.Where(k => k.broj_kartice == brojKartice).First().id;
            }
        }
        public List<korisnik> GetPrvih(int brojKorisnika)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<korisnik> korisnici = ctx.korisnik.OrderBy(k => k.prezime).ThenBy(k => k.ime).Take(brojKorisnika).ToList();
                return korisnici;
            }
        }
        public List<korisnik> GetNakonPrezimenaPrvih(string prezime, int brojKorisnika)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<korisnik> korisnici = ctx.korisnik.Where(k => k.prezime.CompareTo(prezime) >= 0).
                        OrderBy(k => k.prezime).ThenBy(k => k.ime).Take(brojKorisnika).ToList();
                return korisnici;
            }
        }
        public List<korisnik> GetZadnjih(int brojKorisnika)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<korisnik> korisnici = ctx.korisnik.OrderByDescending(k => k.prezime).ThenByDescending(k => k.ime).Take(brojKorisnika).
                        OrderBy(k => k.prezime).ThenBy(k => k.ime).ToList();
                return korisnici;
            }
        }
        public List<korisnik> GetSljedecih(int brojKorisnika, int offset)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<korisnik> korisnici = ctx.korisnik.OrderBy(k => k.prezime).ThenBy(k => k.ime).Skip(offset).Take(brojKorisnika).ToList();
                return korisnici;
            }
        }
        public List<korisnik> GetSljedecihUzPretragu(string ime, string prezime, string jmbg, string brojKartice, bool aktivneKartice, int brojKorisnika, int offset)
        {
            List<korisnik> korisnici = Pretrazi(ime, prezime, jmbg, brojKartice, aktivneKartice);

            return korisnici.Skip(offset).Take(brojKorisnika).ToList();
        }
        public string GetPrezimeKorisnika(int idKorisnika)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.korisnik.Where(k => k.id == idKorisnika).ToList().First().prezime;
            }
        }

        public List<korisnik> Pretrazi(string ime, string prezime, string jmbg, string brojKartice, bool aktivneKartice)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<korisnik> korisnici = null;
                if (aktivneKartice)
                {
                    korisnici = ctx.korisnik.Where(k => k.prezime.StartsWith(prezime) && k.ime.StartsWith(ime) &&
                        k.jmbg.StartsWith(jmbg) && k.broj_kartice.StartsWith(brojKartice) && k.kartica_aktivna != 0).
                        OrderBy(k => k.prezime).ThenBy(k => k.ime).ToList();
                }
                else
                {
                    korisnici = ctx.korisnik.Where(k => k.prezime.StartsWith(prezime) && k.ime.StartsWith(ime) &&
                        k.jmbg.StartsWith(jmbg) && k.broj_kartice.StartsWith(brojKartice)).
                        OrderBy(k => k.prezime).ThenBy(k => k.ime).ToList();
                }
                return korisnici;
            }
        }

        public bool IsBrojKarticeJedinstven(string brojKartice)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                List<korisnik> kartice = ctx.korisnik.Where(k => k.broj_kartice.Equals(brojKartice)).ToList();
                if (kartice.Count == 0)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsJmbgJedinstven(string jmbg)
        {
            using(VideoKlubContext ctx = new VideoKlubContext())
            {
                List<korisnik> kartice = ctx.korisnik.Where(k => k.jmbg.Equals(jmbg)).ToList();
                if (kartice.Count == 0)
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsBrojKarticePostojeci(string brojKartice)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.korisnik.Where(k => k.broj_kartice == brojKartice).Count() != 0;
            }
        }
        public bool IsKarticaAktivna(string brojKartice)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                return ctx.korisnik.Where(k => k.broj_kartice == brojKartice).First().kartica_aktivna != 0;
            }
        }

        public int DodajKorisnika(string ime, string prezime, string jmbg, string brojKartice)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                korisnik korisnik = new korisnik
                {
                    ime = ime,
                    prezime = prezime,
                    jmbg = jmbg,
                    broj_kartice = brojKartice,
                    kartica_aktivna = 1
                };

                ctx.korisnik.Add(korisnik);
                ctx.SaveChanges();

                int id = ctx.korisnik.Where(k => k.broj_kartice == korisnik.broj_kartice).First().id;
                return id;
            }
        }
        public void IzmijeniKorisnika(int idKorisnika, string ime, string prezime)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                korisnik korisnik = ctx.korisnik.Where(k => k.id == idKorisnika).First();

                korisnik.ime = ime;
                korisnik.prezime = prezime;

                ctx.SaveChanges();
            }
        }
        public korisnik AktivirajKorisnika(int idKorisnika)
        {
            using (VideoKlubContext ctx = new VideoKlubContext())
            {
                korisnik korisnik = ctx.korisnik.Where(k => k.id == idKorisnika).First();
                if (korisnik.kartica_aktivna == 0)
                {
                    korisnik.kartica_aktivna = 1;
                }
                else
                {
                    korisnik.kartica_aktivna = 0;
                }

                ctx.SaveChanges();

                return korisnik;
            }
        }
    }
}
