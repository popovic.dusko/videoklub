﻿namespace VideoKlub
{
    partial class DodajZanr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DodajZanr));
            this.lblNoviZanr = new System.Windows.Forms.Label();
            this.lblKategorijaZanra = new System.Windows.Forms.Label();
            this.cmbKategorijeZanra = new System.Windows.Forms.ComboBox();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.tbNazivZanra = new System.Windows.Forms.TextBox();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnNazad = new System.Windows.Forms.Button();
            this.lblZanrPostoji = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNoviZanr
            // 
            this.lblNoviZanr.AutoSize = true;
            this.lblNoviZanr.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoviZanr.Location = new System.Drawing.Point(13, 13);
            this.lblNoviZanr.Name = "lblNoviZanr";
            this.lblNoviZanr.Size = new System.Drawing.Size(104, 24);
            this.lblNoviZanr.TabIndex = 1;
            this.lblNoviZanr.Text = "Novi žanr";
            // 
            // lblKategorijaZanra
            // 
            this.lblKategorijaZanra.AutoSize = true;
            this.lblKategorijaZanra.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKategorijaZanra.Location = new System.Drawing.Point(30, 77);
            this.lblKategorijaZanra.Name = "lblKategorijaZanra";
            this.lblKategorijaZanra.Size = new System.Drawing.Size(108, 22);
            this.lblKategorijaZanra.TabIndex = 2;
            this.lblKategorijaZanra.Text = "Kategorija ";
            // 
            // cmbKategorijeZanra
            // 
            this.cmbKategorijeZanra.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbKategorijeZanra.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbKategorijeZanra.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbKategorijeZanra.FormattingEnabled = true;
            this.cmbKategorijeZanra.Location = new System.Drawing.Point(164, 74);
            this.cmbKategorijeZanra.Name = "cmbKategorijeZanra";
            this.cmbKategorijeZanra.Size = new System.Drawing.Size(211, 30);
            this.cmbKategorijeZanra.TabIndex = 3;
            this.cmbKategorijeZanra.SelectedIndexChanged += new System.EventHandler(this.CmbKategorijeZanra_SelectedIndexChanged);
            this.cmbKategorijeZanra.TextUpdate += new System.EventHandler(this.CmbKategorijeZanra_TextUpdate);
            this.cmbKategorijeZanra.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CmbKategorijeZanra_KeyDown);
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaziv.Location = new System.Drawing.Point(30, 125);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(61, 22);
            this.lblNaziv.TabIndex = 4;
            this.lblNaziv.Text = "Naziv";
            // 
            // tbNazivZanra
            // 
            this.tbNazivZanra.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNazivZanra.Location = new System.Drawing.Point(164, 122);
            this.tbNazivZanra.MaxLength = 255;
            this.tbNazivZanra.Name = "tbNazivZanra";
            this.tbNazivZanra.Size = new System.Drawing.Size(211, 31);
            this.tbNazivZanra.TabIndex = 5;
            this.tbNazivZanra.TextChanged += new System.EventHandler(this.TbNazivZanra_TextChanged);
            this.tbNazivZanra.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbNazivZanra_KeyDown);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Enabled = false;
            this.btnDodaj.FlatAppearance.BorderSize = 2;
            this.btnDodaj.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodaj.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDodaj.Location = new System.Drawing.Point(164, 185);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(103, 34);
            this.btnDodaj.TabIndex = 9;
            this.btnDodaj.Text = "&Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.BtnDodaj_Click);
            // 
            // btnNazad
            // 
            this.btnNazad.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNazad.FlatAppearance.BorderSize = 2;
            this.btnNazad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNazad.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNazad.Location = new System.Drawing.Point(273, 185);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(102, 34);
            this.btnNazad.TabIndex = 10;
            this.btnNazad.Text = "&Nazad";
            this.btnNazad.UseVisualStyleBackColor = true;
            // 
            // lblZanrPostoji
            // 
            this.lblZanrPostoji.AutoSize = true;
            this.lblZanrPostoji.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZanrPostoji.ForeColor = System.Drawing.Color.Red;
            this.lblZanrPostoji.Location = new System.Drawing.Point(160, 154);
            this.lblZanrPostoji.Name = "lblZanrPostoji";
            this.lblZanrPostoji.Size = new System.Drawing.Size(132, 21);
            this.lblZanrPostoji.TabIndex = 19;
            this.lblZanrPostoji.Text = "Žanr već postoji";
            this.lblZanrPostoji.Visible = false;
            // 
            // DodajZanr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.btnNazad;
            this.ClientSize = new System.Drawing.Size(395, 231);
            this.Controls.Add(this.lblZanrPostoji);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.tbNazivZanra);
            this.Controls.Add(this.lblNaziv);
            this.Controls.Add(this.cmbKategorijeZanra);
            this.Controls.Add(this.lblKategorijaZanra);
            this.Controls.Add(this.lblNoviZanr);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DodajZanr";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DodajZanr";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNoviZanr;
        private System.Windows.Forms.Label lblKategorijaZanra;
        private System.Windows.Forms.ComboBox cmbKategorijeZanra;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.TextBox tbNazivZanra;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.Label lblZanrPostoji;
    }
}