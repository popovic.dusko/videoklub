﻿namespace VideoKlub
{
    partial class FilmPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNaslov = new System.Windows.Forms.Label();
            this.lblGodina = new System.Windows.Forms.Label();
            this.lblZanr = new System.Windows.Forms.Label();
            this.lblGlavneUloge = new System.Windows.Forms.Label();
            this.lblOcjena = new System.Windows.Forms.Label();
            this.pbOcjena = new System.Windows.Forms.PictureBox();
            this.pbSlika = new System.Windows.Forms.PictureBox();
            this.lblBrojIzdavanja = new System.Windows.Forms.Label();
            this.lblCijena = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbOcjena)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSlika)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNaslov
            // 
            this.lblNaslov.AutoEllipsis = true;
            this.lblNaslov.AutoSize = true;
            this.lblNaslov.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaslov.Location = new System.Drawing.Point(152, 4);
            this.lblNaslov.MaximumSize = new System.Drawing.Size(435, 0);
            this.lblNaslov.Name = "lblNaslov";
            this.lblNaslov.Size = new System.Drawing.Size(72, 22);
            this.lblNaslov.TabIndex = 1;
            this.lblNaslov.Text = "Naslov";
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGodina.Location = new System.Drawing.Point(152, 26);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(69, 21);
            this.lblGodina.TabIndex = 2;
            this.lblGodina.Text = "Godina";
            // 
            // lblZanr
            // 
            this.lblZanr.AutoEllipsis = true;
            this.lblZanr.AutoSize = true;
            this.lblZanr.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZanr.Location = new System.Drawing.Point(153, 60);
            this.lblZanr.MaximumSize = new System.Drawing.Size(435, 0);
            this.lblZanr.Name = "lblZanr";
            this.lblZanr.Size = new System.Drawing.Size(39, 17);
            this.lblZanr.TabIndex = 3;
            this.lblZanr.Text = "Žanr:";
            // 
            // lblGlavneUloge
            // 
            this.lblGlavneUloge.AutoEllipsis = true;
            this.lblGlavneUloge.AutoSize = true;
            this.lblGlavneUloge.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGlavneUloge.Location = new System.Drawing.Point(153, 88);
            this.lblGlavneUloge.MaximumSize = new System.Drawing.Size(435, 0);
            this.lblGlavneUloge.Name = "lblGlavneUloge";
            this.lblGlavneUloge.Size = new System.Drawing.Size(100, 17);
            this.lblGlavneUloge.TabIndex = 4;
            this.lblGlavneUloge.Text = "Glavne uloge:";
            // 
            // lblOcjena
            // 
            this.lblOcjena.AutoSize = true;
            this.lblOcjena.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOcjena.Location = new System.Drawing.Point(548, 29);
            this.lblOcjena.Name = "lblOcjena";
            this.lblOcjena.Size = new System.Drawing.Size(32, 21);
            this.lblOcjena.TabIndex = 6;
            this.lblOcjena.Text = "5,0";
            // 
            // pbOcjena
            // 
            this.pbOcjena.BackgroundImage = global::VideoKlub.Properties.Resources.gold_star;
            this.pbOcjena.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbOcjena.Location = new System.Drawing.Point(521, 26);
            this.pbOcjena.Name = "pbOcjena";
            this.pbOcjena.Size = new System.Drawing.Size(26, 27);
            this.pbOcjena.TabIndex = 5;
            this.pbOcjena.TabStop = false;
            // 
            // pbSlika
            // 
            this.pbSlika.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbSlika.Location = new System.Drawing.Point(4, 4);
            this.pbSlika.Name = "pbSlika";
            this.pbSlika.Size = new System.Drawing.Size(142, 142);
            this.pbSlika.TabIndex = 0;
            this.pbSlika.TabStop = false;
            // 
            // lblBrojIzdavanja
            // 
            this.lblBrojIzdavanja.AutoSize = true;
            this.lblBrojIzdavanja.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrojIzdavanja.Location = new System.Drawing.Point(4, 153);
            this.lblBrojIzdavanja.Name = "lblBrojIzdavanja";
            this.lblBrojIzdavanja.Size = new System.Drawing.Size(99, 17);
            this.lblBrojIzdavanja.TabIndex = 7;
            this.lblBrojIzdavanja.Text = "Broj izdavanja";
            // 
            // lblCijena
            // 
            this.lblCijena.AutoSize = true;
            this.lblCijena.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCijena.Location = new System.Drawing.Point(507, 150);
            this.lblCijena.Name = "lblCijena";
            this.lblCijena.Size = new System.Drawing.Size(81, 22);
            this.lblCijena.TabIndex = 8;
            this.lblCijena.Text = "0.00 KM";
            // 
            // FilmPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblCijena);
            this.Controls.Add(this.lblBrojIzdavanja);
            this.Controls.Add(this.lblOcjena);
            this.Controls.Add(this.pbOcjena);
            this.Controls.Add(this.lblGlavneUloge);
            this.Controls.Add(this.lblZanr);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.lblNaslov);
            this.Controls.Add(this.pbSlika);
            this.Name = "FilmPanel";
            this.Size = new System.Drawing.Size(592, 176);
            ((System.ComponentModel.ISupportInitialize)(this.pbOcjena)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSlika)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbSlika;
        private System.Windows.Forms.Label lblNaslov;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.Label lblZanr;
        private System.Windows.Forms.Label lblGlavneUloge;
        private System.Windows.Forms.PictureBox pbOcjena;
        private System.Windows.Forms.Label lblOcjena;
        private System.Windows.Forms.Label lblBrojIzdavanja;
        private System.Windows.Forms.Label lblCijena;
    }
}
