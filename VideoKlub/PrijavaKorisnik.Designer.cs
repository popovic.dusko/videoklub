﻿namespace VideoKlub
{
    partial class PrijavaKorisnik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrijavaKorisnik));
            this.lblPrijava = new System.Windows.Forms.Label();
            this.lblBrojKartice = new System.Windows.Forms.Label();
            this.chbZapamti = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnNazad = new System.Windows.Forms.Button();
            this.lblKarticaNeaktivna = new System.Windows.Forms.Label();
            this.cmbBrojKartice = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblPrijava
            // 
            this.lblPrijava.AutoSize = true;
            this.lblPrijava.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrijava.Location = new System.Drawing.Point(13, 13);
            this.lblPrijava.Name = "lblPrijava";
            this.lblPrijava.Size = new System.Drawing.Size(75, 24);
            this.lblPrijava.TabIndex = 2;
            this.lblPrijava.Text = "Prijava";
            // 
            // lblBrojKartice
            // 
            this.lblBrojKartice.AutoSize = true;
            this.lblBrojKartice.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrojKartice.Location = new System.Drawing.Point(30, 77);
            this.lblBrojKartice.Name = "lblBrojKartice";
            this.lblBrojKartice.Size = new System.Drawing.Size(110, 22);
            this.lblBrojKartice.TabIndex = 3;
            this.lblBrojKartice.Text = "Broj kartice";
            // 
            // chbZapamti
            // 
            this.chbZapamti.AutoSize = true;
            this.chbZapamti.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbZapamti.Location = new System.Drawing.Point(158, 122);
            this.chbZapamti.Name = "chbZapamti";
            this.chbZapamti.Size = new System.Drawing.Size(95, 25);
            this.chbZapamti.TabIndex = 7;
            this.chbZapamti.TabStop = false;
            this.chbZapamti.Tag = "";
            this.chbZapamti.Text = "Zapamti";
            this.chbZapamti.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.FlatAppearance.BorderSize = 2;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(158, 161);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(103, 34);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.EnabledChanged += new System.EventHandler(this.BtnOK_EnabledChanged);
            this.btnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // btnNazad
            // 
            this.btnNazad.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNazad.FlatAppearance.BorderSize = 2;
            this.btnNazad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNazad.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNazad.Location = new System.Drawing.Point(267, 161);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(102, 34);
            this.btnNazad.TabIndex = 9;
            this.btnNazad.Text = "&Nazad";
            this.btnNazad.UseVisualStyleBackColor = true;
            // 
            // lblKarticaNeaktivna
            // 
            this.lblKarticaNeaktivna.AutoSize = true;
            this.lblKarticaNeaktivna.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKarticaNeaktivna.ForeColor = System.Drawing.Color.Red;
            this.lblKarticaNeaktivna.Location = new System.Drawing.Point(154, 105);
            this.lblKarticaNeaktivna.Name = "lblKarticaNeaktivna";
            this.lblKarticaNeaktivna.Size = new System.Drawing.Size(206, 21);
            this.lblKarticaNeaktivna.TabIndex = 20;
            this.lblKarticaNeaktivna.Text = "Broj kartice je nepostojeći";
            this.lblKarticaNeaktivna.Visible = false;
            // 
            // cmbBrojKartice
            // 
            this.cmbBrojKartice.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbBrojKartice.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbBrojKartice.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBrojKartice.FormattingEnabled = true;
            this.cmbBrojKartice.Location = new System.Drawing.Point(158, 74);
            this.cmbBrojKartice.MaxLength = 10;
            this.cmbBrojKartice.Name = "cmbBrojKartice";
            this.cmbBrojKartice.Size = new System.Drawing.Size(211, 30);
            this.cmbBrojKartice.TabIndex = 21;
            this.cmbBrojKartice.SelectedIndexChanged += new System.EventHandler(this.CmbBrojKartice_SelectedIndexChanged);
            this.cmbBrojKartice.TextUpdate += new System.EventHandler(this.CmbBrojKartice_TextUpdate);
            this.cmbBrojKartice.Click += new System.EventHandler(this.CmbBrojKartice_Enter);
            this.cmbBrojKartice.Enter += new System.EventHandler(this.CmbBrojKartice_Enter);
            this.cmbBrojKartice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CmbBrojKartice_KeyDown);
            // 
            // PrijavaKorisnik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.btnNazad;
            this.ClientSize = new System.Drawing.Size(390, 207);
            this.Controls.Add(this.cmbBrojKartice);
            this.Controls.Add(this.lblKarticaNeaktivna);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.chbZapamti);
            this.Controls.Add(this.lblBrojKartice);
            this.Controls.Add(this.lblPrijava);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrijavaKorisnik";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrijavaKorisnik";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPrijava;
        private System.Windows.Forms.Label lblBrojKartice;
        private System.Windows.Forms.CheckBox chbZapamti;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.Label lblKarticaNeaktivna;
        private System.Windows.Forms.ComboBox cmbBrojKartice;
    }
}