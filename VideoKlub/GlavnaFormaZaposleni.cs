﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace VideoKlub
{
    public partial class GlavnaFormaZaposleni : Form
    {
        private int idZaposlenog;
        public GlavnaFormaZaposleni(string ime, string prezime, int idZaposlenog)
        {
            InitializeComponent();

            this.idZaposlenog = idZaposlenog;

            lblZaposleni.Text = "Prijavljen: " + ime + " " + prezime;
            btnOdjava.Location = new Point(lblZaposleni.Location.X + lblZaposleni.Size.Width, btnOdjava.Location.Y);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // iscrtavanje linija
        private void GlavnaFormaZaposleni_FormClosing(object sender, FormClosingEventArgs e)
        {
            BackColor = SystemColors.Control;

            if (DialogResult.Yes == new DijalogUpozorenje().ShowDialog())
            {
                FormClosing -= GlavnaFormaZaposleni_FormClosing;
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
            BackColor = SystemColors.Window;
        }

        #region Button-i
        private void BtnOdjava_MouseEnter(object sender, EventArgs e)
        {
            btnOdjava.Cursor = Cursors.Hand;
        }
        private void BtnOdjava_MouseLeave(object sender, EventArgs e)
        {
            btnOdjava.Cursor = Cursors.Default;
        }
        private void BtnOdjava_Click(object sender, EventArgs e)
        {
            BackColor = SystemColors.Control;

            if (DialogResult.Yes == new DijalogUpozorenje("Da li zaista želite da se odjavite?").ShowDialog())
            {
                new PrijavaZaposleni().Show();
                FormClosing -= GlavnaFormaZaposleni_FormClosing;
                Close();
            }

            BackColor = SystemColors.Window;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void BtnKatalogFilmova_Click(object sender, EventArgs e)
        {
            new KatalogFilmova(this).Show();
            Hide();
        }
        private void BtnZaduzenja_Click(object sender, EventArgs e)
        {
            new ZaduzenjaZaposleni(this, idZaposlenog).Show();
            Hide();
        }
        private void BtnFilmovi_Click(object sender, EventArgs e)
        {
            new RadSaFilmovima(this).Show();
            Hide();
        }
        private void BtnKorisnici_Click(object sender, EventArgs e)
        {
            new RadSaKorisnicima(this).Show();
            Hide();
        }

        #endregion
    }
}
