﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class RadSaKorisnicima : Form
    {
        #region Promjenljive
        private Form roditeljskaForma = null;
        private bool celijaFokusiranaNakonPretrage = false; // potrebno za fokus odgovaracujeg tekstualnog polja za pretragu 
        private int offset = 0; // potrebno za ucitavanje korisnika prilikom scroll-ovanja
        private KorisniciUtil korisniciUtil = new KorisniciUtil(); // potrebno za manipulaciju podacima u bazi
        #endregion
        public RadSaKorisnicima(Form roditeljskaForma)
        {
            InitializeComponent();
            btnIzmijeniKorisnika.Enabled = btnAktivirajKorisnika.Enabled = false;  // na pocetku nijedan korisnik nije oznacen
            this.roditeljskaForma = roditeljskaForma;

            ActiveControl = tbPrezime;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);

            pen.Color = Color.Gray;

            g.DrawLine(pen, btnIzmijeniKorisnika.Location.X - 20, btnIzmijeniKorisnika.Location.Y, 
                btnIzmijeniKorisnika.Location.X - 20, btnIzmijeniKorisnika.Location.Y + btnIzmijeniKorisnika.Height);
        } // za iscrtavanje granica forme
        
        private void ZatamniFormu()
        {
            BackColor = SystemColors.Control;
            dgvKorisnici.DefaultCellStyle.BackColor = SystemColors.Control;
            dgvKorisnici.BackgroundColor = SystemColors.Control;
        }
        private void OsvijetliFormu()
        {
            BackColor = SystemColors.Window;
            dgvKorisnici.DefaultCellStyle.BackColor = SystemColors.Window;
            dgvKorisnici.BackgroundColor = SystemColors.Window;
        }

        private void Pretraga()
        {
            string ime, prezime, jmbg, brojKartice;
            bool aktivniKorisnici;

            ime = tbIme.Text.EndsWith("...") ? "" : tbIme.Text;
            prezime = tbPrezime.Text.EndsWith("...") ? "" : tbPrezime.Text;
            jmbg = tbJMBG.Text.EndsWith("...") ? "" : tbJMBG.Text;
            brojKartice = tbBrojKartice.Text.EndsWith("...") ? "" : tbBrojKartice.Text;

            aktivniKorisnici = chbAktivanKorisnik.Checked;

            List<korisnik> korisnici = korisniciUtil.Pretrazi(ime, prezime, jmbg, brojKartice, aktivniKorisnici).Take(20).ToList();

            if (korisnici != null && korisnici.Count == 0)
            {
                btnIzmijeniKorisnika.Enabled = btnAktivirajKorisnika.Enabled = false;
            }

            bsrKorisnici.Clear();
            foreach (korisnik korisnik in korisnici)
            {
                bsrKorisnici.Add(korisnik);
            }

            offset = 0;
        }
        private void PrikaziKorisnikeOdKorisnikaSaId(int idKorisnika)
        {
            string prezimeKorisnika = korisniciUtil.GetPrezimeKorisnika(idKorisnika);
            List<korisnik> korisnici = korisniciUtil.GetNakonPrezimenaPrvih(prezimeKorisnika, 20);
            if (korisnici.Count < 20)
            {
                korisnici = korisniciUtil.GetZadnjih(20);
            }

            tbIme.Clear();
            tbPrezime.Clear();
            tbJMBG.Clear();
            tbBrojKartice.Clear();

            chbAktivanKorisnik.Checked = false;

            if (korisnici != null && korisnici.Count == 0)
            {
                btnIzmijeniKorisnika.Enabled = btnAktivirajKorisnika.Enabled = false;
            }

            bsrKorisnici.Clear();
            foreach (var korisnik in korisnici)
            {
                bsrKorisnici.Add(korisnik);
            }
            foreach (DataGridViewRow row in dgvKorisnici.Rows)
            {
                if ((int)row.Cells["clnId"].Value == idKorisnika)
                {
                    row.Selected = true;
                }
            }
            int indexReda = dgvKorisnici.SelectedRows[0].Index;
            dgvKorisnici.FirstDisplayedScrollingRowIndex = indexReda;

            offset = 0;
        }
        private void RadSaKorisnicima_FormClosing(object sender, FormClosingEventArgs e)
        {
            roditeljskaForma.Show();
        }
        private void RadSaKorisnicima_Load(object sender, EventArgs e)
        {
            List<korisnik> korisnici = korisniciUtil.GetPrvih(20);
            foreach (var korisnik in korisnici)
            {
                bsrKorisnici.Add(korisnik);
            }

            if (dgvKorisnici.SelectedRows.Count != 0)
            {
                dgvKorisnici.SelectedRows[0].Selected = false;
            }

            dgvKorisnici.SelectionChanged += DgvKorisnici_SelectionChanged;
        }

        #region Button-i
        private void BtnClose_Click(object sender, EventArgs e)
        { 
            Close();
        }
        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        private void BtnDodajKorisnika_Click(object sender, EventArgs e)
        {
            // zatamnjivanje forme
            ZatamniFormu();

            // dodavanje novog korisnika
            int idZadnjegDodanogKorisnika;
            DialogResult dialogResult = new DodajKorisnika().ShowDialog(out idZadnjegDodanogKorisnika);
            if (DialogResult.Yes == dialogResult)
            {
                PrikaziKorisnikeOdKorisnikaSaId(idZadnjegDodanogKorisnika);
            }
            else if (dialogResult == DialogResult.Retry) // ukoliko je unesen JMBG ili Broj kartice koji postoji u bazi pristupa se pregledu informacija za tog korisnika
            {
                if (DialogResult.Yes == new IzmijeniKorisnika(idZadnjegDodanogKorisnika).ShowDialog())
                {
                    PrikaziKorisnikeOdKorisnikaSaId(idZadnjegDodanogKorisnika);
                }
            }

            // povratak u formu
            OsvijetliFormu();
            tbPrezime.Focus();
        }
        private void BtnIzmijeniKorisnika_Click(object sender, EventArgs e)
        {
            // zatamnjivanje forme
            ZatamniFormu();

            // izmjena korisnika
            int idSelektovanogKorisnika = (int)dgvKorisnici.SelectedRows[0].Cells["clnId"].Value;
            if (DialogResult.Yes == new IzmijeniKorisnika(idSelektovanogKorisnika).ShowDialog())
            {
                PrikaziKorisnikeOdKorisnikaSaId(idSelektovanogKorisnika);
            }

            // povratak u formu
            OsvijetliFormu();
            tbPrezime.Focus();
        }
        private void BtnIzmijeniKorisnika_EnabledChanged(object sender, EventArgs e)
        {
            // podesavanje izgleda button-a u zavisnoti da li je enabled
            Button btn = sender as Button;
            if (btn.Enabled == true)
            {
                btn.FlatAppearance.BorderColor = Color.Black;
            }
            else
            {
                btn.FlatAppearance.BorderColor = SystemColors.Control;
            }
        }
        private void BtnAktivirajKorisnika_Click(object sender, EventArgs e)
        {
            int idSelektovanogKorisnika = (int)dgvKorisnici.SelectedRows[0].Cells["clnId"].Value;
            int indexReda = dgvKorisnici.SelectedRows[0].Index;

            bsrKorisnici.RemoveAt(indexReda);
            bsrKorisnici.Insert(indexReda, korisniciUtil.AktivirajKorisnika(idSelektovanogKorisnika));

            dgvKorisnici.Rows[indexReda].Selected = true;
        }
        private void BtnNazad_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region DataGridView - korisnici
        private void DgvKorisnici_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if(dgvKorisnici.Columns[e.ColumnIndex].Name == "clnAktivan")
            {
                e.Value = ((sbyte)e.Value == 0 ? "Ne" : "Da");
            }
        }
        private void DgvKorisnici_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            
            // omogucavanje button - a
            if (dgv.SelectedRows.Count > 0)
            {
                btnIzmijeniKorisnika.Enabled = btnAktivirajKorisnika.Enabled = true;
                btnAktivirajKorisnika.Text = (((sbyte)dgv.SelectedRows[0].Cells["clnAktivan"].Value) == 0 ? "Aktiviraj" : "Deaktiviraj") + " korisnika";
            }
        }
        private void DgvKorisnici_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            // ukoliko je neka celija selektovana fokusira se textBox koji odgovara koloni u kojoj se nalazi ta celija
            // zbog mogucnosti pretrage vrijednosti koje se nalaze u toj koloni
            if (celijaFokusiranaNakonPretrage)
            {
                celijaFokusiranaNakonPretrage = false;
                return;
            }
            switch (e.ColumnIndex)
            {
                case 0: tbPrezime.Focus(); break;
                case 1: tbIme.Focus(); break;
                case 2: tbJMBG.Focus(); break;
                case 3: tbBrojKartice.Focus(); break;
                case 4: chbAktivanKorisnik.Focus(); break;
            }
        }
        private void DgvKorisnici_Scroll(object sender, ScrollEventArgs e)
        {
            // dinamicko ucitavanje korisnika prilikom scroll-ovanja
            if ((e.NewValue + ((DataGridView)sender).DisplayedRowCount(false)) % 20 == 0)
            {
                offset += 20;

                string ime, prezime, jmbg, brojKartice;
                bool aktivniKorisnici;

                ime = tbIme.Text.EndsWith("...") ? "" : tbIme.Text;
                prezime = tbPrezime.Text.EndsWith("...") ? "" : tbPrezime.Text;
                jmbg = tbJMBG.Text.EndsWith("...") ? "" : tbJMBG.Text;
                brojKartice = tbBrojKartice.Text.EndsWith("...") ? "" : tbBrojKartice.Text;

                aktivniKorisnici = chbAktivanKorisnik.Checked;

                List<korisnik> korisnici = korisniciUtil.GetSljedecihUzPretragu(ime, prezime,
                    jmbg, brojKartice, aktivniKorisnici, 20, offset);

                foreach (var korisnik in korisnici)
                {
                    bsrKorisnici.Add(korisnik);
                }
            }
        }
        #endregion

        #region GroupBox - pretraga
        private void GrbPretragaKorisnika_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Black, Color.Black);
        }
        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Brisanje teksta i ivica
                g.Clear(this.BackColor);

                // Crtanje teksta
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Crtanje ivica
                //Lijeva
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Desna
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Donja
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Gornja1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Gornja2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }
        #endregion

        #region TextBox-ovi
        private void Tb_EnterFocus(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // onemogucavanje selekcije placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.SelectionStart = 0;
            }
            else
            {
                textBox.SelectAll();
            }
        }
        private void TbPrezime_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbPrezime_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("Prezime..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }
            else if(textBox.Text.EndsWith("Ime..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }
 
            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if(!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ .'-]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                if (textBox.Tag.ToString().Equals("Prezime"))
                {
                    textBox.Text = "Prezime...";
                }
                else if (textBox.Tag.ToString().Equals("Ime"))
                {
                    textBox.Text = "Ime...";
                }
                textBox.ForeColor = Color.LightGray;
            }

            // kako bi textBox koji je bio fokusiran prilikom pretrage ostao fokusiran
            celijaFokusiranaNakonPretrage = true;

            Pretraga();

            textBox.TextChanged += TbPrezime_TextChanged;
        } // koristeno i za TbIme
        private void TbJMBG_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbJMBG_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("JMBG..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }

            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^0-9]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                textBox.Text = "JMBG...";
                textBox.ForeColor = Color.LightGray;
            }

            // kako bi textBox koji je bio fokusiran prilikom pretrage ostao fokusiran
            celijaFokusiranaNakonPretrage = true;

            Pretraga();

            textBox.TextChanged += TbJMBG_TextChanged;
        }
        private void TbBrojKartice_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbBrojKartice_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("Broj kartice..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
                textBox.MaxLength = 10;
            }

            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^0-9a-zA-Z]", "").ToUpper();
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                textBox.MaxLength = 20;
                textBox.Text = "Broj kartice...";
                textBox.ForeColor = Color.LightGray;
            }

            // kako bi textBox koji je bio fokusiran prilikom pretrage ostao fokusiran
            celijaFokusiranaNakonPretrage = true;

            Pretraga();

            textBox.TextChanged += TbBrojKartice_TextChanged;
        }
        private void Tb_MouseUp(object sender, MouseEventArgs e)
        {
            // selekcija ili kompletnog sadrzaja textBox-a ili nista
            TextBox textBox = sender as TextBox;
            if (!textBox.Text.EndsWith("...") && textBox.SelectionLength == textBox.TextLength)
            {
                textBox.SelectAll();
            }
            else if (textBox.SelectionLength > 0)
            {
                textBox.SelectionLength = 0;
            }
        }
        private void Tb_KeyDown(object sender, KeyEventArgs e) // manipulacija redoslijedom fokusiranja komponenti i selekcije korisnika u dataGridView-u
        {
            // ako je neki textBox fokusiran i pritisnut je taster Up vrsi se selekcija korisnika koji se nalazi iznad vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi korisnik u tabeli
            if (e.KeyCode == Keys.Up)
            {
                e.SuppressKeyPress = true;

                if (dgvKorisnici.Rows.Count == 0)
                {
                    return;
                }
                if (dgvKorisnici.SelectedRows.Count > 0)
                {
                    if (dgvKorisnici.SelectedRows[0].Index > 0)
                    {
                        dgvKorisnici.Rows[dgvKorisnici.SelectedRows[0].Index - 1].Selected = true;
                        if (dgvKorisnici.SelectedRows[0].Displayed == false)
                        {
                            if (dgvKorisnici.SelectedRows[0].Index > dgvKorisnici.FirstDisplayedScrollingRowIndex + dgvKorisnici.DisplayedRowCount(false) - 1)
                            {
                                dgvKorisnici.FirstDisplayedScrollingRowIndex = dgvKorisnici.SelectedRows[0].Index - dgvKorisnici.DisplayedRowCount(false) + 1;
                            }
                            else
                            {
                                dgvKorisnici.FirstDisplayedScrollingRowIndex = dgvKorisnici.FirstDisplayedScrollingRowIndex - 1;
                            }
                        }
                    }
                }
                else
                {
                    dgvKorisnici.Rows[0].Selected = true;
                }
            }
            // ako je neki textBox fokusiran i pritisnut je taster Down vrsi se selekcija korisnika koji se nalazi ispod vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi korisnik u tabeli
            else if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;

                if (dgvKorisnici.Rows.Count == 0)
                {
                    return;
                }
                if (dgvKorisnici.SelectedRows.Count > 0)
                {
                    if (dgvKorisnici.SelectedRows[0].Index < dgvKorisnici.Rows.Count - 1)
                    {
                        dgvKorisnici.Rows[dgvKorisnici.SelectedRows[0].Index + 1].Selected = true;
                        if (dgvKorisnici.SelectedRows[0].Displayed == false)
                        {
                            if (dgvKorisnici.SelectedRows[0].Index < dgvKorisnici.FirstDisplayedScrollingRowIndex)
                            {
                                dgvKorisnici.FirstDisplayedScrollingRowIndex = dgvKorisnici.SelectedRows[0].Index;
                            }
                            else
                            {
                                dgvKorisnici.FirstDisplayedScrollingRowIndex = dgvKorisnici.FirstDisplayedScrollingRowIndex + 1;
                            }
                        }
                    }
                }
                else
                {
                    dgvKorisnici.Rows[0].Selected = true;
                }
            }
            // ako je pritisnut taster Right fokusiramo komponentu koja se nalazi desno od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Right)
            {
                e.SuppressKeyPress = true;

                if (sender is TextBox)
                {
                    TextBox textBox = sender as TextBox;

                    switch (textBox.Tag.ToString())
                    {
                        case "Prezime": tbIme.Focus(); break;
                        case "Ime": tbJMBG.Focus(); break;
                        case "JMBG": tbBrojKartice.Focus(); break;
                        case "Broj kartice": chbAktivanKorisnik.Focus(); break;
                    }
                }
                else if (sender is CheckBox)
                {
                    tbPrezime.Focus();
                }
            }
            // ako je pritisnut taster Right fokusiramo komponentu koja se nalazi lijevo od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Left)
            {
                e.SuppressKeyPress = true;

                if (sender is TextBox)
                {
                    TextBox textBox = sender as TextBox;

                    switch (textBox.Tag.ToString())
                    {
                        case "Prezime": chbAktivanKorisnik.Focus(); break;
                        case "Ime": tbPrezime.Focus(); break;
                        case "JMBG": tbIme.Focus(); break;
                        case "Broj kartice": tbJMBG.Focus(); break;
                    }
                }
                else if (sender is CheckBox)
                {
                    tbBrojKartice.Focus();
                }
            }
            // ako je fokusiran neki textBox kombinacijom tastera Control i odredjenih tastera simulira se klik na odgovarajuci button
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodajKorisnika.PerformClick();
                }
                else if (e.KeyCode == Keys.I)
                {
                    e.SuppressKeyPress = true;
                    btnIzmijeniKorisnika.PerformClick();
                }
                else if (e.KeyCode == Keys.A)
                {
                    e.SuppressKeyPress = true;
                    btnAktivirajKorisnika.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
        }
        #endregion

        #region CheckBox - prikaz samo aktivnih korisnika
        private void ChbAktivanKorisnik_CheckedChanged(object sender, EventArgs e)
        {
            // kako bi checkBox ostao fokusiran i nakon pretrage
            celijaFokusiranaNakonPretrage = true;

            Pretraga();
        }
        #endregion
    }
}

