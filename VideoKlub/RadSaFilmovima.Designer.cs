﻿namespace VideoKlub
{
    partial class RadSaFilmovima
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadSaFilmovima));
            this.dgvFilmovi = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnCijena = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnNaStanju = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnAktivanFilm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsrFilmovi = new System.Windows.Forms.BindingSource(this.components);
            this.btnDodajFilm = new System.Windows.Forms.Button();
            this.btnIzmijeniFilm = new System.Windows.Forms.Button();
            this.btnAktivirajFilm = new System.Windows.Forms.Button();
            this.btnNazad = new System.Windows.Forms.Button();
            this.grbPretragaFilmova = new System.Windows.Forms.GroupBox();
            this.chbAktivanFilm = new System.Windows.Forms.CheckBox();
            this.btnGlumci = new System.Windows.Forms.Button();
            this.btnZanr = new System.Windows.Forms.Button();
            this.tbGodina = new System.Windows.Forms.TextBox();
            this.tbNaslovFilma = new System.Windows.Forms.TextBox();
            this.pnlZanr = new System.Windows.Forms.Panel();
            this.spcSplitZanrovi = new System.Windows.Forms.SplitContainer();
            this.lvZanrovi = new System.Windows.Forms.ListView();
            this.clnNaziv = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmbNacinPretrageZanrovi = new System.Windows.Forms.ComboBox();
            this.lblNacinPretrageZanrovi = new System.Windows.Forms.Label();
            this.tbZanrPretraga = new System.Windows.Forms.TextBox();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlGlumci = new System.Windows.Forms.Panel();
            this.spcSplitGlumci = new System.Windows.Forms.SplitContainer();
            this.lvGlumci = new System.Windows.Forms.ListView();
            this.clnPrezime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnIme = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.flpPretragaGlumci = new System.Windows.Forms.FlowLayoutPanel();
            this.tbPrezimePretraga = new System.Windows.Forms.TextBox();
            this.tbImePretraga = new System.Windows.Forms.TextBox();
            this.cmbNacinPretrageGlumci = new System.Windows.Forms.ComboBox();
            this.lblNacinPretrageGlumci = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilmovi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsrFilmovi)).BeginInit();
            this.grbPretragaFilmova.SuspendLayout();
            this.pnlZanr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcSplitZanrovi)).BeginInit();
            this.spcSplitZanrovi.Panel1.SuspendLayout();
            this.spcSplitZanrovi.Panel2.SuspendLayout();
            this.spcSplitZanrovi.SuspendLayout();
            this.pnlGlumci.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcSplitGlumci)).BeginInit();
            this.spcSplitGlumci.Panel1.SuspendLayout();
            this.spcSplitGlumci.Panel2.SuspendLayout();
            this.spcSplitGlumci.SuspendLayout();
            this.flpPretragaGlumci.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvFilmovi
            // 
            this.dgvFilmovi.AllowUserToAddRows = false;
            this.dgvFilmovi.AllowUserToDeleteRows = false;
            this.dgvFilmovi.AutoGenerateColumns = false;
            this.dgvFilmovi.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFilmovi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFilmovi.ColumnHeadersHeight = 30;
            this.dgvFilmovi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvFilmovi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.clnCijena,
            this.clnNaStanju,
            this.clnAktivanFilm,
            this.clnId,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.dgvFilmovi.DataSource = this.bsrFilmovi;
            this.dgvFilmovi.Location = new System.Drawing.Point(12, 115);
            this.dgvFilmovi.MultiSelect = false;
            this.dgvFilmovi.Name = "dgvFilmovi";
            this.dgvFilmovi.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFilmovi.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFilmovi.RowHeadersVisible = false;
            this.dgvFilmovi.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvFilmovi.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvFilmovi.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFilmovi.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvFilmovi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFilmovi.Size = new System.Drawing.Size(786, 252);
            this.dgvFilmovi.TabIndex = 15;
            this.dgvFilmovi.TabStop = false;
            this.dgvFilmovi.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvFilmovi_CellEnter);
            this.dgvFilmovi.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DgvFilmovi_CellFormatting);
            this.dgvFilmovi.Scroll += new System.Windows.Forms.ScrollEventHandler(this.DgvFilmovi_Scroll);
            this.dgvFilmovi.Click += new System.EventHandler(this.DgvFilmovi_Click);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "naslov";
            this.dataGridViewTextBoxColumn2.HeaderText = "Naslov";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 370;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "godina";
            this.dataGridViewTextBoxColumn3.HeaderText = "Godina";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // clnCijena
            // 
            this.clnCijena.HeaderText = "Cijena";
            this.clnCijena.Name = "clnCijena";
            this.clnCijena.ReadOnly = true;
            this.clnCijena.Width = 90;
            // 
            // clnNaStanju
            // 
            this.clnNaStanju.HeaderText = "Na stanju";
            this.clnNaStanju.Name = "clnNaStanju";
            this.clnNaStanju.ReadOnly = true;
            this.clnNaStanju.Width = 110;
            // 
            // clnAktivanFilm
            // 
            this.clnAktivanFilm.HeaderText = "Aktivan";
            this.clnAktivanFilm.Name = "clnAktivanFilm";
            this.clnAktivanFilm.ReadOnly = true;
            // 
            // clnId
            // 
            this.clnId.DataPropertyName = "id";
            this.clnId.HeaderText = "id";
            this.clnId.Name = "clnId";
            this.clnId.ReadOnly = true;
            this.clnId.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "slika";
            this.dataGridViewTextBoxColumn4.HeaderText = "slika";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "film_u_magacinu";
            this.dataGridViewTextBoxColumn5.HeaderText = "film_u_magacinu";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "glumac";
            this.dataGridViewTextBoxColumn6.HeaderText = "glumac";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "zanr";
            this.dataGridViewTextBoxColumn7.HeaderText = "zanr";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // bsrFilmovi
            // 
            this.bsrFilmovi.DataSource = typeof(VideoKlub.film);
            // 
            // btnDodajFilm
            // 
            this.btnDodajFilm.FlatAppearance.BorderSize = 2;
            this.btnDodajFilm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodajFilm.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDodajFilm.Location = new System.Drawing.Point(12, 380);
            this.btnDodajFilm.Name = "btnDodajFilm";
            this.btnDodajFilm.Size = new System.Drawing.Size(144, 58);
            this.btnDodajFilm.TabIndex = 17;
            this.btnDodajFilm.TabStop = false;
            this.btnDodajFilm.Text = "&Dodaj film";
            this.btnDodajFilm.UseVisualStyleBackColor = true;
            this.btnDodajFilm.Click += new System.EventHandler(this.BtnDodajFilm_Click);
            this.btnDodajFilm.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.BtnDodajFilm_PreviewKeyDown);
            // 
            // btnIzmijeniFilm
            // 
            this.btnIzmijeniFilm.FlatAppearance.BorderSize = 2;
            this.btnIzmijeniFilm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIzmijeniFilm.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzmijeniFilm.Location = new System.Drawing.Point(196, 380);
            this.btnIzmijeniFilm.Name = "btnIzmijeniFilm";
            this.btnIzmijeniFilm.Size = new System.Drawing.Size(144, 58);
            this.btnIzmijeniFilm.TabIndex = 18;
            this.btnIzmijeniFilm.TabStop = false;
            this.btnIzmijeniFilm.Text = "&Izmijeni film";
            this.btnIzmijeniFilm.UseVisualStyleBackColor = true;
            this.btnIzmijeniFilm.EnabledChanged += new System.EventHandler(this.BtnIzmijeniFilm_EnabledChanged);
            this.btnIzmijeniFilm.Click += new System.EventHandler(this.BtnIzmijeniFilm_Click);
            this.btnIzmijeniFilm.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.BtnIzmijeniFilm_PreviewKeyDown);
            // 
            // btnAktivirajFilm
            // 
            this.btnAktivirajFilm.FlatAppearance.BorderSize = 2;
            this.btnAktivirajFilm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAktivirajFilm.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAktivirajFilm.Location = new System.Drawing.Point(360, 380);
            this.btnAktivirajFilm.Name = "btnAktivirajFilm";
            this.btnAktivirajFilm.Size = new System.Drawing.Size(144, 58);
            this.btnAktivirajFilm.TabIndex = 19;
            this.btnAktivirajFilm.TabStop = false;
            this.btnAktivirajFilm.Text = "&Aktiviraj / deaktiviraj";
            this.btnAktivirajFilm.UseVisualStyleBackColor = true;
            this.btnAktivirajFilm.EnabledChanged += new System.EventHandler(this.BtnIzmijeniFilm_EnabledChanged);
            this.btnAktivirajFilm.Click += new System.EventHandler(this.BtnAktivirajFilm_Click);
            this.btnAktivirajFilm.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.BtnAktivirajFilm_PreviewKeyDown);
            // 
            // btnNazad
            // 
            this.btnNazad.FlatAppearance.BorderSize = 2;
            this.btnNazad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNazad.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNazad.Location = new System.Drawing.Point(654, 380);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(144, 58);
            this.btnNazad.TabIndex = 20;
            this.btnNazad.TabStop = false;
            this.btnNazad.Text = "&Nazad";
            this.btnNazad.UseVisualStyleBackColor = true;
            this.btnNazad.Click += new System.EventHandler(this.BtnNazad_Click);
            this.btnNazad.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.BtnNazad_PreviewKeyDown);
            // 
            // grbPretragaFilmova
            // 
            this.grbPretragaFilmova.Controls.Add(this.chbAktivanFilm);
            this.grbPretragaFilmova.Controls.Add(this.btnGlumci);
            this.grbPretragaFilmova.Controls.Add(this.btnZanr);
            this.grbPretragaFilmova.Controls.Add(this.tbGodina);
            this.grbPretragaFilmova.Controls.Add(this.tbNaslovFilma);
            this.grbPretragaFilmova.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbPretragaFilmova.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbPretragaFilmova.Location = new System.Drawing.Point(12, 49);
            this.grbPretragaFilmova.Name = "grbPretragaFilmova";
            this.grbPretragaFilmova.Size = new System.Drawing.Size(786, 60);
            this.grbPretragaFilmova.TabIndex = 20;
            this.grbPretragaFilmova.TabStop = false;
            this.grbPretragaFilmova.Text = "Pretraga filmova";
            this.grbPretragaFilmova.Paint += new System.Windows.Forms.PaintEventHandler(this.GrbPretragaFilmova_Paint);
            // 
            // chbAktivanFilm
            // 
            this.chbAktivanFilm.AutoSize = true;
            this.chbAktivanFilm.Location = new System.Drawing.Point(698, 29);
            this.chbAktivanFilm.Name = "chbAktivanFilm";
            this.chbAktivanFilm.Size = new System.Drawing.Size(87, 23);
            this.chbAktivanFilm.TabIndex = 6;
            this.chbAktivanFilm.TabStop = false;
            this.chbAktivanFilm.Tag = "Aktivan film";
            this.chbAktivanFilm.Text = "Aktivan";
            this.chbAktivanFilm.UseVisualStyleBackColor = true;
            this.chbAktivanFilm.CheckedChanged += new System.EventHandler(this.ChbAktivniFilmovi_CheckedChanged);
            this.chbAktivanFilm.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ChbAktivanFilm_PreviewKeyDown);
            // 
            // btnGlumci
            // 
            this.btnGlumci.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGlumci.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGlumci.Image = ((System.Drawing.Image)(resources.GetObject("btnGlumci.Image")));
            this.btnGlumci.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGlumci.Location = new System.Drawing.Point(569, 27);
            this.btnGlumci.Name = "btnGlumci";
            this.btnGlumci.Size = new System.Drawing.Size(121, 27);
            this.btnGlumci.TabIndex = 4;
            this.btnGlumci.TabStop = false;
            this.btnGlumci.Tag = "Glumci";
            this.btnGlumci.Text = "Glumci";
            this.btnGlumci.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGlumci.UseVisualStyleBackColor = true;
            this.btnGlumci.Enter += new System.EventHandler(this.BtnGlumci_Enter);
            this.btnGlumci.Leave += new System.EventHandler(this.BtnGlumci_Leave);
            this.btnGlumci.MouseEnter += new System.EventHandler(this.BtnGlumci_MouseEnter);
            this.btnGlumci.MouseLeave += new System.EventHandler(this.BtnGlumci_MouseLeave);
            // 
            // btnZanr
            // 
            this.btnZanr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZanr.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZanr.Image = ((System.Drawing.Image)(resources.GetObject("btnZanr.Image")));
            this.btnZanr.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnZanr.Location = new System.Drawing.Point(438, 27);
            this.btnZanr.Name = "btnZanr";
            this.btnZanr.Size = new System.Drawing.Size(121, 27);
            this.btnZanr.TabIndex = 2;
            this.btnZanr.TabStop = false;
            this.btnZanr.Tag = "Zanr";
            this.btnZanr.Text = "Žanr";
            this.btnZanr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnZanr.UseVisualStyleBackColor = true;
            this.btnZanr.Enter += new System.EventHandler(this.BtnZanr_Enter);
            this.btnZanr.Leave += new System.EventHandler(this.BtnZanr_Leave);
            this.btnZanr.MouseEnter += new System.EventHandler(this.BtnZanr_MouseEnter);
            this.btnZanr.MouseLeave += new System.EventHandler(this.BtnZanr_MouseLeave);
            // 
            // tbGodina
            // 
            this.tbGodina.ForeColor = System.Drawing.Color.Silver;
            this.tbGodina.Location = new System.Drawing.Point(336, 27);
            this.tbGodina.MaxLength = 255;
            this.tbGodina.Name = "tbGodina";
            this.tbGodina.Size = new System.Drawing.Size(91, 27);
            this.tbGodina.TabIndex = 1;
            this.tbGodina.TabStop = false;
            this.tbGodina.Tag = "Godina";
            this.tbGodina.Text = "Godina...";
            this.tbGodina.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbGodina.TextChanged += new System.EventHandler(this.TbGodina_TextChanged);
            this.tbGodina.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbGodina.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            this.tbGodina.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            this.tbGodina.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.TbGodina_PreviewKeyDown);
            // 
            // tbNaslovFilma
            // 
            this.tbNaslovFilma.ForeColor = System.Drawing.Color.Silver;
            this.tbNaslovFilma.Location = new System.Drawing.Point(7, 27);
            this.tbNaslovFilma.MaxLength = 255;
            this.tbNaslovFilma.Name = "tbNaslovFilma";
            this.tbNaslovFilma.Size = new System.Drawing.Size(319, 27);
            this.tbNaslovFilma.TabIndex = 0;
            this.tbNaslovFilma.TabStop = false;
            this.tbNaslovFilma.Tag = "Naslov filma";
            this.tbNaslovFilma.Text = "Naslov filma...";
            this.tbNaslovFilma.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbNaslovFilma.TextChanged += new System.EventHandler(this.TbNaslovFilma_TextChanged);
            this.tbNaslovFilma.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbNaslovFilma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            this.tbNaslovFilma.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            this.tbNaslovFilma.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.TbNaslovFilma_PreviewKeyDown);
            // 
            // pnlZanr
            // 
            this.pnlZanr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlZanr.Controls.Add(this.spcSplitZanrovi);
            this.pnlZanr.Controls.Add(this.tbZanrPretraga);
            this.pnlZanr.Location = new System.Drawing.Point(450, 103);
            this.pnlZanr.Name = "pnlZanr";
            this.pnlZanr.Size = new System.Drawing.Size(230, 249);
            this.pnlZanr.TabIndex = 21;
            // 
            // spcSplitZanrovi
            // 
            this.spcSplitZanrovi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spcSplitZanrovi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcSplitZanrovi.IsSplitterFixed = true;
            this.spcSplitZanrovi.Location = new System.Drawing.Point(0, 27);
            this.spcSplitZanrovi.Name = "spcSplitZanrovi";
            this.spcSplitZanrovi.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcSplitZanrovi.Panel1
            // 
            this.spcSplitZanrovi.Panel1.Controls.Add(this.lvZanrovi);
            // 
            // spcSplitZanrovi.Panel2
            // 
            this.spcSplitZanrovi.Panel2.Controls.Add(this.cmbNacinPretrageZanrovi);
            this.spcSplitZanrovi.Panel2.Controls.Add(this.lblNacinPretrageZanrovi);
            this.spcSplitZanrovi.Panel2.MouseLeave += new System.EventHandler(this.SpcSplitZanrovi_Panel2_MouseLeave);
            this.spcSplitZanrovi.Size = new System.Drawing.Size(228, 220);
            this.spcSplitZanrovi.SplitterDistance = 176;
            this.spcSplitZanrovi.SplitterWidth = 1;
            this.spcSplitZanrovi.TabIndex = 1;
            this.spcSplitZanrovi.TabStop = false;
            this.spcSplitZanrovi.MouseLeave += new System.EventHandler(this.SpcSplitZanrovi_MouseLeave);
            // 
            // lvZanrovi
            // 
            this.lvZanrovi.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvZanrovi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvZanrovi.CheckBoxes = true;
            this.lvZanrovi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clnNaziv});
            this.lvZanrovi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvZanrovi.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvZanrovi.FullRowSelect = true;
            this.lvZanrovi.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvZanrovi.HideSelection = false;
            this.lvZanrovi.Location = new System.Drawing.Point(0, 0);
            this.lvZanrovi.MultiSelect = false;
            this.lvZanrovi.Name = "lvZanrovi";
            this.lvZanrovi.ShowItemToolTips = true;
            this.lvZanrovi.Size = new System.Drawing.Size(226, 174);
            this.lvZanrovi.TabIndex = 0;
            this.lvZanrovi.TabStop = false;
            this.lvZanrovi.UseCompatibleStateImageBehavior = false;
            this.lvZanrovi.View = System.Windows.Forms.View.Details;
            this.lvZanrovi.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.LvZanrovi_ItemChecked);
            this.lvZanrovi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LvZanrovi_KeyDown);
            this.lvZanrovi.MouseLeave += new System.EventHandler(this.LvZanrovi_MouseLeave);
            // 
            // clnNaziv
            // 
            this.clnNaziv.Text = "Naziv žanra";
            this.clnNaziv.Width = 205;
            // 
            // cmbNacinPretrageZanrovi
            // 
            this.cmbNacinPretrageZanrovi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNacinPretrageZanrovi.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNacinPretrageZanrovi.FormattingEnabled = true;
            this.cmbNacinPretrageZanrovi.Items.AddRange(new object[] {
            "I (And)",
            "Ili (Or)"});
            this.cmbNacinPretrageZanrovi.Location = new System.Drawing.Point(141, 5);
            this.cmbNacinPretrageZanrovi.Name = "cmbNacinPretrageZanrovi";
            this.cmbNacinPretrageZanrovi.Size = new System.Drawing.Size(82, 29);
            this.cmbNacinPretrageZanrovi.TabIndex = 3;
            this.cmbNacinPretrageZanrovi.TabStop = false;
            this.cmbNacinPretrageZanrovi.SelectedIndexChanged += new System.EventHandler(this.CmbNacinPretrageZanrovi_SelectedIndexChanged);
            this.cmbNacinPretrageZanrovi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CmbNacinPretrageZanrovi_KeyDown);
            this.cmbNacinPretrageZanrovi.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.CmbNacinPretrageZanrovi_PreviewKeyDown);
            // 
            // lblNacinPretrageZanrovi
            // 
            this.lblNacinPretrageZanrovi.AutoSize = true;
            this.lblNacinPretrageZanrovi.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacinPretrageZanrovi.Location = new System.Drawing.Point(4, 9);
            this.lblNacinPretrageZanrovi.Name = "lblNacinPretrageZanrovi";
            this.lblNacinPretrageZanrovi.Size = new System.Drawing.Size(134, 21);
            this.lblNacinPretrageZanrovi.TabIndex = 0;
            this.lblNacinPretrageZanrovi.Text = "Način pretrage:";
            // 
            // tbZanrPretraga
            // 
            this.tbZanrPretraga.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbZanrPretraga.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbZanrPretraga.ForeColor = System.Drawing.Color.Silver;
            this.tbZanrPretraga.Location = new System.Drawing.Point(0, 0);
            this.tbZanrPretraga.MaxLength = 255;
            this.tbZanrPretraga.Name = "tbZanrPretraga";
            this.tbZanrPretraga.Size = new System.Drawing.Size(228, 27);
            this.tbZanrPretraga.TabIndex = 0;
            this.tbZanrPretraga.TabStop = false;
            this.tbZanrPretraga.Text = "Žanr...";
            this.tbZanrPretraga.Click += new System.EventHandler(this.TbPretraga_EnterFocus);
            this.tbZanrPretraga.TextChanged += new System.EventHandler(this.TbZanrPretraga_TextChanged);
            this.tbZanrPretraga.Enter += new System.EventHandler(this.TbPretraga_EnterFocus);
            this.tbZanrPretraga.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbZanrPretraga_KeyDown);
            this.tbZanrPretraga.MouseLeave += new System.EventHandler(this.TbZanrPretraga_MouseLeave);
            this.tbZanrPretraga.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            this.tbZanrPretraga.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.TbZanrPretraga_PreviewKeyDown);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackgroundImage = global::VideoKlub.Properties.Resources.minimize_button;
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Location = new System.Drawing.Point(700, 3);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(50, 50);
            this.btnMinimize.TabIndex = 14;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::VideoKlub.Properties.Resources.close_button;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(756, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(50, 50);
            this.btnClose.TabIndex = 13;
            this.btnClose.TabStop = false;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // pnlGlumci
            // 
            this.pnlGlumci.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGlumci.Controls.Add(this.spcSplitGlumci);
            this.pnlGlumci.Location = new System.Drawing.Point(579, 103);
            this.pnlGlumci.Name = "pnlGlumci";
            this.pnlGlumci.Size = new System.Drawing.Size(230, 249);
            this.pnlGlumci.TabIndex = 22;
            this.pnlGlumci.Visible = false;
            // 
            // spcSplitGlumci
            // 
            this.spcSplitGlumci.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spcSplitGlumci.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcSplitGlumci.IsSplitterFixed = true;
            this.spcSplitGlumci.Location = new System.Drawing.Point(0, 0);
            this.spcSplitGlumci.Name = "spcSplitGlumci";
            this.spcSplitGlumci.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcSplitGlumci.Panel1
            // 
            this.spcSplitGlumci.Panel1.Controls.Add(this.lvGlumci);
            this.spcSplitGlumci.Panel1.Controls.Add(this.flpPretragaGlumci);
            // 
            // spcSplitGlumci.Panel2
            // 
            this.spcSplitGlumci.Panel2.Controls.Add(this.cmbNacinPretrageGlumci);
            this.spcSplitGlumci.Panel2.Controls.Add(this.lblNacinPretrageGlumci);
            this.spcSplitGlumci.Panel2.MouseLeave += new System.EventHandler(this.SpcSplitGlumci_Panel2_MouseLeave);
            this.spcSplitGlumci.Size = new System.Drawing.Size(228, 247);
            this.spcSplitGlumci.SplitterDistance = 203;
            this.spcSplitGlumci.SplitterWidth = 1;
            this.spcSplitGlumci.TabIndex = 1;
            this.spcSplitGlumci.TabStop = false;
            this.spcSplitGlumci.MouseLeave += new System.EventHandler(this.SpcSplitGlumci_MouseLeave);
            // 
            // lvGlumci
            // 
            this.lvGlumci.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvGlumci.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvGlumci.CheckBoxes = true;
            this.lvGlumci.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clnPrezime,
            this.clnIme});
            this.lvGlumci.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvGlumci.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvGlumci.FullRowSelect = true;
            this.lvGlumci.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvGlumci.HideSelection = false;
            this.lvGlumci.Location = new System.Drawing.Point(0, 28);
            this.lvGlumci.MultiSelect = false;
            this.lvGlumci.Name = "lvGlumci";
            this.lvGlumci.ShowItemToolTips = true;
            this.lvGlumci.Size = new System.Drawing.Size(226, 173);
            this.lvGlumci.TabIndex = 2;
            this.lvGlumci.TabStop = false;
            this.lvGlumci.UseCompatibleStateImageBehavior = false;
            this.lvGlumci.View = System.Windows.Forms.View.Details;
            this.lvGlumci.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.LvGlumci_ItemChecked);
            this.lvGlumci.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LvGlumci_KeyDown);
            this.lvGlumci.MouseLeave += new System.EventHandler(this.LvGlumci_MouseLeave);
            // 
            // clnPrezime
            // 
            this.clnPrezime.Text = "Prezime";
            this.clnPrezime.Width = 104;
            // 
            // clnIme
            // 
            this.clnIme.Text = "Ime";
            this.clnIme.Width = 104;
            // 
            // flpPretragaGlumci
            // 
            this.flpPretragaGlumci.Controls.Add(this.tbPrezimePretraga);
            this.flpPretragaGlumci.Controls.Add(this.tbImePretraga);
            this.flpPretragaGlumci.Dock = System.Windows.Forms.DockStyle.Top;
            this.flpPretragaGlumci.Location = new System.Drawing.Point(0, 0);
            this.flpPretragaGlumci.Margin = new System.Windows.Forms.Padding(0);
            this.flpPretragaGlumci.Name = "flpPretragaGlumci";
            this.flpPretragaGlumci.Size = new System.Drawing.Size(226, 28);
            this.flpPretragaGlumci.TabIndex = 1;
            // 
            // tbPrezimePretraga
            // 
            this.tbPrezimePretraga.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPrezimePretraga.ForeColor = System.Drawing.Color.Silver;
            this.tbPrezimePretraga.Location = new System.Drawing.Point(0, 0);
            this.tbPrezimePretraga.Margin = new System.Windows.Forms.Padding(0);
            this.tbPrezimePretraga.MaxLength = 255;
            this.tbPrezimePretraga.Name = "tbPrezimePretraga";
            this.tbPrezimePretraga.Size = new System.Drawing.Size(110, 27);
            this.tbPrezimePretraga.TabIndex = 1;
            this.tbPrezimePretraga.TabStop = false;
            this.tbPrezimePretraga.Tag = "Prezime";
            this.tbPrezimePretraga.Text = "Prezime...";
            this.tbPrezimePretraga.Click += new System.EventHandler(this.TbPretraga_EnterFocus);
            this.tbPrezimePretraga.TextChanged += new System.EventHandler(this.TbPrezime_TextChanged);
            this.tbPrezimePretraga.Enter += new System.EventHandler(this.TbPretraga_EnterFocus);
            this.tbPrezimePretraga.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbPrezime_KeyDown);
            this.tbPrezimePretraga.MouseLeave += new System.EventHandler(this.TbPrezimePretraga_MouseLeave);
            this.tbPrezimePretraga.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            this.tbPrezimePretraga.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.TbPrezimePretraga_PreviewKeyDown);
            // 
            // tbImePretraga
            // 
            this.tbImePretraga.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbImePretraga.ForeColor = System.Drawing.Color.Silver;
            this.tbImePretraga.Location = new System.Drawing.Point(110, 0);
            this.tbImePretraga.Margin = new System.Windows.Forms.Padding(0);
            this.tbImePretraga.MaxLength = 255;
            this.tbImePretraga.Name = "tbImePretraga";
            this.tbImePretraga.Size = new System.Drawing.Size(116, 27);
            this.tbImePretraga.TabIndex = 2;
            this.tbImePretraga.TabStop = false;
            this.tbImePretraga.Tag = "Ime";
            this.tbImePretraga.Text = "Ime...";
            this.tbImePretraga.Click += new System.EventHandler(this.TbPretraga_EnterFocus);
            this.tbImePretraga.TextChanged += new System.EventHandler(this.TbPrezime_TextChanged);
            this.tbImePretraga.Enter += new System.EventHandler(this.TbPretraga_EnterFocus);
            this.tbImePretraga.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbPrezime_KeyDown);
            this.tbImePretraga.MouseLeave += new System.EventHandler(this.TbImePretraga_MouseLeave);
            this.tbImePretraga.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            this.tbImePretraga.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.TbImePretraga_PreviewKeyDown);
            // 
            // cmbNacinPretrageGlumci
            // 
            this.cmbNacinPretrageGlumci.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNacinPretrageGlumci.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNacinPretrageGlumci.FormattingEnabled = true;
            this.cmbNacinPretrageGlumci.Items.AddRange(new object[] {
            "I (And)",
            "Ili (Or)"});
            this.cmbNacinPretrageGlumci.Location = new System.Drawing.Point(141, 5);
            this.cmbNacinPretrageGlumci.Name = "cmbNacinPretrageGlumci";
            this.cmbNacinPretrageGlumci.Size = new System.Drawing.Size(82, 29);
            this.cmbNacinPretrageGlumci.TabIndex = 5;
            this.cmbNacinPretrageGlumci.TabStop = false;
            this.cmbNacinPretrageGlumci.SelectedIndexChanged += new System.EventHandler(this.CmbNacinPretrageGlumci_SelectedIndexChanged);
            this.cmbNacinPretrageGlumci.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CmbNacinPretrageGlumci_KeyDown);
            this.cmbNacinPretrageGlumci.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.CmbNacinPretrageGlumci_PreviewKeyDown);
            // 
            // lblNacinPretrageGlumci
            // 
            this.lblNacinPretrageGlumci.AutoSize = true;
            this.lblNacinPretrageGlumci.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacinPretrageGlumci.Location = new System.Drawing.Point(4, 9);
            this.lblNacinPretrageGlumci.Name = "lblNacinPretrageGlumci";
            this.lblNacinPretrageGlumci.Size = new System.Drawing.Size(134, 21);
            this.lblNacinPretrageGlumci.TabIndex = 0;
            this.lblNacinPretrageGlumci.Text = "Način pretrage:";
            // 
            // RadSaFilmovima
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(810, 450);
            this.Controls.Add(this.pnlGlumci);
            this.Controls.Add(this.pnlZanr);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.btnAktivirajFilm);
            this.Controls.Add(this.btnIzmijeniFilm);
            this.Controls.Add(this.btnDodajFilm);
            this.Controls.Add(this.dgvFilmovi);
            this.Controls.Add(this.grbPretragaFilmova);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RadSaFilmovima";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RadSaKorisnicima";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RadSaFilmovima_FormClosing);
            this.Load += new System.EventHandler(this.RadSaFilmovima_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilmovi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsrFilmovi)).EndInit();
            this.grbPretragaFilmova.ResumeLayout(false);
            this.grbPretragaFilmova.PerformLayout();
            this.pnlZanr.ResumeLayout(false);
            this.pnlZanr.PerformLayout();
            this.spcSplitZanrovi.Panel1.ResumeLayout(false);
            this.spcSplitZanrovi.Panel2.ResumeLayout(false);
            this.spcSplitZanrovi.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcSplitZanrovi)).EndInit();
            this.spcSplitZanrovi.ResumeLayout(false);
            this.pnlGlumci.ResumeLayout(false);
            this.spcSplitGlumci.Panel1.ResumeLayout(false);
            this.spcSplitGlumci.Panel2.ResumeLayout(false);
            this.spcSplitGlumci.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcSplitGlumci)).EndInit();
            this.spcSplitGlumci.ResumeLayout(false);
            this.flpPretragaGlumci.ResumeLayout(false);
            this.flpPretragaGlumci.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgvFilmovi;
        private System.Windows.Forms.Button btnDodajFilm;
        private System.Windows.Forms.Button btnIzmijeniFilm;
        private System.Windows.Forms.Button btnAktivirajFilm;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.GroupBox grbPretragaFilmova;
        private System.Windows.Forms.TextBox tbGodina;
        private System.Windows.Forms.TextBox tbNaslovFilma;
        private System.Windows.Forms.BindingSource bsrFilmovi;
        private System.Windows.Forms.DataGridViewTextBoxColumn naslovDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn godinaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn slikaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn filmumagacinuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn glumacDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zanrDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel pnlZanr;
        private System.Windows.Forms.TextBox tbZanrPretraga;
        private System.Windows.Forms.Button btnZanr;
        private System.Windows.Forms.SplitContainer spcSplitZanrovi;
        private System.Windows.Forms.Button btnGlumci;
        private System.Windows.Forms.SplitContainer spcSplitGlumci;
        private System.Windows.Forms.ComboBox cmbNacinPretrageGlumci;
        private System.Windows.Forms.Label lblNacinPretrageGlumci;
        private System.Windows.Forms.Panel pnlGlumci;
        private System.Windows.Forms.ListView lvZanrovi;
        private System.Windows.Forms.ComboBox cmbNacinPretrageZanrovi;
        private System.Windows.Forms.Label lblNacinPretrageZanrovi;
        private System.Windows.Forms.CheckBox chbAktivanFilm;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnCijena;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnNaStanju;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnAktivanFilm;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.ColumnHeader clnNaziv;
        private System.Windows.Forms.ListView lvGlumci;
        private System.Windows.Forms.ColumnHeader clnPrezime;
        private System.Windows.Forms.ColumnHeader clnIme;
        private System.Windows.Forms.FlowLayoutPanel flpPretragaGlumci;
        private System.Windows.Forms.TextBox tbPrezimePretraga;
        private System.Windows.Forms.TextBox tbImePretraga;
    }
}