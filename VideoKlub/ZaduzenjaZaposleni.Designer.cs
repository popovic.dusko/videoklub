﻿namespace VideoKlub
{
    partial class ZaduzenjaZaposleni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZaduzenjaZaposleni));
            this.dgvZaduzenja = new System.Windows.Forms.DataGridView();
            this.clnBrojKartice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnNaslovFilma = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnNovoZaduzenje = new System.Windows.Forms.Button();
            this.btnNazad = new System.Windows.Forms.Button();
            this.grbPretragaZaduzenja = new System.Windows.Forms.GroupBox();
            this.chbNerazduzeno = new System.Windows.Forms.CheckBox();
            this.tbBrojKartice = new System.Windows.Forms.TextBox();
            this.tbNaslovFilma = new System.Windows.Forms.TextBox();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRazduzi = new System.Windows.Forms.Button();
            this.clnDatumZaduzenja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnDatumRazduzenja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ocjenafilmaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaposleniidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnKorisnikId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnFilmUMagacinuId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filmumagacinuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.korisnikDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaposleniDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsrZaduzenja = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvZaduzenja)).BeginInit();
            this.grbPretragaZaduzenja.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsrZaduzenja)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvZaduzenja
            // 
            this.dgvZaduzenja.AllowUserToAddRows = false;
            this.dgvZaduzenja.AllowUserToDeleteRows = false;
            this.dgvZaduzenja.AutoGenerateColumns = false;
            this.dgvZaduzenja.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvZaduzenja.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvZaduzenja.ColumnHeadersHeight = 30;
            this.dgvZaduzenja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvZaduzenja.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clnBrojKartice,
            this.clnNaslovFilma,
            this.clnDatumZaduzenja,
            this.clnDatumRazduzenja,
            this.ocjenafilmaDataGridViewTextBoxColumn,
            this.zaposleniidDataGridViewTextBoxColumn,
            this.clnKorisnikId,
            this.clnFilmUMagacinuId,
            this.filmumagacinuDataGridViewTextBoxColumn,
            this.korisnikDataGridViewTextBoxColumn,
            this.zaposleniDataGridViewTextBoxColumn});
            this.dgvZaduzenja.DataSource = this.bsrZaduzenja;
            this.dgvZaduzenja.Location = new System.Drawing.Point(12, 115);
            this.dgvZaduzenja.MultiSelect = false;
            this.dgvZaduzenja.Name = "dgvZaduzenja";
            this.dgvZaduzenja.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvZaduzenja.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvZaduzenja.RowHeadersVisible = false;
            this.dgvZaduzenja.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvZaduzenja.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvZaduzenja.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvZaduzenja.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvZaduzenja.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvZaduzenja.Size = new System.Drawing.Size(786, 252);
            this.dgvZaduzenja.TabIndex = 15;
            this.dgvZaduzenja.TabStop = false;
            this.dgvZaduzenja.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvZaduzenja_CellEnter);
            this.dgvZaduzenja.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DgvZaduzenja_CellFormatting);
            this.dgvZaduzenja.Scroll += new System.Windows.Forms.ScrollEventHandler(this.DgvZaduzenja_Scroll);
            // 
            // clnBrojKartice
            // 
            this.clnBrojKartice.HeaderText = "Broj kartice";
            this.clnBrojKartice.Name = "clnBrojKartice";
            this.clnBrojKartice.ReadOnly = true;
            this.clnBrojKartice.Width = 150;
            // 
            // clnNaslovFilma
            // 
            this.clnNaslovFilma.HeaderText = "Naslov filma";
            this.clnNaslovFilma.Name = "clnNaslovFilma";
            this.clnNaslovFilma.ReadOnly = true;
            this.clnNaslovFilma.Width = 380;
            // 
            // btnNovoZaduzenje
            // 
            this.btnNovoZaduzenje.FlatAppearance.BorderSize = 2;
            this.btnNovoZaduzenje.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNovoZaduzenje.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovoZaduzenje.Location = new System.Drawing.Point(12, 380);
            this.btnNovoZaduzenje.Name = "btnNovoZaduzenje";
            this.btnNovoZaduzenje.Size = new System.Drawing.Size(144, 58);
            this.btnNovoZaduzenje.TabIndex = 4;
            this.btnNovoZaduzenje.Text = "Novo &zaduženje";
            this.btnNovoZaduzenje.UseVisualStyleBackColor = true;
            this.btnNovoZaduzenje.Click += new System.EventHandler(this.BtnNovoZaduzenje_Click);
            // 
            // btnNazad
            // 
            this.btnNazad.FlatAppearance.BorderSize = 2;
            this.btnNazad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNazad.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNazad.Location = new System.Drawing.Point(654, 380);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(144, 58);
            this.btnNazad.TabIndex = 6;
            this.btnNazad.Text = "&Nazad";
            this.btnNazad.UseVisualStyleBackColor = true;
            this.btnNazad.Click += new System.EventHandler(this.BtnNazad_Click);
            // 
            // grbPretragaZaduzenja
            // 
            this.grbPretragaZaduzenja.Controls.Add(this.chbNerazduzeno);
            this.grbPretragaZaduzenja.Controls.Add(this.tbBrojKartice);
            this.grbPretragaZaduzenja.Controls.Add(this.tbNaslovFilma);
            this.grbPretragaZaduzenja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbPretragaZaduzenja.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbPretragaZaduzenja.Location = new System.Drawing.Point(12, 49);
            this.grbPretragaZaduzenja.Name = "grbPretragaZaduzenja";
            this.grbPretragaZaduzenja.Size = new System.Drawing.Size(786, 60);
            this.grbPretragaZaduzenja.TabIndex = 20;
            this.grbPretragaZaduzenja.TabStop = false;
            this.grbPretragaZaduzenja.Text = "Pretraga zaduženja";
            this.grbPretragaZaduzenja.Paint += new System.Windows.Forms.PaintEventHandler(this.GrbPretragaZaduzenja_Paint);
            // 
            // chbNerazduzeno
            // 
            this.chbNerazduzeno.AutoSize = true;
            this.chbNerazduzeno.Location = new System.Drawing.Point(654, 29);
            this.chbNerazduzeno.Name = "chbNerazduzeno";
            this.chbNerazduzeno.Size = new System.Drawing.Size(131, 23);
            this.chbNerazduzeno.TabIndex = 3;
            this.chbNerazduzeno.Tag = "Nerazduženo";
            this.chbNerazduzeno.Text = "Nerazduženo";
            this.chbNerazduzeno.UseVisualStyleBackColor = true;
            this.chbNerazduzeno.CheckedChanged += new System.EventHandler(this.ChbNerazduzeno_CheckedChanged);
            // 
            // tbBrojKartice
            // 
            this.tbBrojKartice.ForeColor = System.Drawing.Color.Silver;
            this.tbBrojKartice.Location = new System.Drawing.Point(6, 27);
            this.tbBrojKartice.MaxLength = 20;
            this.tbBrojKartice.Name = "tbBrojKartice";
            this.tbBrojKartice.Size = new System.Drawing.Size(141, 27);
            this.tbBrojKartice.TabIndex = 1;
            this.tbBrojKartice.Tag = "Broj kartice";
            this.tbBrojKartice.Text = "Broj kartice...";
            this.tbBrojKartice.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbBrojKartice.TextChanged += new System.EventHandler(this.TbBrojKartice_TextChanged);
            this.tbBrojKartice.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbBrojKartice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            this.tbBrojKartice.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // tbNaslovFilma
            // 
            this.tbNaslovFilma.ForeColor = System.Drawing.Color.Silver;
            this.tbNaslovFilma.Location = new System.Drawing.Point(157, 27);
            this.tbNaslovFilma.MaxLength = 255;
            this.tbNaslovFilma.Name = "tbNaslovFilma";
            this.tbNaslovFilma.Size = new System.Drawing.Size(370, 27);
            this.tbNaslovFilma.TabIndex = 2;
            this.tbNaslovFilma.Tag = "Naslov filma";
            this.tbNaslovFilma.Text = "Naslov filma...";
            this.tbNaslovFilma.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbNaslovFilma.TextChanged += new System.EventHandler(this.TbNaslovFilma_TextChanged);
            this.tbNaslovFilma.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbNaslovFilma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            this.tbNaslovFilma.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackgroundImage = global::VideoKlub.Properties.Resources.minimize_button;
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Location = new System.Drawing.Point(700, 3);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(50, 50);
            this.btnMinimize.TabIndex = 14;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::VideoKlub.Properties.Resources.close_button;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(756, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(50, 50);
            this.btnClose.TabIndex = 13;
            this.btnClose.TabStop = false;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnRazduzi
            // 
            this.btnRazduzi.FlatAppearance.BorderSize = 2;
            this.btnRazduzi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRazduzi.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRazduzi.Location = new System.Drawing.Point(196, 380);
            this.btnRazduzi.Name = "btnRazduzi";
            this.btnRazduzi.Size = new System.Drawing.Size(144, 58);
            this.btnRazduzi.TabIndex = 5;
            this.btnRazduzi.Text = "&Razduži";
            this.btnRazduzi.UseVisualStyleBackColor = true;
            this.btnRazduzi.EnabledChanged += new System.EventHandler(this.BtnRazduzi_EnabledChanged);
            this.btnRazduzi.Click += new System.EventHandler(this.BtnRazduzi_Click);
            // 
            // clnDatumZaduzenja
            // 
            this.clnDatumZaduzenja.DataPropertyName = "datum_zaduzenja";
            this.clnDatumZaduzenja.HeaderText = "Zaduženo";
            this.clnDatumZaduzenja.Name = "clnDatumZaduzenja";
            this.clnDatumZaduzenja.ReadOnly = true;
            this.clnDatumZaduzenja.ToolTipText = "Datum zaduženja";
            this.clnDatumZaduzenja.Width = 120;
            // 
            // clnDatumRazduzenja
            // 
            this.clnDatumRazduzenja.DataPropertyName = "datum_razduzenja";
            this.clnDatumRazduzenja.HeaderText = "Razduženo";
            this.clnDatumRazduzenja.Name = "clnDatumRazduzenja";
            this.clnDatumRazduzenja.ReadOnly = true;
            this.clnDatumRazduzenja.ToolTipText = "Datum razduženja";
            this.clnDatumRazduzenja.Width = 120;
            // 
            // ocjenafilmaDataGridViewTextBoxColumn
            // 
            this.ocjenafilmaDataGridViewTextBoxColumn.DataPropertyName = "ocjena_filma";
            this.ocjenafilmaDataGridViewTextBoxColumn.HeaderText = "ocjena_filma";
            this.ocjenafilmaDataGridViewTextBoxColumn.Name = "ocjenafilmaDataGridViewTextBoxColumn";
            this.ocjenafilmaDataGridViewTextBoxColumn.ReadOnly = true;
            this.ocjenafilmaDataGridViewTextBoxColumn.Visible = false;
            // 
            // zaposleniidDataGridViewTextBoxColumn
            // 
            this.zaposleniidDataGridViewTextBoxColumn.DataPropertyName = "zaposleni_id";
            this.zaposleniidDataGridViewTextBoxColumn.HeaderText = "zaposleni_id";
            this.zaposleniidDataGridViewTextBoxColumn.Name = "zaposleniidDataGridViewTextBoxColumn";
            this.zaposleniidDataGridViewTextBoxColumn.ReadOnly = true;
            this.zaposleniidDataGridViewTextBoxColumn.Visible = false;
            // 
            // clnKorisnikId
            // 
            this.clnKorisnikId.DataPropertyName = "korisnik_id";
            this.clnKorisnikId.HeaderText = "korisnik_id";
            this.clnKorisnikId.Name = "clnKorisnikId";
            this.clnKorisnikId.ReadOnly = true;
            this.clnKorisnikId.Visible = false;
            // 
            // clnFilmUMagacinuId
            // 
            this.clnFilmUMagacinuId.DataPropertyName = "film_u_magacinu_film_id";
            this.clnFilmUMagacinuId.HeaderText = "film_u_magacinu_film_id";
            this.clnFilmUMagacinuId.Name = "clnFilmUMagacinuId";
            this.clnFilmUMagacinuId.ReadOnly = true;
            this.clnFilmUMagacinuId.Visible = false;
            // 
            // filmumagacinuDataGridViewTextBoxColumn
            // 
            this.filmumagacinuDataGridViewTextBoxColumn.DataPropertyName = "film_u_magacinu";
            this.filmumagacinuDataGridViewTextBoxColumn.HeaderText = "film_u_magacinu";
            this.filmumagacinuDataGridViewTextBoxColumn.Name = "filmumagacinuDataGridViewTextBoxColumn";
            this.filmumagacinuDataGridViewTextBoxColumn.ReadOnly = true;
            this.filmumagacinuDataGridViewTextBoxColumn.Visible = false;
            // 
            // korisnikDataGridViewTextBoxColumn
            // 
            this.korisnikDataGridViewTextBoxColumn.DataPropertyName = "korisnik";
            this.korisnikDataGridViewTextBoxColumn.HeaderText = "korisnik";
            this.korisnikDataGridViewTextBoxColumn.Name = "korisnikDataGridViewTextBoxColumn";
            this.korisnikDataGridViewTextBoxColumn.ReadOnly = true;
            this.korisnikDataGridViewTextBoxColumn.Visible = false;
            // 
            // zaposleniDataGridViewTextBoxColumn
            // 
            this.zaposleniDataGridViewTextBoxColumn.DataPropertyName = "zaposleni";
            this.zaposleniDataGridViewTextBoxColumn.HeaderText = "zaposleni";
            this.zaposleniDataGridViewTextBoxColumn.Name = "zaposleniDataGridViewTextBoxColumn";
            this.zaposleniDataGridViewTextBoxColumn.ReadOnly = true;
            this.zaposleniDataGridViewTextBoxColumn.Visible = false;
            // 
            // bsrZaduzenja
            // 
            this.bsrZaduzenja.DataSource = typeof(VideoKlub.zaduzenje_filma);
            // 
            // ZaduzenjaZaposleni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(810, 450);
            this.Controls.Add(this.btnRazduzi);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grbPretragaZaduzenja);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.btnNovoZaduzenje);
            this.Controls.Add(this.dgvZaduzenja);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ZaduzenjaZaposleni";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZaduzenjaZaposleni";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Zaduzenja_FormClosing);
            this.Load += new System.EventHandler(this.Zaduzenja_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvZaduzenja)).EndInit();
            this.grbPretragaZaduzenja.ResumeLayout(false);
            this.grbPretragaZaduzenja.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsrZaduzenja)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgvZaduzenja;
        private System.Windows.Forms.Button btnNovoZaduzenje;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.GroupBox grbPretragaZaduzenja;
        private System.Windows.Forms.TextBox tbNaslovFilma;
        private System.Windows.Forms.TextBox tbBrojKartice;
        private System.Windows.Forms.CheckBox chbNerazduzeno;
        private System.Windows.Forms.BindingSource bsrZaduzenja;
        private System.Windows.Forms.Button btnRazduzi;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnBrojKartice;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnNaslovFilma;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnDatumZaduzenja;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnDatumRazduzenja;
        private System.Windows.Forms.DataGridViewTextBoxColumn ocjenafilmaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zaposleniidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnKorisnikId;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnFilmUMagacinuId;
        private System.Windows.Forms.DataGridViewTextBoxColumn filmumagacinuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn korisnikDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zaposleniDataGridViewTextBoxColumn;
    }
}