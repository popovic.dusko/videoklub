﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace VideoKlub
{
    public partial class PregledSlikeFilma : Form
    {
        public PregledSlikeFilma(string slika)
        {
            InitializeComponent();

            try
            {
                pbSlika.BackgroundImage = Image.FromFile(Application.StartupPath + "\\" + slika);
            }
            catch (FileNotFoundException)
            {
                pbSlika.BackgroundImage = Properties.Resources.logo;
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // za iscrtavanje granica forme
    }
}
