﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class NovoZaduzenje : Form
    {
        #region Promjenljive
        private int idZaposlenog;
        private List<string> brojeviKartica;
        private List<string> nasloviFilmova; // format - Naslov (godina)
        private List<string> zaduzeniNasloviFilmova = new List<string>();
        private ZaduzenjaUtil zaduzenjaUtil = new ZaduzenjaUtil();
        #endregion

        public NovoZaduzenje(int idZaposlenog)
        {
            InitializeComponent();

            this.idZaposlenog = idZaposlenog;

            brojeviKartica = zaduzenjaUtil.GetBrojeveKartica();

            nasloviFilmova = zaduzenjaUtil.GetNasloveFilmova();

            cmbBrojKartice.Items.AddRange(brojeviKartica.ToArray());

            cmbNaslovFilma.DataSource = nasloviFilmova;

            cmbNaslovFilma.SelectedIndex = -1;

            cmbNaslovFilma.Enabled = false;

            OmoguciDugmeDodaj();

            ActiveControl = cmbBrojKartice;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // za iscrtavanje granica forme
        private bool IsBrojKarticeValidan(string brojKartice)
        {
            return cmbBrojKartice.Items.Contains(brojKartice) ? true : false;
        } // koristeno za omogucavanje dugmeta Dodaj
        private bool IsNaslovFilmaValidan(string naslovFilma)
        {
            return cmbNaslovFilma.Items.Contains(naslovFilma) ? true : false;
        } // koristeno za omogucavanje dugmeta Dodaj

        private bool OmoguciDugmeDodaj()
        {
            if (!IsBrojKarticeValidan(cmbBrojKartice.Text) || (cmbNaslovFilma.Enabled && !IsNaslovFilmaValidan(cmbNaslovFilma.Text)))
            {
                btnDodaj.FlatAppearance.BorderColor = SystemColors.Control;
                return btnDodaj.Enabled = false;
            }
            else if (!cmbNaslovFilma.Enabled)
            {
                btnDodaj.FlatAppearance.BorderColor = SystemColors.Control;
                return btnDodaj.Enabled = false;
            }
            btnDodaj.FlatAppearance.BorderColor = Color.Black;
            return btnDodaj.Enabled = true;
        } // ukoliko potrebni podaci nisu uneseni dugme ce biti onemogucen

        private void CmbBrojKartice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lblKarticaNeaktivna.Visible)
            {
                lblKarticaNeaktivna.Text = "Kartica nije aktivna";
                lblKarticaNeaktivna.Visible = false;
            }

            if (zaduzeniNasloviFilmova.Count > 0)
            {
                foreach (string naslov in zaduzeniNasloviFilmova)
                {
                    if (!nasloviFilmova.Contains(naslov))
                    {
                        nasloviFilmova.Add(naslov);
                    }
                }
                nasloviFilmova = nasloviFilmova.OrderBy(nf => nf).ToList();

                zaduzeniNasloviFilmova.Clear();
            }

            cmbNaslovFilma.Text = "";
            cmbNaslovFilma.Enabled = false;
            cmbNaslovFilma.SelectedIndexChanged -= CmbNaslovFilma_SelectedIndexChanged;


            if (!zaduzenjaUtil.IsKarticaAktivna(cmbBrojKartice.Text))
            {
                lblKarticaNeaktivna.Visible = true;
                return;
            }

            List<string> zaduzenjaKorisnika = zaduzenjaUtil.GetZaduzeneFilmoveKorisnika(cmbBrojKartice.Text);

            if (zaduzenjaKorisnika != null) 
            {
                if ((zaduzenjaKorisnika.Count == 3))
                {
                    lblKarticaNeaktivna.Text = "Broj zaduženja je maksimalan";
                    lblKarticaNeaktivna.Visible = true;
                    return;
                }
                else
                {
                    foreach (string zaduzenje in zaduzenjaKorisnika)
                    {
                        zaduzeniNasloviFilmova.Add(zaduzenje);
                        nasloviFilmova.Remove(zaduzenje);
                    }
                }
            }

            cmbNaslovFilma.DataSource = null;
            cmbNaslovFilma.DataSource = nasloviFilmova;
            cmbNaslovFilma.SelectedIndex = -1;

            cmbNaslovFilma.Enabled = true;
            cmbNaslovFilma.SelectedIndexChanged += CmbNaslovFilma_SelectedIndexChanged;

            cmbNaslovFilma.Focus();

            OmoguciDugmeDodaj();
        }
        private void CmbBrojKartice_TextUpdate(object sender, EventArgs e)
        {
            if (lblKarticaNeaktivna.Visible)
            {
                lblKarticaNeaktivna.Visible = false;
            }

            ComboBox comboBox = sender as ComboBox;

            comboBox.TextUpdate -= CmbBrojKartice_TextUpdate;
            comboBox.SelectedIndexChanged -= CmbBrojKartice_SelectedIndexChanged;

            // provjera ispravnosti unosa
            var pozicijaKursora = comboBox.SelectionStart;
            comboBox.Text = Regex.Replace(comboBox.Text, "[^0-9a-zA-Z]", "").ToUpper();
            comboBox.SelectionStart = pozicijaKursora;

            comboBox.TextUpdate += CmbBrojKartice_TextUpdate;
            comboBox.SelectedIndexChanged += CmbBrojKartice_SelectedIndexChanged;

            OmoguciDugmeDodaj();
        }
        private void Cmb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == (Keys.Control))
            {
                e.SuppressKeyPress = true;
                if (e.KeyCode == Keys.D)
                {
                    btnDodaj.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    btnNazad.PerformClick();
                }
            }
        }

        private void CmbNaslovFilma_SelectedIndexChanged(object sender, EventArgs e)
        {
            OmoguciDugmeDodaj();
        }
        private void CmbNaslovFilma_TextUpdate(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            comboBox.TextUpdate -= CmbNaslovFilma_TextUpdate;

            // provjera ispravnosti unosa
            var pozicijaKursora = comboBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(comboBox.Text) && comboBox.Text[0] >= 'a' && comboBox.Text[0] <= 'z')
            {
                char[] tekst = comboBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                comboBox.Text = new string(tekst);
            }
            comboBox.Text = Regex.Replace(comboBox.Text, "[^0-9a-zA-ZšđčćžŠĐČĆŽ '.:!?,&()-]", "");
            comboBox.SelectionStart = pozicijaKursora;

            comboBox.TextUpdate += CmbNaslovFilma_TextUpdate;

            OmoguciDugmeDodaj();
        }

        private void BtnDodaj_Click(object sender, EventArgs e)
        {
            string brojKartice = cmbBrojKartice.Text;
            string naslovFilma = cmbNaslovFilma.Text;

            zaduzenjaUtil.Zaduzi(brojKartice, naslovFilma, idZaposlenog);

            DialogResult = DialogResult.Yes;
        }
    }
}
