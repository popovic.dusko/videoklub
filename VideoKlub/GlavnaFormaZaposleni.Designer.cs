﻿namespace VideoKlub
{
    partial class GlavnaFormaZaposleni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GlavnaFormaZaposleni));
            this.btnZaduzenja = new System.Windows.Forms.Button();
            this.btnKatalogFilmova = new System.Windows.Forms.Button();
            this.btnFilmovi = new System.Windows.Forms.Button();
            this.btnKorisnici = new System.Windows.Forms.Button();
            this.lblZaposleni = new System.Windows.Forms.Label();
            this.btnOdjava = new System.Windows.Forms.Button();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnZaduzenja
            // 
            this.btnZaduzenja.FlatAppearance.BorderSize = 2;
            this.btnZaduzenja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZaduzenja.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZaduzenja.Location = new System.Drawing.Point(414, 169);
            this.btnZaduzenja.Name = "btnZaduzenja";
            this.btnZaduzenja.Size = new System.Drawing.Size(326, 55);
            this.btnZaduzenja.TabIndex = 10;
            this.btnZaduzenja.Text = "&Zaduženja";
            this.btnZaduzenja.UseVisualStyleBackColor = true;
            this.btnZaduzenja.Click += new System.EventHandler(this.BtnZaduzenja_Click);
            // 
            // btnKatalogFilmova
            // 
            this.btnKatalogFilmova.FlatAppearance.BorderSize = 2;
            this.btnKatalogFilmova.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKatalogFilmova.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKatalogFilmova.Location = new System.Drawing.Point(414, 97);
            this.btnKatalogFilmova.Name = "btnKatalogFilmova";
            this.btnKatalogFilmova.Size = new System.Drawing.Size(326, 58);
            this.btnKatalogFilmova.TabIndex = 9;
            this.btnKatalogFilmova.Text = "&Katalog filmova";
            this.btnKatalogFilmova.UseVisualStyleBackColor = true;
            this.btnKatalogFilmova.Click += new System.EventHandler(this.BtnKatalogFilmova_Click);
            // 
            // btnFilmovi
            // 
            this.btnFilmovi.FlatAppearance.BorderSize = 2;
            this.btnFilmovi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFilmovi.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFilmovi.Location = new System.Drawing.Point(414, 241);
            this.btnFilmovi.Name = "btnFilmovi";
            this.btnFilmovi.Size = new System.Drawing.Size(326, 55);
            this.btnFilmovi.TabIndex = 13;
            this.btnFilmovi.Text = "&Filmovi";
            this.btnFilmovi.UseVisualStyleBackColor = true;
            this.btnFilmovi.Click += new System.EventHandler(this.BtnFilmovi_Click);
            // 
            // btnKorisnici
            // 
            this.btnKorisnici.FlatAppearance.BorderSize = 2;
            this.btnKorisnici.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKorisnici.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKorisnici.Location = new System.Drawing.Point(414, 313);
            this.btnKorisnici.Name = "btnKorisnici";
            this.btnKorisnici.Size = new System.Drawing.Size(326, 55);
            this.btnKorisnici.TabIndex = 14;
            this.btnKorisnici.Text = "Ko&risnici";
            this.btnKorisnici.UseVisualStyleBackColor = true;
            this.btnKorisnici.Click += new System.EventHandler(this.BtnKorisnici_Click);
            // 
            // lblZaposleni
            // 
            this.lblZaposleni.AutoSize = true;
            this.lblZaposleni.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZaposleni.Location = new System.Drawing.Point(54, 13);
            this.lblZaposleni.Name = "lblZaposleni";
            this.lblZaposleni.Size = new System.Drawing.Size(101, 24);
            this.lblZaposleni.TabIndex = 15;
            this.lblZaposleni.Text = "Prijavljen:";
            // 
            // btnOdjava
            // 
            this.btnOdjava.FlatAppearance.BorderSize = 0;
            this.btnOdjava.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.btnOdjava.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOdjava.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOdjava.Location = new System.Drawing.Point(161, 18);
            this.btnOdjava.Name = "btnOdjava";
            this.btnOdjava.Size = new System.Drawing.Size(70, 23);
            this.btnOdjava.TabIndex = 16;
            this.btnOdjava.Text = "&Odjavi se";
            this.btnOdjava.UseVisualStyleBackColor = true;
            this.btnOdjava.Click += new System.EventHandler(this.BtnOdjava_Click);
            this.btnOdjava.MouseEnter += new System.EventHandler(this.BtnOdjava_MouseEnter);
            this.btnOdjava.MouseLeave += new System.EventHandler(this.BtnOdjava_MouseLeave);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackgroundImage = global::VideoKlub.Properties.Resources.minimize_button;
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Location = new System.Drawing.Point(690, 3);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(50, 50);
            this.btnMinimize.TabIndex = 12;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::VideoKlub.Properties.Resources.close_button;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(746, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(50, 50);
            this.btnClose.TabIndex = 11;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // pbLogo
            // 
            this.pbLogo.BackgroundImage = global::VideoKlub.Properties.Resources.logo;
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbLogo.Location = new System.Drawing.Point(54, 70);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(300, 300);
            this.pbLogo.TabIndex = 8;
            this.pbLogo.TabStop = false;
            // 
            // GlavnaFormaZaposleni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnOdjava);
            this.Controls.Add(this.lblZaposleni);
            this.Controls.Add(this.btnKorisnici);
            this.Controls.Add(this.btnFilmovi);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnZaduzenja);
            this.Controls.Add(this.btnKatalogFilmova);
            this.Controls.Add(this.pbLogo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GlavnaFormaZaposleni";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GlavnaFormaZaposleni";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GlavnaFormaZaposleni_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnZaduzenja;
        private System.Windows.Forms.Button btnKatalogFilmova;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Button btnFilmovi;
        private System.Windows.Forms.Button btnKorisnici;
        private System.Windows.Forms.Label lblZaposleni;
        private System.Windows.Forms.Button btnOdjava;
    }
}