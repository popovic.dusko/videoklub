﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class DodajFilm : Form
    {
        #region Promjenljive
        private int idDodanogFilma;
        private bool tbFokusiranProgramski = false;
        private bool lvGlumciSimuliraniKeyDown = false;
        private bool lvZanroviSimuliraniKeyDown = false;
        private List<string> cekiraniGlumci = new List<string>();
        private List<string> cekiraniZanrovi = new List<string>();
        private FilmoviUtil filmoviUtil = new FilmoviUtil();
        #endregion

        public DodajFilm()
        {
            InitializeComponent();

            nudCijena.TextChanged += Nud_TextChanged;
            nudGodina.TextChanged += Nud_TextChanged;
            nudNaStanju.TextChanged += Nud_TextChanged;

            nudGodina.Value = DateTime.Now.Year;

            ActiveControl = tbNaslov;
        }
        public DialogResult ShowDialog(out int idDodanogFilma)
        {
            DialogResult dialogResult = ShowDialog();
            if (DialogResult.Yes == dialogResult)
            {
                idDodanogFilma = this.idDodanogFilma;
                return DialogResult.Yes;
            }
            else if (DialogResult.Retry == dialogResult)
            {
                idDodanogFilma = this.idDodanogFilma;
                return DialogResult.Retry;
            }
            idDodanogFilma = -1;
            return DialogResult.Cancel;
        } // prilagodjena metoda ShowDialog kako bi se znalo koji je ID dodanog filma
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);
        } // za iscrtavanje granica forme
        private void DodajFilm_Load(object sender, EventArgs e)
        {
            OmoguciDugmeDodaj();
            UcitajZanrove();
            UcitajGlumce();
            OmoguciDugmeUkloniZanr();
            OmoguciDugmeUkloniGlumca();
        }

        private void ZatamniFormu()
        {
            BackColor = SystemColors.Control;
            lvGlumci.BackColor = SystemColors.Control;
            lvZanrovi.BackColor = SystemColors.Control;
        }
        private void OsvijetliFormu()
        {
            BackColor = SystemColors.Window;
            lvGlumci.BackColor = SystemColors.Window;
            lvZanrovi.BackColor = SystemColors.Window;
        }

        #region GroupBox
        private void Grb_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Black, Color.Black);
        }
        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Brisanje teksta i ivica
                g.Clear(this.BackColor);

                // Crtanje teksta
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Crtanje ivica
                //Lijeva
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Desna
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Donja
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Gornja1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Gornja2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        #endregion // iscrtavanje groupBox-ova

        #region Zanrovi
        private bool OmoguciDugmeUkloniZanr()
        {
            if (lvZanrovi.SelectedIndices.Count == 0)
            {
                btnUkloniZanr.FlatAppearance.BorderColor = SystemColors.Control;
                return btnUkloniZanr.Enabled = false;
            }
            else
            {
                btnUkloniZanr.FlatAppearance.BorderColor = Color.Black;
                return btnUkloniZanr.Enabled = true;
            }

        }
        private void UcitajZanrove()
        {
            lvZanrovi.Items.Clear();
            lvZanrovi.Groups.Clear();

            List<kategorija_zanra> kategorijeZanra = filmoviUtil.GetKategorijeZanra();

            foreach (var kategorijaZanra in kategorijeZanra)
            {
                ListViewGroup lvg = new ListViewGroup
                {
                    Header = kategorijaZanra.opis,
                    Name = "lvg" + kategorijaZanra.opis.Replace(" ", ""),
                };

                lvZanrovi.Groups.Add(lvg);

                List<zanr> zanroviIzKategorije = filmoviUtil.GetZanroveIzKategorije(kategorijaZanra.id);

                if (zanroviIzKategorije != null && zanroviIzKategorije.Count != 0)
                {
                    foreach (var zanr in zanroviIzKategorije)
                    {
                        ListViewItem item = new ListViewItem
                        {
                            Group = lvg,
                            Name = "lvi" + zanr.naziv.Replace(" ", "") + kategorijaZanra.opis.Replace(" ", "") + zanr.id,
                            Text = zanr.naziv,
                            Tag = kategorijaZanra.opis
                        };

                        if (cekiraniZanrovi.Contains(item.Name))
                        {
                            item.Checked = true;
                        }

                        lvZanrovi.Items.Add(item);
                    }
                }
            }
        }
        private void PretragaZanrova()
        {
            string nazivZanra = tbZanrPretraga.Text.EndsWith("...") ? "" : tbZanrPretraga.Text;

            lvZanrovi.ItemChecked -= LvZanrovi_ItemChecked;

            lvZanrovi.Items.Clear();
            lvZanrovi.Groups.Clear();

            List<kategorija_zanra> kategorijeZanra = filmoviUtil.GetKategorijeZanra();

            foreach (var kategorijaZanra in kategorijeZanra)
            {
                ListViewGroup lvg = new ListViewGroup
                {
                    Header = kategorijaZanra.opis,
                    Name = "lvg" + kategorijaZanra.opis.Replace(" ", ""),
                };

                lvZanrovi.Groups.Add(lvg);

                List<zanr> zanroviIzKategorije = filmoviUtil.GetZanroveIzKategorijeUzPretragu(kategorijaZanra.id, nazivZanra);

                if (zanroviIzKategorije != null && zanroviIzKategorije.Count != 0)
                {
                    foreach (var zanr in zanroviIzKategorije)
                    {
                        ListViewItem item = new ListViewItem
                        {
                            Group = lvg,
                            Name = "lvi" + zanr.naziv.Replace(" ", "") + kategorijaZanra.opis.Replace(" ", "") + zanr.id,
                            Text = zanr.naziv,
                            Tag = kategorijaZanra.opis
                        };

                        if (cekiraniZanrovi.Contains(item.Name))
                        {
                            item.Checked = true;
                        }

                        lvZanrovi.Items.Add(item);
                    }
                }
            }

            if (lvZanrovi.Items.Count > 0)
            {
                lvZanrovi.Items[0].Selected = true;
                lvZanrovi.TopItem = lvZanrovi.Items[0];
                lvZanrovi.EnsureVisible(0);
            }
            else
            {
                OmoguciDugmeUkloniZanr();
            }

            lvZanrovi.ItemChecked += LvZanrovi_ItemChecked;
        }
        private bool IsPrviElemenatUGrupiZanrovi()
        {
            if (lvZanrovi.SelectedItems.Count > 0)
            {
                var item = lvZanrovi.SelectedItems[0];

                string grupa = "lvg" + item.Name.Replace("lvi" + item.Text, "");
                grupa = Regex.Replace(grupa, "[0-9]", "");

                if (lvZanrovi.Groups[grupa].Items[0] == item)
                {
                    return true;
                }
            }

            return false;
        }
        private bool IsPoslednjiElemenatUGrupiZanrovi()
        {
            if (lvZanrovi.SelectedItems.Count > 0)
            {
                var item = lvZanrovi.SelectedItems[0];

                string grupa = "lvg" + item.Name.Replace("lvi" + item.Text, "");
                grupa = Regex.Replace(grupa, "[0-9]", "");

                if (lvZanrovi.Groups[grupa].Items[lvZanrovi.Groups[grupa].Items.Count - 1] == item)
                {
                    return true;
                }
            }
            return false;
        }

        #region LvZanrovi
        private void LvZanrovi_SelectedIndexChanged(object sender, EventArgs e)
        {
            OmoguciDugmeUkloniZanr();
        }
        private void LvZanrovi_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                e.Item.Selected = true;
                if (!cekiraniZanrovi.Contains(e.Item.Name))
                {
                    cekiraniZanrovi.Add(e.Item.Name);
                }
            }
            else
            {
                cekiraniZanrovi.Remove(e.Item.Name);
            }
        }
        private void LvZanrovi_KeyDown(object sender, KeyEventArgs e)
        {
            if (lvZanroviSimuliraniKeyDown)
            {
                lvZanroviSimuliraniKeyDown = false;
                return;
            }
            if (e.KeyCode == Keys.Up)
            {
                if (IsPrviElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;

                    SendKeys.SendWait("{UP}");
                }
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (IsPoslednjiElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;

                    SendKeys.SendWait("{DOWN}");
                }
            }
            else if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodajZanr.PerformClick();
                }
                else if (e.KeyCode == Keys.U)
                {
                    e.SuppressKeyPress = true;
                    btnUkloniZanr.PerformClick();
                }
            }
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodaj.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
        }
        #endregion

        #region TbZanrPretraga
        private void TbZanrPretraga_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbZanrPretraga_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }

            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                textBox.Text = "Žanr...";
                textBox.ForeColor = Color.Silver;
            }

            PretragaZanrova();

            textBox.TextChanged += TbZanrPretraga_TextChanged;
        }
        private void TbZanrPretraga_KeyDown(object sender, KeyEventArgs e)
        {
            // ako je textBox fokusiran i pritisnut je taster Up vrsi se selekcija zanra koji se nalazi iznad vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi zanr u listi
            if (e.KeyCode == Keys.Up)
            {
                e.SuppressKeyPress = true;

                lvZanrovi.Focus();

                if (IsPrviElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;
                    SendKeys.SendWait("{UP}");
                }

                lvZanroviSimuliraniKeyDown = true;
                SendKeys.SendWait("{UP}");

                tbFokusiranProgramski = true;
                tbZanrPretraga.Focus();
            }
            // ako je neki textBox fokusiran i pritisnut je taster Down vrsi se selekcija zanra koji se nalazi ispod vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi zanr u listi
            else if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;

                lvZanrovi.Focus();

                if (IsPoslednjiElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;
                    SendKeys.SendWait("{DOWN}");
                }

                lvZanroviSimuliraniKeyDown = true;
                SendKeys.SendWait("{DOWN}");

                tbFokusiranProgramski = true;
                tbZanrPretraga.Focus();

            }
            else if (e.KeyCode == Keys.Right)
            {
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Left)
            {
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;

                if (lvZanrovi.SelectedItems.Count > 0)
                {
                    lvZanrovi.SelectedItems[0].Checked = !lvZanrovi.SelectedItems[0].Checked;
                }
            }
            // ako je fokusiran neki textBox kombinacijom tastera Control i odredjenih tastera simulira se klik na odgovarajuci button
            else if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodajZanr.PerformClick();
                }
                else if (e.KeyCode == Keys.U)
                {
                    e.SuppressKeyPress = true;
                    btnUkloniZanr.PerformClick();
                }
            }
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodaj.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
        }
        #endregion

        private void BtnDodajZanr_Click(object sender, EventArgs e)
        {
            ZatamniFormu();

            string nazivZanra, kategorijaZanra;

            if (DialogResult.Yes == new DodajZanr().ShowDialog(out nazivZanra, out kategorijaZanra))
            {
                //UcitajZanrove();
                tbZanrPretraga.Text = "";

                foreach (ListViewItem item in lvZanrovi.Items)
                {
                    if (item.Text == nazivZanra && item.Group.Header == kategorijaZanra)
                    {
                        item.Checked = true;
                        item.Selected = true;
                        lvZanrovi.TopItem = item;
                        lvZanrovi.EnsureVisible(item.Index);
                    }
                }
            }

            OsvijetliFormu();
        }
        private void BtnUkloniZanr_Click(object sender, EventArgs e)
        {
            ListViewItem item = lvZanrovi.SelectedItems[0];
            string nazivZanra = item.Text;
            string kategorijaZanra = item.Group.Header;

            bool isZanrKoristen = filmoviUtil.IsZanrKoristen(kategorijaZanra, nazivZanra);

            ZatamniFormu();

            if (!isZanrKoristen)
            {
                if (DialogResult.Yes == new DijalogUpozorenje("Da li zaista želite ukloniti žanr?", "Žanr će biti permanentno uklonjen!").ShowDialog())
                {
                    if (lvZanrovi.SelectedIndices[0] > 0)
                    {
                        lvZanrovi.Items[lvZanrovi.SelectedIndices[0] - 1].Selected = true;
                    }
                    else if (lvZanrovi.SelectedIndices[0] == 0 && lvZanrovi.Items.Count > 1)
                    {
                        lvZanrovi.Items[1].Selected = true;
                    }

                    lvZanrovi.Items.Remove(item);

                    filmoviUtil.UkloniZanr(kategorijaZanra, nazivZanra);
                }
            }
            else
            {
                new DijalogGreska("Nije moguće ukloniti žanr!", "Žanr je korišten u drugim filmovima!").ShowDialog();
            }

            OsvijetliFormu();
        }

        #endregion

        #region Glumci
        private bool OmoguciDugmeUkloniGlumca()
        {
            if (lvGlumci.SelectedIndices.Count == 0)
            {
                btnUkloniGlumca.FlatAppearance.BorderColor = SystemColors.Control;
                return btnUkloniGlumca.Enabled = false;
            }
            else
            {
                btnUkloniGlumca.FlatAppearance.BorderColor = Color.Black;
                return btnUkloniGlumca.Enabled = true;
            }

        }
        private void UcitajGlumce()
        {

            lvGlumci.Items.Clear();
            lvGlumci.Groups.Clear();

            List<glumac> glumci = filmoviUtil.GetGlumci();

            foreach (var glumac in glumci)
            {
                ListViewGroup lvg = lvGlumci.Groups["lvg" + glumac.prezime[0]];

                if (lvg == null)
                {
                    lvg = new ListViewGroup
                    {
                        Header = glumac.prezime[0].ToString(),
                        Name = "lvg" + glumac.prezime[0]
                    };

                    lvGlumci.Groups.Add(lvg);
                }

                ListViewItem item = new ListViewItem
                {
                    Group = lvg,
                    Name = "lvi" + glumac.ime.Trim() + glumac.prezime.Trim() + glumac.id,
                    Text = glumac.prezime
                };

                item.SubItems.Add(glumac.ime);

                if (cekiraniGlumci.Contains(item.Name))
                {
                    item.Checked = true;
                }

                lvGlumci.Items.Add(item);
            }
        }
        private void PretragaGlumaca()
        {
            string ime, prezime;

            ime = tbIme.Text.EndsWith("...") ? "" : tbIme.Text;
            prezime = tbPrezime.Text.EndsWith("...") ? "" : tbPrezime.Text;

            lvGlumci.ItemChecked -= LvGlumci_ItemChecked;

            lvGlumci.Items.Clear();
            lvGlumci.Groups.Clear();

            List<glumac> glumci = filmoviUtil.GetGlumciUzPretragu(prezime, ime);

            foreach (var glumac in glumci)
            {
                ListViewGroup lvg = lvGlumci.Groups["lvg" + glumac.prezime[0]];

                if (lvg == null)
                {
                    lvg = new ListViewGroup
                    {
                        Header = glumac.prezime[0].ToString(),
                        Name = "lvg" + glumac.prezime[0]
                    };

                    lvGlumci.Groups.Add(lvg);
                }

                ListViewItem item = new ListViewItem
                {
                    Group = lvg,
                    Name = "lvi" + glumac.ime.Trim() + glumac.prezime.Trim() + glumac.id,
                    Text = glumac.prezime
                };

                item.SubItems.Add(glumac.ime);

                if (cekiraniGlumci.Contains(item.Name))
                {
                    item.Checked = true;
                }

                lvGlumci.Items.Add(item);
            }

            if (lvGlumci.Items.Count > 0)
            {
                lvGlumci.Items[0].Selected = true;
                lvGlumci.TopItem = lvGlumci.Items[0];
                lvGlumci.EnsureVisible(0);
            }
            else
            {
                OmoguciDugmeUkloniGlumca();
            }

            lvGlumci.ItemChecked += LvGlumci_ItemChecked;
        }
        private bool IsPrviElemenatUGrupiGlumci()
        {
            if (lvGlumci.SelectedItems.Count > 0)
            {
                var item = lvGlumci.SelectedItems[0];

                string grupa = "lvg" + item.Text[0];

                if (lvGlumci.Groups[grupa].Items[0] == item)
                {
                    return true;
                }
            }

            return false;
        }
        private bool IsPoslednjiElemenatUGrupiGlumci()
        {
            if (lvGlumci.SelectedItems.Count > 0)
            {
                var item = lvGlumci.SelectedItems[0];

                string grupa = "lvg" + item.Text[0];

                if (lvGlumci.Groups[grupa].Items[lvGlumci.Groups[grupa].Items.Count - 1] == item)
                {
                    return true;
                }
            }
            return false;
        }

        #region LvGlumci
        private void LvGlumci_SelectedIndexChanged(object sender, EventArgs e)
        {
            OmoguciDugmeUkloniGlumca();
        }
        private void LvGlumci_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                e.Item.Selected = true;
                if (!cekiraniGlumci.Contains(e.Item.Name))
                {
                    cekiraniGlumci.Add(e.Item.Name);
                }
            }
            else
            {
                cekiraniGlumci.Remove(e.Item.Name);
            }
        }
        private void LvGlumci_KeyDown(object sender, KeyEventArgs e)
        {
            if (lvGlumciSimuliraniKeyDown)
            {
                lvGlumciSimuliraniKeyDown = false;
                return;
            }
            if (e.KeyCode == Keys.Up)
            {
                if (IsPrviElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;

                    SendKeys.SendWait("{UP}");
                }
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (IsPoslednjiElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;

                    SendKeys.SendWait("{DOWN}");
                }
            }
            else if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodajGlumca.PerformClick();
                }
                else if (e.KeyCode == Keys.U)
                {
                    e.SuppressKeyPress = true;
                    btnUkloniGlumca.PerformClick();
                }
            }
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodaj.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
        }
        #endregion

        #region TbPrezime
        private void TbPrezime_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbPrezime_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("Prezime..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }
            else if (textBox.Text.EndsWith("Ime..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }

            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ. '-]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                if (textBox.Tag.ToString().Equals("Prezime"))
                {
                    textBox.Text = "Prezime...";
                }
                else if (textBox.Tag.ToString().Equals("Ime"))
                {
                    textBox.Text = "Ime...";
                }
                textBox.ForeColor = Color.LightGray;
            }

            PretragaGlumaca();

            textBox.TextChanged += TbPrezime_TextChanged;
        }
        private void TbPrezime_KeyDown(object sender, KeyEventArgs e) // manipulacija redoslijedom selekcije glumaca u listView-u
        {
            TextBox textBox = sender as TextBox;
            // ako je neki textBox fokusiran i pritisnut je taster Up vrsi se selekcija glumca koji se nalazi iznad vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi glumac u listi
            if (e.KeyCode == Keys.Up)
            {
                e.SuppressKeyPress = true;

                lvGlumci.Focus();

                if (IsPrviElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;
                    SendKeys.SendWait("{UP}");
                }

                lvGlumciSimuliraniKeyDown = true;
                SendKeys.SendWait("{UP}");

                tbFokusiranProgramski = true;
                textBox.Focus();
            }
            // ako je neki textBox fokusiran i pritisnut je taster Down vrsi se selekcija glumca koji se nalazi ispod vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi glumac u listi
            else if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;

                lvGlumci.Focus();

                if (IsPoslednjiElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;
                    SendKeys.SendWait("{DOWN}");
                }

                lvGlumciSimuliraniKeyDown = true;
                SendKeys.SendWait("{DOWN}");

                tbFokusiranProgramski = true;
                textBox.Focus();

            }
            // ako je pritisnut taster Right fokusiramo komponentu koja se nalazi desno od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Right)
            {
                e.SuppressKeyPress = true;

                if (textBox.Tag.ToString() == "Prezime")
                {
                    tbIme.Focus();
                }
            }
            // ako je pritisnut taster Left fokusiramo komponentu koja se nalazi lijevo od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Left)
            {
                e.SuppressKeyPress = true;

                if (textBox.Tag.ToString() == "Ime")
                {
                    tbPrezime.Focus();
                }
            }
            else if (e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;

                if (lvGlumci.SelectedItems.Count > 0)
                {
                    lvGlumci.SelectedItems[0].Checked = !lvGlumci.SelectedItems[0].Checked;
                }
            }
            // ako je fokusiran neki textBox kombinacijom tastera Control i odredjenih tastera simulira se klik na odgovarajuci button
            else if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodajGlumca.PerformClick();
                }
                else if (e.KeyCode == Keys.U)
                {
                    e.SuppressKeyPress = true;
                    btnUkloniGlumca.PerformClick();
                }
            }
            // Control + Shift
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodaj.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
        }
        #endregion

        private void BtnDodajGlumca_Click(object sender, EventArgs e)
        {
            ZatamniFormu();

            int idGlumca;

            if (DialogResult.Yes == new DodajGlumca().ShowDialog(out idGlumca))
            {
                //UcitajGlumce();
                tbIme.Text = tbPrezime.Text = "";

                foreach (ListViewItem item in lvGlumci.Items)
                {
                    string id = Regex.Replace(item.Name, "[^0-9]", "");
                    if (id == idGlumca.ToString())
                    {
                        item.Checked = true;
                        item.Selected = true;
                        lvGlumci.TopItem = item;
                        lvGlumci.EnsureVisible(item.Index);
                    }
                }
            }

            OsvijetliFormu();
        }
        private void BtnUkloniGlumca_Click(object sender, EventArgs e)
        {
            ListViewItem item = lvGlumci.SelectedItems[0];

            int idGlumca = int.Parse(Regex.Replace(item.Name, "[^0-9]", ""));

            bool isGlumacKoristen = filmoviUtil.IsGlumacKoristen(idGlumca);

            ZatamniFormu();

            if (!isGlumacKoristen)
            {
                if (DialogResult.Yes == new DijalogUpozorenje("Da li zaista želite ukloniti glumca?", "Glumac će biti permanentno uklonjen!").ShowDialog())
                {
                    if (lvGlumci.SelectedIndices[0] > 0)
                    {
                        lvGlumci.Items[lvGlumci.SelectedIndices[0] - 1].Selected = true;
                    }
                    else if (lvGlumci.SelectedIndices[0] == 0 && lvGlumci.Items.Count > 1)
                    {
                        lvGlumci.Items[1].Selected = true;
                    }

                    lvGlumci.Items.Remove(item);

                    filmoviUtil.UkloniGlumca(idGlumca);
                }
            }
            else
            {
                new DijalogGreska("Nije moguće ukloniti glumca!", "Glumac je korišten u drugim filmovima!").ShowDialog();
            }

            OsvijetliFormu();
        }

        #endregion

        #region Slika
        private void BtnBrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.RestoreDirectory = true;
                ofd.Filter = "Image Files (*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF";
                ofd.Multiselect = false;

                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    if (ofd.FileName.StartsWith(Application.StartupPath + "\\" + "Resources"))
                    {
                        tbSlika.Text = ofd.FileName.Replace(Application.StartupPath + "\\", "");

                        pbSlika.BackgroundImage = Image.FromFile(ofd.FileName);
                        pbSlika.Tag = tbSlika.Text;

                        btnUkloniSliku.Visible = true;
                    }
                }
            }
        }
        private void PbSlika_Click(object sender, EventArgs e)
        {
            if (pbSlika.Tag.ToString() == "Logo")
            {
                return;
            }

            ZatamniFormu();

            new PregledSlikeFilma(pbSlika.Tag.ToString()).ShowDialog();

            OsvijetliFormu();
        }
        private void BtnUkloniSliku_Click(object sender, EventArgs e)
        {
            tbSlika.Text = "";
            pbSlika.BackgroundImage = Properties.Resources.logo;
            pbSlika.Tag = "Logo";
            btnUkloniSliku.Visible = false;
        }

        #endregion

        private bool OmoguciDugmeDodaj()
        {
            if (string.IsNullOrEmpty(tbNaslov.Text) || string.IsNullOrEmpty(nudGodina.Text)
                || string.IsNullOrEmpty(nudCijena.Text) || string.IsNullOrEmpty(nudNaStanju.Text))
            {
                btnDodaj.FlatAppearance.BorderColor = SystemColors.Control;
                return btnDodaj.Enabled = false;
            }
            btnDodaj.FlatAppearance.BorderColor = Color.Black;
            return btnDodaj.Enabled = true;
        } // ukoliko potrebni podaci nisu uneseni dugme ce biti onemogucen

        private void TbNaslov_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            var pozicijaKursora = textBox.SelectionStart;
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-Z0-9šđčćžŠĐČĆŽ '.:?!,&-]", "");
            textBox.SelectionStart = pozicijaKursora;

            OmoguciDugmeDodaj();
        }
        private void Nud_TextChanged(object sender, EventArgs e)
        {
            OmoguciDugmeDodaj();
        }

        private void Tb_EnterFocus(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // onemogucavanje selekcije placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.SelectionStart = 0;
            }
            else
            {
                if (tbFokusiranProgramski == false)
                {
                    textBox.SelectAll();
                }
                else
                {
                    tbFokusiranProgramski = false;
                }
            }
        }
        private void Tb_MouseUp(object sender, MouseEventArgs e)
        {
            // selekcija ili kompletnog sadrzaja textBox-a ili nista
            TextBox textBox = sender as TextBox;
            if (textBox.Text.EndsWith("..."))
            {
                textBox.SelectionLength = 0;
                textBox.SelectionStart = 0;
            }
            if (!textBox.Text.EndsWith("...") && textBox.SelectionLength == textBox.TextLength)
            {
                textBox.SelectAll();
            }
            else if (textBox.SelectionLength > 0)
            {
                textBox.SelectionLength = 0;
                textBox.SelectionStart = 0;
            }
        }
        private void Nud_Enter(object sender, EventArgs e)
        {
            NumericUpDown nud = sender as NumericUpDown;

            nud.Select(0, nud.Text.Length);
        }
        private void Nud_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                btnDodaj.PerformClick();
            }
        }

        private void BtnDodaj_Click(object sender, EventArgs e)
        {
            string naslov = tbNaslov.Text;
            int godina = (int)nudGodina.Value;

            if(filmoviUtil.IsFilmPostojeci(naslov, godina))
            {
                ZatamniFormu();

                if (DialogResult.Yes == new DijalogUpozorenje($"Film \"{naslov}\" ({godina}) već postoji!", "Pregledati informacije o filmu?").ShowDialog())
                {
                        idDodanogFilma = filmoviUtil.GetIdFilma(naslov, godina);
                        DialogResult = DialogResult.Retry;
                }

                OsvijetliFormu();

                DialogResult = DialogResult.Retry;
                return;
            }

            string slika = string.IsNullOrEmpty(tbSlika.Text) ? null : tbSlika.Text;

            List<zanr> zanrovi = null;

            if (cekiraniZanrovi.Count > 0)
            {
                zanrovi = new List<zanr>();
                foreach (string zanr in cekiraniZanrovi)
                {
                    int idZanra = int.Parse(Regex.Replace(zanr, "[^0-9]", ""));
                    zanrovi.Add(filmoviUtil.GetZanr(idZanra));
                }
            }

            List<glumac> glumci = null;

            if (cekiraniGlumci.Count > 0)
            {
                glumci = new List<glumac>();
                foreach (string glumac in cekiraniGlumci)
                {
                    int idGlumca = int.Parse(Regex.Replace(glumac, "[^0-9]", ""));
                    glumci.Add(filmoviUtil.GetGlumac(idGlumca));
                }
            }

            decimal cijena = nudCijena.Value;
            int naStanju = (int)nudNaStanju.Value;

            idDodanogFilma = filmoviUtil.DodajFilm(naslov, godina, slika, zanrovi, glumci, cijena, naStanju);

            DialogResult = DialogResult.Yes;
        }

    }
}
