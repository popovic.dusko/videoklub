﻿namespace VideoKlub
{
    partial class KatalogFilmova
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KatalogFilmova));
            this.tbFilmoviPretraga = new System.Windows.Forms.TextBox();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.flpFilteriPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlPoredaj = new System.Windows.Forms.Panel();
            this.lblPoredajPo = new System.Windows.Forms.Label();
            this.cmbPoredajPo = new System.Windows.Forms.ComboBox();
            this.pnlGodinaFilma = new System.Windows.Forms.Panel();
            this.lblGodinaFilma = new System.Windows.Forms.Label();
            this.lblGodinaOd = new System.Windows.Forms.Label();
            this.nudGodinaOd = new System.Windows.Forms.NumericUpDown();
            this.lblGodinaDo = new System.Windows.Forms.Label();
            this.nudGodinaDo = new System.Windows.Forms.NumericUpDown();
            this.pnlOcjenaFilma = new System.Windows.Forms.Panel();
            this.lblOcjenaFilma = new System.Windows.Forms.Label();
            this.lblOcjenaOd = new System.Windows.Forms.Label();
            this.nudOcjenaOd = new System.Windows.Forms.NumericUpDown();
            this.lblOcjenaDo = new System.Windows.Forms.Label();
            this.nudOcjenaDo = new System.Windows.Forms.NumericUpDown();
            this.pnlCijenaFilma = new System.Windows.Forms.Panel();
            this.lblCijenaFilma = new System.Windows.Forms.Label();
            this.lblCijenaOd = new System.Windows.Forms.Label();
            this.nudCijenaOd = new System.Windows.Forms.NumericUpDown();
            this.lblCijenaDo = new System.Windows.Forms.Label();
            this.nudCijenaDo = new System.Windows.Forms.NumericUpDown();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.flpFilmoviPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.flpFilteri2 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlFilterZanr = new System.Windows.Forms.Panel();
            this.lblFilterZanr = new System.Windows.Forms.Label();
            this.tbZanrPretraga = new System.Windows.Forms.TextBox();
            this.lvZanrovi = new System.Windows.Forms.ListView();
            this.clnNaziv = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlNacinPretrageZanrovi = new System.Windows.Forms.Panel();
            this.cmbNacinPretrageZanrovi = new System.Windows.Forms.ComboBox();
            this.lblNacinPretrageZanrovi = new System.Windows.Forms.Label();
            this.pnlFilterGlumci = new System.Windows.Forms.Panel();
            this.lblFilterGlumci = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbImePretraga = new System.Windows.Forms.TextBox();
            this.tbPrezimePretraga = new System.Windows.Forms.TextBox();
            this.lvGlumci = new System.Windows.Forms.ListView();
            this.clnPrezime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnIme = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbNacinPretrageGlumci = new System.Windows.Forms.ComboBox();
            this.lblNacinPretrageGlumci = new System.Windows.Forms.Label();
            this.pbPretraga = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.flpFilteriPanel.SuspendLayout();
            this.pnlPoredaj.SuspendLayout();
            this.pnlGodinaFilma.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodinaOd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodinaDo)).BeginInit();
            this.pnlOcjenaFilma.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudOcjenaOd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOcjenaDo)).BeginInit();
            this.pnlCijenaFilma.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCijenaOd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCijenaDo)).BeginInit();
            this.flpFilteri2.SuspendLayout();
            this.pnlFilterZanr.SuspendLayout();
            this.pnlNacinPretrageZanrovi.SuspendLayout();
            this.pnlFilterGlumci.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPretraga)).BeginInit();
            this.SuspendLayout();
            // 
            // tbFilmoviPretraga
            // 
            this.tbFilmoviPretraga.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFilmoviPretraga.ForeColor = System.Drawing.Color.Gray;
            this.tbFilmoviPretraga.Location = new System.Drawing.Point(222, 67);
            this.tbFilmoviPretraga.Name = "tbFilmoviPretraga";
            this.tbFilmoviPretraga.Size = new System.Drawing.Size(560, 33);
            this.tbFilmoviPretraga.TabIndex = 1;
            this.tbFilmoviPretraga.Text = "Pretraži filmove...";
            this.tbFilmoviPretraga.TextChanged += new System.EventHandler(this.TbFilmoviPretraga_TextChanged);
            this.tbFilmoviPretraga.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbFilmoviPretraga.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // pbLogo
            // 
            this.pbLogo.BackgroundImage = global::VideoKlub.Properties.Resources.logo;
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbLogo.Location = new System.Drawing.Point(36, 14);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(125, 125);
            this.pbLogo.TabIndex = 0;
            this.pbLogo.TabStop = false;
            // 
            // flpFilteriPanel
            // 
            this.flpFilteriPanel.BackColor = System.Drawing.SystemColors.HighlightText;
            this.flpFilteriPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpFilteriPanel.Controls.Add(this.pnlPoredaj);
            this.flpFilteriPanel.Controls.Add(this.cmbPoredajPo);
            this.flpFilteriPanel.Controls.Add(this.pnlGodinaFilma);
            this.flpFilteriPanel.Controls.Add(this.lblGodinaOd);
            this.flpFilteriPanel.Controls.Add(this.nudGodinaOd);
            this.flpFilteriPanel.Controls.Add(this.lblGodinaDo);
            this.flpFilteriPanel.Controls.Add(this.nudGodinaDo);
            this.flpFilteriPanel.Controls.Add(this.pnlOcjenaFilma);
            this.flpFilteriPanel.Controls.Add(this.lblOcjenaOd);
            this.flpFilteriPanel.Controls.Add(this.nudOcjenaOd);
            this.flpFilteriPanel.Controls.Add(this.lblOcjenaDo);
            this.flpFilteriPanel.Controls.Add(this.nudOcjenaDo);
            this.flpFilteriPanel.Controls.Add(this.pnlCijenaFilma);
            this.flpFilteriPanel.Controls.Add(this.lblCijenaOd);
            this.flpFilteriPanel.Controls.Add(this.nudCijenaOd);
            this.flpFilteriPanel.Controls.Add(this.lblCijenaDo);
            this.flpFilteriPanel.Controls.Add(this.nudCijenaDo);
            this.flpFilteriPanel.Location = new System.Drawing.Point(13, 145);
            this.flpFilteriPanel.Name = "flpFilteriPanel";
            this.flpFilteriPanel.Size = new System.Drawing.Size(173, 482);
            this.flpFilteriPanel.TabIndex = 3;
            // 
            // pnlPoredaj
            // 
            this.pnlPoredaj.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlPoredaj.Controls.Add(this.lblPoredajPo);
            this.pnlPoredaj.Location = new System.Drawing.Point(1, 3);
            this.pnlPoredaj.Margin = new System.Windows.Forms.Padding(1, 3, 3, 3);
            this.pnlPoredaj.Name = "pnlPoredaj";
            this.pnlPoredaj.Size = new System.Drawing.Size(168, 25);
            this.pnlPoredaj.TabIndex = 0;
            // 
            // lblPoredajPo
            // 
            this.lblPoredajPo.AutoSize = true;
            this.lblPoredajPo.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoredajPo.Location = new System.Drawing.Point(4, 4);
            this.lblPoredajPo.Name = "lblPoredajPo";
            this.lblPoredajPo.Size = new System.Drawing.Size(100, 18);
            this.lblPoredajPo.TabIndex = 0;
            this.lblPoredajPo.Text = "POREDAJ PO";
            // 
            // cmbPoredajPo
            // 
            this.cmbPoredajPo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPoredajPo.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPoredajPo.FormattingEnabled = true;
            this.cmbPoredajPo.Items.AddRange(new object[] {
            "Naslovu",
            "Najnoviji filmovi",
            "Najstariji filmovi",
            "Ocjeni - najnižoj",
            "Ocjeni - najvišoj",
            "Cijeni - najnižoj",
            "Cijeni - najvišoj"});
            this.cmbPoredajPo.Location = new System.Drawing.Point(7, 34);
            this.cmbPoredajPo.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.cmbPoredajPo.Name = "cmbPoredajPo";
            this.cmbPoredajPo.Size = new System.Drawing.Size(158, 28);
            this.cmbPoredajPo.TabIndex = 1;
            this.cmbPoredajPo.SelectedIndexChanged += new System.EventHandler(this.CmbPoredajPo_SelectedIndexChanged);
            // 
            // pnlGodinaFilma
            // 
            this.pnlGodinaFilma.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlGodinaFilma.Controls.Add(this.lblGodinaFilma);
            this.pnlGodinaFilma.Location = new System.Drawing.Point(1, 75);
            this.pnlGodinaFilma.Margin = new System.Windows.Forms.Padding(1, 10, 3, 3);
            this.pnlGodinaFilma.Name = "pnlGodinaFilma";
            this.pnlGodinaFilma.Size = new System.Drawing.Size(168, 25);
            this.pnlGodinaFilma.TabIndex = 2;
            // 
            // lblGodinaFilma
            // 
            this.lblGodinaFilma.AutoSize = true;
            this.lblGodinaFilma.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGodinaFilma.Location = new System.Drawing.Point(4, 4);
            this.lblGodinaFilma.Name = "lblGodinaFilma";
            this.lblGodinaFilma.Size = new System.Drawing.Size(118, 18);
            this.lblGodinaFilma.TabIndex = 0;
            this.lblGodinaFilma.Text = "GODINA FILMA";
            // 
            // lblGodinaOd
            // 
            this.lblGodinaOd.AutoSize = true;
            this.lblGodinaOd.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGodinaOd.Location = new System.Drawing.Point(3, 108);
            this.lblGodinaOd.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.lblGodinaOd.Name = "lblGodinaOd";
            this.lblGodinaOd.Size = new System.Drawing.Size(32, 20);
            this.lblGodinaOd.TabIndex = 3;
            this.lblGodinaOd.Text = "Od";
            // 
            // nudGodinaOd
            // 
            this.nudGodinaOd.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudGodinaOd.Location = new System.Drawing.Point(41, 106);
            this.nudGodinaOd.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudGodinaOd.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.nudGodinaOd.Name = "nudGodinaOd";
            this.nudGodinaOd.Size = new System.Drawing.Size(120, 26);
            this.nudGodinaOd.TabIndex = 4;
            this.nudGodinaOd.Tag = "GodinaOd";
            this.nudGodinaOd.Value = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.nudGodinaOd.ValueChanged += new System.EventHandler(this.Nud_ValueChanged);
            this.nudGodinaOd.Click += new System.EventHandler(this.Nud_Enter);
            this.nudGodinaOd.Enter += new System.EventHandler(this.Nud_Enter);
            // 
            // lblGodinaDo
            // 
            this.lblGodinaDo.AutoSize = true;
            this.lblGodinaDo.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGodinaDo.Location = new System.Drawing.Point(3, 138);
            this.lblGodinaDo.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.lblGodinaDo.Name = "lblGodinaDo";
            this.lblGodinaDo.Size = new System.Drawing.Size(30, 20);
            this.lblGodinaDo.TabIndex = 5;
            this.lblGodinaDo.Text = "Do";
            // 
            // nudGodinaDo
            // 
            this.nudGodinaDo.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudGodinaDo.Location = new System.Drawing.Point(41, 138);
            this.nudGodinaDo.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.nudGodinaDo.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudGodinaDo.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.nudGodinaDo.Name = "nudGodinaDo";
            this.nudGodinaDo.Size = new System.Drawing.Size(120, 26);
            this.nudGodinaDo.TabIndex = 6;
            this.nudGodinaDo.Tag = "GodinaDo";
            this.nudGodinaDo.Value = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.nudGodinaDo.ValueChanged += new System.EventHandler(this.Nud_ValueChanged);
            this.nudGodinaDo.Click += new System.EventHandler(this.Nud_Enter);
            this.nudGodinaDo.Enter += new System.EventHandler(this.Nud_Enter);
            // 
            // pnlOcjenaFilma
            // 
            this.pnlOcjenaFilma.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlOcjenaFilma.Controls.Add(this.lblOcjenaFilma);
            this.pnlOcjenaFilma.Location = new System.Drawing.Point(1, 177);
            this.pnlOcjenaFilma.Margin = new System.Windows.Forms.Padding(1, 10, 3, 3);
            this.pnlOcjenaFilma.Name = "pnlOcjenaFilma";
            this.pnlOcjenaFilma.Size = new System.Drawing.Size(168, 25);
            this.pnlOcjenaFilma.TabIndex = 7;
            // 
            // lblOcjenaFilma
            // 
            this.lblOcjenaFilma.AutoSize = true;
            this.lblOcjenaFilma.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOcjenaFilma.Location = new System.Drawing.Point(4, 4);
            this.lblOcjenaFilma.Name = "lblOcjenaFilma";
            this.lblOcjenaFilma.Size = new System.Drawing.Size(117, 18);
            this.lblOcjenaFilma.TabIndex = 0;
            this.lblOcjenaFilma.Text = "OCJENA FILMA";
            // 
            // lblOcjenaOd
            // 
            this.lblOcjenaOd.AutoSize = true;
            this.lblOcjenaOd.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOcjenaOd.Location = new System.Drawing.Point(3, 210);
            this.lblOcjenaOd.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.lblOcjenaOd.Name = "lblOcjenaOd";
            this.lblOcjenaOd.Size = new System.Drawing.Size(32, 20);
            this.lblOcjenaOd.TabIndex = 8;
            this.lblOcjenaOd.Text = "Od";
            // 
            // nudOcjenaOd
            // 
            this.nudOcjenaOd.DecimalPlaces = 1;
            this.nudOcjenaOd.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudOcjenaOd.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudOcjenaOd.Location = new System.Drawing.Point(41, 208);
            this.nudOcjenaOd.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudOcjenaOd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudOcjenaOd.Name = "nudOcjenaOd";
            this.nudOcjenaOd.Size = new System.Drawing.Size(120, 26);
            this.nudOcjenaOd.TabIndex = 9;
            this.nudOcjenaOd.Tag = "OcjenaOd";
            this.nudOcjenaOd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudOcjenaOd.ValueChanged += new System.EventHandler(this.Nud_ValueChanged);
            this.nudOcjenaOd.Click += new System.EventHandler(this.Nud_Enter);
            this.nudOcjenaOd.Enter += new System.EventHandler(this.Nud_Enter);
            this.nudOcjenaOd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Nud_KeyDown);
            // 
            // lblOcjenaDo
            // 
            this.lblOcjenaDo.AutoSize = true;
            this.lblOcjenaDo.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOcjenaDo.Location = new System.Drawing.Point(3, 240);
            this.lblOcjenaDo.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.lblOcjenaDo.Name = "lblOcjenaDo";
            this.lblOcjenaDo.Size = new System.Drawing.Size(30, 20);
            this.lblOcjenaDo.TabIndex = 10;
            this.lblOcjenaDo.Text = "Do";
            // 
            // nudOcjenaDo
            // 
            this.nudOcjenaDo.DecimalPlaces = 1;
            this.nudOcjenaDo.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudOcjenaDo.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudOcjenaDo.Location = new System.Drawing.Point(41, 240);
            this.nudOcjenaDo.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.nudOcjenaDo.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudOcjenaDo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudOcjenaDo.Name = "nudOcjenaDo";
            this.nudOcjenaDo.Size = new System.Drawing.Size(120, 26);
            this.nudOcjenaDo.TabIndex = 11;
            this.nudOcjenaDo.Tag = "OcjenaDo";
            this.nudOcjenaDo.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudOcjenaDo.ValueChanged += new System.EventHandler(this.Nud_ValueChanged);
            this.nudOcjenaDo.Click += new System.EventHandler(this.Nud_Enter);
            this.nudOcjenaDo.Enter += new System.EventHandler(this.Nud_Enter);
            this.nudOcjenaDo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Nud_KeyDown);
            // 
            // pnlCijenaFilma
            // 
            this.pnlCijenaFilma.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlCijenaFilma.Controls.Add(this.lblCijenaFilma);
            this.pnlCijenaFilma.Location = new System.Drawing.Point(1, 279);
            this.pnlCijenaFilma.Margin = new System.Windows.Forms.Padding(1, 10, 3, 3);
            this.pnlCijenaFilma.Name = "pnlCijenaFilma";
            this.pnlCijenaFilma.Size = new System.Drawing.Size(168, 25);
            this.pnlCijenaFilma.TabIndex = 12;
            // 
            // lblCijenaFilma
            // 
            this.lblCijenaFilma.AutoSize = true;
            this.lblCijenaFilma.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCijenaFilma.Location = new System.Drawing.Point(4, 4);
            this.lblCijenaFilma.Name = "lblCijenaFilma";
            this.lblCijenaFilma.Size = new System.Drawing.Size(108, 18);
            this.lblCijenaFilma.TabIndex = 0;
            this.lblCijenaFilma.Text = "CIJENA FILMA";
            // 
            // lblCijenaOd
            // 
            this.lblCijenaOd.AutoSize = true;
            this.lblCijenaOd.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCijenaOd.Location = new System.Drawing.Point(3, 312);
            this.lblCijenaOd.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.lblCijenaOd.Name = "lblCijenaOd";
            this.lblCijenaOd.Size = new System.Drawing.Size(32, 20);
            this.lblCijenaOd.TabIndex = 13;
            this.lblCijenaOd.Text = "Od";
            // 
            // nudCijenaOd
            // 
            this.nudCijenaOd.DecimalPlaces = 2;
            this.nudCijenaOd.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCijenaOd.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudCijenaOd.Location = new System.Drawing.Point(41, 310);
            this.nudCijenaOd.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudCijenaOd.Name = "nudCijenaOd";
            this.nudCijenaOd.Size = new System.Drawing.Size(120, 26);
            this.nudCijenaOd.TabIndex = 14;
            this.nudCijenaOd.Tag = "CijenaOd";
            this.nudCijenaOd.ValueChanged += new System.EventHandler(this.Nud_ValueChanged);
            this.nudCijenaOd.Click += new System.EventHandler(this.Nud_Enter);
            this.nudCijenaOd.Enter += new System.EventHandler(this.Nud_Enter);
            this.nudCijenaOd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Nud_KeyDown);
            // 
            // lblCijenaDo
            // 
            this.lblCijenaDo.AutoSize = true;
            this.lblCijenaDo.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCijenaDo.Location = new System.Drawing.Point(3, 342);
            this.lblCijenaDo.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.lblCijenaDo.Name = "lblCijenaDo";
            this.lblCijenaDo.Size = new System.Drawing.Size(30, 20);
            this.lblCijenaDo.TabIndex = 15;
            this.lblCijenaDo.Text = "Do";
            // 
            // nudCijenaDo
            // 
            this.nudCijenaDo.DecimalPlaces = 2;
            this.nudCijenaDo.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudCijenaDo.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudCijenaDo.Location = new System.Drawing.Point(41, 342);
            this.nudCijenaDo.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.nudCijenaDo.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nudCijenaDo.Name = "nudCijenaDo";
            this.nudCijenaDo.Size = new System.Drawing.Size(120, 26);
            this.nudCijenaDo.TabIndex = 16;
            this.nudCijenaDo.Tag = "CijenaDo";
            this.nudCijenaDo.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudCijenaDo.ValueChanged += new System.EventHandler(this.Nud_ValueChanged);
            this.nudCijenaDo.Click += new System.EventHandler(this.Nud_Enter);
            this.nudCijenaDo.Enter += new System.EventHandler(this.Nud_Enter);
            this.nudCijenaDo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Nud_KeyDown);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackgroundImage = global::VideoKlub.Properties.Resources.minimize_button;
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Location = new System.Drawing.Point(967, 3);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(50, 50);
            this.btnMinimize.TabIndex = 16;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::VideoKlub.Properties.Resources.close_button;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(1023, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(50, 50);
            this.btnClose.TabIndex = 15;
            this.btnClose.TabStop = false;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // flpFilmoviPanel
            // 
            this.flpFilmoviPanel.AutoScroll = true;
            this.flpFilmoviPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpFilmoviPanel.Location = new System.Drawing.Point(193, 145);
            this.flpFilmoviPanel.Name = "flpFilmoviPanel";
            this.flpFilmoviPanel.Size = new System.Drawing.Size(622, 482);
            this.flpFilmoviPanel.TabIndex = 17;
            this.flpFilmoviPanel.Scroll += new System.Windows.Forms.ScrollEventHandler(this.FlpFilmoviPanel_Scroll);
            // 
            // flpFilteri2
            // 
            this.flpFilteri2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpFilteri2.Controls.Add(this.pnlFilterZanr);
            this.flpFilteri2.Controls.Add(this.tbZanrPretraga);
            this.flpFilteri2.Controls.Add(this.lvZanrovi);
            this.flpFilteri2.Controls.Add(this.pnlNacinPretrageZanrovi);
            this.flpFilteri2.Controls.Add(this.pnlFilterGlumci);
            this.flpFilteri2.Controls.Add(this.panel1);
            this.flpFilteri2.Controls.Add(this.lvGlumci);
            this.flpFilteri2.Controls.Add(this.panel2);
            this.flpFilteri2.Location = new System.Drawing.Point(821, 145);
            this.flpFilteri2.Name = "flpFilteri2";
            this.flpFilteri2.Size = new System.Drawing.Size(245, 482);
            this.flpFilteri2.TabIndex = 18;
            // 
            // pnlFilterZanr
            // 
            this.pnlFilterZanr.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlFilterZanr.Controls.Add(this.lblFilterZanr);
            this.pnlFilterZanr.Location = new System.Drawing.Point(1, 3);
            this.pnlFilterZanr.Margin = new System.Windows.Forms.Padding(1, 3, 3, 3);
            this.pnlFilterZanr.Name = "pnlFilterZanr";
            this.pnlFilterZanr.Size = new System.Drawing.Size(242, 25);
            this.pnlFilterZanr.TabIndex = 1;
            // 
            // lblFilterZanr
            // 
            this.lblFilterZanr.AutoSize = true;
            this.lblFilterZanr.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilterZanr.Location = new System.Drawing.Point(4, 4);
            this.lblFilterZanr.Name = "lblFilterZanr";
            this.lblFilterZanr.Size = new System.Drawing.Size(75, 18);
            this.lblFilterZanr.TabIndex = 0;
            this.lblFilterZanr.Text = "ŽANROVI";
            // 
            // tbZanrPretraga
            // 
            this.tbZanrPretraga.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbZanrPretraga.ForeColor = System.Drawing.Color.Gray;
            this.tbZanrPretraga.Location = new System.Drawing.Point(7, 34);
            this.tbZanrPretraga.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.tbZanrPretraga.Name = "tbZanrPretraga";
            this.tbZanrPretraga.Size = new System.Drawing.Size(227, 26);
            this.tbZanrPretraga.TabIndex = 2;
            this.tbZanrPretraga.Text = "Žanr...";
            this.tbZanrPretraga.TextChanged += new System.EventHandler(this.TbZanrPretraga_TextChanged);
            this.tbZanrPretraga.Enter += new System.EventHandler(this.TbPretraga_EnterFocus);
            this.tbZanrPretraga.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbZanrPretraga_KeyDown);
            this.tbZanrPretraga.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // lvZanrovi
            // 
            this.lvZanrovi.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvZanrovi.CheckBoxes = true;
            this.lvZanrovi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clnNaziv});
            this.lvZanrovi.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvZanrovi.FullRowSelect = true;
            this.lvZanrovi.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvZanrovi.HideSelection = false;
            this.lvZanrovi.Location = new System.Drawing.Point(7, 63);
            this.lvZanrovi.Margin = new System.Windows.Forms.Padding(7, 0, 3, 0);
            this.lvZanrovi.MultiSelect = false;
            this.lvZanrovi.Name = "lvZanrovi";
            this.lvZanrovi.ShowItemToolTips = true;
            this.lvZanrovi.Size = new System.Drawing.Size(227, 136);
            this.lvZanrovi.TabIndex = 3;
            this.lvZanrovi.TabStop = false;
            this.lvZanrovi.UseCompatibleStateImageBehavior = false;
            this.lvZanrovi.View = System.Windows.Forms.View.Details;
            this.lvZanrovi.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.LvZanrovi_ItemChecked);
            this.lvZanrovi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LvZanrovi_KeyDown);
            // 
            // clnNaziv
            // 
            this.clnNaziv.Width = 205;
            // 
            // pnlNacinPretrageZanrovi
            // 
            this.pnlNacinPretrageZanrovi.Controls.Add(this.cmbNacinPretrageZanrovi);
            this.pnlNacinPretrageZanrovi.Controls.Add(this.lblNacinPretrageZanrovi);
            this.pnlNacinPretrageZanrovi.Location = new System.Drawing.Point(7, 200);
            this.pnlNacinPretrageZanrovi.Margin = new System.Windows.Forms.Padding(7, 1, 3, 3);
            this.pnlNacinPretrageZanrovi.Name = "pnlNacinPretrageZanrovi";
            this.pnlNacinPretrageZanrovi.Size = new System.Drawing.Size(227, 37);
            this.pnlNacinPretrageZanrovi.TabIndex = 4;
            // 
            // cmbNacinPretrageZanrovi
            // 
            this.cmbNacinPretrageZanrovi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNacinPretrageZanrovi.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNacinPretrageZanrovi.FormattingEnabled = true;
            this.cmbNacinPretrageZanrovi.Items.AddRange(new object[] {
            "I (And)",
            "Ili (Or)"});
            this.cmbNacinPretrageZanrovi.Location = new System.Drawing.Point(145, 4);
            this.cmbNacinPretrageZanrovi.Name = "cmbNacinPretrageZanrovi";
            this.cmbNacinPretrageZanrovi.Size = new System.Drawing.Size(82, 29);
            this.cmbNacinPretrageZanrovi.TabIndex = 6;
            this.cmbNacinPretrageZanrovi.TabStop = false;
            this.cmbNacinPretrageZanrovi.SelectedIndexChanged += new System.EventHandler(this.CmbNacinPretrageZanrovi_SelectedIndexChanged);
            this.cmbNacinPretrageZanrovi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CmbNacinPretrageZanrovi_KeyDown);
            // 
            // lblNacinPretrageZanrovi
            // 
            this.lblNacinPretrageZanrovi.AutoSize = true;
            this.lblNacinPretrageZanrovi.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacinPretrageZanrovi.Location = new System.Drawing.Point(-1, 8);
            this.lblNacinPretrageZanrovi.Name = "lblNacinPretrageZanrovi";
            this.lblNacinPretrageZanrovi.Size = new System.Drawing.Size(125, 20);
            this.lblNacinPretrageZanrovi.TabIndex = 1;
            this.lblNacinPretrageZanrovi.Text = "Način pretrage:";
            // 
            // pnlFilterGlumci
            // 
            this.pnlFilterGlumci.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlFilterGlumci.Controls.Add(this.lblFilterGlumci);
            this.pnlFilterGlumci.Location = new System.Drawing.Point(1, 243);
            this.pnlFilterGlumci.Margin = new System.Windows.Forms.Padding(1, 3, 3, 3);
            this.pnlFilterGlumci.Name = "pnlFilterGlumci";
            this.pnlFilterGlumci.Size = new System.Drawing.Size(242, 25);
            this.pnlFilterGlumci.TabIndex = 5;
            // 
            // lblFilterGlumci
            // 
            this.lblFilterGlumci.AutoSize = true;
            this.lblFilterGlumci.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilterGlumci.Location = new System.Drawing.Point(4, 4);
            this.lblFilterGlumci.Name = "lblFilterGlumci";
            this.lblFilterGlumci.Size = new System.Drawing.Size(68, 18);
            this.lblFilterGlumci.TabIndex = 0;
            this.lblFilterGlumci.Text = "GLUMCI";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tbImePretraga);
            this.panel1.Controls.Add(this.tbPrezimePretraga);
            this.panel1.Location = new System.Drawing.Point(0, 271);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(242, 30);
            this.panel1.TabIndex = 6;
            // 
            // tbImePretraga
            // 
            this.tbImePretraga.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbImePretraga.ForeColor = System.Drawing.Color.Gray;
            this.tbImePretraga.Location = new System.Drawing.Point(121, 1);
            this.tbImePretraga.Name = "tbImePretraga";
            this.tbImePretraga.Size = new System.Drawing.Size(113, 26);
            this.tbImePretraga.TabIndex = 4;
            this.tbImePretraga.Tag = "Ime";
            this.tbImePretraga.Text = "Ime...";
            this.tbImePretraga.TextChanged += new System.EventHandler(this.TbPrezime_TextChanged);
            this.tbImePretraga.Enter += new System.EventHandler(this.TbPretraga_EnterFocus);
            this.tbImePretraga.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbPrezime_KeyDown);
            this.tbImePretraga.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // tbPrezimePretraga
            // 
            this.tbPrezimePretraga.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPrezimePretraga.ForeColor = System.Drawing.Color.Gray;
            this.tbPrezimePretraga.Location = new System.Drawing.Point(7, 1);
            this.tbPrezimePretraga.Name = "tbPrezimePretraga";
            this.tbPrezimePretraga.Size = new System.Drawing.Size(113, 26);
            this.tbPrezimePretraga.TabIndex = 3;
            this.tbPrezimePretraga.Tag = "Prezime";
            this.tbPrezimePretraga.Text = "Prezime...";
            this.tbPrezimePretraga.TextChanged += new System.EventHandler(this.TbPrezime_TextChanged);
            this.tbPrezimePretraga.Enter += new System.EventHandler(this.TbPretraga_EnterFocus);
            this.tbPrezimePretraga.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbPrezime_KeyDown);
            this.tbPrezimePretraga.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // lvGlumci
            // 
            this.lvGlumci.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvGlumci.CheckBoxes = true;
            this.lvGlumci.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clnPrezime,
            this.clnIme});
            this.lvGlumci.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvGlumci.FullRowSelect = true;
            this.lvGlumci.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvGlumci.HideSelection = false;
            this.lvGlumci.Location = new System.Drawing.Point(7, 301);
            this.lvGlumci.Margin = new System.Windows.Forms.Padding(7, 0, 3, 0);
            this.lvGlumci.MultiSelect = false;
            this.lvGlumci.Name = "lvGlumci";
            this.lvGlumci.ShowItemToolTips = true;
            this.lvGlumci.Size = new System.Drawing.Size(227, 136);
            this.lvGlumci.TabIndex = 7;
            this.lvGlumci.TabStop = false;
            this.lvGlumci.UseCompatibleStateImageBehavior = false;
            this.lvGlumci.View = System.Windows.Forms.View.Details;
            this.lvGlumci.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.LvGlumci_ItemChecked);
            this.lvGlumci.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LvGlumci_KeyDown);
            // 
            // clnPrezime
            // 
            this.clnPrezime.Width = 102;
            // 
            // clnIme
            // 
            this.clnIme.Width = 102;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cmbNacinPretrageGlumci);
            this.panel2.Controls.Add(this.lblNacinPretrageGlumci);
            this.panel2.Location = new System.Drawing.Point(7, 438);
            this.panel2.Margin = new System.Windows.Forms.Padding(7, 1, 3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(227, 37);
            this.panel2.TabIndex = 8;
            // 
            // cmbNacinPretrageGlumci
            // 
            this.cmbNacinPretrageGlumci.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNacinPretrageGlumci.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNacinPretrageGlumci.FormattingEnabled = true;
            this.cmbNacinPretrageGlumci.Items.AddRange(new object[] {
            "I (And)",
            "Ili (Or)"});
            this.cmbNacinPretrageGlumci.Location = new System.Drawing.Point(145, 4);
            this.cmbNacinPretrageGlumci.Name = "cmbNacinPretrageGlumci";
            this.cmbNacinPretrageGlumci.Size = new System.Drawing.Size(82, 29);
            this.cmbNacinPretrageGlumci.TabIndex = 6;
            this.cmbNacinPretrageGlumci.TabStop = false;
            this.cmbNacinPretrageGlumci.SelectedIndexChanged += new System.EventHandler(this.CmbNacinPretrageGlumci_SelectedIndexChanged);
            this.cmbNacinPretrageGlumci.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CmbNacinPretrageGlumci_KeyDown);
            // 
            // lblNacinPretrageGlumci
            // 
            this.lblNacinPretrageGlumci.AutoSize = true;
            this.lblNacinPretrageGlumci.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNacinPretrageGlumci.Location = new System.Drawing.Point(-4, 8);
            this.lblNacinPretrageGlumci.Name = "lblNacinPretrageGlumci";
            this.lblNacinPretrageGlumci.Size = new System.Drawing.Size(125, 20);
            this.lblNacinPretrageGlumci.TabIndex = 1;
            this.lblNacinPretrageGlumci.Text = "Način pretrage:";
            // 
            // pbPretraga
            // 
            this.pbPretraga.BackgroundImage = global::VideoKlub.Properties.Resources.search;
            this.pbPretraga.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbPretraga.Location = new System.Drawing.Point(746, 68);
            this.pbPretraga.Name = "pbPretraga";
            this.pbPretraga.Size = new System.Drawing.Size(35, 31);
            this.pbPretraga.TabIndex = 19;
            this.pbPretraga.TabStop = false;
            // 
            // KatalogFilmova
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1078, 646);
            this.Controls.Add(this.pbPretraga);
            this.Controls.Add(this.flpFilteri2);
            this.Controls.Add(this.flpFilmoviPanel);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.flpFilteriPanel);
            this.Controls.Add(this.tbFilmoviPretraga);
            this.Controls.Add(this.pbLogo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "KatalogFilmova";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KatalogFilmova";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.KatalogFilmova_FormClosing);
            this.Shown += new System.EventHandler(this.KatalogFilmova_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.flpFilteriPanel.ResumeLayout(false);
            this.flpFilteriPanel.PerformLayout();
            this.pnlPoredaj.ResumeLayout(false);
            this.pnlPoredaj.PerformLayout();
            this.pnlGodinaFilma.ResumeLayout(false);
            this.pnlGodinaFilma.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodinaOd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodinaDo)).EndInit();
            this.pnlOcjenaFilma.ResumeLayout(false);
            this.pnlOcjenaFilma.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudOcjenaOd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOcjenaDo)).EndInit();
            this.pnlCijenaFilma.ResumeLayout(false);
            this.pnlCijenaFilma.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCijenaOd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCijenaDo)).EndInit();
            this.flpFilteri2.ResumeLayout(false);
            this.flpFilteri2.PerformLayout();
            this.pnlFilterZanr.ResumeLayout(false);
            this.pnlFilterZanr.PerformLayout();
            this.pnlNacinPretrageZanrovi.ResumeLayout(false);
            this.pnlNacinPretrageZanrovi.PerformLayout();
            this.pnlFilterGlumci.ResumeLayout(false);
            this.pnlFilterGlumci.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPretraga)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.TextBox tbFilmoviPretraga;
        private System.Windows.Forms.FlowLayoutPanel flpFilteriPanel;
        private System.Windows.Forms.Panel pnlPoredaj;
        private System.Windows.Forms.Label lblPoredajPo;
        private System.Windows.Forms.ComboBox cmbPoredajPo;
        private System.Windows.Forms.Panel pnlGodinaFilma;
        private System.Windows.Forms.Label lblGodinaFilma;
        private System.Windows.Forms.Label lblGodinaOd;
        private System.Windows.Forms.NumericUpDown nudGodinaOd;
        private System.Windows.Forms.Label lblGodinaDo;
        private System.Windows.Forms.NumericUpDown nudGodinaDo;
        private System.Windows.Forms.Panel pnlOcjenaFilma;
        private System.Windows.Forms.Label lblOcjenaFilma;
        private System.Windows.Forms.Label lblOcjenaOd;
        private System.Windows.Forms.NumericUpDown nudOcjenaOd;
        private System.Windows.Forms.Label lblOcjenaDo;
        private System.Windows.Forms.NumericUpDown nudOcjenaDo;
        private System.Windows.Forms.Panel pnlCijenaFilma;
        private System.Windows.Forms.Label lblCijenaFilma;
        private System.Windows.Forms.Label lblCijenaOd;
        private System.Windows.Forms.NumericUpDown nudCijenaOd;
        private System.Windows.Forms.Label lblCijenaDo;
        private System.Windows.Forms.NumericUpDown nudCijenaDo;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.FlowLayoutPanel flpFilmoviPanel;
        private System.Windows.Forms.FlowLayoutPanel flpFilteri2;
        private System.Windows.Forms.Panel pnlFilterZanr;
        private System.Windows.Forms.Label lblFilterZanr;
        private System.Windows.Forms.TextBox tbZanrPretraga;
        private System.Windows.Forms.ListView lvZanrovi;
        private System.Windows.Forms.Panel pnlNacinPretrageZanrovi;
        private System.Windows.Forms.Label lblNacinPretrageZanrovi;
        private System.Windows.Forms.ComboBox cmbNacinPretrageZanrovi;
        private System.Windows.Forms.Panel pnlFilterGlumci;
        private System.Windows.Forms.Label lblFilterGlumci;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbImePretraga;
        private System.Windows.Forms.TextBox tbPrezimePretraga;
        private System.Windows.Forms.ListView lvGlumci;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbNacinPretrageGlumci;
        private System.Windows.Forms.Label lblNacinPretrageGlumci;
        private System.Windows.Forms.ColumnHeader clnNaziv;
        private System.Windows.Forms.ColumnHeader clnPrezime;
        private System.Windows.Forms.ColumnHeader clnIme;
        private System.Windows.Forms.PictureBox pbPretraga;
    }
}