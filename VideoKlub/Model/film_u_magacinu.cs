namespace VideoKlub
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("video_klub.film_u_magacinu")]
    public partial class film_u_magacinu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public film_u_magacinu()
        {
            zaduzenje_filma = new HashSet<zaduzenje_filma>();
        }

        public int? na_stanju { get; set; }

        public decimal? ocjena { get; set; }

        public decimal? cijena { get; set; }

        public sbyte aktivan { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int film_id { get; set; }

        public virtual film film { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<zaduzenje_filma> zaduzenje_filma { get; set; }
    }
}
