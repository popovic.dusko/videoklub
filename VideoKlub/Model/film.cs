namespace VideoKlub
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("video_klub.film")]
    public partial class film
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public film()
        {
            glumac = new HashSet<glumac>();
            zanr = new HashSet<zanr>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string naslov { get; set; }

        public int godina { get; set; }

        [StringLength(255)]
        public string slika { get; set; }

        public virtual film_u_magacinu film_u_magacinu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<glumac> glumac { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<zanr> zanr { get; set; }
    }
}
