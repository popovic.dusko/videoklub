namespace VideoKlub
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("video_klub.zaduzenje_filma")]
    public partial class zaduzenje_filma
    {
        [Key]
        [Column(Order = 0)]
        public DateTime datum_zaduzenja { get; set; }

        public DateTime? datum_razduzenja { get; set; }

        public int? ocjena_filma { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int zaposleni_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int korisnik_id { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int film_u_magacinu_film_id { get; set; }

        public virtual film_u_magacinu film_u_magacinu { get; set; }

        public virtual korisnik korisnik { get; set; }

        public virtual zaposleni zaposleni { get; set; }
    }
}
