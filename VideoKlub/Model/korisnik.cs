namespace VideoKlub
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("video_klub.korisnik")]
    public partial class korisnik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public korisnik()
        {
            zaduzenje_filma = new HashSet<zaduzenje_filma>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string ime { get; set; }

        [Required]
        [StringLength(255)]
        public string prezime { get; set; }

        [Column(TypeName = "char")]
        [Required]
        [StringLength(13)]
        public string jmbg { get; set; }

        [Column(TypeName = "char")]
        [Required]
        [StringLength(10)]
        public string broj_kartice { get; set; }

        public sbyte kartica_aktivna { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<zaduzenje_filma> zaduzenje_filma { get; set; }
    }
}
