namespace VideoKlub
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("video_klub.zanr")]
    public partial class zanr
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public zanr()
        {
            film = new HashSet<film>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string naziv { get; set; }

        public int kategorija_zanra_id { get; set; }

        public virtual kategorija_zanra kategorija_zanra { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<film> film { get; set; }
    }
}
