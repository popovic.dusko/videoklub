namespace VideoKlub
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("video_klub.zaposleni")]
    public partial class zaposleni
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public zaposleni()
        {
            zaduzenje_filma = new HashSet<zaduzenje_filma>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string ime { get; set; }

        [Required]
        [StringLength(255)]
        public string prezime { get; set; }

        [Required]
        [StringLength(255)]
        public string korisnicko_ime { get; set; }

        [Required]
        [StringLength(255)]
        public string lozinka { get; set; }

        public sbyte aktivan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<zaduzenje_filma> zaduzenje_filma { get; set; }
    }
}
