namespace VideoKlub
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class VideoKlubContext : DbContext
    {
        public VideoKlubContext()
            : base("name=VideoKlubContext")
        {
        }

        public virtual DbSet<film> film { get; set; }
        public virtual DbSet<film_u_magacinu> film_u_magacinu { get; set; }
        public virtual DbSet<glumac> glumac { get; set; }
        public virtual DbSet<kategorija_zanra> kategorija_zanra { get; set; }
        public virtual DbSet<korisnik> korisnik { get; set; }
        public virtual DbSet<zaduzenje_filma> zaduzenje_filma { get; set; }
        public virtual DbSet<zanr> zanr { get; set; }
        public virtual DbSet<zaposleni> zaposleni { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<film>()
                .Property(e => e.naslov)
                .IsUnicode(false);

            modelBuilder.Entity<film>()
                .Property(e => e.slika)
                .IsUnicode(false);

            modelBuilder.Entity<film>()
                .HasOptional(e => e.film_u_magacinu)
                .WithRequired(e => e.film);

            modelBuilder.Entity<film>()
                .HasMany(e => e.glumac)
                .WithMany(e => e.film)
                .Map(m => m.ToTable("film_glumac", "video_klub"));

            modelBuilder.Entity<film>()
                .HasMany(e => e.zanr)
                .WithMany(e => e.film)
                .Map(m => m.ToTable("film_zanr", "video_klub"));

            modelBuilder.Entity<film_u_magacinu>()
                .HasMany(e => e.zaduzenje_filma)
                .WithRequired(e => e.film_u_magacinu)
                .HasForeignKey(e => e.film_u_magacinu_film_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<glumac>()
                .Property(e => e.ime)
                .IsUnicode(false);

            modelBuilder.Entity<glumac>()
                .Property(e => e.prezime)
                .IsUnicode(false);

            modelBuilder.Entity<kategorija_zanra>()
                .Property(e => e.opis)
                .IsUnicode(false);

            modelBuilder.Entity<kategorija_zanra>()
                .HasMany(e => e.zanr)
                .WithRequired(e => e.kategorija_zanra)
                .HasForeignKey(e => e.kategorija_zanra_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<korisnik>()
                .Property(e => e.ime)
                .IsUnicode(false);

            modelBuilder.Entity<korisnik>()
                .Property(e => e.prezime)
                .IsUnicode(false);

            modelBuilder.Entity<korisnik>()
                .Property(e => e.jmbg)
                .IsUnicode(false);

            modelBuilder.Entity<korisnik>()
                .Property(e => e.broj_kartice)
                .IsUnicode(false);

            modelBuilder.Entity<korisnik>()
                .HasMany(e => e.zaduzenje_filma)
                .WithRequired(e => e.korisnik)
                .HasForeignKey(e => e.korisnik_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<zanr>()
                .Property(e => e.naziv)
                .IsUnicode(false);

            modelBuilder.Entity<zaposleni>()
                .Property(e => e.ime)
                .IsUnicode(false);

            modelBuilder.Entity<zaposleni>()
                .Property(e => e.prezime)
                .IsUnicode(false);

            modelBuilder.Entity<zaposleni>()
                .Property(e => e.korisnicko_ime)
                .IsUnicode(false);

            modelBuilder.Entity<zaposleni>()
                .Property(e => e.lozinka)
                .IsUnicode(false);

            modelBuilder.Entity<zaposleni>()
                .HasMany(e => e.zaduzenje_filma)
                .WithRequired(e => e.zaposleni)
                .HasForeignKey(e => e.zaposleni_id)
                .WillCascadeOnDelete(false);
        }
    }
}
