namespace VideoKlub
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("video_klub.glumac")]
    public partial class glumac
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public glumac()
        {
            film = new HashSet<film>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string ime { get; set; }

        [Required]
        [StringLength(255)]
        public string prezime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<film> film { get; set; }
    }
}
