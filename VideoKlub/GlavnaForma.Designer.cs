﻿namespace VideoKlub
{
    partial class GlavnaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GlavnaForma));
            this.btnKatalogFilmova = new System.Windows.Forms.Button();
            this.btnZaduzenja = new System.Windows.Forms.Button();
            this.btnPrijava = new System.Windows.Forms.Button();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnKatalogFilmova
            // 
            this.btnKatalogFilmova.FlatAppearance.BorderSize = 2;
            this.btnKatalogFilmova.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKatalogFilmova.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKatalogFilmova.Location = new System.Drawing.Point(414, 133);
            this.btnKatalogFilmova.Name = "btnKatalogFilmova";
            this.btnKatalogFilmova.Size = new System.Drawing.Size(326, 58);
            this.btnKatalogFilmova.TabIndex = 1;
            this.btnKatalogFilmova.Text = "&Katalog filmova";
            this.btnKatalogFilmova.UseVisualStyleBackColor = true;
            this.btnKatalogFilmova.Click += new System.EventHandler(this.BtnKatalogFilmova_Click);
            // 
            // btnZaduzenja
            // 
            this.btnZaduzenja.FlatAppearance.BorderSize = 2;
            this.btnZaduzenja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZaduzenja.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZaduzenja.Location = new System.Drawing.Point(414, 245);
            this.btnZaduzenja.Name = "btnZaduzenja";
            this.btnZaduzenja.Size = new System.Drawing.Size(326, 58);
            this.btnZaduzenja.TabIndex = 2;
            this.btnZaduzenja.Text = "&Zaduženja";
            this.btnZaduzenja.UseVisualStyleBackColor = true;
            this.btnZaduzenja.Click += new System.EventHandler(this.BtnZaduzenja_Click);
            // 
            // btnPrijava
            // 
            this.btnPrijava.FlatAppearance.BorderSize = 0;
            this.btnPrijava.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.btnPrijava.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrijava.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrijava.Location = new System.Drawing.Point(663, 423);
            this.btnPrijava.Name = "btnPrijava";
            this.btnPrijava.Size = new System.Drawing.Size(135, 23);
            this.btnPrijava.TabIndex = 3;
            this.btnPrijava.Tag = "Prijava";
            this.btnPrijava.Text = "&Prijava za zaposlenog";
            this.btnPrijava.UseVisualStyleBackColor = true;
            this.btnPrijava.Click += new System.EventHandler(this.BtnPrijava_Click);
            this.btnPrijava.MouseEnter += new System.EventHandler(this.BtnPrijava_MouseEnter);
            this.btnPrijava.MouseLeave += new System.EventHandler(this.BtnPrijava_MouseLeave);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackgroundImage = global::VideoKlub.Properties.Resources.minimize_button;
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Location = new System.Drawing.Point(690, 3);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(50, 50);
            this.btnMinimize.TabIndex = 7;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::VideoKlub.Properties.Resources.close_button;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(746, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(50, 50);
            this.btnClose.TabIndex = 6;
            this.btnClose.TabStop = false;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // pbLogo
            // 
            this.pbLogo.BackgroundImage = global::VideoKlub.Properties.Resources.logo;
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbLogo.Location = new System.Drawing.Point(54, 70);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(300, 300);
            this.pbLogo.TabIndex = 2;
            this.pbLogo.TabStop = false;
            // 
            // GlavnaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnPrijava);
            this.Controls.Add(this.btnZaduzenja);
            this.Controls.Add(this.btnKatalogFilmova);
            this.Controls.Add(this.pbLogo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GlavnaForma";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Video klub";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GlavnaForma_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Button btnKatalogFilmova;
        private System.Windows.Forms.Button btnZaduzenja;
        private System.Windows.Forms.Button btnPrijava;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnMinimize;
    }
}

