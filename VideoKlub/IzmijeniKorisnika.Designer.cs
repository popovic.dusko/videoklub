﻿namespace VideoKlub
{
    partial class IzmijeniKorisnika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IzmijeniKorisnika));
            this.lblNoviKorisnik = new System.Windows.Forms.Label();
            this.lblIme = new System.Windows.Forms.Label();
            this.lblPrezime = new System.Windows.Forms.Label();
            this.lblJMBG = new System.Windows.Forms.Label();
            this.btnNazad = new System.Windows.Forms.Button();
            this.btnIzmijeni = new System.Windows.Forms.Button();
            this.lblBrojKartice = new System.Windows.Forms.Label();
            this.tbIme = new System.Windows.Forms.TextBox();
            this.tbPrezime = new System.Windows.Forms.TextBox();
            this.tbJMBG = new System.Windows.Forms.TextBox();
            this.tbBrojKartice = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblNoviKorisnik
            // 
            this.lblNoviKorisnik.AutoSize = true;
            this.lblNoviKorisnik.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoviKorisnik.Location = new System.Drawing.Point(13, 13);
            this.lblNoviKorisnik.Name = "lblNoviKorisnik";
            this.lblNoviKorisnik.Size = new System.Drawing.Size(80, 24);
            this.lblNoviKorisnik.TabIndex = 0;
            this.lblNoviKorisnik.Text = "Korisnik";
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIme.Location = new System.Drawing.Point(30, 77);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(44, 22);
            this.lblIme.TabIndex = 1;
            this.lblIme.Text = "Ime";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrezime.Location = new System.Drawing.Point(30, 122);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(79, 22);
            this.lblPrezime.TabIndex = 2;
            this.lblPrezime.Text = "Prezime";
            // 
            // lblJMBG
            // 
            this.lblJMBG.AutoSize = true;
            this.lblJMBG.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJMBG.Location = new System.Drawing.Point(30, 167);
            this.lblJMBG.Name = "lblJMBG";
            this.lblJMBG.Size = new System.Drawing.Size(64, 22);
            this.lblJMBG.TabIndex = 3;
            this.lblJMBG.Text = "JMBG";
            // 
            // btnNazad
            // 
            this.btnNazad.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNazad.FlatAppearance.BorderSize = 2;
            this.btnNazad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNazad.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNazad.Location = new System.Drawing.Point(260, 257);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(102, 34);
            this.btnNazad.TabIndex = 7;
            this.btnNazad.Text = "Nazad";
            this.btnNazad.UseVisualStyleBackColor = true;
            // 
            // btnIzmijeni
            // 
            this.btnIzmijeni.Enabled = false;
            this.btnIzmijeni.FlatAppearance.BorderSize = 2;
            this.btnIzmijeni.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIzmijeni.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzmijeni.Location = new System.Drawing.Point(151, 257);
            this.btnIzmijeni.Name = "btnIzmijeni";
            this.btnIzmijeni.Size = new System.Drawing.Size(103, 34);
            this.btnIzmijeni.TabIndex = 8;
            this.btnIzmijeni.Text = "Izmijeni";
            this.btnIzmijeni.UseVisualStyleBackColor = true;
            this.btnIzmijeni.Click += new System.EventHandler(this.BtnIzmijeni_Click);
            // 
            // lblBrojKartice
            // 
            this.lblBrojKartice.AutoSize = true;
            this.lblBrojKartice.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrojKartice.Location = new System.Drawing.Point(30, 212);
            this.lblBrojKartice.Name = "lblBrojKartice";
            this.lblBrojKartice.Size = new System.Drawing.Size(110, 22);
            this.lblBrojKartice.TabIndex = 9;
            this.lblBrojKartice.Text = "Broj kartice";
            // 
            // tbIme
            // 
            this.tbIme.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIme.Location = new System.Drawing.Point(151, 74);
            this.tbIme.MaxLength = 255;
            this.tbIme.Name = "tbIme";
            this.tbIme.Size = new System.Drawing.Size(211, 31);
            this.tbIme.TabIndex = 10;
            this.tbIme.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbIme.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            // 
            // tbPrezime
            // 
            this.tbPrezime.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPrezime.Location = new System.Drawing.Point(151, 119);
            this.tbPrezime.MaxLength = 255;
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.Size = new System.Drawing.Size(211, 31);
            this.tbPrezime.TabIndex = 11;
            this.tbPrezime.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbPrezime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            // 
            // tbJMBG
            // 
            this.tbJMBG.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbJMBG.Location = new System.Drawing.Point(151, 164);
            this.tbJMBG.MaxLength = 13;
            this.tbJMBG.Name = "tbJMBG";
            this.tbJMBG.Size = new System.Drawing.Size(211, 31);
            this.tbJMBG.TabIndex = 12;
            // 
            // tbBrojKartice
            // 
            this.tbBrojKartice.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBrojKartice.Location = new System.Drawing.Point(151, 209);
            this.tbBrojKartice.MaxLength = 10;
            this.tbBrojKartice.Name = "tbBrojKartice";
            this.tbBrojKartice.Size = new System.Drawing.Size(211, 31);
            this.tbBrojKartice.TabIndex = 13;
            // 
            // IzmijeniKorisnika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.btnNazad;
            this.ClientSize = new System.Drawing.Size(390, 307);
            this.Controls.Add(this.tbBrojKartice);
            this.Controls.Add(this.tbJMBG);
            this.Controls.Add(this.tbPrezime);
            this.Controls.Add(this.tbIme);
            this.Controls.Add(this.lblBrojKartice);
            this.Controls.Add(this.btnIzmijeni);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.lblJMBG);
            this.Controls.Add(this.lblPrezime);
            this.Controls.Add(this.lblIme);
            this.Controls.Add(this.lblNoviKorisnik);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "IzmijeniKorisnika";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IzmijeniKorisnika";
            this.Load += new System.EventHandler(this.IzmijeniKorisnika_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNoviKorisnik;
        private System.Windows.Forms.Label lblIme;
        private System.Windows.Forms.Label lblPrezime;
        private System.Windows.Forms.Label lblJMBG;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.Button btnIzmijeni;
        private System.Windows.Forms.Label lblBrojKartice;
        private System.Windows.Forms.TextBox tbIme;
        private System.Windows.Forms.TextBox tbPrezime;
        private System.Windows.Forms.TextBox tbJMBG;
        private System.Windows.Forms.TextBox tbBrojKartice;
    }
}