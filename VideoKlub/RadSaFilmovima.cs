﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class RadSaFilmovima : Form
    {
        #region Promjenljive
        private Form roditeljskaForma = null;
        private bool celijaFokusiranaNakonPretrage = false; // potrebno za fokus odgovaracujeg tekstualnog polja za pretragu 
        private int offset = 0; // potrebno za ucitavanje filmova prilikom scroll-ovanja
        private bool tbFokusiranProgramski = false;
        private bool lvGlumciSimuliraniKeyDown = false;
        private bool lvZanroviSimuliraniKeyDown = false;
        private bool lvGlumciPrviItemChecked = false;
        private bool lvZanroviPrviItemChecked = false;
        private bool zanroviPretrazivani = false;
        private bool glumciPretrazivani = false;
        private List<string> cekiraniGlumci = new List<string>();
        private List<string> cekiraniZanrovi = new List<string>();
        private FilmoviUtil filmoviUtil = new FilmoviUtil(); // potrebno za manipulaciju podacima u bazi
        #endregion
        public RadSaFilmovima(Form roditeljskaForma)
        {
            InitializeComponent();
            btnIzmijeniFilm.Enabled = btnAktivirajFilm.Enabled = false;  // na pocetku nijedan film nije oznacen
            this.roditeljskaForma = roditeljskaForma;

            pnlZanr.Visible = false;
            pnlGlumci.Visible = false;

            cmbNacinPretrageZanrovi.SelectedIndex = 0;
            cmbNacinPretrageGlumci.SelectedIndex = 0;

            ActiveControl = tbNaslovFilma;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);

            pen.Color = Color.Gray;

            g.DrawLine(pen, btnIzmijeniFilm.Location.X - 20, btnIzmijeniFilm.Location.Y,
                btnIzmijeniFilm.Location.X - 20, btnIzmijeniFilm.Location.Y + btnIzmijeniFilm.Height);
        } // za iscrtavanje granica forme

        private void ZatamniFormu()
        {
            BackColor = SystemColors.Control;
            dgvFilmovi.DefaultCellStyle.BackColor = SystemColors.Control;
            dgvFilmovi.BackgroundColor = SystemColors.Control;
        }
        private void OsvijetliFormu()
        {
            BackColor = SystemColors.Window;
            dgvFilmovi.DefaultCellStyle.BackColor = SystemColors.Window;
            dgvFilmovi.BackgroundColor = SystemColors.Window;
        }

        private void Pretraga()
        {
            string naslov = tbNaslovFilma.Text.EndsWith("...") ? "" : tbNaslovFilma.Text;
            string godina = tbGodina.Text.EndsWith("...") ? "" : tbGodina.Text;
            bool aktivniFilmovi = chbAktivanFilm.Checked;

            List<zanr> zanrovi = null;

            if (cekiraniZanrovi.Count > 0)
            {
                zanrovi = new List<zanr>();
                foreach (string zanr in cekiraniZanrovi)
                {
                    int idZanra = int.Parse(Regex.Replace(zanr, "[^0-9]", ""));
                    zanrovi.Add(filmoviUtil.GetZanr(idZanra));
                }
            }

            List<glumac> glumci = null;

            if (cekiraniGlumci.Count > 0)
            {
                glumci = new List<glumac>();
                foreach (string glumac in cekiraniGlumci)
                {
                    int idGlumca = int.Parse(Regex.Replace(glumac, "[^0-9]", ""));
                    glumci.Add(filmoviUtil.GetGlumac(idGlumca));
                }
            }

            List<film> filmovi = filmoviUtil.PretraziFilmove(naslov, godina, aktivniFilmovi, zanrovi, cmbNacinPretrageZanrovi.Text, 
                glumci, cmbNacinPretrageGlumci.Text).Take(20).ToList();

            if (filmovi != null && filmovi.Count == 0)
            {
                btnIzmijeniFilm.Enabled = btnAktivirajFilm.Enabled = false;
            }

            bsrFilmovi.Clear();

            foreach (film film in filmovi)
            {
                bsrFilmovi.Add(film);
            }

            offset = 0;
        }
        private void PrikaziFilmoveOdFilmaSaId(int idFilma)
        {
            string naslovFilma = filmoviUtil.GetNaslovFilma(idFilma);
            List<film> filmovi = filmoviUtil.GetNakonNaslovaPrvihFilmova(naslovFilma, 20);
            if (filmovi.Count < 20)
            {
                filmovi = filmoviUtil.GetZadnjihFilmova(20);
            }

            tbGodina.Clear();
            tbNaslovFilma.Clear();

            chbAktivanFilm.Checked = false;
            
            if (lvZanrovi.CheckedItems.Count > 0)
            {
                foreach(ListViewItem item in lvZanrovi.CheckedItems)
                {
                    item.Checked = false;
                    cekiraniZanrovi.Remove(item.Name);
                }
            }

            if(lvGlumci.CheckedItems.Count > 0)
            {
                foreach (ListViewItem item in lvGlumci.CheckedItems)
                {
                    item.Checked = false;
                    cekiraniGlumci.Remove(item.Name);
                }
            }

            if (filmovi != null && filmovi.Count == 0)
            {
                btnIzmijeniFilm.Enabled = btnAktivirajFilm.Enabled = false;
            }

            bsrFilmovi.Clear();

            foreach (var film in filmovi)
            {
                bsrFilmovi.Add(film);
            }
            foreach (DataGridViewRow row in dgvFilmovi.Rows)
            {
                if ((int)row.Cells["clnId"].Value == idFilma)
                {
                    row.Selected = true;
                }
            }
            int indexReda = dgvFilmovi.SelectedRows[0].Index;
            dgvFilmovi.FirstDisplayedScrollingRowIndex = indexReda;

            offset = 0;
        }

        private void RadSaFilmovima_FormClosing(object sender, FormClosingEventArgs e)
        {
            roditeljskaForma.Show();
        }
        private void RadSaFilmovima_Load(object sender, EventArgs e)
        {
            UcitajZanrove();
            UcitajGlumce();

            List<film> filmovi = filmoviUtil.GetPrvihFilmova(20);
            foreach (var film in filmovi)
            {
                bsrFilmovi.Add(film);
            }

            if (dgvFilmovi.SelectedRows.Count != 0)
            {
                dgvFilmovi.SelectedRows[0].Selected = false;
            }

            dgvFilmovi.SelectionChanged += DgvFilmovi_SelectionChanged;
        }

        #region Button-i
        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        private void BtnDodajFilm_Click(object sender, EventArgs e)
        {
            // zatamnjivanje forme
            ZatamniFormu();

            // dodavanje novog filma
            int idZadnjegDodanogFilma;
            DialogResult dialogResult = new DodajFilm().ShowDialog(out idZadnjegDodanogFilma);
            if (DialogResult.Yes == dialogResult)
            {
                PrikaziFilmoveOdFilmaSaId(idZadnjegDodanogFilma);
            }
            else if (dialogResult == DialogResult.Retry) // ukoliko je unesen naslov i godina koji postoji u bazi pristupa se pregledu informacija za taj film
            {
                if (DialogResult.Yes == new IzmijeniFilm(idZadnjegDodanogFilma).ShowDialog())
                {
                    PrikaziFilmoveOdFilmaSaId(idZadnjegDodanogFilma);
                }
            }

            // povratak u formu
            OsvijetliFormu();
            tbNaslovFilma.Focus();
        }
        private void BtnIzmijeniFilm_Click(object sender, EventArgs e)
        {
            // zatamnjivanje forme
            ZatamniFormu();

            // izmjena filma
            int idSelektovanogFilma = (int)dgvFilmovi.SelectedRows[0].Cells["clnId"].Value;
            if (DialogResult.Yes == new IzmijeniFilm(idSelektovanogFilma).ShowDialog())
            {
                PrikaziFilmoveOdFilmaSaId(idSelektovanogFilma);
            }

            // povratak u formu
            OsvijetliFormu();
            tbNaslovFilma.Focus();
        }
        private void BtnIzmijeniFilm_EnabledChanged(object sender, EventArgs e)
        {
            // podesavanje izgleda button-a u zavisnoti da li je enabled
            Button btn = sender as Button;
            if (btn.Enabled == true)
            {
                btn.FlatAppearance.BorderColor = Color.Black;
            }
            else
            {
                btn.FlatAppearance.BorderColor = SystemColors.Control;
            }
        }
        private void BtnAktivirajFilm_Click(object sender, EventArgs e)
        {
            int idSelektovanogFilma = (int)dgvFilmovi.SelectedRows[0].Cells["clnId"].Value;
            int indexReda = dgvFilmovi.SelectedRows[0].Index;

            if (btnAktivirajFilm.Text.StartsWith("Deaktiviraj") && filmoviUtil.IsZaduzen(idSelektovanogFilma))
            {
                ZatamniFormu();
                new DijalogGreska("Film je zadužen!", "Nije moguće deaktivirati zaduženi film!").ShowDialog();
                OsvijetliFormu();
                return;
            }

            bsrFilmovi.RemoveAt(indexReda);
            bsrFilmovi.Insert(indexReda, filmoviUtil.AktivirajFilm(idSelektovanogFilma));

            dgvFilmovi.Rows[indexReda].Selected = true;
        }
        private void BtnNazad_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region DataGridView - filmovi
        private void DgvFilmovi_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        { 
            string naslov = dgvFilmovi.Rows[e.RowIndex].Cells[0].Value.ToString();
            int godina = (int)dgvFilmovi.Rows[e.RowIndex].Cells[1].Value;

            if (dgvFilmovi.Columns[e.ColumnIndex].Name == "clnAktivanFilm")
            {
                sbyte aktivan = filmoviUtil.GetIsAktivan(naslov, godina);
                e.Value = aktivan != 0 ? "Da" : "Ne";
            }
            else if (dgvFilmovi.Columns[e.ColumnIndex].Name == "clnNaStanju")
            {
                int naStanju = filmoviUtil.GetNaStanju(naslov, godina);
                e.Value = naStanju;
            }
            else if (dgvFilmovi.Columns[e.ColumnIndex].Name == "clnCijena")
            {
                decimal cijena = filmoviUtil.GetCijena(naslov, godina);
                e.Value = cijena;
            }
        }
        private void DgvFilmovi_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            // omogucavanje button - a
            if (dgv.SelectedRows.Count > 0)
            {
                btnIzmijeniFilm.Enabled = btnAktivirajFilm.Enabled = true;
                btnAktivirajFilm.Text = (filmoviUtil.IsAktivan((int)dgvFilmovi.SelectedRows[0].Cells["clnId"].Value) ? "Deaktiviraj" : "Aktiviraj") + " film";
            }
        }
        private void DgvFilmovi_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            // ukoliko je neka celija selektovana fokusira se textBox koji odgovara koloni u kojoj se nalazi ta celija
            // zbog mogucnosti pretrage vrijednosti koje se nalaze u toj koloni
            if(zanroviPretrazivani)
            {
                zanroviPretrazivani = false;
                return;
            }
            if(glumciPretrazivani)
            {
                glumciPretrazivani = true;
                return;
            }
            if (celijaFokusiranaNakonPretrage)
            {
                celijaFokusiranaNakonPretrage = false;
                return;
            }
            switch (e.ColumnIndex)
            {
                case 0: tbNaslovFilma.Focus(); break;
                case 1: tbGodina.Focus(); break;
                case 4: chbAktivanFilm.Focus(); break;
            }
        }
        private void DgvFilmovi_Scroll(object sender, ScrollEventArgs e)
        {
            // dinamicko ucitavanje filmova prilikom scroll-ovanja
            if ((e.NewValue + ((DataGridView)sender).DisplayedRowCount(false)) % 20 == 0)
            {
                offset += 20;

                string naslov, godina;
                bool aktivniFilmovi;

                naslov = tbNaslovFilma.Text.EndsWith("...") ? "" : tbNaslovFilma.Text;
                godina = tbGodina.Text.EndsWith("...") ? "" : tbGodina.Text;

                aktivniFilmovi = chbAktivanFilm.Checked;

                List<zanr> zanrovi = null;

                if (lvZanrovi.CheckedItems.Count > 0)
                {
                    zanrovi = new List<zanr>();
                    foreach (ListViewItem item in lvZanrovi.CheckedItems)
                    {
                        string nazivZanra = item.Text;
                        string kategorijaZanra = item.Tag.ToString();

                        zanrovi.Add(filmoviUtil.GetZanr(kategorijaZanra, nazivZanra));
                    }
                }

                List<glumac> glumci = null;

                if (lvGlumci.CheckedItems.Count > 0)
                {
                    glumci = new List<glumac>();
                    foreach (ListViewItem item in lvGlumci.CheckedItems)
                    {
                        int idGlumca = int.Parse(Regex.Replace(item.Name, "[^0-9]", ""));
                        glumci.Add(filmoviUtil.GetGlumac(idGlumca));
                    }
                }

                List<film> filmovi = filmoviUtil.GetSljedecihUzPretragu(naslov, godina, aktivniFilmovi, 20, offset, 
                    zanrovi, cmbNacinPretrageZanrovi.Text, glumci, cmbNacinPretrageGlumci.Text);

                foreach (var film in filmovi)
                {
                    bsrFilmovi.Add(film);
                }
            }
        }
        private void DgvFilmovi_Click(object sender, EventArgs e)
        {
            pnlZanr.Visible = false;
            pnlGlumci.Visible = false;
        }
        #endregion

        #region GroupBox - pretraga filmova
        private void GrbPretragaFilmova_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Black, Color.Black);
        }
        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Brisanje teksta i ivica
                g.Clear(this.BackColor);

                // Crtanje teksta
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Crtanje ivica
                //Lijeva
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Desna
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Donja
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Gornja1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Gornja2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }
        #endregion

        #region TextBox-ovi
        private void Tb_EnterFocus(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // onemogucavanje selekcije placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.SelectionStart = 0;
            }
            else
            {
                textBox.SelectAll();
            }
        }
        private void TbNaslovFilma_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbNaslovFilma_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }
            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-Z0-9šđčćžŠĐČĆŽ '.:!?,&-]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                textBox.Text = "Naslov filma...";
                textBox.ForeColor = Color.LightGray;
            }

            // kako bi textBox koji je bio fokusiran prilikom pretrage ostao fokusiran
            celijaFokusiranaNakonPretrage = true;

            Pretraga();

            textBox.TextChanged += TbNaslovFilma_TextChanged;
        } 
        private void TbGodina_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbGodina_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }

            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^0-9]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                textBox.Text = "Godina...";
                textBox.ForeColor = Color.LightGray;
            }

            // kako bi textBox koji je bio fokusiran prilikom pretrage ostao fokusiran
            celijaFokusiranaNakonPretrage = true;

            Pretraga();

            textBox.TextChanged += TbGodina_TextChanged;
        }
        private void Tb_MouseUp(object sender, MouseEventArgs e)
        {
            // selekcija ili kompletnog sadrzaja textBox-a ili nista
            TextBox textBox = sender as TextBox;
            if (!textBox.Text.EndsWith("...") && textBox.SelectionLength == textBox.TextLength)
            {
                textBox.SelectAll();
            }
            else if (textBox.SelectionLength > 0)
            {
                textBox.SelectionLength = 0;
            }
        }
        private void Tb_KeyDown(object sender, KeyEventArgs e) // manipulacija redoslijedom fokusiranja komponenti i selekcije filmova u dataGridView-u
        {
            // ako je neki textBox fokusiran i pritisnut je taster Up vrsi se selekcija filma koji se nalazi iznad vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi film u tabeli
            if (e.KeyCode == Keys.Up)
            {
                e.SuppressKeyPress = true;

                if (dgvFilmovi.Rows.Count == 0)
                {
                    return;
                }
                if (dgvFilmovi.SelectedRows.Count > 0)
                {
                    if (dgvFilmovi.SelectedRows[0].Index > 0)
                    {
                        dgvFilmovi.Rows[dgvFilmovi.SelectedRows[0].Index - 1].Selected = true;
                        if (dgvFilmovi.SelectedRows[0].Displayed == false)
                        {
                            if (dgvFilmovi.SelectedRows[0].Index > dgvFilmovi.FirstDisplayedScrollingRowIndex + dgvFilmovi.DisplayedRowCount(false) - 1)
                            {
                                dgvFilmovi.FirstDisplayedScrollingRowIndex = dgvFilmovi.SelectedRows[0].Index - dgvFilmovi.DisplayedRowCount(false) + 1;
                            }
                            else
                            {
                                dgvFilmovi.FirstDisplayedScrollingRowIndex = dgvFilmovi.FirstDisplayedScrollingRowIndex - 1;
                            }
                        }
                    }
                }
                else
                {
                    dgvFilmovi.Rows[0].Selected = true;
                }
            }
            // ako je neki textBox fokusiran i pritisnut je taster Down vrsi se selekcija korisnika koji se nalazi ispod vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi korisnik u tabeli
            else if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;

                if (dgvFilmovi.Rows.Count == 0)
                {
                    return;
                }
                if (dgvFilmovi.SelectedRows.Count > 0)
                {
                    if (dgvFilmovi.SelectedRows[0].Index < dgvFilmovi.Rows.Count - 1)
                    {
                        dgvFilmovi.Rows[dgvFilmovi.SelectedRows[0].Index + 1].Selected = true;
                        if (dgvFilmovi.SelectedRows[0].Displayed == false)
                        {
                            if (dgvFilmovi.SelectedRows[0].Index < dgvFilmovi.FirstDisplayedScrollingRowIndex)
                            {
                                dgvFilmovi.FirstDisplayedScrollingRowIndex = dgvFilmovi.SelectedRows[0].Index;
                            }
                            else
                            {
                                dgvFilmovi.FirstDisplayedScrollingRowIndex = dgvFilmovi.FirstDisplayedScrollingRowIndex + 1;
                            }
                        }
                    }
                }
                else
                {
                    dgvFilmovi.Rows[0].Selected = true;
                }
            }
            // ako je pritisnut taster Right fokusiramo komponentu koja se nalazi desno od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Right)
            {
                e.SuppressKeyPress = true;

                if (sender is TextBox)
                {
                    TextBox textBox = sender as TextBox;

                    switch (textBox.Tag.ToString())
                    {
                        case "Naslov filma": tbGodina.Focus(); break;
                        case "Godina": btnZanr.Focus(); break;
                    }
                }
                else if (sender is CheckBox)
                {
                    tbNaslovFilma.Focus();
                }
            }
            // ako je pritisnut taster Right fokusiramo komponentu koja se nalazi lijevo od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Left)
            {
                e.SuppressKeyPress = true;

                if (sender is TextBox)
                {
                    TextBox textBox = sender as TextBox;

                    switch (textBox.Tag.ToString())
                    {
                        case "Naslov filma": chbAktivanFilm.Focus(); break;
                        case "Godina": tbNaslovFilma.Focus(); break;
                    }
                }
                else if (sender is CheckBox)
                {
                    btnGlumci.Focus();
                }
            }
            // ako je fokusiran neki textBox kombinacijom tastera Control i Shift i odredjenih tastera simulira se klik na odgovarajuci button
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodajFilm.PerformClick();
                }
                else if (e.KeyCode == Keys.I)
                {
                    e.SuppressKeyPress = true;
                    btnIzmijeniFilm.PerformClick();
                }
                else if (e.KeyCode == Keys.A)
                {
                    e.SuppressKeyPress = true;
                    btnAktivirajFilm.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
        }
        private void TbPretraga_EnterFocus(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // onemogucavanje selekcije placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.SelectionStart = 0;
            }
            else
            {
                if (tbFokusiranProgramski == false)
                {
                    textBox.SelectAll();
                }
                else
                {
                    tbFokusiranProgramski = false;
                }
            }
        }
        #endregion

        #region CheckBox - prikaz samo aktivnih filmova
        private void ChbAktivniFilmovi_CheckedChanged(object sender, EventArgs e)
        {
            // kako bi checkBox ostao fokusiran i nakon pretrage
            celijaFokusiranaNakonPretrage = true;

            Pretraga();
        }
        #endregion

        #region Zanrovi
        private void UcitajZanrove()
        {
            lvZanrovi.Items.Clear();
            lvZanrovi.Groups.Clear();

            List<kategorija_zanra> kategorijeZanra = filmoviUtil.GetKategorijeZanra();

            foreach (var kategorijaZanra in kategorijeZanra)
            {
                ListViewGroup lvg = new ListViewGroup
                {
                    Header = kategorijaZanra.opis,
                    Name = "lvg" + kategorijaZanra.opis.Replace(" ", ""),
                };

                lvZanrovi.Groups.Add(lvg);

                List<zanr> zanroviIzKategorije = filmoviUtil.GetZanroveIzKategorije(kategorijaZanra.id);

                if (zanroviIzKategorije != null && zanroviIzKategorije.Count != 0)
                {
                    foreach (var zanr in zanroviIzKategorije)
                    {
                        ListViewItem item = new ListViewItem
                        {
                            Group = lvg,
                            Name = "lvi" + zanr.naziv.Replace(" ", "") + kategorijaZanra.opis.Replace(" ", "") + zanr.id,
                            Text = zanr.naziv,
                            Tag = kategorijaZanra.opis
                        };

                        if (cekiraniZanrovi.Contains(item.Name))
                        {
                            item.Checked = true;
                        }

                        lvZanrovi.Items.Add(item);
                    }
                }
            }
        }
        private void PretragaZanrova()
        {
            string nazivZanra = tbZanrPretraga.Text.EndsWith("...") ? "" : tbZanrPretraga.Text;

            lvZanrovi.ItemChecked -= LvZanrovi_ItemChecked;

            lvZanrovi.Items.Clear();
            lvZanrovi.Groups.Clear();

            List<kategorija_zanra> kategorijeZanra = filmoviUtil.GetKategorijeZanra();

            foreach (var kategorijaZanra in kategorijeZanra)
            {
                ListViewGroup lvg = new ListViewGroup
                {
                    Header = kategorijaZanra.opis,
                    Name = "lvg" + kategorijaZanra.opis.Replace(" ", ""),
                };

                lvZanrovi.Groups.Add(lvg);

                List<zanr> zanroviIzKategorije = filmoviUtil.GetZanroveIzKategorijeUzPretragu(kategorijaZanra.id, nazivZanra);

                if (zanroviIzKategorije != null && zanroviIzKategorije.Count != 0)
                {
                    foreach (var zanr in zanroviIzKategorije)
                    {
                        ListViewItem item = new ListViewItem
                        {
                            Group = lvg,
                            Name = "lvi" + zanr.naziv.Replace(" ", "") + kategorijaZanra.opis.Replace(" ", "") + zanr.id,
                            Text = zanr.naziv,
                            Tag = kategorijaZanra.opis
                        };

                        if (cekiraniZanrovi.Contains(item.Name))
                        {
                            item.Checked = true;
                        }

                        lvZanrovi.Items.Add(item);
                    }
                }
            }

            if (lvZanrovi.Items.Count > 0)
            {
                lvZanrovi.Items[0].Selected = true;
                lvZanrovi.TopItem = lvZanrovi.Items[0];
                lvZanrovi.EnsureVisible(0);
            }

            lvZanrovi.ItemChecked += LvZanrovi_ItemChecked;
        }
        private bool IsPrviElemenatUGrupiZanrovi()
        {
            if (lvZanrovi.SelectedItems.Count > 0)
            {
                var item = lvZanrovi.SelectedItems[0];

                string grupa = "lvg" + item.Name.Replace("lvi" + item.Text, "");
                grupa = Regex.Replace(grupa, "[0-9]", "");

                if (lvZanrovi.Groups[grupa].Items[0] == item)
                {
                    return true;
                }
            }

            return false;
        }
        private bool IsPoslednjiElemenatUGrupiZanrovi()
        {
            if (lvZanrovi.SelectedItems.Count > 0)
            {
                var item = lvZanrovi.SelectedItems[0];

                string grupa = "lvg" + item.Name.Replace("lvi" + item.Text, "");
                grupa = Regex.Replace(grupa, "[0-9]", "");

                if (lvZanrovi.Groups[grupa].Items[lvZanrovi.Groups[grupa].Items.Count - 1] == item)
                {
                    return true;
                }
            }
            return false;
        }

        #region BtnZanr
        private void BtnZanr_MouseEnter(object sender, EventArgs e)
        {
            pnlZanr.Visible = true;
            tbZanrPretraga.Focus();

        }
        private void BtnZanr_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = btnZanr.PointToClient(Cursor.Position);

            if (pozicijaKursora.Y >= btnZanr.Height &&
                pozicijaKursora.X >= 0 &&
                pozicijaKursora.X <= btnZanr.Width)
            {
                pnlZanr.Visible = true;
                tbZanrPretraga.Focus();
                return;
            }

            pnlZanr.Visible = false;
        }
        private void BtnZanr_Enter(object sender, EventArgs e)
        {
            pnlZanr.Visible = true;
            tbZanrPretraga.Focus();
        }
        private void BtnZanr_Leave(object sender, EventArgs e)
        {
            pnlZanr.Visible = false;
        }
        #endregion

        #region TbZanrPretraga
        private void TbZanrPretraga_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = tbZanrPretraga.PointToClient(Cursor.Position);

            if (pozicijaKursora.Y > 0 &&
                pozicijaKursora.X >= 0 &&
                pozicijaKursora.X <= tbZanrPretraga.Width - 5)
            {
                pnlZanr.Visible = true;
                tbZanrPretraga.Focus();
                return;
            }

            pnlZanr.Visible = false;
        }
        private void TbZanrPretraga_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbZanrPretraga_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }

            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                textBox.Text = "Žanr...";
                textBox.ForeColor = Color.Silver;
            }

            PretragaZanrova();
            
            textBox.TextChanged += TbZanrPretraga_TextChanged;
        }
        private void TbZanrPretraga_KeyDown(object sender, KeyEventArgs e)
        {
            // ako je textBox fokusiran i pritisnut je taster Up vrsi se selekcija zanra koji se nalazi iznad vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi zanr u listi
            if (e.KeyCode == Keys.Up)
            {
                e.SuppressKeyPress = true;

                lvZanrovi.Focus();

                if (IsPrviElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;
                    SendKeys.SendWait("{UP}");
                }

                lvZanroviSimuliraniKeyDown = true;
                SendKeys.SendWait("{UP}");

                tbFokusiranProgramski = true;
                tbZanrPretraga.Focus();
            }
            // ako je neki textBox fokusiran i pritisnut je taster Down vrsi se selekcija zanra koji se nalazi ispod vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi zanr u listi
            else if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;

                lvZanrovi.Focus();

                if (IsPoslednjiElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;
                    SendKeys.SendWait("{DOWN}");
                }

                lvZanroviSimuliraniKeyDown = true;
                SendKeys.SendWait("{DOWN}");

                tbFokusiranProgramski = true;
                tbZanrPretraga.Focus();

            }
            else if (e.KeyCode == Keys.Right)
            {
                e.SuppressKeyPress = true;
                pnlZanr.Visible = false;
                btnGlumci.Focus();
            }
            else if (e.KeyCode == Keys.Left)
            {
                e.SuppressKeyPress = true;
                pnlZanr.Visible = false;
                tbGodina.Focus();
            }
            else if (e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;

                if (lvZanrovi.SelectedItems.Count > 0)
                {
                    lvZanrovi.SelectedItems[0].Checked = !lvZanrovi.SelectedItems[0].Checked;
                }
            }
            // Control + Shift
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodajFilm.PerformClick();
                }
                else if (e.KeyCode == Keys.I)
                {
                    e.SuppressKeyPress = true;
                    btnIzmijeniFilm.PerformClick();
                }
                else if (e.KeyCode == Keys.A)
                {
                    e.SuppressKeyPress = true;
                    btnAktivirajFilm.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
        }
        #endregion

        #region LvZanrovi
        private void LvZanrovi_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = lvZanrovi.PointToClient(Cursor.Position);

            if (pozicijaKursora.Y <= 0 &&
                pozicijaKursora.X >= 0 &&
                pozicijaKursora.X <= lvZanrovi.Width)
            {
                pnlZanr.Visible = true;
                tbZanrPretraga.Focus();
                return;
            }
            else if (pozicijaKursora.Y >= lvZanrovi.Height &&
                pozicijaKursora.X >= 0 &&
                pozicijaKursora.X <= lvZanrovi.Width)
            {
                pnlZanr.Visible = true;
                tbZanrPretraga.Focus();
                return;
            }

            pnlZanr.Visible = false;
        }
        private void LvZanrovi_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                lvZanroviPrviItemChecked = true;
                e.Item.Selected = true;
                if (!cekiraniZanrovi.Contains(e.Item.Name))
                {
                    cekiraniZanrovi.Add(e.Item.Name);
                }
            }
            else
            {
                cekiraniZanrovi.Remove(e.Item.Name);
            }

            if (!lvZanroviPrviItemChecked)
            {
                return;
            }
            zanroviPretrazivani = true;
            Pretraga();
        }
        private void LvZanrovi_KeyDown(object sender, KeyEventArgs e)
        {
            if (lvZanroviSimuliraniKeyDown)
            {
                lvZanroviSimuliraniKeyDown = false;
                return;
            }
            if (e.KeyCode == Keys.Up)
            {
                if (IsPrviElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;

                    SendKeys.SendWait("{UP}");
                }
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (IsPoslednjiElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;

                    SendKeys.SendWait("{DOWN}");
                }
            }
        }

        #endregion

        #region SpcSplitZanrovi
        private void SpcSplitZanrovi_Panel2_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = spcSplitZanrovi.Panel2.PointToClient(Cursor.Position);

            if (pozicijaKursora.Y > spcSplitZanrovi.Panel2.Height - 5 ||
               (pozicijaKursora.X < 0 || pozicijaKursora.X > spcSplitZanrovi.Panel2.Width - 5))
            {
                pnlZanr.Visible = false;
            }
        }
        private void SpcSplitZanrovi_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = spcSplitZanrovi.PointToClient(Cursor.Position);

            if (pozicijaKursora.X < 0 || pozicijaKursora.X > spcSplitZanrovi.Width - 5)
            {
                pnlZanr.Visible = false;
            }
        }
        #endregion

        #region CmbNacinPretrageZanrovi
        private void CmbNacinPretrageZanrovi_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Enter)
            {
                if (lvZanrovi.CheckedItems.Count > 0)
                {
                    Pretraga();
                }
                tbZanrPretraga.Focus();
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Up)
            {
                cmbNacinPretrageZanrovi.SelectedIndexChanged -= CmbNacinPretrageZanrovi_SelectedIndexChanged;
                cmbNacinPretrageZanrovi.SelectedIndex = 0;
                cmbNacinPretrageZanrovi.SelectedIndexChanged += CmbNacinPretrageZanrovi_SelectedIndexChanged;
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                cmbNacinPretrageZanrovi.SelectedIndexChanged -= CmbNacinPretrageZanrovi_SelectedIndexChanged;
                cmbNacinPretrageZanrovi.SelectedIndex = 1;
                cmbNacinPretrageZanrovi.SelectedIndexChanged += CmbNacinPretrageZanrovi_SelectedIndexChanged;
                e.SuppressKeyPress = true;
            }
        }
        private void CmbNacinPretrageZanrovi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvZanrovi.CheckedItems.Count > 0)
            {
                Pretraga();
            }
            tbZanrPretraga.Focus();
        }
        #endregion
        #endregion

        #region Glumci
        private void UcitajGlumce()
        {

            lvGlumci.Items.Clear();
            lvGlumci.Groups.Clear();

            List<glumac> glumci = filmoviUtil.GetGlumci();

            foreach (var glumac in glumci)
            {
                ListViewGroup lvg = lvGlumci.Groups["lvg" + glumac.prezime[0]];

                if (lvg == null)
                {
                    lvg = new ListViewGroup
                    {
                        Header = glumac.prezime[0].ToString(),
                        Name = "lvg" + glumac.prezime[0]
                    };

                    lvGlumci.Groups.Add(lvg);
                }

                ListViewItem item = new ListViewItem
                {
                    Group = lvg,
                    Name = "lvi" + glumac.ime.Trim() + glumac.prezime.Trim() + glumac.id,
                    Text = glumac.prezime
                };

                item.SubItems.Add(glumac.ime);

                if (cekiraniGlumci.Contains(item.Name))
                {
                    item.Checked = true;
                }

                lvGlumci.Items.Add(item);
            }
        }
        private void PretragaGlumaca()
        {
            string ime, prezime;

            ime = tbImePretraga.Text.EndsWith("...") ? "" : tbImePretraga.Text;
            prezime = tbPrezimePretraga.Text.EndsWith("...") ? "" : tbPrezimePretraga.Text;

            lvGlumci.ItemChecked -= LvGlumci_ItemChecked;

            lvGlumci.Items.Clear();
            lvGlumci.Groups.Clear();

            List<glumac> glumci = filmoviUtil.GetGlumciUzPretragu(prezime, ime);

            foreach (var glumac in glumci)
            {
                ListViewGroup lvg = lvGlumci.Groups["lvg" + glumac.prezime[0]];

                if (lvg == null)
                {
                    lvg = new ListViewGroup
                    {
                        Header = glumac.prezime[0].ToString(),
                        Name = "lvg" + glumac.prezime[0]
                    };

                    lvGlumci.Groups.Add(lvg);
                }

                ListViewItem item = new ListViewItem
                {
                    Group = lvg,
                    Name = "lvi" + glumac.ime.Trim() + glumac.prezime.Trim() + glumac.id,
                    Text = glumac.prezime
                };

                item.SubItems.Add(glumac.ime);

                if (cekiraniGlumci.Contains(item.Name))
                {
                    item.Checked = true;
                }

                lvGlumci.Items.Add(item);
            }

            if (lvGlumci.Items.Count > 0)
            {
                lvGlumci.Items[0].Selected = true;
                lvGlumci.TopItem = lvGlumci.Items[0];
                lvGlumci.EnsureVisible(0);
            }

            lvGlumci.ItemChecked += LvGlumci_ItemChecked;
        }
        private bool IsPrviElemenatUGrupiGlumci()
        {
            if (lvGlumci.SelectedItems.Count > 0)
            {
                var item = lvGlumci.SelectedItems[0];

                string grupa = "lvg" + item.Text[0];

                if (lvGlumci.Groups[grupa].Items[0] == item)
                {
                    return true;
                }
            }

            return false;
        }
        private bool IsPoslednjiElemenatUGrupiGlumci()
        {
            if (lvGlumci.SelectedItems.Count > 0)
            {
                var item = lvGlumci.SelectedItems[0];

                string grupa = "lvg" + item.Text[0];

                if (lvGlumci.Groups[grupa].Items[lvGlumci.Groups[grupa].Items.Count - 1] == item)
                {
                    return true;
                }
            }
            return false;
        }

        #region BtnGlumci
        private void BtnGlumci_MouseEnter(object sender, EventArgs e)
        {
            pnlGlumci.Visible = true;
            tbPrezimePretraga.Focus();
        }
        private void BtnGlumci_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = btnGlumci.PointToClient(Cursor.Position);

            if (pozicijaKursora.Y >= btnGlumci.Height &&
                pozicijaKursora.X >= 0 &&
                pozicijaKursora.X <= btnGlumci.Width)
            {
                pnlGlumci.Visible = true;
                tbPrezimePretraga.Focus();
                return;
            }

            pnlGlumci.Visible = false;
        }
        private void BtnGlumci_Enter(object sender, EventArgs e)
        {
            pnlGlumci.Visible = true;
            tbPrezimePretraga.Focus();
        }
        private void BtnGlumci_Leave(object sender, EventArgs e)
        {
            pnlGlumci.Visible = false;
        }
        #endregion

        #region TbPrezimePretraga i TbImePretraga
        private void TbPrezimePretraga_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = tbPrezimePretraga.PointToClient(Cursor.Position);

            if (pozicijaKursora.Y > 0 &&
                pozicijaKursora.X >= 0)
            {
                pnlGlumci.Visible = true;
                tbPrezimePretraga.Focus();
                return;
            }

            pnlGlumci.Visible = false;
        }
        private void TbImePretraga_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = tbImePretraga.PointToClient(Cursor.Position);

            if (pozicijaKursora.Y > 0 &&
                pozicijaKursora.X <= tbImePretraga.Width - 5)
            {
                pnlGlumci.Visible = true;
                tbImePretraga.Focus();
                return;
            }

            pnlGlumci.Visible = false;
        }
        private void TbPrezime_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbPrezime_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("Prezime..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }
            else if (textBox.Text.EndsWith("Ime..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }

            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ .'-]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                if (textBox.Tag.ToString().Equals("Prezime"))
                {
                    textBox.Text = "Prezime...";
                }
                else if (textBox.Tag.ToString().Equals("Ime"))
                {
                    textBox.Text = "Ime...";
                }
                textBox.ForeColor = Color.LightGray;
            }

            PretragaGlumaca();

            textBox.TextChanged += TbPrezime_TextChanged;
        }
        private void TbPrezime_KeyDown(object sender, KeyEventArgs e) // manipulacija redoslijedom selekcije glumaca u listView-u
        {
            TextBox textBox = sender as TextBox;
            // ako je neki textBox fokusiran i pritisnut je taster Up vrsi se selekcija glumca koji se nalazi iznad vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi glumac u listi
            if (e.KeyCode == Keys.Up)
            {
                e.SuppressKeyPress = true;

                lvGlumci.Focus();

                if (IsPrviElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;
                    SendKeys.SendWait("{UP}");
                }

                lvGlumciSimuliraniKeyDown = true;
                SendKeys.SendWait("{UP}");

                tbFokusiranProgramski = true;
                textBox.Focus();
            }
            // ako je neki textBox fokusiran i pritisnut je taster Down vrsi se selekcija glumca koji se nalazi ispod vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi glumac u listi
            else if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;

                lvGlumci.Focus();

                if (IsPoslednjiElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;
                    SendKeys.SendWait("{DOWN}");
                }

                lvGlumciSimuliraniKeyDown = true;
                SendKeys.SendWait("{DOWN}");

                tbFokusiranProgramski = true;
                textBox.Focus();

            }
            // ako je pritisnut taster Right fokusiramo komponentu koja se nalazi desno od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Right)
            {
                e.SuppressKeyPress = true;

                if (textBox.Tag.ToString() == "Prezime")
                {
                    tbImePretraga.Focus();
                }
                else if (textBox.Tag.ToString() == "Ime")
                {
                    pnlGlumci.Visible = false;
                    chbAktivanFilm.Focus();
                }
            }
            // ako je pritisnut taster Left fokusiramo komponentu koja se nalazi lijevo od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Left)
            {
                e.SuppressKeyPress = true;

                if (textBox.Tag.ToString() == "Ime")
                {
                    tbPrezimePretraga.Focus();
                }
                else if (textBox.Tag.ToString() == "Prezime")
                {
                    pnlGlumci.Visible = false;
                    btnZanr.Focus();
                }
            }
            else if (e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;

                if (lvGlumci.SelectedItems.Count > 0)
                {
                    lvGlumci.SelectedItems[0].Checked = !lvGlumci.SelectedItems[0].Checked;
                }
            }
            // Control + Shift
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.D)
                {
                    e.SuppressKeyPress = true;
                    btnDodajFilm.PerformClick();
                }
                else if (e.KeyCode == Keys.I)
                {
                    e.SuppressKeyPress = true;
                    btnIzmijeniFilm.PerformClick();
                }
                else if (e.KeyCode == Keys.A)
                {
                    e.SuppressKeyPress = true;
                    btnAktivirajFilm.PerformClick();
                }
                else if (e.KeyCode == Keys.N)
                {
                    e.SuppressKeyPress = true;
                    btnNazad.PerformClick();
                }
            }
        }

        #endregion

        #region LvGlumci
        private void LvGlumci_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = lvGlumci.PointToClient(Cursor.Position);

            if (pozicijaKursora.Y <= 0 &&
                pozicijaKursora.X >= 0 &&
                pozicijaKursora.X <= lvGlumci.Width)
            {
                pnlGlumci.Visible = true;
                tbPrezimePretraga.Focus();
                return;
            }
            else if (pozicijaKursora.Y >= lvGlumci.Height &&
                pozicijaKursora.X >= 0 &&
                pozicijaKursora.X <= lvGlumci.Width)
            {
                pnlGlumci.Visible = true;
                tbPrezimePretraga.Focus();
                return;
            }

            pnlGlumci.Visible = false;
        }
        private void LvGlumci_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                lvGlumciPrviItemChecked = true;
                e.Item.Selected = true;
                if (!cekiraniGlumci.Contains(e.Item.Name))
                {
                    cekiraniGlumci.Add(e.Item.Name);
                }
            }
            else
            {
                cekiraniGlumci.Remove(e.Item.Name);
            }

            if(!lvGlumciPrviItemChecked)
            {
                return;
            }
            glumciPretrazivani = true;
            Pretraga();
        }
        private void LvGlumci_KeyDown(object sender, KeyEventArgs e)
        {
            if (lvGlumciSimuliraniKeyDown)
            {
                lvGlumciSimuliraniKeyDown = false;
                return;
            }
            if (e.KeyCode == Keys.Up)
            {
                if (IsPrviElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;

                    SendKeys.SendWait("{UP}");
                }
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (IsPoslednjiElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;

                    SendKeys.SendWait("{DOWN}");
                }
            }

        }
        #endregion

        #region SpcSplitGlumci
        private void SpcSplitGlumci_Panel2_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = spcSplitGlumci.Panel2.PointToClient(Cursor.Position);

            if (pozicijaKursora.Y > spcSplitGlumci.Panel2.Height - 5 ||
               (pozicijaKursora.X < 0 || pozicijaKursora.X > spcSplitGlumci.Panel2.Width - 5))
            {
                pnlGlumci.Visible = false;
            }
        }
        private void SpcSplitGlumci_MouseLeave(object sender, EventArgs e)
        {
            var pozicijaKursora = spcSplitGlumci.PointToClient(Cursor.Position);

            if (pozicijaKursora.X < 0 || pozicijaKursora.X > spcSplitGlumci.Width - 5)
            {
                pnlGlumci.Visible = false;
            }
        }
        #endregion

        #region CmbNacinPretrageGlumci
        private void CmbNacinPretrageGlumci_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Enter)
            {
                if (lvGlumci.CheckedItems.Count > 0)
                {
                    Pretraga();
                }
                tbPrezimePretraga.Focus();
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Up)
            {
                cmbNacinPretrageGlumci.SelectedIndexChanged -= CmbNacinPretrageGlumci_SelectedIndexChanged;
                cmbNacinPretrageGlumci.SelectedIndex = 0;
                cmbNacinPretrageGlumci.SelectedIndexChanged += CmbNacinPretrageGlumci_SelectedIndexChanged;
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                cmbNacinPretrageGlumci.SelectedIndexChanged -= CmbNacinPretrageGlumci_SelectedIndexChanged;
                cmbNacinPretrageGlumci.SelectedIndex = 1;
                cmbNacinPretrageGlumci.SelectedIndexChanged += CmbNacinPretrageGlumci_SelectedIndexChanged;
                e.SuppressKeyPress = true;
            }
        }
        private void CmbNacinPretrageGlumci_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvGlumci.CheckedItems.Count > 0)
            {
                Pretraga();
            }
            tbPrezimePretraga.Focus();
        }
        #endregion

        #endregion

        #region Tab kontrola
        private void TbNaslovFilma_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                tbGodina.Focus();
            }
        }

        private void TbGodina_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                btnZanr.Focus();
            }
        }

        private void TbZanrPretraga_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                cmbNacinPretrageZanrovi.Focus();
            }
        }
        private void CmbNacinPretrageZanrovi_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                btnGlumci.Focus();
                pnlZanr.Visible = false;
            }
        }

        private void TbPrezimePretraga_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                cmbNacinPretrageGlumci.Focus();
            }
        }

        private void TbImePretraga_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                cmbNacinPretrageGlumci.Focus();
            }
        }

        private void CmbNacinPretrageGlumci_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                chbAktivanFilm.Focus();
                pnlGlumci.Visible = false;
            }
        }

        private void ChbAktivanFilm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                btnDodajFilm.Focus();
            }
        }

        private void BtnDodajFilm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                if (btnIzmijeniFilm.Enabled)
                {
                    btnIzmijeniFilm.Focus();
                }
                else
                {
                    btnNazad.Focus();
                }
            }
        }

        private void BtnIzmijeniFilm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                btnAktivirajFilm.Focus();
            }
        }

        private void BtnAktivirajFilm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                btnNazad.Focus();
            }
        }

        private void BtnNazad_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                tbNaslovFilma.Focus();
            }
        }
        #endregion

    }
}

