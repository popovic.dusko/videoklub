﻿namespace VideoKlub
{
    partial class RadSaKorisnicima
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadSaKorisnicima));
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvKorisnici = new System.Windows.Forms.DataGridView();
            this.clnPrezime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnIme = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnJMBG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnBroj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnAktivan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaduzenjefilmaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsrKorisnici = new System.Windows.Forms.BindingSource(this.components);
            this.btnDodajKorisnika = new System.Windows.Forms.Button();
            this.btnIzmijeniKorisnika = new System.Windows.Forms.Button();
            this.btnAktivirajKorisnika = new System.Windows.Forms.Button();
            this.btnNazad = new System.Windows.Forms.Button();
            this.grbPretragaKorisnika = new System.Windows.Forms.GroupBox();
            this.chbAktivanKorisnik = new System.Windows.Forms.CheckBox();
            this.tbBrojKartice = new System.Windows.Forms.TextBox();
            this.tbJMBG = new System.Windows.Forms.TextBox();
            this.tbIme = new System.Windows.Forms.TextBox();
            this.tbPrezime = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKorisnici)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsrKorisnici)).BeginInit();
            this.grbPretragaKorisnika.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackgroundImage = global::VideoKlub.Properties.Resources.minimize_button;
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Location = new System.Drawing.Point(700, 3);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(50, 50);
            this.btnMinimize.TabIndex = 14;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::VideoKlub.Properties.Resources.close_button;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(756, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(50, 50);
            this.btnClose.TabIndex = 13;
            this.btnClose.TabStop = false;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // dgvKorisnici
            // 
            this.dgvKorisnici.AllowUserToAddRows = false;
            this.dgvKorisnici.AllowUserToDeleteRows = false;
            this.dgvKorisnici.AutoGenerateColumns = false;
            this.dgvKorisnici.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKorisnici.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvKorisnici.ColumnHeadersHeight = 30;
            this.dgvKorisnici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvKorisnici.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clnPrezime,
            this.clnIme,
            this.clnJMBG,
            this.clnBroj,
            this.clnAktivan,
            this.clnId,
            this.zaduzenjefilmaDataGridViewTextBoxColumn});
            this.dgvKorisnici.DataSource = this.bsrKorisnici;
            this.dgvKorisnici.Location = new System.Drawing.Point(12, 115);
            this.dgvKorisnici.MultiSelect = false;
            this.dgvKorisnici.Name = "dgvKorisnici";
            this.dgvKorisnici.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKorisnici.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvKorisnici.RowHeadersVisible = false;
            this.dgvKorisnici.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvKorisnici.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvKorisnici.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvKorisnici.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvKorisnici.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKorisnici.Size = new System.Drawing.Size(786, 252);
            this.dgvKorisnici.TabIndex = 15;
            this.dgvKorisnici.TabStop = false;
            this.dgvKorisnici.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvKorisnici_CellEnter);
            this.dgvKorisnici.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DgvKorisnici_CellFormatting);
            this.dgvKorisnici.Scroll += new System.Windows.Forms.ScrollEventHandler(this.DgvKorisnici_Scroll);
            // 
            // clnPrezime
            // 
            this.clnPrezime.DataPropertyName = "prezime";
            this.clnPrezime.HeaderText = "Prezime";
            this.clnPrezime.Name = "clnPrezime";
            this.clnPrezime.ReadOnly = true;
            this.clnPrezime.Width = 175;
            // 
            // clnIme
            // 
            this.clnIme.DataPropertyName = "ime";
            this.clnIme.HeaderText = "Ime";
            this.clnIme.Name = "clnIme";
            this.clnIme.ReadOnly = true;
            this.clnIme.Width = 175;
            // 
            // clnJMBG
            // 
            this.clnJMBG.DataPropertyName = "jmbg";
            this.clnJMBG.HeaderText = "JMBG";
            this.clnJMBG.Name = "clnJMBG";
            this.clnJMBG.ReadOnly = true;
            this.clnJMBG.Width = 160;
            // 
            // clnBroj
            // 
            this.clnBroj.DataPropertyName = "broj_kartice";
            this.clnBroj.HeaderText = "Broj kartice";
            this.clnBroj.Name = "clnBroj";
            this.clnBroj.ReadOnly = true;
            this.clnBroj.Width = 155;
            // 
            // clnAktivan
            // 
            this.clnAktivan.DataPropertyName = "kartica_aktivna";
            this.clnAktivan.HeaderText = "Aktivan";
            this.clnAktivan.Name = "clnAktivan";
            this.clnAktivan.ReadOnly = true;
            this.clnAktivan.Width = 108;
            // 
            // clnId
            // 
            this.clnId.DataPropertyName = "id";
            this.clnId.HeaderText = "id";
            this.clnId.Name = "clnId";
            this.clnId.ReadOnly = true;
            this.clnId.Visible = false;
            // 
            // zaduzenjefilmaDataGridViewTextBoxColumn
            // 
            this.zaduzenjefilmaDataGridViewTextBoxColumn.DataPropertyName = "zaduzenje_filma";
            this.zaduzenjefilmaDataGridViewTextBoxColumn.HeaderText = "zaduzenje_filma";
            this.zaduzenjefilmaDataGridViewTextBoxColumn.Name = "zaduzenjefilmaDataGridViewTextBoxColumn";
            this.zaduzenjefilmaDataGridViewTextBoxColumn.ReadOnly = true;
            this.zaduzenjefilmaDataGridViewTextBoxColumn.Visible = false;
            // 
            // bsrKorisnici
            // 
            this.bsrKorisnici.DataSource = typeof(VideoKlub.korisnik);
            // 
            // btnDodajKorisnika
            // 
            this.btnDodajKorisnika.FlatAppearance.BorderSize = 2;
            this.btnDodajKorisnika.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodajKorisnika.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDodajKorisnika.Location = new System.Drawing.Point(12, 380);
            this.btnDodajKorisnika.Name = "btnDodajKorisnika";
            this.btnDodajKorisnika.Size = new System.Drawing.Size(144, 58);
            this.btnDodajKorisnika.TabIndex = 5;
            this.btnDodajKorisnika.Text = "&Dodaj korisnika";
            this.btnDodajKorisnika.UseVisualStyleBackColor = true;
            this.btnDodajKorisnika.Click += new System.EventHandler(this.BtnDodajKorisnika_Click);
            // 
            // btnIzmijeniKorisnika
            // 
            this.btnIzmijeniKorisnika.FlatAppearance.BorderSize = 2;
            this.btnIzmijeniKorisnika.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIzmijeniKorisnika.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzmijeniKorisnika.Location = new System.Drawing.Point(196, 380);
            this.btnIzmijeniKorisnika.Name = "btnIzmijeniKorisnika";
            this.btnIzmijeniKorisnika.Size = new System.Drawing.Size(144, 58);
            this.btnIzmijeniKorisnika.TabIndex = 6;
            this.btnIzmijeniKorisnika.Text = "&Izmijeni korisnika";
            this.btnIzmijeniKorisnika.UseVisualStyleBackColor = true;
            this.btnIzmijeniKorisnika.EnabledChanged += new System.EventHandler(this.BtnIzmijeniKorisnika_EnabledChanged);
            this.btnIzmijeniKorisnika.Click += new System.EventHandler(this.BtnIzmijeniKorisnika_Click);
            // 
            // btnAktivirajKorisnika
            // 
            this.btnAktivirajKorisnika.FlatAppearance.BorderSize = 2;
            this.btnAktivirajKorisnika.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAktivirajKorisnika.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAktivirajKorisnika.Location = new System.Drawing.Point(360, 380);
            this.btnAktivirajKorisnika.Name = "btnAktivirajKorisnika";
            this.btnAktivirajKorisnika.Size = new System.Drawing.Size(144, 58);
            this.btnAktivirajKorisnika.TabIndex = 7;
            this.btnAktivirajKorisnika.Text = "&Aktiviraj / deaktiviraj";
            this.btnAktivirajKorisnika.UseVisualStyleBackColor = true;
            this.btnAktivirajKorisnika.EnabledChanged += new System.EventHandler(this.BtnIzmijeniKorisnika_EnabledChanged);
            this.btnAktivirajKorisnika.Click += new System.EventHandler(this.BtnAktivirajKorisnika_Click);
            // 
            // btnNazad
            // 
            this.btnNazad.FlatAppearance.BorderSize = 2;
            this.btnNazad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNazad.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNazad.Location = new System.Drawing.Point(654, 380);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(144, 58);
            this.btnNazad.TabIndex = 8;
            this.btnNazad.Text = "&Nazad";
            this.btnNazad.UseVisualStyleBackColor = true;
            this.btnNazad.Click += new System.EventHandler(this.BtnNazad_Click);
            // 
            // grbPretragaKorisnika
            // 
            this.grbPretragaKorisnika.Controls.Add(this.chbAktivanKorisnik);
            this.grbPretragaKorisnika.Controls.Add(this.tbBrojKartice);
            this.grbPretragaKorisnika.Controls.Add(this.tbJMBG);
            this.grbPretragaKorisnika.Controls.Add(this.tbIme);
            this.grbPretragaKorisnika.Controls.Add(this.tbPrezime);
            this.grbPretragaKorisnika.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbPretragaKorisnika.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbPretragaKorisnika.Location = new System.Drawing.Point(12, 49);
            this.grbPretragaKorisnika.Name = "grbPretragaKorisnika";
            this.grbPretragaKorisnika.Size = new System.Drawing.Size(786, 60);
            this.grbPretragaKorisnika.TabIndex = 20;
            this.grbPretragaKorisnika.TabStop = false;
            this.grbPretragaKorisnika.Text = "Pretraga korisnika";
            this.grbPretragaKorisnika.Paint += new System.Windows.Forms.PaintEventHandler(this.GrbPretragaKorisnika_Paint);
            // 
            // chbAktivanKorisnik
            // 
            this.chbAktivanKorisnik.AutoSize = true;
            this.chbAktivanKorisnik.Location = new System.Drawing.Point(675, 29);
            this.chbAktivanKorisnik.Name = "chbAktivanKorisnik";
            this.chbAktivanKorisnik.Size = new System.Drawing.Size(87, 23);
            this.chbAktivanKorisnik.TabIndex = 4;
            this.chbAktivanKorisnik.Tag = "Aktivan korisnik";
            this.chbAktivanKorisnik.Text = "Aktivan";
            this.chbAktivanKorisnik.UseVisualStyleBackColor = true;
            this.chbAktivanKorisnik.CheckedChanged += new System.EventHandler(this.ChbAktivanKorisnik_CheckedChanged);
            // 
            // tbBrojKartice
            // 
            this.tbBrojKartice.ForeColor = System.Drawing.Color.Silver;
            this.tbBrojKartice.Location = new System.Drawing.Point(517, 27);
            this.tbBrojKartice.MaxLength = 20;
            this.tbBrojKartice.Name = "tbBrojKartice";
            this.tbBrojKartice.Size = new System.Drawing.Size(141, 27);
            this.tbBrojKartice.TabIndex = 3;
            this.tbBrojKartice.Tag = "Broj kartice";
            this.tbBrojKartice.Text = "Broj kartice...";
            this.tbBrojKartice.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbBrojKartice.TextChanged += new System.EventHandler(this.TbBrojKartice_TextChanged);
            this.tbBrojKartice.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbBrojKartice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            this.tbBrojKartice.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // tbJMBG
            // 
            this.tbJMBG.ForeColor = System.Drawing.Color.Silver;
            this.tbJMBG.Location = new System.Drawing.Point(355, 27);
            this.tbJMBG.MaxLength = 13;
            this.tbJMBG.Name = "tbJMBG";
            this.tbJMBG.Size = new System.Drawing.Size(151, 27);
            this.tbJMBG.TabIndex = 2;
            this.tbJMBG.Tag = "JMBG";
            this.tbJMBG.Text = "JMBG...";
            this.tbJMBG.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbJMBG.TextChanged += new System.EventHandler(this.TbJMBG_TextChanged);
            this.tbJMBG.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbJMBG.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            this.tbJMBG.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // tbIme
            // 
            this.tbIme.ForeColor = System.Drawing.Color.Silver;
            this.tbIme.Location = new System.Drawing.Point(181, 27);
            this.tbIme.MaxLength = 255;
            this.tbIme.Name = "tbIme";
            this.tbIme.Size = new System.Drawing.Size(163, 27);
            this.tbIme.TabIndex = 1;
            this.tbIme.Tag = "Ime";
            this.tbIme.Text = "Ime...";
            this.tbIme.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbIme.TextChanged += new System.EventHandler(this.TbPrezime_TextChanged);
            this.tbIme.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbIme.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            this.tbIme.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // tbPrezime
            // 
            this.tbPrezime.ForeColor = System.Drawing.Color.Silver;
            this.tbPrezime.Location = new System.Drawing.Point(7, 27);
            this.tbPrezime.MaxLength = 255;
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.Size = new System.Drawing.Size(163, 27);
            this.tbPrezime.TabIndex = 0;
            this.tbPrezime.Tag = "Prezime";
            this.tbPrezime.Text = "Prezime...";
            this.tbPrezime.Click += new System.EventHandler(this.Tb_EnterFocus);
            this.tbPrezime.TextChanged += new System.EventHandler(this.TbPrezime_TextChanged);
            this.tbPrezime.Enter += new System.EventHandler(this.Tb_EnterFocus);
            this.tbPrezime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Tb_KeyDown);
            this.tbPrezime.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Tb_MouseUp);
            // 
            // RadSaKorisnicima
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(810, 450);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grbPretragaKorisnika);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.btnAktivirajKorisnika);
            this.Controls.Add(this.btnIzmijeniKorisnika);
            this.Controls.Add(this.btnDodajKorisnika);
            this.Controls.Add(this.dgvKorisnici);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RadSaKorisnicima";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RadSaKorisnicima";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RadSaKorisnicima_FormClosing);
            this.Load += new System.EventHandler(this.RadSaKorisnicima_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKorisnici)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsrKorisnici)).EndInit();
            this.grbPretragaKorisnika.ResumeLayout(false);
            this.grbPretragaKorisnika.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgvKorisnici;
        private System.Windows.Forms.BindingSource bsrKorisnici;
        private System.Windows.Forms.Button btnDodajKorisnika;
        private System.Windows.Forms.Button btnIzmijeniKorisnika;
        private System.Windows.Forms.Button btnAktivirajKorisnika;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.GroupBox grbPretragaKorisnika;
        private System.Windows.Forms.TextBox tbIme;
        private System.Windows.Forms.TextBox tbPrezime;
        private System.Windows.Forms.TextBox tbBrojKartice;
        private System.Windows.Forms.TextBox tbJMBG;
        private System.Windows.Forms.CheckBox chbAktivanKorisnik;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnPrezime;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnIme;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnJMBG;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnBroj;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnAktivan;
        private System.Windows.Forms.DataGridViewTextBoxColumn clnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn zaduzenjefilmaDataGridViewTextBoxColumn;
    }
}