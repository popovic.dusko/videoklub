﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using VideoKlub.Util;

namespace VideoKlub
{
    public partial class KatalogFilmova : Form
    {
        #region Promjenljive
        private Form roditeljskaForma = null;
        private bool tbFokusiranProgramski = false;
        private bool lvGlumciSimuliraniKeyDown = false;
        private bool lvZanroviSimuliraniKeyDown = false;
        private bool lvGlumciPrviItemChecked = false;
        private bool lvZanroviPrviItemChecked = false;
        private bool nudOcjenaOdPrvaIzmjena = true;
        private bool nudOcjenaDoPrvaIzmjena = true;
        private bool nudCijenaOdPrvaIzmjena = true;
        private bool nudCijenaDoPrvaIzmjena = true;
        private bool nudOcjenaOdClicked = false;
        private bool nudOcjenaDoClicked = false;
        private bool nudCijenaOdClicked = false;
        private bool nudCijenaDoClicked = false;
        private int offset = 0; // potrebno za ucitavanje filmova prilikom scroll-ovanja
        private List<string> cekiraniGlumci = new List<string>();
        private List<string> cekiraniZanrovi = new List<string>();
        FilmoviUtil filmoviUtil = new FilmoviUtil();
        #endregion
        public KatalogFilmova(Form roditeljskaForma)
        {
            InitializeComponent();

            this.roditeljskaForma = roditeljskaForma;

            cmbPoredajPo.SelectedIndex = 0;
            cmbNacinPretrageZanrovi.SelectedIndex = 0;
            cmbNacinPretrageGlumci.SelectedIndex = 0;

            nudGodinaDo.Value = DateTime.Now.Year;

            nudGodinaOd.Text = "";
            nudGodinaDo.Text = "";
            nudOcjenaOd.Text = "";
            nudOcjenaDo.Text = "";
            nudCijenaOd.Text = "";
            nudCijenaDo.Text = "";

            nudOcjenaOd.TextChanged += Nud_TextChanged;
            nudOcjenaDo.TextChanged += Nud_TextChanged;
            nudGodinaOd.TextChanged += Nud_TextChanged;
            nudGodinaDo.TextChanged += Nud_TextChanged;
            nudCijenaOd.TextChanged += Nud_TextChanged;
            nudCijenaDo.TextChanged += Nud_TextChanged;

            nudGodinaOd.MouseWheel += Nud_MouseWheel;
            nudGodinaDo.MouseWheel += Nud_MouseWheel;
            nudOcjenaOd.MouseWheel += Nud_MouseWheel;
            nudOcjenaDo.MouseWheel += Nud_MouseWheel;
            nudCijenaOd.MouseWheel += Nud_MouseWheel;
            nudCijenaDo.MouseWheel += Nud_MouseWheel;
            cmbPoredajPo.MouseWheel += Nud_MouseWheel;
            cmbNacinPretrageGlumci.MouseWheel += Nud_MouseWheel;
            cmbNacinPretrageZanrovi.MouseWheel += Nud_MouseWheel;

            flpFilmoviPanel.MouseWheel += Flp_MouseWheel;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, 0, 1, Size.Width, 1);
            g.DrawLine(pen, 1, 1, 1, Size.Height - 1);
            g.DrawLine(pen, 0, Size.Height - 1, Size.Width, Size.Height - 1);
            g.DrawLine(pen, Size.Width - 1, 0, Size.Width - 1, Size.Height);

            pen.Color = Color.Gray;
        } // za iscrtavanje granica forme

        private void Pretraga()
        {
            string naslov = tbFilmoviPretraga.Text.EndsWith("...") ? "" : tbFilmoviPretraga.Text;

            List<zanr> zanrovi = null;

            if(cekiraniZanrovi.Count > 0)
            {
                zanrovi = new List<zanr>();
                foreach(string zanr in cekiraniZanrovi)
                {
                    int idZanra = int.Parse(Regex.Replace(zanr, "[^0-9]", ""));
                    zanrovi.Add(filmoviUtil.GetZanr(idZanra));
                }
            }
            
            List<glumac> glumci = null;

            if(cekiraniGlumci.Count > 0)
            {
                glumci = new List<glumac>();
                foreach (string glumac in cekiraniGlumci)
                {
                    int idGlumca = int.Parse(Regex.Replace(glumac, "[^0-9]", ""));
                    glumci.Add(filmoviUtil.GetGlumac(idGlumca));
                }
            }


            List<film> filmovi = filmoviUtil.PretraziFilmoveZaKatalog(naslov, cmbPoredajPo.Text, 
                    nudGodinaOd.Text == "" ? 1900 : (int)nudGodinaOd.Value, nudGodinaDo.Text == "" ? DateTime.Now.Year : (int)nudGodinaDo.Value,
                    nudCijenaOd.Text == "" ? 0.0m : nudCijenaOd.Value, nudCijenaDo.Text == ""? 999.0m : nudCijenaDo.Value, 
                    nudOcjenaOd.Text == "" ? 0.0m : nudOcjenaOd.Value, nudOcjenaDo.Text == "" ? 5.0m : nudOcjenaDo.Value,
                    zanrovi, cmbNacinPretrageZanrovi.Text, glumci, cmbNacinPretrageGlumci.Text).Take(10).ToList();

            flpFilmoviPanel.Controls.Clear();
            FilmPanel filmPanel = null;

            foreach (film film in filmovi)
            {
                filmPanel = new FilmPanel(film, filmoviUtil.GetZanrove(film.id), filmoviUtil.GetGlumci(film.id),
                    filmoviUtil.GetOcjena(film.id), filmoviUtil.GetBrojIzdavanja(film.id), filmoviUtil.GetCijena(film.naslov, film.godina));

                if (flpFilmoviPanel.Controls.Count == 0)
                {
                    filmPanel.Location = new Point(4, 4);
                }
                else
                {
                    int y = flpFilmoviPanel.Controls[flpFilmoviPanel.Controls.Count - 1].Location.Y + filmPanel.Height;
                    filmPanel.Location = new Point(4, y);
                }

                flpFilmoviPanel.Controls.Add(filmPanel);
                flpFilmoviPanel.SetFlowBreak(filmPanel, true);
            }

            offset = 0;
        }

        #region Button - i
        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        #endregion

        #region Zanrovi
        private void UcitajZanrove()
        {
            lvZanrovi.Items.Clear();
            lvZanrovi.Groups.Clear();

            List<kategorija_zanra> kategorijeZanra = filmoviUtil.GetKategorijeZanra();

            foreach (var kategorijaZanra in kategorijeZanra)
            {
                ListViewGroup lvg = new ListViewGroup
                {
                    Header = kategorijaZanra.opis,
                    Name = "lvg" + kategorijaZanra.opis.Replace(" ", ""),
                };

                lvZanrovi.Groups.Add(lvg);

                List<zanr> zanroviIzKategorije = filmoviUtil.GetZanroveIzKategorije(kategorijaZanra.id);

                if (zanroviIzKategorije != null && zanroviIzKategorije.Count != 0)
                {
                    foreach (var zanr in zanroviIzKategorije)
                    {
                        ListViewItem item = new ListViewItem
                        {
                            Group = lvg,
                            Name = "lvi" + zanr.naziv.Replace(" ", "") + kategorijaZanra.opis.Replace(" ", "") + zanr.id,
                            Text = zanr.naziv,
                            Tag = kategorijaZanra.opis
                        };

                        if (cekiraniZanrovi.Contains(item.Name))
                        {
                            item.Checked = true;
                        }

                        lvZanrovi.Items.Add(item);
                    }
                }
            }
        }
        private void PretragaZanrova()
        {
            string nazivZanra = tbZanrPretraga.Text.EndsWith("...") ? "" : tbZanrPretraga.Text;

            lvZanrovi.ItemChecked -= LvZanrovi_ItemChecked;

            lvZanrovi.Items.Clear();
            lvZanrovi.Groups.Clear();

            List<kategorija_zanra> kategorijeZanra = filmoviUtil.GetKategorijeZanra();

            foreach (var kategorijaZanra in kategorijeZanra)
            {
                ListViewGroup lvg = new ListViewGroup
                {
                    Header = kategorijaZanra.opis,
                    Name = "lvg" + kategorijaZanra.opis.Replace(" ", ""),
                };

                lvZanrovi.Groups.Add(lvg);

                List<zanr> zanroviIzKategorije = filmoviUtil.GetZanroveIzKategorijeUzPretragu(kategorijaZanra.id, nazivZanra);

                if (zanroviIzKategorije != null && zanroviIzKategorije.Count != 0)
                {
                    foreach (var zanr in zanroviIzKategorije)
                    {
                        ListViewItem item = new ListViewItem
                        {
                            Group = lvg,
                            Name = "lvi" + zanr.naziv.Replace(" ", "") + kategorijaZanra.opis.Replace(" ", "") + zanr.id,
                            Text = zanr.naziv,
                            Tag = kategorijaZanra.opis
                        };

                        if (cekiraniZanrovi.Contains(item.Name))
                        {
                            item.Checked = true;
                        }

                        lvZanrovi.Items.Add(item);
                    }
                }
            }

            if (lvZanrovi.Items.Count > 0)
            {
                lvZanrovi.Items[0].Selected = true;
                lvZanrovi.TopItem = lvZanrovi.Items[0];
                lvZanrovi.EnsureVisible(0);
            }

            lvZanrovi.ItemChecked += LvZanrovi_ItemChecked;
        }
        private bool IsPrviElemenatUGrupiZanrovi()
        {
            if (lvZanrovi.SelectedItems.Count > 0)
            {
                var item = lvZanrovi.SelectedItems[0];

                string grupa = "lvg" + item.Name.Replace("lvi" + item.Text, "");
                grupa = Regex.Replace(grupa, "[0-9]", "");

                if (lvZanrovi.Groups[grupa].Items[0] == item)
                {
                    return true;
                }
            }

            return false;
        }
        private bool IsPoslednjiElemenatUGrupiZanrovi()
        {
            if (lvZanrovi.SelectedItems.Count > 0)
            {
                var item = lvZanrovi.SelectedItems[0];

                string grupa = "lvg" + item.Name.Replace("lvi" + item.Text, "");
                grupa = Regex.Replace(grupa, "[0-9]", "");

                if (lvZanrovi.Groups[grupa].Items[lvZanrovi.Groups[grupa].Items.Count - 1] == item)
                {
                    return true;
                }
            }
            return false;
        }

        #region TbZanrPretraga
        private void TbZanrPretraga_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbZanrPretraga_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }

            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                textBox.Text = "Žanr...";
                textBox.ForeColor = Color.Gray;
            }

            PretragaZanrova();

            textBox.TextChanged += TbZanrPretraga_TextChanged;
        }
        private void TbZanrPretraga_KeyDown(object sender, KeyEventArgs e)
        {
            // ako je textBox fokusiran i pritisnut je taster Up vrsi se selekcija zanra koji se nalazi iznad vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi zanr u listi
            if (e.KeyCode == Keys.Up)
            {
                e.SuppressKeyPress = true;

                lvZanrovi.Focus();

                if (IsPrviElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;
                    SendKeys.SendWait("{UP}");
                }

                lvZanroviSimuliraniKeyDown = true;
                SendKeys.SendWait("{UP}");

                tbFokusiranProgramski = true;
                tbZanrPretraga.Focus();
            }
            // ako je neki textBox fokusiran i pritisnut je taster Down vrsi se selekcija zanra koji se nalazi ispod vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi zanr u listi
            else if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;

                lvZanrovi.Focus();

                if (IsPoslednjiElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;
                    SendKeys.SendWait("{DOWN}");
                }

                lvZanroviSimuliraniKeyDown = true;
                SendKeys.SendWait("{DOWN}");

                tbFokusiranProgramski = true;
                tbZanrPretraga.Focus();

            }
            else if (e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;

                if (lvZanrovi.SelectedItems.Count > 0)
                {
                    lvZanrovi.SelectedItems[0].Checked = !lvZanrovi.SelectedItems[0].Checked;
                }
            }
            // Control + Shift
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.P)
                {
                    e.SuppressKeyPress = true;
                    tbFilmoviPretraga.Focus();
                }
            }
        }
        #endregion

        #region LvZanrovi
        private void LvZanrovi_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                lvZanroviPrviItemChecked = true;
                e.Item.Selected = true;
                if (!cekiraniZanrovi.Contains(e.Item.Name))
                {
                    cekiraniZanrovi.Add(e.Item.Name);
                }
            }
            else
            {
                cekiraniZanrovi.Remove(e.Item.Name);
            }

            if (!lvZanroviPrviItemChecked)
            {
                return;
            }
            Pretraga();
        }
        private void LvZanrovi_KeyDown(object sender, KeyEventArgs e)
        {
            if (lvZanroviSimuliraniKeyDown)
            {
                lvZanroviSimuliraniKeyDown = false;
                return;
            }
            if (e.KeyCode == Keys.Up)
            {
                if (IsPrviElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;

                    SendKeys.SendWait("{UP}");
                }
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (IsPoslednjiElemenatUGrupiZanrovi())
                {
                    lvZanroviSimuliraniKeyDown = true;

                    SendKeys.SendWait("{DOWN}");
                }
            }
        }
        #endregion

        #region CmbNacinPretrageZanrovi
        private void CmbNacinPretrageZanrovi_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Enter)
            {
                if (lvZanrovi.CheckedItems.Count > 0)
                {
                    //Pretraga();
                }
                tbZanrPretraga.Focus();
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Up)
            {
                cmbNacinPretrageZanrovi.SelectedIndexChanged -= CmbNacinPretrageZanrovi_SelectedIndexChanged;
                cmbNacinPretrageZanrovi.SelectedIndex = 0;
                cmbNacinPretrageZanrovi.SelectedIndexChanged += CmbNacinPretrageZanrovi_SelectedIndexChanged;
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                cmbNacinPretrageZanrovi.SelectedIndexChanged -= CmbNacinPretrageZanrovi_SelectedIndexChanged;
                cmbNacinPretrageZanrovi.SelectedIndex = 1;
                cmbNacinPretrageZanrovi.SelectedIndexChanged += CmbNacinPretrageZanrovi_SelectedIndexChanged;
                e.SuppressKeyPress = true;
            }
        }
        private void CmbNacinPretrageZanrovi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvZanrovi.CheckedItems.Count > 0)
            {
                Pretraga();
            }
            tbZanrPretraga.Focus();
        }
        #endregion
        #endregion

        #region Glumci
        private void UcitajGlumce()
        {

            lvGlumci.Items.Clear();
            lvGlumci.Groups.Clear();

            List<glumac> glumci = filmoviUtil.GetGlumci();

            foreach (var glumac in glumci)
            {
                ListViewGroup lvg = lvGlumci.Groups["lvg" + glumac.prezime[0]];

                if (lvg == null)
                {
                    lvg = new ListViewGroup
                    {
                        Header = glumac.prezime[0].ToString(),
                        Name = "lvg" + glumac.prezime[0]
                    };

                    lvGlumci.Groups.Add(lvg);
                }

                ListViewItem item = new ListViewItem
                {
                    Group = lvg,
                    Name = "lvi" + glumac.ime.Trim() + glumac.prezime.Trim() + glumac.id,
                    Text = glumac.prezime
                };

                item.SubItems.Add(glumac.ime);

                if (cekiraniGlumci.Contains(item.Name))
                {
                    item.Checked = true;
                }

                lvGlumci.Items.Add(item);
            }
        }
        private void PretragaGlumaca()
        {
            string ime, prezime;

            ime = tbImePretraga.Text.EndsWith("...") ? "" : tbImePretraga.Text;
            prezime = tbPrezimePretraga.Text.EndsWith("...") ? "" : tbPrezimePretraga.Text;

            lvGlumci.ItemChecked -= LvGlumci_ItemChecked;

            lvGlumci.Items.Clear();
            lvGlumci.Groups.Clear();

            List<glumac> glumci = filmoviUtil.GetGlumciUzPretragu(prezime, ime);

            foreach (var glumac in glumci)
            {
                ListViewGroup lvg = lvGlumci.Groups["lvg" + glumac.prezime[0]];

                if (lvg == null)
                {
                    lvg = new ListViewGroup
                    {
                        Header = glumac.prezime[0].ToString(),
                        Name = "lvg" + glumac.prezime[0]
                    };

                    lvGlumci.Groups.Add(lvg);
                }

                ListViewItem item = new ListViewItem
                {
                    Group = lvg,
                    Name = "lvi" + glumac.ime.Trim() + glumac.prezime.Trim() + glumac.id,
                    Text = glumac.prezime
                };

                item.SubItems.Add(glumac.ime);

                if (cekiraniGlumci.Contains(item.Name))
                {
                    item.Checked = true;
                }

                lvGlumci.Items.Add(item);
            }

            if (lvGlumci.Items.Count > 0)
            {
                lvGlumci.Items[0].Selected = true;
                lvGlumci.TopItem = lvGlumci.Items[0];
                lvGlumci.EnsureVisible(0);
            }

            lvGlumci.ItemChecked += LvGlumci_ItemChecked;
        }
        private bool IsPrviElemenatUGrupiGlumci()
        {
            if (lvGlumci.SelectedItems.Count > 0)
            {
                var item = lvGlumci.SelectedItems[0];

                string grupa = "lvg" + item.Text[0];

                if (lvGlumci.Groups[grupa].Items[0] == item)
                {
                    return true;
                }
            }

            return false;
        }
        private bool IsPoslednjiElemenatUGrupiGlumci()
        {
            if (lvGlumci.SelectedItems.Count > 0)
            {
                var item = lvGlumci.SelectedItems[0];

                string grupa = "lvg" + item.Text[0];

                if (lvGlumci.Groups[grupa].Items[lvGlumci.Groups[grupa].Items.Count - 1] == item)
                {
                    return true;
                }
            }
            return false;
        }

        #region TbPrezimePretraga i TbImePretraga
        private void TbPrezime_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbPrezime_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("Prezime..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }
            else if (textBox.Text.EndsWith("Ime..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }

            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-ZšđčćžŠĐČĆŽ. -]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                if (textBox.Tag.ToString().Equals("Prezime"))
                {
                    textBox.Text = "Prezime...";
                }
                else if (textBox.Tag.ToString().Equals("Ime"))
                {
                    textBox.Text = "Ime...";
                }
                textBox.ForeColor = Color.Gray;
            }

            PretragaGlumaca();

            textBox.TextChanged += TbPrezime_TextChanged;
        }
        private void TbPrezime_KeyDown(object sender, KeyEventArgs e) // manipulacija redoslijedom selekcije glumaca u listView-u
        {
            TextBox textBox = sender as TextBox;
            // ako je neki textBox fokusiran i pritisnut je taster Up vrsi se selekcija glumca koji se nalazi iznad vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi glumac u listi
            if (e.KeyCode == Keys.Up)
            {
                e.SuppressKeyPress = true;

                lvGlumci.Focus();

                if (IsPrviElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;
                    SendKeys.SendWait("{UP}");
                }

                lvGlumciSimuliraniKeyDown = true;
                SendKeys.SendWait("{UP}");

                tbFokusiranProgramski = true;
                textBox.Focus();
            }
            // ako je neki textBox fokusiran i pritisnut je taster Down vrsi se selekcija glumca koji se nalazi ispod vec odabranog
            // a ukoliko nije odabran ni jedan onda se selktuje prvi glumac u listi
            else if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;

                lvGlumci.Focus();

                if (IsPoslednjiElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;
                    SendKeys.SendWait("{DOWN}");
                }

                lvGlumciSimuliraniKeyDown = true;
                SendKeys.SendWait("{DOWN}");

                tbFokusiranProgramski = true;
                textBox.Focus();

            }
            // ako je pritisnut taster Right fokusiramo komponentu koja se nalazi desno od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Right)
            {
                e.SuppressKeyPress = true;

                if (textBox.Tag.ToString() == "Prezime")
                {
                    tbImePretraga.Focus();
                }
            }
            // ako je pritisnut taster Left fokusiramo komponentu koja se nalazi lijevo od fokusirane komponente u groupBox-u
            else if (e.KeyCode == Keys.Left)
            {
                e.SuppressKeyPress = true;

                if (textBox.Tag.ToString() == "Ime")
                {
                    tbPrezimePretraga.Focus();
                }
            }
            else if (e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;

                if (lvGlumci.SelectedItems.Count > 0)
                {
                    lvGlumci.SelectedItems[0].Checked = !lvGlumci.SelectedItems[0].Checked;
                }
            }
            // Control + Shift
            else if (e.Modifiers == (Keys.Control | Keys.Shift))
            {
                if (e.KeyCode == Keys.P)
                {
                    e.SuppressKeyPress = true;
                    tbFilmoviPretraga.Focus();
                }
            }
        }

        #endregion

        #region LvGlumci
        private void LvGlumci_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                lvGlumciPrviItemChecked = true;
                e.Item.Selected = true;
                if (!cekiraniGlumci.Contains(e.Item.Name))
                {
                    cekiraniGlumci.Add(e.Item.Name);
                }
            }
            else
            {
                cekiraniGlumci.Remove(e.Item.Name);
            }

            if (!lvGlumciPrviItemChecked)
            {
                return;
            }
            Pretraga();
        }
        private void LvGlumci_KeyDown(object sender, KeyEventArgs e)
        {
            if (lvGlumciSimuliraniKeyDown)
            {
                lvGlumciSimuliraniKeyDown = false;
                return;
            }
            if (e.KeyCode == Keys.Up)
            {
                if (IsPrviElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;

                    SendKeys.SendWait("{UP}");
                }
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (IsPoslednjiElemenatUGrupiGlumci())
                {
                    lvGlumciSimuliraniKeyDown = true;

                    SendKeys.SendWait("{DOWN}");
                }
            }

        }
        #endregion

        #region CmbNacinPretrageGlumci
        private void CmbNacinPretrageGlumci_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Enter)
            {
                if (lvGlumci.CheckedItems.Count > 0)
                {
                    //Pretraga();
                }
                tbPrezimePretraga.Focus();
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Up)
            {
                cmbNacinPretrageGlumci.SelectedIndexChanged -= CmbNacinPretrageGlumci_SelectedIndexChanged;
                cmbNacinPretrageGlumci.SelectedIndex = 0;
                cmbNacinPretrageGlumci.SelectedIndexChanged += CmbNacinPretrageGlumci_SelectedIndexChanged;
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                cmbNacinPretrageGlumci.SelectedIndexChanged -= CmbNacinPretrageGlumci_SelectedIndexChanged;
                cmbNacinPretrageGlumci.SelectedIndex = 1;
                cmbNacinPretrageGlumci.SelectedIndexChanged += CmbNacinPretrageGlumci_SelectedIndexChanged;
                e.SuppressKeyPress = true;
            }
        }
        private void CmbNacinPretrageGlumci_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvGlumci.CheckedItems.Count > 0)
            {
                Pretraga();
            }
            tbPrezimePretraga.Focus();
        }
        #endregion
        #endregion

        #region TextBox-ovi
        private void Tb_EnterFocus(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // onemogucavanje selekcije placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.SelectionStart = 0;
            }
            else
            {
                textBox.SelectAll();
            }
        }
        private void TbFilmoviPretraga_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // kako se event ne bi pojavljivao prilikom manipulacije sadrzajem textBox-a u ovom handler-u
            textBox.TextChanged -= TbFilmoviPretraga_TextChanged;

            // brisanje placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.Text = textBox.Text.Remove(1);
                textBox.SelectionStart = 1;
            }
            // provjera ispravnosti unosa
            var pozicijaKursora = textBox.SelectionStart;

            // pocetno slovo treba biti veliko
            if (!string.IsNullOrEmpty(textBox.Text))
            {
                char[] tekst = textBox.Text.ToCharArray();
                tekst[0] = Char.ToUpper(tekst[0]);
                textBox.Text = new string(tekst);
            }
            textBox.ForeColor = Color.Black;
            textBox.Text = Regex.Replace(textBox.Text, "[^a-zA-Z0-9šđčćžŠĐČĆŽ '.:!?,&-]", "");
            textBox.SelectionStart = pozicijaKursora;

            // ukoliko nema korisnickog unosa postaviti placeholder
            if (string.IsNullOrEmpty(textBox.Text))
            {
                textBox.Text = "Naslov filma...";
                textBox.ForeColor = Color.Gray;
            }

            Pretraga();

            textBox.TextChanged += TbFilmoviPretraga_TextChanged;
        }
        private void Tb_MouseUp(object sender, MouseEventArgs e)
        {
            // selekcija ili kompletnog sadrzaja textBox-a ili nista
            TextBox textBox = sender as TextBox;
            if (textBox.Text.EndsWith("..."))
            {
                textBox.SelectionLength = 0;
                textBox.SelectionStart = 0;
            }
            if (!textBox.Text.EndsWith("...") && textBox.SelectionLength == textBox.TextLength)
            {
                textBox.SelectAll();
            }
            else if (textBox.SelectionLength > 0)
            {
                textBox.SelectionLength = 0;
                textBox.SelectionStart = 0;
            }
        }
        private void TbPretraga_EnterFocus(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            // onemogucavanje selekcije placeholder-a
            if (textBox.Text.EndsWith("..."))
            {
                textBox.SelectionStart = 0;
            }
            else
            {
                if (tbFokusiranProgramski == false)
                {
                    textBox.SelectAll();
                }
                else
                {
                    tbFokusiranProgramski = false;
                }
            }
        }
        #endregion

        #region NumericUpDown
        private void KorekcijaUnosa(object sender)
        {
            NumericUpDown nud = sender as NumericUpDown;

            if (nud.Tag.ToString() == "GodinaOd")
            {
                if (nudGodinaOd.Value > nudGodinaDo.Value)
                {
                    nudGodinaOd.Value = nudGodinaDo.Value;
                }
            }
            else if (nud.Tag.ToString() == "GodinaDo")
            {
                if (nudGodinaDo.Value < nudGodinaOd.Value)
                {
                    nudGodinaDo.Value = nudGodinaOd.Value;
                }
            }
            else if (nud.Tag.ToString() == "OcjenaOd")
            {
                if (nudOcjenaOd.Value > nudOcjenaDo.Value)
                {
                    nudOcjenaOd.Value = nudOcjenaDo.Value;
                }
            }
            else if (nud.Tag.ToString() == "OcjenaDo")
            {
                if (nudOcjenaDo.Value < nudOcjenaOd.Value)
                {
                    nudOcjenaDo.Value = nudOcjenaOd.Value;
                }
            }
            else if (nud.Tag.ToString() == "CijenaOd")
            {
                if (nudCijenaOd.Value > nudCijenaDo.Value)
                {
                    nudCijenaOd.Value = nudCijenaDo.Value;
                }
            }
            else if (nud.Tag.ToString() == "CijenaDo")
            {
                if (nudCijenaDo.Value < nudCijenaOd.Value)
                {
                    nudCijenaDo.Value = nudCijenaOd.Value;
                }
            }
        }
        private void Nud_Enter(object sender, EventArgs e)
        {
            NumericUpDown nud = sender as NumericUpDown;

            nud.Select(0, nud.Text.Length);
        }
        private void Nud_TextChanged(object sender, EventArgs e)
        {
            NumericUpDown nud = sender as NumericUpDown;

            if (nud.Text == "")
            {
                Pretraga();
                return;
            }

            if (nudOcjenaOdPrvaIzmjena && !nudOcjenaOdClicked && nud.Tag.ToString() == "OcjenaOd")
            {
                nudOcjenaOdPrvaIzmjena = false;
                Pretraga();
            }
            else if (nudOcjenaDoPrvaIzmjena && !nudOcjenaDoClicked && nud.Tag.ToString() == "OcjenaDo")
            {
                nudOcjenaDoPrvaIzmjena = false;
                Pretraga();
            }
            else if (nudCijenaOdPrvaIzmjena && !nudCijenaOdClicked && nud.Tag.ToString() == "CijenaOd")
            {
                nudCijenaOdPrvaIzmjena = false;
                Pretraga();
            }
            else if (nudCijenaDoPrvaIzmjena && !nudCijenaDoClicked && nud.Tag.ToString() == "CijenaDo")
            {
                nudCijenaDoPrvaIzmjena = false;
                Pretraga();
            }
        }

        private void Nud_KeyDown(object sender, KeyEventArgs e)
        {
            NumericUpDown nud = sender as NumericUpDown;
            if (nud.Tag.ToString() == "OcjenaOd")
            {
                nudOcjenaOdClicked = true;
            }
            else if (nud.Tag.ToString() == "OcjenaDo")
            {
                nudOcjenaDoClicked = true;
            }
            else if (nud.Tag.ToString() == "CijenaOd")
            {
                nudCijenaOdClicked = true;
            }
            else if (nud.Tag.ToString() == "CijenaDo")
            {
                nudCijenaDoClicked = true;
            }
        }

        private void Nud_ValueChanged(object sender, EventArgs e)
        {
            KorekcijaUnosa(sender);

            Pretraga();

            NumericUpDown nud = sender as NumericUpDown;

            nud.Select(0, nud.Text.Length);
        }
        private void Nud_MouseWheel(object sender, EventArgs e)
        {
            HandledMouseEventArgs ee = (HandledMouseEventArgs)e;
            ee.Handled = true;
        }
        #endregion

        #region FlowLayoutPanel
        private void Flp_Scroll(object sender)
        {
            // dinamicko ucitavanje filmova prilikom scroll-ovanja
            if (((FlowLayoutPanel)sender).VerticalScroll.Value >= (int)(((FlowLayoutPanel)sender).VerticalScroll.Maximum - 500))
            {
                offset += 10;

                string naslov = tbFilmoviPretraga.Text.EndsWith("...") ? "" : tbFilmoviPretraga.Text;

                List<zanr> zanrovi = null;

                if (cekiraniZanrovi.Count > 0)
                {
                    zanrovi = new List<zanr>();
                    foreach (string zanr in cekiraniZanrovi)
                    {
                        int idZanra = int.Parse(Regex.Replace(zanr, "[^0-9]", ""));
                        zanrovi.Add(filmoviUtil.GetZanr(idZanra));
                    }
                }

                List<glumac> glumci = null;

                if (cekiraniGlumci.Count > 0)
                {
                    glumci = new List<glumac>();
                    foreach (string glumac in cekiraniGlumci)
                    {
                        int idGlumca = int.Parse(Regex.Replace(glumac, "[^0-9]", ""));
                        glumci.Add(filmoviUtil.GetGlumac(idGlumca));
                    }
                }

                List<film> filmovi = filmoviUtil.GetSljedecihUzPretraguZaKatalog(naslov, cmbPoredajPo.Text, (int)nudGodinaOd.Value, (int)nudGodinaDo.Value,
                    nudCijenaOd.Text == "" ? 0.0m : nudCijenaOd.Value, nudCijenaDo.Text == "" ? 999.0m : nudCijenaDo.Value,
                    nudOcjenaOd.Text == "" ? 0.0m : nudOcjenaOd.Value, nudOcjenaDo.Text == "" ? 5.0m : nudOcjenaDo.Value, 10, offset,
                    zanrovi, cmbNacinPretrageZanrovi.Text, glumci, cmbNacinPretrageGlumci.Text);

                FilmPanel filmPanel = null;

                foreach (film film in filmovi)
                {
                    filmPanel = new FilmPanel(film, filmoviUtil.GetZanrove(film.id), filmoviUtil.GetGlumci(film.id),
                        filmoviUtil.GetOcjena(film.id), filmoviUtil.GetBrojIzdavanja(film.id), filmoviUtil.GetCijena(film.naslov, film.godina));

                    if (flpFilmoviPanel.Controls.Count == 0)
                    {
                        filmPanel.Location = new Point(4, 4);
                    }
                    else
                    {
                        int y = flpFilmoviPanel.Controls[flpFilmoviPanel.Controls.Count - 1].Location.Y + filmPanel.Height;
                        filmPanel.Location = new Point(4, y);
                    }

                    flpFilmoviPanel.Controls.Add(filmPanel);
                    flpFilmoviPanel.SetFlowBreak(filmPanel, true);
                }
            }
        }
        private void FlpFilmoviPanel_Scroll(object sender, ScrollEventArgs e)
        {
            // dinamicko ucitavanje filmova prilikom scroll-ovanja
            Flp_Scroll(sender);
        }
        private void Flp_MouseWheel(object sender, MouseEventArgs e)
        {
            // dinamicko ucitavanje filmova prilikom scroll-ovanja
            Flp_Scroll(sender);
        }
        #endregion

        private void KatalogFilmova_Shown(object sender, EventArgs e)
        {
            UcitajZanrove();
            UcitajGlumce();

            Pretraga();
        }
        private void KatalogFilmova_FormClosing(object sender, FormClosingEventArgs e)
        {
            roditeljskaForma.Show();
        }

        private void CmbPoredajPo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pretraga();
        }
    }
}
    
